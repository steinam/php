.. include:: images.txt



Chapter 10. Behavioral Testing
==============================

In `*Chapter 7* <#Top_of_ch07_html>`__, *Testing Web Applications*, you
learned how to write unit tests in order to test small pieces of code in
an isolated way. Even though this is a must, it is not enough alone to
make sure your application works as it should. The scope of your test
could be so small that even though the algorithm that you test makes
sense, it would not be what the business asked you to create.

Acceptance tests were born in order to add this level of security to the
business side, complementing the already existing unit tests. In the
same way, BDD originated from TDD in order to write code based on these
acceptance tests in an attempt to involve business and managers in the
development process. As PHP is one of the favorite languages of web
developers, it is just natural to find powerful tools to implement BDD
in your projects. You will be positively surprised by what you can do
with Behat and Mink, the two most popular BDD frameworks at the moment.

In this chapter, you will learn about:

-  Acceptance tests and BDD

-  Writing features with Gherkin

-  Implementing and running tests with Behat

-  Writing tests against browsers with Mink

Behavior-driven development
---------------------------

We already exposed in `*Chapter 7* <#Top_of_ch07_html>`__, *Testing Web
Applications*, the different tools we can use in order to make our
applications bug-free, such as automated tests. We described what unit
tests are and how they can help us achieve our goals, but this is far
from enough. In this section, we will describe the process of creating a
real-world application, how unit tests are not enough, and what other
techniques we can include in this life cycle in order to succeed in our
task—in this case, behavioral tests.

Introducing continuous integration
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

There is a huge difference between developing a small web application by
yourself and being part of a big team of developers, managers, marketing
people, and so on, that works around the same big web application.
Working on an application used by thousands or millions of users has a
clear risk: if you mess it up, there will be a huge number of unhappy
affected users, which may translate into sales going down, partnerships
terminated, and so on.

From this scenario, you can imagine that people would be scared when
they have to change anything in production. Before doing so, they will
make sure that everything works perfectly fine. For this reason, there
is always a heavy process around all the changes affecting a web
application in production, including loads of tests of all kinds.

Some think that by reducing the number of times they deploy to
production, they can reduce the risk of failure, which ends up with them
having releases every several months with an uncountable number of
changes.

Now, imagine releasing the result of two or three months of code changes
at once and something mysteriously fails in production: do you know
where to even start looking for the cause of the problem? What if your
team is good enough to make perfect releases, but the end result is not
what the market needs? You might end up wasting months of work!

Even though there are different approaches and not all companies use
them, let's try to describe one of the most famous ones from the last
few years: continuous integration (CI). The idea is to integrate small
pieces of work often rather than big ones every once in a while. Of
course, releasing is still a constraint in your system, which means that
it takes a lot of time and resources. CI tries to automatize this
process as much as possible, reducing the amount of time and resources
that you need to invest. There are huge benefits with this approach,
which are as follows:

-  Releases do not take forever to be done, and there isn't an entire
   team focused on releasing as this is done automatically.

-  You can release changes one by one as they come. If something fails,
   you know exactly what the change was and where to start looking for
   the error. You can even revert the changes easily if you need to.

-  As you release so often, you can get quick feedback from everyone.
   You will be able to change your plans in time if you need to instead
   of waiting for months to get any feedback and wasting all the effort
   you put on this release.

The idea seems perfect, but how do we implement it? First, let's focus
on the manual part of the process: developing the features using a
version control system (VCS). The following diagram shows a very common
approach:

|image47|

As we already mentioned, a VCS allows developers to work on the same
codebase, tracking all the changes that everyone makes and helping on
the resolution of conflicts. A VCS usually allows you to have different
branches; that is, you can diverge from the main line of development and
continue to do work without messing with it. The previous graph shows
you how to use branches to write new features and can be explained as
follows:

-  A: A team needs to start working on feature A. They create a new
   branch from the master, in which they will add all the changes for
   this feature.

-  B: A different team also needs to start working on a feature. They
   create a new branch from master, same as before. At this point, they
   are not aware of what the first team is doing as they do it on their
   own branch.

-  C: The second team finishes their job. No one else changed master, so
   they can merge their changes straight away. At this point, the CI
   process will start the release process.

-  D: The first team finishes the feature. In order to merge it to
   master, they need to first rebase their branch with the new changes
   of master and solve any conflicts that might take place. The older
   the branch is the more chances of getting conflicts you will have, so
   you can imagine that smaller and faster features are preferred.

Now, let's take a look at how the automated side of the process looks.
The following graph shows you all the steps from the merging into master
to production deployment:

|image48|

Until you merge your code into master, you are in the development
environment. The CI tool will listen to all the changes on the master
branch of your project, and for each of them, it will trigger a job.
This job will take care of building the project if necessary and then
run all the tests. If there is any error or test failure, it will let
everyone now, and the team that triggered this job should take care of
fixing it. The master branch is considered unstable at this point.

If all tests pass, the CI tool will deploy your code into staging.
Staging is an environment that emulates production as much as possible;
that is, it has the same server configuration, database structure, and
so on. Once the application is here, you can run all the tests that you
need until you are confident to continue the deployment to production.
As you make small changes, you do not need to manually test absolutely
everything. Instead, you can test your changes and the main use cases of
your application.

Unit tests versus acceptance tests
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

We said that the goal of CI is to have a process as automatized as
possible. However, we still need to manually test the application in
staging, right? Acceptance tests to the rescue!

Writing unit tests is nice and a must, but they test only small pieces
of code in an isolated way. Even if your entire unit tests suite passes,
you cannot be sure that your application works at all as you might not
integrate all the parts properly because you are missing functionalities
or the functionalities that you built were not what the business needed.
Acceptance tests test the entire flow of a specific use case.

If your application is a website, acceptance tests will probably launch
a browser and emulate user actions, such as clicking and typing, in
order to assert that the page returns what is expected. Yes, from a few
lines of code, you can execute all the tests that were previously manual
in an automated way.

Now, imagine that you wrote acceptance tests for all the features of
your application. Once the code is in staging, the CI tool can
automatically run all of these tests and make sure that the new code
does not break any existing functionality. You can even run them using
as many different browsers as you need to make sure that your
application works fine in all of them. If a test fails, the CI tool will
notify the team responsible, and they will have to fix it. If all the
tests pass, the CI tool can automatically deploy your code into
production.

Why do we need to write unit tests then, if acceptance tests test what
the business really cares about? There are several reasons to keep both
acceptance and unit tests; in fact, you should have way more unit tests
than acceptance tests.

-  Unit tests check small pieces of code, which make them
   orders-of-magnitude faster than acceptance tests, which test the
   whole flow against a browser. That means that you can run all your
   unit tests in a few seconds or minutes, but it will take much longer
   to run all your acceptance tests.

-  Writing acceptance tests that cover absolutely all the possible
   combinations of use cases is virtually impossible. Writing unit tests
   that cover a high percentage of use cases for a given method or piece
   of code is rather easy. You should have loads of unit tests testing
   as many edge cases as possible but only some acceptance tests testing
   the main use cases.

When should you run each type of test then? As unit tests are faster,
they should be executed during the first stages of deployment. Only once
we know that they all have passed do we want to spend time deploying to
staging and running acceptance tests.

TDD versus BDD
~~~~~~~~~~~~~~

In `*Chapter 7* <#Top_of_ch07_html>`__, *Testing Web Applications*, you
learned that TDD or test-driven development is the practice of writing
first the unit tests and then the code in an attempt to write testable
and cleaner code and to make sure that your test suite is always up to
date. With the appearance of acceptance tests, TDD evolved to BDD or
behavior-driven development.

BDD is quite similar to TDD, in that you should write the tests first
and then the code that makes these tests pass. The only difference is
that with BDD, we write tests that specify the desired behavior of the
code, which can be translated to acceptance tests. Even though it will
always depend on the situation, you should write acceptance tests that
test a very specific part of the application rather than long use cases
that contain several steps. With BDD, as with TDD, you want to get quick
feedback, and if you write a broad test, you will have to write a lot of
code in order to make it pass, which is not the goal that BDD wants to
achieve.

Business writing tests
~~~~~~~~~~~~~~~~~~~~~~

The whole point of acceptance tests and BDD is to make sure that your
application works as expected, not only your code. Acceptance tests,
then, should not be written by developers but by the business itself. Of
course, you cannot expect that managers and executives will learn how to
code in order to create acceptance tests, but there is a bunch of tools
that allow you to translate plain English instructions or behavioral
specifications into acceptance tests' code. Of course, these
instructions have to follow some patterns. Behavioral specifications
have the following parts:

-  A title, which describes briefly, but in a very clear way, what use
   case the behavioral specification covers.

-  A narrative, which specifies who performs the test, what the business
   value is, and what the expected outcome is. Usually the format of the
   narrative is the following:

    In order to <business value> As a <stakeholder> I want to <expected
    outcome>

-  A set of scenarios, which is a description and a set of steps of each
   specific use case that we want to cover. Each scenario has a
   description and a list of instructions in the Given-When-Then format;
   we will discuss more on this in the next section. A common patterns
   is:

    Scenario: <short description> Given <set up scenario> When <steps to
    take> Then <expected outcome>

In the next two sections, we will discover two tools in PHP that you can
use in order to understand behavioral scenarios and run them as
acceptance tests.

BDD with Behat
--------------

The first of the tools we will introduce is Behat. Behat is a PHP
framework that can transform behavioral scenarios into acceptance tests
and then run them, providing feedback similar to PHPUnit. The idea is to
match each of the steps in English with the scenarios in a PHP function
that performs some action or asserts some results.

In this section, we will try to add some acceptance tests to our
application. The application will be a simple database migration script
that will allow us to keep track of the changes that we will add to our
schema. The idea is that each time that you want to change your
database, you will write the changes on a migration file and then
execute the script. The application will check what was the last
migration executed and will perform new ones. We will first write the
acceptance tests and then introduce the code progressively as BDD
suggests.

In order to install Behat on your development environment, you can use
Composer. The command is as follows:

    $ composer require behat/behat

Behat actually does not come with any set of assertion functions, so you
will have to either implement your own by writing conditionals and
throwing exceptions or you could integrate any library that provides
them. Developers usually choose PHPUnit for this as they are already
used to its assertions. Add it, then, to your project via the following:

    $ composer require phpunit/phpunit

As with PHPUnit, Behat needs to know where your test suite is located.
You can either have a configuration file stating this and other
configuration options, which is similar to the phpunit.xml configuration
file for PHPUnit, or you could follow the conventions that Behat sets
and skip the configuration step. If you choose the second option, you
can let Behat create the folder structure and PHP test class for you
with the following command:

    $ ./vendor/bin/behat --init

After running this command, you should have a
features/bootstrap/FeatureContext.php file, which is where you need to
add the steps of the PHP functions' matching scenarios. More on this
shortly, but first, let's find out how to write behavioral
specifications so that Behat can understand them.

Introducing the Gherkin language
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Gherkin is the language, or rather the format, that behavioral
specifications have to follow. Using Gherkin naming, each behavioral
specification is a feature. Each feature is added to the features
directory and should have the .feature extension. Feature files should
start with the Feature keyword followed by the title and the narrative
in the same format that we already mentioned before—that is, the *In
order to–As a–I need to* structure. In fact, Gherkin will only print
these lines, but keeping it consistent will help your developers and
business know what they are trying to achieve.

Our application will have two features: one for the setup of our
database to allow the migrations tool to work, and the other one for the
correct behavior when adding migrations to the database. Add the
following content to the features/setup.feature file:

    Feature: Setup In order to run database migrations As a developer I
    need to be able to create the empty schema and migrations table.

Then, add the following feature definition to the
features/migrations.feature file:

    Feature: Migrations In order to add changes to my database schema As
    a developer I need to be able to run the migrations script

Defining scenarios
~~~~~~~~~~~~~~~~~~

The title and narrative of features does not really do anything more
than give information to the person who runs the tests. The real work is
done in scenarios, which are specific use cases with a set of steps to
take and some assertions. You can add as many scenarios as you need to
each feature file as long as they represent different use cases of the
same feature. For example, for setup.feature, we can add a couple of
scenarios: one where it is the first time that the user runs the script,
so the application will have to set up the database, and one where the
user already executed the script previously, so the application does not
need to go through the setup process.

As Behat needs to be able to translate the scenarios written in plain
English to PHP functions, you will have to follow some conventions. In
fact, you will see that they are very similar to the ones that we
already mentioned in the behavioral specifications section.

Writing Given-When-Then test cases
++++++++++++++++++++++++++++++++++

A scenario must start with the Scenario keyword followed by a short
description of what use case the scenario covers. Then, you need to add
the list of steps and assertions. Gherkin allows you to use four
keywords for this: Given, When, Then, and And. In fact, they all have
the same meaning when it comes to code, but they add a lot of semantic
value to your scenarios. Let's consider an example; add the following
scenario at the end of your setup.feature file:

    Scenario: Schema does not exist and I do not have migrations Given I
    do not have the "bdd\_db\_test" schema And I do not have migration
    files When I run the migrations script Then I should have an empty
    migrations table And I should get: """ Latest version applied is 0.
    """

This scenario tests what happens when we do not have any schema
information and run the migrations script. First, it describes the state
of the scenario: *Given I do not have the bdd\_db\_test schema And I do
not have migration files*. These two lines will be translated to one
method each, which will remove the schema and all migration files. Then,
the scenario describes what the user will do: *When I run the migrations
script*. Finally, we set the expectations for this scenario: *Then I
should have an empty migrations table And I should get Latest version
applied is 0.*.

In general, the same step will always start by the same keyword—that is,
*I run the migrations script* will always be preceded by When. The And
keyword is a special one as it matches all the three keywords; its only
purpose is to have steps as English-friendly as possible; although if
you prefer, you could write *Given I do not have migration files*.

Another thing to note in this example is the use of arguments as part of
the step. The line *And I should get* is followed by a string enclosed
by """. The PHP function will get this string as an argument, so you can
have one unique step definition—that is, the function—for a wide variety
of situations just using different strings.

Reusing parts of scenarios
++++++++++++++++++++++++++

It is quite common that for a given feature, you always start from the
same scenario. For example, setup.feature has a scenario in which we can
run the migrations for the first time without any migration file, but we
will also add another scenario in which we want to run the migrations
script for the first time with some migration files to make sure that it
will apply all of them. Both scenarios have in common one thing: they do
not have the database set up.

Gherkin allows you to define some steps that will be applied to all the
scenarios of the feature. You can use the Background keyword and a list
of steps, usually Given. Add these two lines between the feature
narrative and scenario definition:

    Background: Given I do not have the "bdd\_db\_test" schema

Now, you can remove the first step from the existing scenario as
Background will take care of it.

Writing step definitions
~~~~~~~~~~~~~~~~~~~~~~~~

So far, we have written features using the Gherkin language, but we
still have not considered how any of the steps in each scenario is
translated to actual code. The easiest way to note this is by asking
Behat to run the acceptance tests; as the steps are not defined
anywhere, Behat will print out all the functions that you need to add to
your FeatureContext class. To run the tests, just execute the following
command:

    $ ./vendor/bin/behat

The following screenshot shows the output that you should get if you
have no step definitions:

|image49|

As you can note, Behat complained about some missing steps and then
printed in yellow the methods that you could use in order to implement
them. Copy and paste them into your autogenerated
features/bootstrap/FeatureContext.php file. The following FeatureContext
class has already implemented all of them:

    <?php use Behat\\Behat\\Context\\Context; use
    Behat\\Behat\\Context\\SnippetAcceptingContext; use
    Behat\\Gherkin\\Node\\PyStringNode; require\_once \_\_DIR\_\_ .
    '/../../vendor/phpunit/phpunit/src/Framework/Assert/Functions.php';
    class FeatureContext implements Context, SnippetAcceptingContext {
    private $db; private $config; private $output; public function
    \_\_construct() { $configFileContent = file\_get\_contents(
    \_\_DIR\_\_ . '/../../config/app.json' ); $this->config =
    json\_decode($configFileContent, true); } private function getDb():
    PDO { if ($this->db === null) { $this->db = new PDO(
    "mysql:host={$this->config['host']}; " . "dbname=bdd\_db\_test",
    $this->config['user'], $this->config['password'] ); } return
    $this->db; } /\*\* \* @Given I do not have the "bdd\_db\_test"
    schema \*/ public function iDoNotHaveTheSchema() {
    $this->executeQuery('DROP SCHEMA IF EXISTS bdd\_db\_test'); } /\*\*
    \* @Given I do not have migration files \*/ public function
    iDoNotHaveMigrationFiles() { exec('rm db/migrations/\*.sql >
    /dev/null 2>&1'); } /\*\* \* @When I run the migrations script \*/
    public function iRunTheMigrationsScript() { exec('php migrate.php',
    $this->output); } /\*\* \* @Then I should have an empty migrations
    table \*/ public function iShouldHaveAnEmptyMigrationsTable() {
    $migrations = $this->getDb() ->query('SELECT \* FROM migrations')
    ->fetch(); assertEmpty($migrations); } private function
    executeQuery(string $query) { $removeSchemaCommand = sprintf( 'mysql
    -u %s %s -h %s -e "%s"', $this->config['user'],
    empty($this->config['password']) ? '' :
    "-p{$this->config['password']}", $this->config['host'], $query );
    exec($removeSchemaCommand); } }

As you can note, we read the configuration from the config/app.json
file. This is the same configuration file that the application will use,
and it contains the database's credentials. We also instantiated a PDO
object to access the database so that we could add or remove tables or
take a look at what the script did.

Step definitions are a set of methods with a comment on each of them.
This comment is an annotation as it starts with @ and is basically a
regular expression matching the plain English step defined in the
feature. Each of them has its implementation: either removing a database
or migration files, executing the migrations script, or checking what
the migrations table contains.

The parameterization of steps
+++++++++++++++++++++++++++++

In the previous FeatureContext class, we intentionally missed the
iShouldGet method. As you might recall, this step has a string argument
identified by a string enclosed between """. The implementation for this
method looks as follows:

    /\*\* \* @Then I should get: \*/ public function
    iShouldGet(PyStringNode $string) { assertEquals(implode("\\n",
    $this->output), $string); }

Note how the regular expression does not contain the string. This
happens when using long strings with """. Also, the argument is an
instance of PyStringNode, which is a bit more complex than a normal
string. However, fear not; when you compare it with a string, PHP will
look for the \_\_toString method, which just prints the content of the
string.

Running feature tests
~~~~~~~~~~~~~~~~~~~~~

In the previous sections, we wrote acceptance tests using Behat, but we
have not written a single line of code yet. Before running them, though,
add the config/app.json configuration file with the credentials of your
database user so that the FeatureContext constructor can find it, as
follows:

    { "host": "127.0.0.1", "schema": "bdd\_db\_test", "user": "root",
    "password": "" }

Now, let's run the acceptance tests, expecting them to fail; otherwise,
our tests will not be valid at all. The output should be something
similar to this:

|image50|

As expected, the Then steps failed. Let's implement the minimum code
necessary in order to make the tests pass. For starters, add the
autoloader into your composer.json file and run composer update:

    "autoload": { "psr-4": { "Migrations\\\\": "src/" } }

We would like to implement a Schema class that contains the helpers
necessary to set up a database, run migrations, and so on. Right now,
the feature is only concerned about the setup of the database—that is,
creating the database, adding the empty migrations table to keep track
of all the migrations added, and the ability to get the latest migration
registered as successful. Add the following code as src/Schema.php:

    <?php namespace Migrations; use Exception; use PDO; class Schema {
    const SETUP\_FILE = \_\_DIR\_\_ . '/../db/setup.sql'; const
    MIGRATIONS\_DIR = \_\_DIR\_\_ . '/../db/migrations/'; private
    $config; private $connection; public function \_\_construct(array
    $config) { $this->config = $config; } private function
    getConnection(): PDO { if ($this->connection === null) {
    $this->connection = new PDO( "mysql:host={$this->config['host']};" .
    "dbname={$this->config['schema']}", $this->config['user'],
    $this->config['password'] ); } return $this->connection; } }

Even though the focus of this chapter is to write acceptance tests,
let's go through the different implemented methods:

-  The constructor and getConnection just read the configuration file in
   config/app.json and instantiated the PDO object.

-  The createSchema executed CREATE SCHEMA IF NOT EXISTS, so if the
   schema already exists, it will do nothing. We executed the command
   with exec instead of PDO as PDO always needs to use an existing
   database.

-  The getLatestMigration will first check whether the migrations table
   exists; if not, we will create it using setup.sql and then fetch the
   last successful migration.

We also need to add the migrations/setup.sql file with the query to
create the migrations table, as follows:

    CREATE TABLE IF NOT EXISTS migrations( version INT UNSIGNED NOT
    NULL, \`time\` TIMESTAMP NOT NULL DEFAULT CURRENT\_TIMESTAMP, status
    ENUM('success', 'error'), PRIMARY KEY (version, status) );

Finally, we need to add the migrate.php file, which is the one that the
user will execute. This file will get the configuration, instantiate the
Schema class, set up the database, and retrieve the last migration
applied. Run the following code:

    <?php require\_once \_\_DIR\_\_ . '/vendor/autoload.php';
    $configFileContent = file\_get\_contents(\_\_DIR\_\_ .
    '/config/app.json'); $config = json\_decode($configFileContent,
    true); $schema = new Migrations\\Schema($config);
    $schema->createSchema(); $version = $schema->getLatestMigration();
    echo "Latest version applied is $version.\\n";

You are now good to run the tests again. This time, the output should be
similar to this screenshot, where all the steps are in green:

|image51|

Now that our acceptance test is passing, we need to add the rest of the
tests. To make things quicker, we will add all the scenarios, and then
we will implement the necessary code to make them pass, but it would be
better if you add one scenario at a time. The second scenario of
setup.feature could look as follows (remember that the feature contains
a Background section, in which we clean the database):

    Scenario: Schema does not exists and I have migrations Given I have
    migration file 1: """ CREATE TABLE test1(id INT); """ And I have
    migration file 2: """ CREATE TABLE test2(id INT); """ When I run the
    migrations script Then I should only have the following tables: \|
    migrations \| \| test1 \| \| test2 \| And I should have the
    following migrations: \| 1 \| success \| \| 2 \| success \| And I
    should get: """ Latest version applied is 0. Applied migration 1
    successfully. Applied migration 2 successfully. """

This scenario is important as it used parameters inside the step
definitions. For example, the *I have migration file* step is presented
twice, each time with a different migration file number. The
implementation of this step is as follows:

    /\*\* \* @Given I have migration file :version: \*/ public function
    iHaveMigrationFile( string $version, PyStringNode $file ) {
    $filePath = \_\_DIR\_\_ . "/../../db/migrations/$version.sql";
    file\_put\_contents($filePath, $file->getRaw()); }

The annotation of this method, which is a regular expression, used
:version as a wildcard. Any step that starts with *Given I have
migration file* followed by something else will match this step
definition, and the "something else" bit will be received as the
$version argument as a string.

Here, we introduced yet another type of argument: tables. The *Then I
should only have the following tables* step defined a table of two rows
of one column each, and the *Then I should have the following
migrations* bit sent a table of two rows of two columns each. The
implementation for the new steps is as follows:

    /\*\* \* @Then I should only have the following tables: \*/ public
    function iShouldOnlyHaveTheFollowingTables(TableNode $tables) {
    $tablesInDb = $this->getDb() ->query('SHOW TABLES')
    ->fetchAll(PDO::FETCH\_NUM); assertEquals($tablesInDb,
    array\_values($tables->getRows())); } /\*\* \* @Then I should have
    the following migrations: \*/ public function
    iShouldHaveTheFollowingMigrations( TableNode $migrations ) { $query
    = 'SELECT version, status FROM migrations'; $migrationsInDb =
    $this->getDb() ->query($query) ->fetchAll(PDO::FETCH\_NUM);
    assertEquals($migrations->getRows(), $migrationsInDb); }

The tables are received as TableNode arguments. This class contains a
getRows method that returns an array with the rows defined in the
feature file.

The other feature that we would like to add is
features/migrations.feature. This feature will assume that the user
already has the database set up, so we will add a Background section
with this step. We will add one scenario in which the migration file
numbers are not consecutive, in which case the application should stop
at the last consecutive migration file. The other scenario will make
sure that when there is an error, the application does not continue the
migration process. The feature should look similar to the following:

    Feature: Migrations In order to add changes to my database schema As
    a developer I need to be able to run the migrations script
    Background: Given I have the bdd\_db\_test Scenario: Migrations are
    not consecutive Given I have migration 3 And I have migration file
    4: """ CREATE TABLE test4(id INT); """ And I have migration file 6:
    """ CREATE TABLE test6(id INT); """ When I run the migrations script
    Then I should only have the following tables: \| migrations \| \|
    test4 \| And I should have the following migrations: \| 3 \| success
    \| \| 4 \| success \| And I should get: """ Latest version applied
    is 3. Applied migration 4 successfully. """ Scenario: A migration
    throws an error Given I have migration file 1: """ CREATE TABLE
    test1(id INT); """ And I have migration file 2: """ CREATE TABLE
    test1(id INT); """ And I have migration file 3: """ CREATE TABLE
    test3(id INT); """ When I run the migrations script Then I should
    only have the following tables: \| migrations \| \| test1 \| And I
    should have the following migrations: \| 1 \| success \| \| 2 \|
    error \| And I should get: """ Latest version applied is 0. Applied
    migration 1 successfully. Error applying migration 2: Table 'test1'
    already exists. """

There aren't any new Gherkin features. The two new step implementations
look as follows:

    /\*\* \* @Given I have the bdd\_db\_test \*/ public function
    iHaveTheBddDbTest() { $this->executeQuery('CREATE SCHEMA
    bdd\_db\_test'); } /\*\* \* @Given I have migration :version \*/
    public function iHaveMigration(string $version) {
    $this->getDb()->exec( file\_get\_contents(\_\_DIR\_\_ .
    '/../../db/setup.sql') ); $query = <<<SQL INSERT INTO migrations
    (version, status) VALUES(:version, 'success') SQL; $this->getDb()
    ->prepare($query) ->execute(['version' => $version]); }

Now, it is time to add the needed implementation to make the tests pass.
There are only two changes needed. The first one is an
applyMigrationsFrom method in the Schema class that, given a version
number, will try to apply the migration file for this number. If the
migration is successful, it will add a row in the migrations table, with
the new version added successfully. If the migration failed, we would
add the record in the migrations table as a failure and then throw an
exception so that the script is aware of it. Finally, if the migration
file does not exist, the returning value will be false. Add this code to
the Schema class:

    public function applyMigrationsFrom(int $version): bool { $filePath
    = self::MIGRATIONS\_DIR . "$version.sql"; if
    (!file\_exists($filePath)) { return false; } $connection =
    $this->getConnection(); if
    ($connection->exec(file\_get\_contents($filePath)) === false) {
    $error = $connection->errorInfo()[2];
    $this->registerMigration($version, 'error'); throw new
    Exception($error); } $this->registerMigration($version, 'success');
    return true; } private function registerMigration(int $version,
    string $status) { $query = <<<SQL INSERT INTO migrations (version,
    status) VALUES(:version, :status) SQL; $params = ['version' =>
    $version, 'status' => $status];
    $this->getConnection()->prepare($query)->execute($params); }

The other bit missing is in the migrate.php script. We need to call the
newly created applyMigrationsFrom method with consecutive versions
starting from the latest one, until we get either a false value or an
exception. We also want to print out information about what is going on
so that the user is aware of what migrations were added. Add the
following code at the end of the migrate.php script:

    do { $version++; try { $result =
    $schema->applyMigrationsFrom($version); if ($result) { echo "Applied
    migration $version successfully.\\n"; } } catch (Exception $e) {
    $error = $e->getMessage(); echo "Error applying migration $version:
    $error.\\n"; exit(1); } } while ($result);

Now, run the tests and voilà! They all pass. You now have a library that
manages database migrations, and you are 100% sure that it works thanks
to your acceptance tests.

Testing with a browser using Mink
---------------------------------

So far, we have been able to write acceptance tests for a script, but
most of you are reading this book in order to write nice and shiny web
applications. How can you take advantage of acceptance tests then? It is
time to introduce the second PHP tool of this chapter: Mink.

Mink is actually an extension of Behat, which adds implementations of
several steps related to web browser testing. For example, if you add
Mink to your application, you will be able to add scenarios where Mink
will launch a browser and click or type as requested, saving you a lot
of time and effort in manual testing. However, first, let's take a look
at how Mink can achieve this.

Types of web drivers
~~~~~~~~~~~~~~~~~~~~

Mink makes use of web drivers—that is, libraries that have an API that
allows you to interact with a browser. You can send commands, such as
*go to this page*, *click on this link*, *fill this input field with
this text*, and so on, and the web driver will translate this into the
correct instruction for your browser. There are several web drivers,
each of them implemented following a different approach. It is for this
reason that depending on the web driver, you will have some features or
others.

Web drivers can be divided into two groups depending on how they work:

-  Headless browsers: These drivers do not really launch a browser; they
   only try to emulate one. They actually request for the web page and
   render the HTML and JavaScript code, so they are aware of how the
   page looks, but they do not display it. They have a huge benefit:
   they are easy to install and manage, and as they do not have to build
   the graphical representation, they are extremely fast. The
   disadvantage is that they have severe restrictions in terms of CSS
   and some JavaScript functionalities, especially AJAX.

-  Web drivers that launch real browsers like a user would do: These web
   drivers can do almost anything and are way more powerful than
   headless browsers. The problem is that they can be a bit tricky to
   install and are very, very slow—as slow as a real user trying to go
   through the scenarios.

So, which one should you choose? As always, it will depend on what your
application is. If you have an application that does not make heavy use
of CSS and JavaScript and it is not critical for your business, you
could use headless browsers. Instead, if the application is the
cornerstone of your business and you need to be absolutely certain that
all the UI features work as expected, you might want to go for web
drivers that launch browsers.

Installing Mink with Goutte
~~~~~~~~~~~~~~~~~~~~~~~~~~~

In this chapter, we will use Goutte, a headless web driver written by
the same guys that worked on Symfony, to add some acceptance tests to
the repositories page of GitHub. The required components of your project
will be Behat, Mink, and the Goutte driver. Add them with Composer via
the following commands:

    $ composer require behat/behat $ composer require
    behat/mink-extension $ composer require behat/mink-goutte-driver

Now, execute the following line to ask Behat to create the basic
directory structure:

    $ ./vendor/bin/behat –init

The only change we will add to the FeatureContext class is where it
extends from. This time, we will use MinkContext in order to get all the
step definitions related to web testing. The FeatureContext class should
look similar to this:

    <?php use Behat\\MinkExtension\\Context\\MinkContext; require
    \_\_DIR\_\_ . '/../../vendor/autoload.php'; class FeatureContext
    extends MinkContext { }

Mink also needs some configuration in order to let Behat know which web
driver we want to use or what the base URL for our tests is. Add the
following information to behat.yml:

    default: extensions: Behat\\MinkExtension: base\_url:
    "https://github.com" sessions: default\_session: goutte: ~

With this configuration, we let Behat know that we are using the Mink
extension, that Mink will use Goutte in all the sessions (you could
actually define different sessions with different web drivers if
necessary), and that the base URL for these tests is the GitHub one.
Behat is already instructed to look for the behat.yml file in the same
directory that we executed it in, so there is nothing else that we need
to do.

Interaction with the browser
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Now, let's look at the magic. If you know the steps to use, writing
acceptance tests with Mink will be like a game. First, add the following
feature in feature/search.feature:

    Feature: Search In order to find repositories As a website user I
    need to be able to search repositories by name Background: Given I
    am on "/picahielos" And I follow "Repositories" Scenario: Searching
    existing repository When I fill in "zap" for "q" And I press
    "Search" Then I should see "picahielos/zap" Scenario: Searching
    non-existing repository When I fill in "yolo" for "q" And I press
    "Search" Then I should not see "picahielos/yolo"

The first thing to note is that we have a Background section. This
section assumes that the user visited the
`*https://github.com/picahielos* <https://github.com/picahielos>`__ page
and clicked on the Repositories link. Using *I follow* with some string
is the equivalent of trying to find a link with this string and clicking
on it.

The first scenario used the *When I fill <field> with <value>* step,
which basically tries to find the input field on the page (you can
either specify the ID or name), and types the value for you. In this
case, the q field was the search bar, and we typed zap. Then, similar to
when clicking on the links, the *I press <button>* line will try to find
the button by name, ID, or value, and will click on it. Finally, *Then I
should see* followed by a string will assert that the given string could
be found on the page. In short, the test launched a browser, going to
the specified URL, clicking on the Repositories link, searching for the
zap repository, and asserting that it could find it. In a similar way,
the second scenario tried to find a repository that does not exist.

If you run the tests, they should pass, but you will not see any
browser. Remember that Goutte is a headless browser web driver. However,
check how fast these tests are executed; in my laptop, it took less than
3 seconds! Can you imagine anyone performing these two tests manually in
less than this time?

One last thing: having a cheat sheet of predefined Mink steps is one of
the handiest things to have near your desk; you can find one at
`*http://blog.lepine.pro/images/2012-03-behat-cheat-sheet-en.pdf* <http://blog.lepine.pro/images/2012-03-behat-cheat-sheet-en.pdf>`__.
As you can see, we did not write a single line of code, and we still
have two tests making sure that the website works as expected. Also, if
you need to add a fancier step, do not worry; you can still implement
your step definitions as we did in Behat previously while taking
advantage of the web driver's interface that Mink provides. We recommend
you to go through the official documentation in order to take a look at
the complete list of things that you can do with Mink.

Summary
-------

In this concluding chapter, you learned how important it is to
coordinate the business with the application. For this, you saw what BDD
is and how to implement it with your PHP web applications using Behat
and Mink. This also gives you the ability to test the UI with web
drivers, which you could not do it with unit tests and PHPUnit. Now, you
can make sure that not only is your application bug-free and secure, but
also that it does what the business needs it to do.

Congratulations on reaching the end of the book! You started as an
inexperienced developer, but now you are able to write simple and
complex websites and REST APIs with PHP and have an extensive knowledge
of good test practices. You have even worked with a couple of famous PHP
frameworks, so you are ready to either start a new project with them or
join a team that uses one of them.

Now, you might be wondering: what do I do next? You already know the
theory—well, some of it—so we would recommend that you practice a lot.
There are several ways you can do this: by creating your own
application, joining a team working on open source projects, or working
for a company. Try to keep up to date with new releases of the language
or the tools and frameworks, discover a new framework from time to time,
and never stop reading. Expanding your set of skills is always a great
idea!

If you run out of ideas on what to read next, here are some hints. We
did not go through the frontend part too much, so you might be
interested in reading about CSS and specially JavaScript. JavaScript has
become the main character in these last few years, so do not miss it
out. If you are rather interested in the backend side and how to manage
applications properly, try discovering new technologies, such as
continuous integration tools similar to Jenkins. Finally, if you prefer
to focus on the theory and "science" side, you can read about how to
write quality code with *Code Complete*, *Steve McConnell*, or how to
make good use of design patterns with *Design Patterns: Elements of
Reusable Object-Oriented Software*, *Erich Gamma, John Vlissides, Ralph
Johnson, and Richard Helm*, a gang of four.

Always enjoy and have fun when developing. Always!

