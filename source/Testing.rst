Chapter 7. Testing Web Applications
===================================


We are pretty sure you have heard the term "bug" when speaking about
applications. Sentences such as "We found a bug in the application
that…" followed by some very undesirable behavior are more common than
you think. Writing code is not the only task of a developer; testing it
is crucial too. You should not release a version of your application
that has not been tested. However, could you imagine having to test your
entire application every time you change a line? It would be a
nightmare!

Well, we are not the first ones to have this issue, so, luckily enough,
developers have already found a pretty good solution to this problem. In
fact, they found more than one solution, turning testing into a very hot
topic of discussion. Even being a test developer has become quite a
common role. In this chapter, we will introduce you to one of the
approaches of testing your code: unit tests.

In this chapter, you will learn about:

-  How unit tests work

-  Configuring PHPUnit to test your code

-  Writing tests with assertions, data providers, and mocks

-  Good and bad practices when writing unit tests

The necessity for tests
-----------------------

When you work on a project, chances are that you are not the only
developer who will work with this code. Even in the case where you are
the only one who will ever change it, if you do this a few weeks after
creating it, you will probably not remember all the places that this
piece of code is affected. Okay, let's assume that you are the only
developer and your memory is beyond limits; would you be able to verify
that a change on a frequently used object, such as a request, will
always work as expected? More importantly, would you like to do it every
single time you make a tiny change?

Types of tests
~~~~~~~~~~~~~~

While writing your application, making changes to the existing code, or
adding new features, it is very important to get good *feedback*. How do
you know that the feedback you get is good enough? It should accomplish
the AEIOU principles:

-  Automatic: Getting the feedback should be as painless as possible.
   Getting it by running just one command is always preferable to having
   to test your application manually.

-  Extensive: We should be able to cover as many use cases as possible,
   including edge cases that are difficult to foresee when writing code.

-  Immediate: You should get it as soon as possible. This means that the
   feedback that you get just after introducing a change is way better
   than the feedback that you get after your code is in production.

-  Open: The results should be transparent, and also, the tests should
   give us insight to other developers as to how to integrate or operate
   with the code.

-  Useful: It should answer questions such as "Will this change work?",
   "Will it break the application unexpectedly?", or "Is there any edge
   case that does not work properly?".

So, even though the concept is quite weird at the beginning, the best
way to test your code is… with more code. Exactly! We will write code
with the goal of testing the code of our application. Why? Well, it is
the best way we know to satisfy all the AEIU principles, and it has the
following advantages:

-  We can execute the tests by just running one command from our command
   line or even from our favorite IDE. There is no need to manually test
   your application via a browser continually.

-  We need to write the test just once. At the beginning, it may be a
   bit painful, but once the code is written, you will not need to
   repeat it again and again. This means that after some work, we will
   be able to test every single case effortlessly. If we had to test it
   manually, along with all the use cases and edge cases, it would be a
   nightmare.

-  You do not need to have the whole application working in order to
   know whether your code works. Imagine that you are writing your
   router: in order to know whether it works, you will have to wait
   until your application works in a browser. Instead, you can write
   your tests and run them as soon as you finish your class.

-  When writing your tests, you will be provided with feedback on what
   is failing. This is very useful to know when a specific function of
   the router does not work and the reason for the failure, which is
   better than getting a 500 error on our browser.

We hope that by now we have sold you on the idea that writing tests is
indispensable. This was the easy part, though. The problem is that we
know several different approaches. Do we write tests that test the
entire application or tests that test specific parts? Do we isolate the
tested area from the rest? Do we want to interact with the database or
with other external resources while testing? Depending on your answers,
you will decide on which type of tests you want to write. Let's discuss
the three main approaches that developers agree with:

-  Unit tests: These are tests that have a very focused scope. Their aim
   is to test a single class or method, isolating them from the rest of
   code. Take your Sale domain class as an example: it has some logic
   regarding the addition of books, right? A unit test might just
   instantiate a new sale, add books to the object, and verify that the
   array of books is valid. Unit tests are super fast due to their
   reduced scope, so you can have several different scenarios of the
   same functionality easily, covering all the edge cases you can
   imagine. They are also isolated, which means that we will not care
   too much about how all the pieces of our application are integrated.
   Instead, we will make sure that each piece works perfectly fine.

-  Integration tests: These are tests with a wider scope. Their aim is
   to verify that all the pieces of your application work together, so
   their scope is not limited to a class or function but rather includes
   a set of classes or the whole application. There is still some
   isolation in case we do not want to use a real database or depend on
   some other external web service. An example in our application would
   be to simulate a Request object, send it to the router, and verify
   that the response is as expected.

-  Acceptance tests: These are tests with an even wider scope. They try
   to test a whole functionality from the user's point of view. In web
   applications, this means that we can launch a browser and simulate
   the clicks that the user would make, asserting the response in the
   browser each time. And yes, all of this through code! These tests are
   slower to run, as you can imagine, because their scope is larger and
   working with a browser slows them down quite a lot too.

So, with all these types of tests, which one should you write? The
answer is all of them. The trick is to know when and how many of each
type you should write. One good approach is to write a lot of unit
tests, covering absolutely everything in your code, then writing fewer
integration tests to make sure that all the components of your
application work together, and finally writing acceptance tests but
testing only the main flows of your application. The following test
pyramid represents this idea:

|image29|

The reason is simple: your real feedback will come from your unit tests.
They will tell you if you messed up something with your changes as soon
as you finish writing them because executing unit tests is easy and
fast. Once you know that all your classes and functions behave as
expected, you need to verify that they can work together. However, for
this, you do not need to test all the edge cases again; you already did
this when writing unit tests. Here, you need to write just a few
integration tests that confirm that all the pieces communicate properly.
Finally, to make sure that not only that the code works but also the
user experience is the desired one, we will write acceptance tests that
emulate a user going through the different views. Here, tests are very
slow and only possible once the flow is complete, so the feedback comes
later. We will add acceptance tests to make sure that the main flows
work, but we do not need to test every single scenario as we already did
this with integration and unit tests.

Unit tests and code coverage
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Now that you know what tests are, why we need them, and which types of
tests we have, we will focus the rest of the chapter on writing good
unit tests as they will be the ones that will occupy most of your time.

As we explained before, the idea of a unit test is to make sure that a
piece of code, usually a class or method, works as expected. As the
amount of code that a method contains should be small, running the test
should take almost no time. Taking advantage of this, we will run
several tests, trying to cover as many use cases as possible.

If this is not the first time you've heard about unit tests, you might
know the concept of code coverage. This concept refers to the amount of
code that our tests execute, that is, the percentage of tested code. For
example, if your application has 10,000 lines and your tests test a
total of 7,500 lines, your code coverage is 75%. There are tools that
show marks on your code to indicate whether a certain line is tested or
not, which is very useful in order to identify which parts of your
application are not tested and thus warn you that it is more dangerous
to change them.

However, code coverage is a double-edge sword. Why is this so? This is
because developers tend to get obsessed with code coverage, aiming for a
100% coverage. However, you should be aware that code coverage is just a
consequence, not your goal. Your goal is to write unit tests that verify
all the use cases of certain pieces of code in order to make you feel
safer each time that you have to change this code. This means that for a
given method, it might not be enough to write one test because the same
line with different input values may behave differently. However, if
your focus was on code coverage, writing one test would satisfy it, and
you might not need to write any more tests.

Integrating PHPUnit
-------------------

Writing tests is a task that you could do by yourself; you just need to
write code that throws exceptions when conditions are not met and then
run the script any time you need. Luckily, other developers were not
satisfied with this manual process, so they implemented tools to help us
automate this process and get good feedback. The most used in PHP is
PHPUnit. PHPUnit is a framework that provides a set of tools to write
tests in an easier manner, gives us the ability to run tests
automatically, and delivers useful feedback to the developer.

In order to use PHPUnit, traditionally, we installed it on our laptop.
In doing so, we added the classes of the framework to include the path
of PHP and also the executable to run the tests. This was less than
ideal as we forced developers to install one more tool on their
development machine. Nowadays, Composer (refer to `*Chapter
6* <#Top_of_ch06_html>`__, *Adapting to MVC*, in order to refresh your
memory) helps us in including PHPUnit as a dependency of the project.
This means that running Composer, which you will do for sure in order to
get the rest of the dependencies, will get PHPUnit too. Add, then, the
following into composer.json:

    { //... "require": { "monolog/monolog": "^1.17", "twig/twig":
    "^1.23" }, "require-dev": { "phpunit/phpunit": "5.1.3" },
    "autoload": { "psr-4": { "Bookstore\\\\": "src" } } }

Note that this dependency is added as require-dev. This means that the
dependency will be downloaded only when we are on a development
environment, but it will not be part of the application that we will
deploy on production as we do not need to run tests there. To get the
dependency, as always, run composer update.

A different approach is to install PHPUnit globally so that all the
projects on your development environment can use it instead of
installing it locally each time. You can read about how to install tools
globally with Composer at
`*https://akrabat.com/global-installation-of-php-tools-with-composer/* <https://akrabat.com/global-installation-of-php-tools-with-composer/>`__.

The phpunit.xml file
~~~~~~~~~~~~~~~~~~~~

PHPUnit needs a phpunit.xml file in order to define the way we want to
run the tests. This file defines a set of rules like where the tests
are, what code are the tests testing, and so on. Add the following file
in your root directory:

    <?xml version="1.0" encoding="UTF-8"?> <phpunit
    backupGlobals="false" backupStaticAttributes="false" colors="true"
    convertErrorsToExceptions="true" convertNoticesToExceptions="true"
    convertWarningsToExceptions="true" processIsolation="false"
    stopOnFailure="false" syntaxCheck="false"
    bootstrap="vendor/autoload.php" > <testsuites> <testsuite
    name="Bookstore Test Suite"> <directory>./tests/</directory>
    </testsuite> </testsuites> <filter> <whitelist>
    <directory>./src</directory> </whitelist> </filter> </phpunit>

This file defines quite a lot of things. The most important are
explained as follows:

-  Setting convertErrorsToExceptions, convertNoticesToExceptions, and
   convertWarningsToExceptions to true will make your tests fail if
   there is a PHP error, warning, or notice. The goal is to make sure
   that your code does not contain minor errors on edge cases, which are
   always the source of potential problems.

-  The stopOnFailure tells PHPUnit whether it should continue executing
   the rest of tests or not when there is a failed test. In this case,
   we want to run all of them to know how many tests are failing and
   why.

-  The bootstrap defines which file we should execute before starting to
   run the tests. The most common usage is to include the autoloader,
   but you could also include a file that initializes some dependencies,
   such as databases or configuration readers.

-  The testsuites defines the directories where PHPUnit will look for
   tests. In our case, we defined ./tests, but we could add more if we
   had them in different directories.

-  The whitelist defines the list of directories that contain the code
   that we are testing. This can be useful to generate output related to
   the code coverage.

When running the tests with PHPUnit, just make sure that you run the
command from the same directory where the phpunit.xml file is. We will
show you how in the next section.

Your first test
~~~~~~~~~~~~~~~

Right, that's enough preparations and theory; let's write some code. We
will write tests for the basic customer, which is a domain object with
little logic. First of all, we need to refactor the Unique trait as it
still contains some unnecessary code after integrating our application
with MySQL. We are talking about the ability to assign the next
available ID, which is now handled by the autoincremental field. Remove
it, leaving the code as follows:

    <?php namespace Bookstore\\Utils; trait Unique { protected $id;
    public function setId(int $id) { $this->id = $id; } public function
    getId(): int { return $this->id; } }

The tests will be inside the tests/ directory. The structure of
directories should be the same as in the src/ directory so that it is
easier to identify where each test should be. The file and the class
names need to end with Test so that PHPUnit knows that a file contains
tests. Knowing this, our test should be in
tests/Domain/Customer/BasicTest.php, as follows:

    <?php namespace Bookstore\\Tests\\Domain\\Customer; use
    Bookstore\\Domain\\Customer\\Basic; use
    PHPUnit\_Framework\_TestCase; class BasicTest extends
    PHPUnit\_Framework\_TestCase { public function testAmountToBorrow()
    { $customer = new Basic(1, 'han', 'solo', 'han@solo.com');
    $this->assertSame( 3, $customer->getAmountToBorrow(), 'Basic
    customer should borrow up to 3 books.' ); } }

As you can note, the BasicTest class extends from
PHPUnit\_Framework\_TestCase. All test classes have to extend from this
class. This class comes with a set of methods that allow you to make
assertions. An assertion in PHPUnit is just a check performed on a
value. Assertions can be comparisons to other values, a verification of
some attributes of the values, and so on. If an assertion is not true,
the test will be marked as failed, outputting the proper error message
to the developer. The example shows an assertion using the assertSame
method, which will compare two values, expecting that both of them are
exactly the same. The third argument is an error message that the
assertion will show in case it fails.

Also, note that the function names that start with test are the ones
executed with PHPUnit. In this example, we have one unique test named
testAmountToBorrow that instantiates a basic customer and verifies that
the amount of books that the customer can borrow is 3. In the next
section, we will show you how to run this test and get feedback from it.

Optionally, you could use any function name if you add the @test
annotation in the method's DocBlock, as follows:

    /\*\* \* @test \*/ public function thisIsATestToo() { //... }

Running tests
~~~~~~~~~~~~~

In order to run the tests you wrote, you need to execute the script that
Composer generated in vendor/bin. Remember always to run from the root
directory of the project so that PHPUnit can find your phpunit.xml
configuration file. Then, type ./vendor/bin/phpunit.

|image30|

When executing this program, we will get the feedback given by the
tests. The output shows us that there is one test (one method) and one
assertion and whether these were satisfactory. This output is what you
would like to see every time you run your tests, but you will get more
failed tests than you would like. Let's take a look at them by adding
the following test:

    public function testFail() { $customer = new Basic(1, 'han', 'solo',
    'han@solo.com'); $this->assertSame( 4,
    $customer->getAmountToBorrow(), 'Basic customer should borrow up to
    3 books.' ); }

This test will fail as we are checking whether getAmountToBorrow returns
4, but you know that it always returns 3. Let's run the tests and take a
look at what kind of output we get.

|image31|

We can quickly note that the output is not good due to the red color. It
shows us that there is a failure, pointing to the class and test method
that failed. The feedback points out the type of failure (as 3 is not
identical to 4) and optionally, the error message we added when invoking
the assert method.

Writing unit tests
------------------

Let's start digging into all the features that PHPUnit offers us in
order to write tests. We will divide these features in different
subsections: setting up a test, assertions, exceptions, and data
providers. Of course, you do not need to use all of these tools each
time you write a test.

The start and end of a test
~~~~~~~~~~~~~~~~~~~~~~~~~~~

PHPUnit gives you the opportunity to set up a common scenario for each
test in a class. For this, you need to use the setUp method, which, if
present, is executed each time that a test of this class is executed.
The instance of the class that invokes the setUp and test methods is the
same, so you can use the properties of the class to save the context.
One common use would be to create the object that we will use for our
tests in case this is always the same. For an example, write the
following code in tests/Domain/Customer/BasicTest.php:

    <?php namespace Bookstore\\Tests\\Domain\\Customer; use
    Bookstore\\Domain\\Customer\\Basic; use
    PHPUnit\_Framework\_TestCase; class BasicTest extends
    PHPUnit\_Framework\_TestCase { private $customer; public function
    setUp() { $this->customer = new Basic( 1, 'han', 'solo',
    'han@solo.com' ); } public function testAmountToBorrow() {
    $this->assertSame( 3, $this->customer->getAmountToBorrow(), 'Basic
    customer should borrow up to 3 books.' ); } }

When testAmountToBorrow is invoked, the $customer property is already
initialized through the execution of the setUp method. If the class had
more than one test, the setUp method would be executed each time.

Even though it is less common to use, there is another method used to
clean up the scenario after the test is executed: tearDown. This works
in the same way, but it is executed after each test of this class is
executed. Possible uses would be to clean up database data, close
connections, delete files, and so on.

Assertions
~~~~~~~~~~

You have already been introduced to the concept of assertions, so let's
just list the most common ones in this section. For the full list, we
recommend you to visit the official documentation at
`*https://phpunit.de/manual/current/en/appendixes.assertions.html* <https://phpunit.de/manual/current/en/appendixes.assertions.html>`__
as it is quite extensive; however, to be honest, you will probably not
use many of them.

The first type of assertion that we will see is the Boolean assertion,
that is, the one that checks whether a value is true or false. The
methods are as simple as assertTrue and assertFalse, and they expect one
parameter, which is the value to assert, and optionally, a text to
display in case of failure. In the same BasicTest class, add the
following test:

    public function testIsExemptOfTaxes() { $this->assertFalse(
    $this->customer->isExemptOfTaxes(), 'Basic customer should be exempt
    of taxes.' ); }

This test makes sure that a basic customer is never exempt of taxes.
Note that we could do the same assertion by writing the following:

    $this->assertSame( $this->customer->isExemptOfTaxes(), false, 'Basic
    customer should be exempt of taxes.' );

A second group of assertions would be the comparison assertions. The
most famous ones are assertSame and assertEquals. You have already used
the first one, but are you sure of its meaning? Let's add another test
and run it:

    public function testGetMonthlyFee() { $this->assertSame( 5,
    $this->customer->getMonthlyFee(), 'Basic customer should pay 5 a
    month.' ); }

The result of the test is shown in the following screenshot:

|image32|

The test failed! The reason is that assertSame is the equivalent to
comparing using identity, that is, without using type juggling. The
result of the getMonthlyFee method is always a float, and we will
compare it with an integer, so it will never be the same, as the error
message tells us. Change the assertion to assertEquals, which compares
using equality, and the test will pass now.

When working with objects, we can use an assertion to check whether a
given object is an instance of the expected class or not. When doing so,
remember to send the full name of the class as this is a quite common
mistake. Even better, you could get the class name using ::class, for
example, Basic::class. Add the following test in
tests/Domain/Customer/CustomerFactoryTest.php:

    <?php namespace Bookstore\\Tests\\Domain\\Customer; use
    Bookstore\\Domain\\Customer\\CustomerFactory; use
    PHPUnit\_Framework\_TestCase; class CustomerFactoryTest extends
    PHPUnit\_Framework\_TestCase { public function testFactoryBasic() {
    $customer = CustomerFactory::factory( 'basic', 1, 'han', 'solo',
    'han@solo.com' ); $this->assertInstanceOf( Basic::class, $customer,
    'basic should create a Customer\\Basic object.' ); } }

This test creates a customer using the customer factory. As the type of
customer was basic, the result should be an instance of Basic, which is
what we are testing with assertInstanceOf. The first argument is the
expected class, the second is the object that we are testing, and the
third is the error message. This test also helps us to note the behavior
of comparison assertions with objects. Let's create a basic customer
object as expected and compare it with the result of the factory. Then,
run the test, as follows:

    $expectedBasicCustomer = new Basic(1, 'han', 'solo',
    'han@solo.com'); $this->assertSame( $customer,
    $expectedBasicCustomer, 'Customer object is not as expected.' );

The result of this test is shown in the following screenshot:

|image33|

The test failed because when you compare two objects with identity
comparison, you comparing the object reference, and it will only be the
same if the two objects are exactly the same instance. If you create two
objects with the same properties, they will be equal but never
identical. To fix the test, change the assertion as follows:

    $expectedBasicCustomer = new Basic(1, 'han', 'solo',
    'han@solo.com'); $this->assertEquals( $customer,
    $expectedBasicCustomer, 'Customer object is not as expected.' );

Let's now write the tests for the sale domain object at
tests/Domain/SaleTest.php. This class is very easy to test and allows us
to use some new assertions, as follows:

    <?php namespace Bookstore\\Tests\\Domain\\Customer; use
    Bookstore\\Domain\\Sale; use PHPUnit\_Framework\_TestCase; class
    SaleTest extends PHPUnit\_Framework\_TestCase { public function
    testNewSaleHasNoBooks() { $sale = new Sale(); $this->assertEmpty(
    $sale->getBooks(), 'When new, sale should have no books.' ); }
    public function testAddNewBook() { $sale = new Sale();
    $sale->addBook(123); $this->assertCount( 1, $sale->getBooks(),
    'Number of books not valid.' ); $this->assertArrayHasKey( 123,
    $sale->getBooks(), 'Book id could not be found in array.' );
    $this->assertSame( $sale->getBooks()[123], 1, 'When not specified,
    amount of books is 1.' ); } }

We added two tests here: one makes sure that for a new sale instance,
the list of books associated with it is empty. For this, we used the
assertEmpty method, which takes an array as an argument and will assert
that it is empty. The second test is adding a book to the sale and then
making sure that the list of books has the correct content. For this, we
will use the assertCount method, which verifies that the array, that is,
the second argument, has as many elements as the first argument
provided. In this case, we expect that the list of books has only one
entry. The second assertion of this test is verifying that the array of
books contains a specific key, which is the ID of the book, with the
assertArrayHasKey method, in which the first argument is the key, and
the second one is the array. Finally, we will check with the already
known assertSame method that the amount of books inserted is 1.

Even though these two new assertion methods are useful sometimes, all
the three assertions of the last test can be replaced by just an
assertSame method, comparing the whole array of books with the expected
one, as follows:

    $this->assertSame( [123 => 1], $sale->getBooks(), 'Books array does
    not match.' );

The suite of tests for the sale domain object would not be enough if we
were not testing how the class behaves when adding multiple books. In
this case, using assertCount and assertArrayHasKey would make the test
unnecessarily long, so let's just compare the array with an expected one
via the following code:

    public function testAddMultipleBooks() { $sale = new Sale();
    $sale->addBook(123, 4); $sale->addBook(456, 2); $sale->addBook(456,
    8); $this->assertSame( [123 => 4, 456 => 10], $sale->getBooks(),
    'Books are not as expected.' ); }

Expecting exceptions
~~~~~~~~~~~~~~~~~~~~

Sometimes, a method is expected to throw an exception for certain
unexpected use cases. When this happens, you could try to capture this
exception inside the test or take advantage of another tool that PHPUnit
offers: expecting exceptions. To mark a test to expect a given
exception, just add the @expectedException annotation followed by the
exception's class full name. Optionally, you can use
@expectedExceptionMessage to assert the message of the exception. Let's
add the following tests to our CustomerFactoryTest class:

    /\*\* \* @expectedException \\InvalidArgumentException \*
    @expectedExceptionMessage Wrong type. \*/ public function
    testCreatingWrongTypeOfCustomer() { $customer =
    CustomerFactory::factory( 'deluxe', 1, 'han', 'solo', 'han@solo.com'
    ); }

In this test we will try to create a deluxe customer with our factory,
but as this type of customer does not exist, we will get an exception.
The type of the expected exception is InvalidArgumentException, and the
error message is "Wrong type". If you run the tests, you will see that
they pass.

If we defined an expected exception and the exception is never thrown,
the test will fail; expecting exceptions is just another type of
assertion. To see this happen, add the following to your test and run
it; you will get a failure, and PHPUnit will complain saying that it
expected the exception, but it was never thrown:

    /\*\* \* @expectedException \\InvalidArgumentException \*/ public
    function testCreatingCorrectCustomer() { $customer =
    CustomerFactory::factory( 'basic', 1, 'han', 'solo', 'han@solo.com'
    ); }

Data providers
~~~~~~~~~~~~~~

If you think about the flow of a test, most of the time, we invoke a
method with an input and expect an output. In order to cover all the
edge cases, it is natural that we will repeat the same action with a set
of inputs and expected outputs. PHPUnit gives us the ability to do so,
thus removing a lot of duplicated code. This feature is called data
providing.

A data provider is a public method defined in the test class that
returns an array with a specific schema. Each entry of the array
represents a test in which the key is the name of the test—optionally,
you could use numeric keys—and the value is the parameter that the test
needs. A test will declare that it needs a data provider with the
@dataProvider annotation, and when executing tests, the data provider
injects the arguments that the test method needs. Let's consider an
example to make it easier. Write the following two methods in your
CustomerFactoryTest class:

    public function providerFactoryValidCustomerTypes() { return [
    'Basic customer, lowercase' => [ 'type' => 'basic', 'expectedType'
    => '\\Bookstore\\Domain\\Customer\\Basic' ], 'Basic customer,
    uppercase' => [ 'type' => 'BASIC', 'expectedType' =>
    '\\Bookstore\\Domain\\Customer\\Basic' ], 'Premium customer,
    lowercase' => [ 'type' => 'premium', 'expectedType' =>
    '\\Bookstore\\Domain\\Customer\\Premium' ], 'Premium customer,
    uppercase' => [ 'type' => 'PREMIUM', 'expectedType' =>
    '\\Bookstore\\Domain\\Customer\\Premium' ] ]; } /\*\* \*
    @dataProvider providerFactoryValidCustomerTypes \* @param string
    $type \* @param string $expectedType \*/ public function
    testFactoryValidCustomerTypes( string $type, string $expectedType )
    { $customer = CustomerFactory::factory( $type, 1, 'han', 'solo',
    'han@solo.com' ); $this->assertInstanceOf( $expectedType, $customer,
    'Factory created the wrong type of customer.' ); }

The test here is testFactoryValidCustomerTypes, which expects two
arguments: $type and $expectedType. The test uses them to create a
customer with the factory and verify the type of the result, which we
already did by hardcoding the types. The test also declares that it
needs the providerFactoryValidCustomerTypes data provider. This data
provider returns an array of four entries, which means that the test
will be executed four times with four different sets of arguments. The
name of each test is the key of each entry—for example, "Basic customer,
lowercase". This is very useful in case a test fails because it will be
displayed as part of the error messages. Each entry is a map with two
values, type and expectedType, which are the names of the arguments of
the test method. The values of these entries are the values that the
test method will get.

The bottom line is that the code we wrote would be the same as if we
wrote testFactoryValidCustomerTypes four times, hardcoding $type and
$expectedType each time. Imagine now that the test method contains tens
of lines of code or we want to repeat the same test with tens of
datasets; do you see how powerful it is?

Testing with doubles
--------------------

So far, we tested classes that are quite isolated; that is, they do not
have much interaction with other classes. Nevertheless, we have classes
that use several classes, such as controllers. What can we do with these
interactions? The idea of unit tests is to test a specific method and
not the whole code base, right?

PHPUnit allows you to mock these dependencies; that is, you can provide
fake objects that look similar to the dependencies that the tested class
needs, but they do not use code from those classes. The goal of this is
to provide a dummy instance that the class can use and invoke its
methods without the side effect of what these invocations might have.
Imagine as an example the case of the models: if the controller uses a
real model, then when invoking methods from it, the model would access
the database each time, making the tests quite unpredictable.

If we use a mock as the model instead, the controller can invoke its
methods as many times as needed without any side effect. Even better, we
can make assertions of the arguments that the mock received or force it
to return specific values. Let's take a look at how to use them.

Injecting models with DI
~~~~~~~~~~~~~~~~~~~~~~~~

The first thing we need to understand is that if we create objects using
new inside the controller, we will not be able to mock them. This means
that we need to inject all the dependencies—for example, using a
dependency injector. We will do this for all of the dependencies but
one: the models. In this section, we will test the borrow method of the
BookController class, so we will show the changes that this method
needs. Of course, if you want to test the rest of the code, you should
apply these same changes to the rest of the controllers.

The first thing to do is to add the BookModel instance to the dependency
injector in our index.php file. As this class also has a dependency,
PDO, use the same dependency injector to get an instance of it, as
follows:

    $di->set('BookModel', new BookModel($di->get('PDO')));

Now, in the borrow method of the BookController class, we will change
the new instantiation of the model to the following:

    public function borrow(int $bookId): string { $bookModel =
    $this->di->get('BookModel'); try { //...

Customizing TestCase
~~~~~~~~~~~~~~~~~~~~

When writing your unit test's suite, it is quite common to have a
customized TestCase class from which all tests extend. This class always
extends from PHPUnit\_Framework\_TestCase, so we still get all the
assertions and other methods. As all tests have to import this class,
let's change our autoloader so that it can recognize namespaces from the
tests directory. After this, run composer update, as follows:

    "autoload": { "psr-4": { "Bookstore\\\\Tests\\\\": "tests",
    "Bookstore\\\\": "src" } }

With this change, we will tell Composer that all the namespaces starting
with Bookstore\\Tests will be located under the tests directory, and the
rest will follow the previous rules.

Let's add now our customized TestCase class. The only helper method we
need right now is one to create mocks. It is not really necessary, but
it makes things cleaner. Add the following class in
tests/AbstractTestClase.php:

    <?php namespace Bookstore\\Tests; use PHPUnit\_Framework\_TestCase;
    use InvalidArgumentException; abstract class AbstractTestCase
    extends PHPUnit\_Framework\_TestCase { protected function
    mock(string $className) { if (strpos($className, '\\\\') !== 0) {
    $className = '\\\\' . $className; } if (!class\_exists($className))
    { $className = '\\Bookstore\\\\' . trim($className, '\\\\'); if
    (!class\_exists($className)) { throw new InvalidArgumentException(
    "Class $className not found." ); } } return
    $this->getMockBuilder($className) ->disableOriginalConstructor()
    ->getMock(); } }

This method takes the name of a class and tries to figure out whether
the class is part of the Bookstore namespace or not. This will be handy
when mocking objects of our own codebase as we will not have to write
Bookstore each time. After figuring out what the real full class name
is, it uses the mock builder from PHPUnit to create one and then returns
it.

More helpers! This time, they are for controllers. Every single
controller will always need the same dependencies: logger, database
connection, template engine, and configuration reader. Knowing this,
let's create a ControllerTestCase class from where all the tests
covering controllers will extend. This class will contain a setUp method
that creates all the common mocks and sets them in the dependency
injector. Add it as your tests/ControllerTestCase.php file, as follows:

    <?php namespace Bookstore\\Tests; use
    Bookstore\\Utils\\DependencyInjector; use Bookstore\\Core\\Config;
    use Monolog\\Logger; use Twig\_Environment; use PDO; abstract class
    ControllerTestCase extends AbstractTestCase { protected $di; public
    function setUp() { $this->di = new DependencyInjector();
    $this->di->set('PDO', $this->mock(PDO::class));
    $this->di->set('Utils\\Config', $this->mock(Config::class));
    $this->di->set( 'Twig\_Environment',
    $this->mock(Twig\_Environment::class) ); $this->di->set('Logger',
    $this->mock(Logger::class)); } }

Using mocks
~~~~~~~~~~~

Well, we've had enough of the helpers; let's start with the tests. The
difficult part here is how to play with mocks. When you create one, you
can add some expectations and return values. The methods are:

-  expects: This specifies the amount of times the mock's method is
   invoked. You can send $this->never(), $this->once(), or $this->any()
   as an argument to specify 0, 1, or any invocations.

-  method: This is used to specify the method we are talking about. The
   argument that it expects is just the name of the method.

-  with: This is a method used to set the expectations of the arguments
   that the mock will receive when it is invoked. For example, if the
   mocked method is expected to get basic as the first argument and 123
   as the second, the with method will be invoked as with("basic", 123).
   This method is optional, but if we set it, PHPUnit will throw an
   error in case the mocked method does not get the expected arguments,
   so it works as an assertion.

-  will: This is used to define what the mock will return. The two most
   common usages are $this->returnValue($value) or
   $this->throwException($exception). This method is also optional, and
   if not invoked, the mock will always return null.

Let's add the first test to see how it would work. Add the following
code to the tests/Controllers/BookControllerTest.php file:

    <?php namespace Bookstore\\Tests\\Controllers; use
    Bookstore\\Controllers\\BookController; use
    Bookstore\\Core\\Request; use
    Bookstore\\Exceptions\\NotFoundException; use
    Bookstore\\Models\\BookModel; use
    Bookstore\\Tests\\ControllerTestCase; use Twig\_Template; class
    BookControllerTest extends ControllerTestCase { private function
    getController( Request $request = null ): BookController { if
    ($request === null) { $request = $this->mock('Core\\Request'); }
    return new BookController($this->di, $request); } public function
    testBookNotFound() { $bookModel = $this->mock(BookModel::class);
    $bookModel ->expects($this->once()) ->method('get') ->with(123)
    ->will( $this->throwException( new NotFoundException() ) );
    $this->di->set('BookModel', $bookModel); $response = "Rendered
    template"; $template = $this->mock(Twig\_Template::class); $template
    ->expects($this->once()) ->method('render') ->with(['errorMessage'
    => 'Book not found.']) ->will($this->returnValue($response));
    $this->di->get('Twig\_Environment') ->expects($this->once())
    ->method('loadTemplate') ->with('error.twig')
    ->will($this->returnValue($template)); $result =
    $this->getController()->borrow(123); $this->assertSame( $result,
    $response, 'Response object is not the expected one.' ); } }

The first thing the test does is to create a mock of the BookModel
class. Then, it adds an expectation that goes like this: the get method
will be called once with one argument, 123, and it will throw
NotFoundException. This makes sense as the test tries to emulate a
scenario in which we cannot find the book in the database.

The second part of the test consists of adding the expectations of the
template engine. This is a bit more complex as there are two mocks
involved. The loadTemplate method of Twig\_Environment is expected to be
called once with the error.twig argument as the template name. This mock
should return Twig\_Template, which is another mock. The render method
of this second mock is expected to be called once with the correct error
message, returning the response, which is a hardcoded string. After all
the dependencies are defined, we just need to invoke the borrow method
of the controller and expect a response.

Remember that this test does not have only one assertion, but four: the
assertSame method and the three mock expectations. If any of them are
not accomplished, the test will fail, so we can say that this method is
quite robust.

With our first test, we verified that the scenario in which the book is
not found works. There are two more scenarios that fail as well: when
there are not enough copies of the book to borrow and when there is a
database error when trying to save the borrowed book. However, you can
see now that all of them share a piece of code that mocks the template.
Let's extract this code to a protected method that generates the mocks
when it is given the template name, the parameters are sent to the
template, and the expected response is received. Run the following:

    protected function mockTemplate( string $templateName, array
    $params, $response ) { $template =
    $this->mock(Twig\_Template::class); $template
    ->expects($this->once()) ->method('render') ->with($params)
    ->will($this->returnValue($response));
    $this->di->get('Twig\_Environment') ->expects($this->once())
    ->method('loadTemplate') ->with($templateName)
    ->will($this->returnValue($template)); } public function
    testNotEnoughCopies() { $bookModel = $this->mock(BookModel::class);
    $bookModel ->expects($this->once()) ->method('get') ->with(123)
    ->will($this->returnValue(new Book())); $bookModel
    ->expects($this->never()) ->method('borrow');
    $this->di->set('BookModel', $bookModel); $response = "Rendered
    template"; $this->mockTemplate( 'error.twig', ['errorMessage' =>
    'There are no copies left.'], $response ); $result =
    $this->getController()->borrow(123); $this->assertSame( $result,
    $response, 'Response object is not the expected one.' ); } public
    function testErrorSaving() { $controller = $this->getController();
    $controller->setCustomerId(9); $book = new Book(); $book->addCopy();
    $bookModel = $this->mock(BookModel::class); $bookModel
    ->expects($this->once()) ->method('get') ->with(123)
    ->will($this->returnValue($book)); $bookModel
    ->expects($this->once()) ->method('borrow') ->with(new Book(), 9)
    ->will($this->throwException(new DbException()));
    $this->di->set('BookModel', $bookModel); $response = "Rendered
    template"; $this->mockTemplate( 'error.twig', ['errorMessage' =>
    'Error borrowing book.'], $response ); $result =
    $controller->borrow(123); $this->assertSame( $result, $response,
    'Response object is not the expected one.' ); }

The only novelty here is when we expect that the borrow method is never
invoked. As we do not expect it to be invoked, there is no reason to use
the with nor will method. If the code actually invokes this method,
PHPUnit will mark the test as failed.

We already tested and found that all the scenarios that can fail have
failed. Let's add a test now where a user can successfully borrow a
book, which means that we will return valid books and customers from the
database, the save method will be invoked correctly, and the template
will get all the correct parameters. The test looks as follows:

    public function testBorrowingBook() { $controller =
    $this->getController(); $controller->setCustomerId(9); $book = new
    Book(); $book->addCopy(); $bookModel =
    $this->mock(BookModel::class); $bookModel ->expects($this->once())
    ->method('get') ->with(123) ->will($this->returnValue($book));
    $bookModel ->expects($this->once()) ->method('borrow') ->with(new
    Book(), 9); $bookModel ->expects($this->once())
    ->method('getByUser') ->with(9) ->will($this->returnValue(['book1',
    'book2'])); $this->di->set('BookModel', $bookModel); $response =
    "Rendered template"; $this->mockTemplate( 'books.twig', [ 'books' =>
    ['book1', 'book2'], 'currentPage' => 1, 'lastPage' => true ],
    $response ); $result = $controller->borrow(123); $this->assertSame(
    $result, $response, 'Response object is not the expected one.' ); }

So this is it. You have written one of the most complex tests you will
need to write during this book. What do you think of it? Well, as you do
not have much experience with tests, you might be quite satisfied with
the result, but let's try to analyze it a bit further.

Database testing
----------------

This will be the most controversial of the sections of this chapter by
far. When it comes to database testing, there are different schools of
thought. Should we use the database or not? Should we use our
development database or one in memory? It is quite out of the scope of
the book to explain how to mock the database or prepare a fresh one for
each test, but we will try to summarize some of the techniques here:

-  We will mock the database connection and write expectations to all
   the interactions between the model and the database. In our case,
   this would mean that we would inject a mock of the PDO object. As we
   will write the queries manually, chances are that we might introduce
   a wrong query. Mocking the connection would not help us detect this
   error. This solution would be good if we used ORM instead of writing
   the queries manually, but we will leave this topic out of the book.

-  For each test, we will create a brand new database in which we add
   the data we would like to have for the specific test. This approach
   might take a lot of time, but it assures you that you will be testing
   against a real database and that there is no unexpected data that
   might make our tests fail; that is, the tests are fully isolated. In
   most of the cases, this would be the preferable approach, even though
   it might not be the one that performs faster. To solve this
   inconvenience, we will create in-memory databases.

-  Tests run against an already existing database. Usually, at the
   beginning of the test we start a transaction that we roll back at the
   end of the test, leaving the database without any change. This
   approach emulates a real scenario, in which we can find all sorts of
   data and our code should always behave as expected. However, using a
   shared database always has some side effects; for example, if we want
   to introduce changes to the database schema, we will have to apply
   them to the database before running the tests, but the rest of the
   applications or developers that use the database are not yet ready
   for these changes.

In order to keep things small, we will try to implement a mixture of the
second and third options. We will use our existing database, but after
starting the transaction of each test, we will clean all the tables
involved with the test. This looks as though we need a ModelTestCase to
handle this. Add the following into tests/ModelTestCase.php:

    <?php namespace Bookstore\\Tests; use Bookstore\\Core\\Config; use
    PDO; abstract class ModelTestCase extends AbstractTestCase {
    protected $db; protected $tables = []; public function setUp() {
    $config = new Config(); $dbConfig = $config->get('db'); $this->db =
    new PDO( 'mysql:host=127.0.0.1;dbname=bookstore', $dbConfig['user'],
    $dbConfig['password'] ); $this->db->beginTransaction();
    $this->cleanAllTables(); } public function tearDown() {
    $this->db->rollBack(); } protected function cleanAllTables() {
    foreach ($this->tables as $table) { $this->db->exec("delete from
    $table"); } } }

The setUp method creates a database connection with the same credentials
found in the config/app.yml file. Then, we will start a transaction and
invoke the cleanAllTables method, which iterates the tables in the
$tables property and deletes all the content from them. The tearDown
method rolls back the transaction.

Note

Extending from ModelTestCase

If you write a test extending from this class that needs to implement
either the setUp or tearDown method, always remember to invoke the ones
from the parent.

Let's write tests for the borrow method of the BookModel class. This
method uses books and customers, so we would like to clean the tables
that contain them. Create the test class and save it in
tests/Models/BookModelTest.php:

    <?php namespace Bookstore\\Tests\\Models; use
    Bookstore\\Models\\BookModel; use Bookstore\\Tests\\ModelTestCase;
    class BookModelTest extends ModelTestCase { protected $tables = [
    'borrowed\_books', 'customer', 'book' ]; protected $model; public
    function setUp() { parent::setUp(); $this->model = new
    BookModel($this->db); } }

Note how we also overrode the setUp method, invoking the one in the
parent and creating the model instance that all tests will use, which is
safe to do as we will not keep any context on this object. Before adding
the tests though, let's add some more helpers to ModelTestCase: one to
create book objects given an array of parameters and two to save books
and customers in the database. Run the following code:

    protected function buildBook(array $properties): Book { $book = new
    Book(); $reflectionClass = new ReflectionClass(Book::class); foreach
    ($properties as $key => $value) { $property =
    $reflectionClass->getProperty($key); $property->setAccessible(true);
    $property->setValue($book, $value); } return $book; } protected
    function addBook(array $params) { $default = [ 'id' => null, 'isbn'
    => 'isbn', 'title' => 'title', 'author' => 'author', 'stock' => 1,
    'price' => 10.0, ]; $params = array\_merge($default, $params);
    $query = <<<SQL insert into book (id, isbn, title, author, stock,
    price) values(:id, :isbn, :title, :author, :stock, :price) SQL;
    $this->db->prepare($query)->execute($params); } protected function
    addCustomer(array $params) { $default = [ 'id' => null, 'firstname'
    => 'firstname', 'surname' => 'surname', 'email' => 'email', 'type'
    => 'basic' ]; $params = array\_merge($default, $params); $query =
    <<<SQL insert into customer (id, firstname, surname, email, type)
    values(:id, :firstname, :surname, :email, :type) SQL;
    $this->db->prepare($query)->execute($params); }

As you can note, we added default values for all the fields, so we are
not forced to define the whole book/customer each time we want to save
one. Instead, we just sent the relevant fields and merged them to the
default ones.

Also, note that the buildBook method used a new concept, reflection, to
access the private properties of an instance. This is way beyond the
scope of the book, but if you are interested, you can read more at
`*http://php.net/manual/en/book.reflection.php* <http://php.net/manual/en/book.reflection.php>`__.

We are now ready to start writing tests. With all these helpers, adding
tests will be very easy and clean. The borrow method has different use
cases: trying to borrow a book that is not in the database, trying to
use a customer not registered, and borrowing a book successfully. Let's
add them as follows:

    /\*\* \* @expectedException \\Bookstore\\Exceptions\\DbException \*/
    public function testBorrowBookNotFound() { $book =
    $this->buildBook(['id' => 123]); $this->model->borrow($book, 123); }
    /\*\* \* @expectedException \\Bookstore\\Exceptions\\DbException \*/
    public function testBorrowCustomerNotFound() { $book =
    $this->buildBook(['id' => 123]); $this->addBook(['id' => 123]);
    $this->model->borrow($book, 123); } public function testBorrow() {
    $book = $this->buildBook(['id' => 123, 'stock' => 12]);
    $this->addBook(['id' => 123, 'stock' => 12]);
    $this->addCustomer(['id' => 123]); $this->model->borrow($book, 123);
    }

Impressed? Compared to the controller tests, these tests are way
simpler, mainly because their code performs only one action, but also
thanks to all the methods added to ModelTestCase. Once you need to work
with other objects, such as sales, you can add addSale or buildSale to
this same class to make things cleaner.

Test-driven development
-----------------------

You might realize already that there is no unique way to do things when
talking about developing an application. It is out of the scope of this
book to show you all of them—and by the time you are done reading these
lines, more techniques will have been incorporated already—but there is
one approach that is very useful when it comes to writing good, testable
code: test-driven development (TDD).

This methodology consists of writing the unit tests before writing the
code itself. The idea, though, is not to write all the tests at once and
then write the class or method but rather to do it in a progressive way.
Let's consider an example to make it easier. Imagine that your Sale
class is yet to be implemented and the only thing we know is that we
have to be able to add books. Rename your src/Domain/Sale.php file to
src/Domain/Sale2.php or just delete it so that the application does not
know about it.

Note

Is all this verbosity necessary?

You will note in this example that we will perform an excessive amount
of steps to come up with a very simple piece of code. Indeed, they are
too many for this example, but there will be times when this amount is
just fine. Finding these moments comes with experience, so we recommend
you to practice first with simple examples. Eventually, it will come
naturally to you.

The mechanics of TDD consist of four steps, as follows:

1. Write a test for some functionality that is not yet implemented.

2. Run the unit tests, and they should fail. If they do not, either your
   test is wrong, or your code already implements this functionality.

3. Write the minimum amount of code to make the tests pass.

4. Run the unit tests again. This time, they should pass.

We do not have the sale domain object, so the first thing, as we should
start from small things and then move on to bigger things, is to assure
that we can instantiate the sale object. Write the following unit test
in tests/Domain/SaleTest.php as we will write all the existing tests,
but using TDD; you can remove the existing tests in this file.

    <?php namespace Bookstore\\Tests\\Domain; use
    Bookstore\\Domain\\Sale; use PHPUnit\_Framework\_TestCase; class
    SaleTest extends PHPUnit\_Framework\_TestCase { public function
    testCanCreate() { $sale = new Sale(); } }

Run the tests to make sure that they are failing. In order to run one
specific test, you can mention the file of the test when running
PHPUnit, as shown in the following script:

|image34|

Good, they are failing. That means that PHP cannot find the object to
instantiate it. Let's now write the minimum amount of code required to
make this test pass. In this case, creating the class would be enough,
and you can do this through the following lines of code:

    <?php namespace Bookstore\\Domain; class Sale { }

Now, run the tests to make sure that there are no errors.

|image35|

This is easy, right? So, what we need to do is repeat this process,
adding more functionality each time. Let's focus on the books that a
sale holds; when created, the book's list should be empty, as follows:

    public function testWhenCreatedBookListIsEmpty() { $sale = new
    Sale(); $this->assertEmpty($sale->getBooks()); }

Run the tests to make sure that they fail—they do. Now, write the
following method in the class:

    public function getBooks(): array { return []; }

Now, if you run... wait, what? We are forcing the getBooks method to
return an empty array always? This is not the implementation that we
need—nor the one we deserve—so why do we do it? The reason is the
wording of step 3: "Write the minimum amount of code to make the tests
pass.". Our test suite should be extensive enough to detect this kind of
problem, and this is our way to make sure it does. This time, we will
write bad code on purpose, but next time, we might introduce a bug
unintentionally, and our unit tests should be able to detect it as soon
as possible. Run the tests; they will pass.

Now, let's discuss the next functionality. When adding a book to the
list, we should see this book with amount 1. The test should be as
follows:

    public function testWhenAddingABookIGetOneBook() { $sale = new
    Sale(); $sale->addBook(123); $this->assertSame( $sale->getBooks(),
    [123 => 1] ); }

This test is very useful. Not only does it force us to implement the
addBook method, but also it helps us fix the getBooks method—as it is
hardcoded right now—to always return an empty array. As the getBooks
method now expects two different results, we cannot trick the tests any
more. The new code for the class should be as follows:

    class Sale { private $books = []; public function getBooks(): array
    { return $this->books; } public function addBook(int $bookId) {
    $this->books[123] = 1; } }

A new test we can write is the one that allows you to add more than one
book at a time, sending the amount as the second argument. The test
would look similar to the following:

    public function testSpecifyAmountBooks() { $sale = new Sale();
    $sale->addBook(123, 5); $this->assertSame( $sale->getBooks(), [123
    => 5] ); }

Now, the tests do not pass, so we need to fix them. Let's refactor
addBook so that it can accept a second argument as the amount :

    public function addBook(int $bookId, int $amount = 1) {
    $this->books[123] = $amount; }

The next functionality we would like to add is the same book invoking
the method several times, keeping track of the total amount of books
added. The test could be as follows:

    public function testAddMultipleTimesSameBook() { $sale = new Sale();
    $sale->addBook(123, 5); $sale->addBook(123); $sale->addBook(123, 5);
    $this->assertSame( $sale->getBooks(), [123 => 11] ); }

This test will fail as the current execution will not add all the
amounts but will instead keep the last one. Let's fix it by executing
the following code:

    public function addBook(int $bookId, int $amount = 1) { if
    (!isset($this->books[123])) { $this->books[123] = 0; }
    $this->books[123] += $amount; }

Well, we are almost there. There is one last test we should add, which
is the ability to add more than one different book. The test is as
follows:

    public function testAddDifferentBooks() { $sale = new Sale();
    $sale->addBook(123, 5); $sale->addBook(456, 2); $sale->addBook(789,
    5); $this->assertSame( $sale->getBooks(), [123 => 5, 456 => 2, 789
    => 5] ); }

This test fails due to the hardcoded book ID in our implementation. If
we did not do this, the test would have already passed. Let's fix it
then; run the following:

    public function addBook(int $bookId, int $amount = 1) { if
    (!isset($this->books[$bookId])) { $this->books[$bookId] = 0; }
    $this->books[$bookId] += $amount; }

We are done! Does it look familiar? It is the same code we wrote on our
first implementation except for the rest of the properties. You can now
replace the sale domain object with the previous one, so you have all
the functionalities needed.

Theory versus practice
~~~~~~~~~~~~~~~~~~~~~~

As mentioned before, this is a quite long and verbose process that very
few experienced developers follow from start to end but one that most of
them encourage people to follow. Why is this so? When you write all your
code first and leave the unit tests for the end, there are two problems:

-  Firstly, in too many cases developers are lazy enough to skip tests,
   telling themselves that the code already works, so there is no need
   to write the tests. You already know that one of the goals of tests
   is to make sure that future changes do not break the current
   features, so this is not a valid reason.

-  Secondly, the tests written after the code usually test the code
   rather than the functionality. Imagine that you have a method that
   was initially meant to perform an action. After writing the method,
   we will not perform the action perfectly due to a bug or bad design;
   instead, we will either do too much or leave some edge cases
   untreated. When we write the test after writing the code, we will
   test what we see in the method, not what the original functionality
   was!

If you instead force yourself to write the tests first and then the
code, you make sure that you always have tests and that they test what
the code is meant to do, leading to a code that performs as expected and
is fully covered. Also, by doing it in small intervals, you get quick
feedback and don't have to wait for hours to know whether all the tests
and code you wrote make sense at all. Even though this idea is quite
simple and makes a lot of sense, many novice developers find it hard to
implement.

Experienced developers have written code for several years, so they have
already internalized all of this. This is the reason why some of them
prefer to either write several tests before starting with the code or
the other way around, that is, writing code and then testing it as they
are more productive this way. However, if there is something that all of
them have in common it is that their applications will always be full of
tests.

Summary
-------

In this chapter, you learned the importance of testing your code using
unit tests. You now know how to configure PHPUnit on your application so
that you can not only run your tests but also get good feedback. You got
a good introduction on how to write unit tests properly, and now, it is
safer for you to introduce changes in your application.

In the next chapter, we will study some existing frameworks, which you
can use instead of writing your own every time you start an application.
In this way, not only will you save time and effort, but also other
developers will be able to join you and understand your code easily.

