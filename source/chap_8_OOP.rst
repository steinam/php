.. include:: images.txt

chap_8_Classes and objects
==========================


Intro
-----

When applications start growing, representing more complex data structures becomes necessary. Primitive types like integers, strings, or arrays are not enough when you want to associate specific behavior to data. More than half a century ago, computer scientists started using the concept of objects to refer to the encapsulation of properties and functionality that represented an object in real life.


Nowadays, OOP is one of the most used programming paradigms, and you will be glad to know that PHP supports it. Knowing OOP is not just a matter of knowing the syntax of the language, but knowing when and how to use it. But do not worry, after this chapter and a bit of practice, you will become a confident OOP developer.

In this chapter, you will learn about the following:

-  Classes and objects

-  Visibility, static properties, and methods

-  Namespaces

-  Autoloading classes

-  Inheritance, interfaces, and traits

-  Handling exceptions

-  Design patterns

-  Anonymous functions


Koncept
-------

Objects are representations of real-life elements. Each object has a set of attributes that differentiates it from the rest of the objects of the same class, and is capable of a set of actions. A class is the definition of what an object looks like and what it can do, like a pattern for objects.

Let's take our bookstore example, and think of the kind of real-life objects it contains. We store books, and let people take them if they are available. We could think of two types of objects: books and customers. We can define these two classes as follows:

.. code-block:: php

    <?php 
        
        class Book { } 
        class Customer { }

A class is defined by the keyword class followed by a valid class name—that follows the same rules as any other PHP label, like variable names—and a block of code. But if we want to have a specific book, that is, an object Book—or instance of the class Book—we have to instantiate it. To instantiate an object, we use the keyword new followed by the name of the class. We assign the instance to a variable, as if it was a primitive type:

.. code-block:: php

    <?php
    $book = new Book(); 
    $customer = new Customer();

You can create as many instances as you need, as long as you assign them to different variables:

.. code-block:: php

    <?php
    $book1 = new Book(); 
    $book2 = new Book();


Class properties
----------------

Let's think about the properties of books first: they have a title, an author, and an ISBN. They can also be available or unavailable. Write the following code inside Book.php:

.. code-block:: php

    <?php 
        class Book { 
            public $isbn; 
            public $title; 
            public $author;
            public $available; 
        }

This preceding snippet defines a class that represents the properties that a book has. Do not bother about the word public; we will explain what it means when talking about visibility in the next section. For now, just think of properties as variables inside the class. We can use these variables in objects. Try adding this code at the end of the Book.php file:

.. code-block:: php

    <?php
    $book = new Book(); 
    $book->title = "1984"; 
    $book->author = "George Orwell"; 
    $book->available = true; 
    
    var_dump($book);

Printing the object shows the value of each of its properties, in a way  similar to the way arrays do with their keys. You can see that properties have a type at the moment of printing, but we did not define this type explicitly; instead, the variable took the type of the value assigned. This works exactly the same way that normal variables do.

When creating multiple instances of an object and assigning values to their properties, each object will have their own values, so you will not override them. The next bit of code shows you how this works:

.. code-block:: php

    <?php
    $book1 = new Book(); 
    $book1->title = "1984"; 
    $book2 = new Book();
    $book2->title = "To Kill a Mockingbird"; 
    
    var_dump($book1, $book2);

Class methods
-------------

Methods are functions defined inside a class. Like functions, methods get some arguments and perform some actions, optionally returning a value. The advantage of methods is that they can use the properties of the object that invoked them. Thus, calling the same method in two different objects might have two different results.

Even though it is usually a bad idea to mix HTML with PHP, for the sake of learning, let's add a method in our class Book that returns the book as in our already existing function printableTitle:

.. code-block:: php

    <?php 
        class Book { 
            public $isbn; 
            public $title; 
            public $author;
            public $available; 
            
            public function getPrintableTitle(): string {
                $result = '<i>' . $this->title . '</i> - ' . $this->author; 
                if(!$this->available) { 
                    $result .= ' <b>Not available</b>'; } 
                    return $result; 
            } 
        }

As with properties, we add the keyword public at the beginning of the function, but other than that, the rest looks just as a normal function. The other special bit is the use of $this: it represents the object itself, and allows you to access the properties and methods of that same object. Note how we refer to the title, author, and available properties.

You can also update the values of the current object from one of its functions. Let's use the available property as an integer that shows the number of units available instead of just a Boolean. With that, we can allow multiple customers to borrow different copies of the same book. Let's add a method to give one copy of a book to a customer, updating the number of units available:

.. code-block:: php

    <?php
    public function getCopy(): bool { 
        if ($this->available < 1) { 
            return  false; 
        } 
        else { 
            $this->available--; 
            return true; 
        } 
    }

In this preceding method, we first check if we have at least one available unit. If we do not, we return false to let them know that the operation was not successful. If we do have a unit for the customer, we decrease the number of available units, and then return true, letting them know that the operation was successful. Let's see how you can use this class:

.. code-block:: php

    <?php 
        $book = new Book(); 
        $book->title = "1984"; 
        $book->author = "George Orwell"; 
        $book->isbn = 9785267006323; 
        $book->available = 12;
        if ($book->getCopy()) { 
            echo 'Here, your copy.'; 
        } 
        else { 
            echo 'I am afraid that book is not available.'; 
        }

What would this last piece of code print? Exactly, Here, your copy. But what would be the value of the property available? It would be 11, which is the result of the invocation of getCopy.

Class constructors
------------------

You might have noticed that it looks like a pain to instantiate the Book class, and set all its values each time. What if our class has 30 properties instead of four? Well, hopefully, you will never do that, as it is very bad practice. Still, there is a way to mitigate that pain: constructors.

Constructors are functions that are invoked when someone creates a new instance of the class. They look like normal methods, with the exception that their name is always __construct, and that they do not have a return statement, as they always have to return the new instance. Let's see an example:

.. code-block:: php

    <?php
    public function __construct(int $isbn, string $title, string $author, int $available) {
        $this->isbn = $isbn; 
        $this->title = $title; 
        $this->author = $author; 
        $this->available = $available; 
    }

The constructor takes four arguments, and then assigns the value of one of the arguments to each of the properties of the instance. To instantiate the Book class, we use the following:

.. code-block:: php

    $book = new Book("1984", "George Orwell", 9785267006323, 12);

This object is exactly the same as the object when we set the value to each of its properties manually. But this one looks cleaner, right? This does not mean you cannot set new values to this object manually, it just helps you in constructing new objects.

As a constructor is still a function, it can use default arguments. Imagine that the number of units will usually be 0 when creating the object, and later, the librarian will add units when available. We could set a default value to the $available argument of the constructor, so if we do not send the number of units when creating the object, the object will be instantiated with its default value:

.. code-block:: php

    <?php
    public function __construct(int $isbn, string $title, string $author, int $available = 0 ) { 
        $this->isbn = $isbn; 
        $this->title = $title; 
        $this->author = $author; 
        $this->available = $available; 
    }

We could use the preceding constructor in two different ways:

.. code-block:: php

    <?php
    $book1 = new Book("1984", "George Orwell", 9785267006323, 12);
    $book2 = new Book("1984", "George Orwell", 9785267006323);

$book1 will set the number of units available to 12, whereas $book2 will set it to the default value of 0. But do not trust me; try it by yourself!


Magic methods
-------------

There is a special group of methods that have a different behavior than the normal ones. Those methods are called magic methods, and they usually are triggered by the interaction of the class or object, and not by invocations. You have already seen one of them, the constructor of the class, __construct. This method is not invoked directly, but rather used when creating a new instance with new. You can easily identify magic methods, because they start with __. The following are some of the most used magic methods:

-  __toString: This method is invoked when we try to cast an object to
   a string. It takes no parameters, and it is expected to return a
   string.

-  __call: This is the method that PHP calls when you try to invoke a
   method on a class that does not exist. It gets the name of the method
   as a string and the list of parameters used in the invocation as an
   array, through the argument.

-  __get: This is a version of __call for properties. It gets the
   name of the property that the user was trying to access through
   parameters, and it can return anything.

You could use the __toString method to replace the current getPrintableTitle method in our Book class. To do that, just change the name of the method as follows:

.. code-block:: php

    <?php
    public function __toString() { 
        $result = '<i>' . $this->title . '</i> - ' . $this->author; 
        if (!$this->available) { 
            $result .= ' <b>Not available</b>'; 
        } 
        return $result; 
    }

To try the preceding code, you can just add the following snippet that creates an object book and then casts it to a string, invoking the __toString method:

.. code-block:: php

    $book = new Book(1234, 'title', 'author'); 
    $string = (string) $book; // title - author Not available

As the name suggests, those are magic methods, so most of the time their features will look like magic. For obvious reasons, we personally encourage developers to use constructors and maybe __toString, but be careful about when to use the rest, as you might make your code quite
unpredictable for people not familiar with it.

More on OOP
===========

Properties and methods visibility
---------------------------------

So far, all the properties and methods defined in our Book class were tagged as public. That means that they are accessible to anyone, or more precisely, from anywhere. This is called the visibility of the property or method, and there are three types of visibility. In the order of being more restrictive to less, they are as follows:

-  private: This type allows access only to members of the same class.
   If A and B are instances of the class C, A can access the properties
   and methods of B.

-  protected: This type allows access to members of the same class and
   instances from classes that inherit from that one only. You will see
   inheritance in the next section.

-  public: This type refers to a property or method that is accessible
   from anywhere. Any classes or code in general from outside the class
   can access it.

In order to show some examples, let's first create a second class in our application. Save this into a Customer.php file:

.. code-block:: php

    <?php 
        class Customer { 
            private $id; 
            private $firstname; 
            private $surname; 
            private $email; 
            
            public function __construct( int $id, string $firstname, string $surname, string $email ){
                $this->id = $id; 
                $this->firstname = $firstname; 
                $this->surname = $surname;
                $this->email = $email; 
            } 
        }

This class represents a customer, and its properties consist of the general information that the bookstores usually know about their customers. But for security reasons, we cannot let everybody know about the personal data of our customers, so we set every property as private.

So far, we have been adding the code to create objects in the same Book.php file, but since now we have two classes, it seems natural to leave the classes in their respective files, and create and play with objects in a separate file. Let's name this third file init.php. In order to instantiate objects of a given class, PHP needs to know where the class is. For that, just include the file with require_once.

.. code-block:: php

    <?php 
    require_once __DIR__ . '/Book.php'; 
    require_once __DIR__ . '/Customer.php'; 
    $book1 = new Book("1984", "George Orwell", 9785267006323, 12); 
    $book2 = new Book("To Kill a Mockingbird", "Harper Lee", 9780061120084, 2); 
    $customer1 = new Customer(1, 'John', 'Doe', 'johndoe@mail.com'); 
    $customer2 = new Customer(2, 'Mary', 'Poppins', 'mp@mail.com');

You do not need to include the files every single time. Once you include them, PHP will know where to find the classes, even though your code is in a different file.

.. admonition::  Conventions for classes

    When working with classes, you should know that there are some conventions that everyone tries to follow in order to ensure clean code which is easy to maintain. The most important ones are as follows:

    -  Each class should be in a file named the same as the class along with the .php extension

    -  Class names should be in CamelCase, that is, each word should start with an uppercase letter, followed by the rest of the word in lowercase

    -  A file should contain only the code of one class

    -  Inside a class, you should first place the properties, then the constructor, and finally, the rest of the methods

To show how visibility works, let's try the following code:

.. code-block:: php

    <?php
        $book1->available = 2; // OK 
        $customer1->id = 3; // Error!

We already know that the properties of the Book class' objects are public, and therefore, editable from outside. But when trying to change a value from Customer, PHP complains, as its properties are private.


Encapsulation
-------------

.. sidebar:: Properties y/no

    https://stackoverflow.com/questions/4478661/getter-and-setter


When working with objects, one of the most important concepts you have to know and apply is encapsulation. Encapsulation tries to group the data of the object with its methods in an attempt to hide the internal structure of the object from the rest of the world. In simple words, you could say that you use encapsulation if the properties of an object are private, and the only way to update them is through public methods.

The reason for using encapsulation is to make it easier for a developer to make changes to the internal structure of the class without directly affecting the external code that uses that class. For example, imagine that our Customer class, that now has two properties to define its name—firstname and surname—has to change. From now on, we only have one property name that contains both. If we were accessing its properties straightaway, we should change all of those accesses!

Instead, if we set the properties as private and enable two public methods, getFirstname and getSurname, even if we have to change the internal structure of the class, we could just change the implementation of those two methods—which is at one place only—and the rest of the code that uses our class will not be affected at all. This concept is also known as information hiding.

The easiest way to implement this idea is by setting all the properties of the class as private and enabling two methods for each of the properties: one will get the current value (also known as getter), and the other will allow you to set a new value (known as setter). That's at least the most common and easy way to encapsulate data.

But let's go one step further: when defining a class, think of the data
that you want the user to be able to change and to retrieve, and only
add setters and getters for them. For example, customers might change
their e-mail address, but their name, surname, and ID remains the same
once we create them. The new definition of the class would look like the
following:

.. code-block:: php

    <?php 

        class Customer { 
            private $id; 
            private $name; 
            private $surname;
            private $email; 
            
            public function __construct( int $id, string $firstname, string $surname, string $email ) { 
                $this->id = $id;
                $this->firstname = $firstname; 
                $this->surname = $surname;
                $this->email = $email; 
            } 
            
            public function getId(): id { 
                return $this->id; 
            } 
            
            public function getFirstname(): string { 
                return $this->firstname; 
            } 
            
            public function getSurname(): string { 
                return $this->surname; 
            } 
            
            public function getEmail(): string { 
                return $this->email; 
            } 
            
            public function setEmail(string $email) {
                $this->email = $email; } }

On the other hand, our books also remain almost the same. The only change possible is the number of available units. But we usually take or add one book at a time instead of setting the specific number of units available, so a setter here is not really useful. We already have the getCopy method that takes one copy when possible; let's add an addCopy method, plus the rest of the getters:

.. code-block:: php

    <?php 
    
    class Book { 
        private $isbn; 
        private $title; 
        private $author;
        private $available; 
        
        public function __construct( int $isbn, string $title, string $author, int $available = 0 ) { 
            $this->isbn = $isbn;
            $this->title = $title; 
            $this->author = $author; 
            $this->available = $available; 
        } 
        
        public function getIsbn(): int { 
            return $this->isbn; 
        }
        
        public function getTitle(): string { 
            return $this->title; 
        } 
        
        public function getAuthor(): string { 
            return $this->author; 
        } 
        
        public function isAvailable(): bool { 
            return $this->available; 
        } 
        
        public function getPrintableTitle(): string { 
            $result = '<i>' . $this->title . '</i> - ' . $this->author; 
            if (!$this->available) {
                $result .= ' <b>Not available</b>'; 
            } 
            return $result; 
        } 
        
        public function getCopy(): bool { 
            if ($this->available < 1) { 
                return false;
            } 
            else { 
                $this->available--; return true; 
            } 
        } 
        
        public function addCopy() { 
            $this->available++; 
        } 
    }

When the number of classes in your application, and with it, the number of relationships between classes increases, it is helpful to represent these classes in a diagram. Let's call this diagram a UML diagram of classes, or just an hierarchic tree. The hierarchic tree for our two classes would look as follows:

|image16|

We only show public methods, as the protected or private ones cannot be called from outside the class, and thus, they are not useful for a developer who just wants to use these classes externally.


Static properties and methods
-----------------------------

So far, all the properties and methods were linked to a specific instance; so two different instances could have two different values for the same property. PHP allows you to have properties and methods linked to the class itself rather than to the object. These properties and methods are defined with the keyword static.

.. code-block:: php

    <?php
    private static $lastId = 0;

Add the preceding property to the Customer class. This property shows the last ID assigned to a user, and is useful in order to know the ID that should be assigned to a new user. Let's change the constructor of our class as follows:

.. code-block:: php

    <?php
    public function __construct( int $id, string $name, string $surname, string $email ) {
        if ($id == null) { 
            $this->id = ++self::$lastId; 
        } 
        else { 
            $this->id = $id; 
            if ($id > self::$lastId){ 
                self::$lastId = $id; 
            } 
        } 
        $this->name = $name; $this->surname = $surname; $this->email = $email; 
    }

Note that when referring to a static property, we do not use the variable $this. Instead, we use self::, which is not tied to any instance but to the class itself. In this last constructor, we have two options. We are either provided with an ID value that is not null, or we send a null in its place. When the received ID is null, we use the static property $lastId to know the last ID used, increase it by one, and assign it to the property $id. If the last ID we inserted was 5, this will update the static property to 6, and then assign it to the instance property. Next time we create a new customer, the $lastId static property will be 6. Instead, if we get a valid ID as part of the arguments, we assign it, and check if the assigned $id is greater than the static $lastId. If it is, we update it. Let's see how we would use this:

.. code-block:: php

    <?php
    $customer1 = new Customer(3, 'John', 'Doe', 'johndoe@mail.com');
    $customer2 = new Customer(null, 'Mary', 'Poppins', 'mp@mail.com');
    $customer3 = new Customer(7, 'James', 'Bond', '007@mail.com');

In the preceding example, $customer1 specifies that his ID is 3, probably because he is an existing customer and wants to keep the same ID. That sets both his ID and the last static ID to 3. When creating the second customer, we do not specify the ID, so the constructor will take the last ID, increase it by 1, and assign it to the customer. So $customer2 will have the ID 4, and the latest ID will be 4 too. Finally, our secret agent knows what he wants, so he forces the system to have the ID as 7. The latest ID will be updated to 7 too.

Another benefit of static properties and methods is that we do not need an object to use them. You can refer to a static property or method by specifying the name of the class, followed by ::, and the name of the property/method. That is, of course, if the visibility rules allow you to do that, which, in this case, it does not, as the property is private. Let's add a public static method to retrieve the last ID:

.. code-block:: php

    <?php
    public static function getLastId(): int { return self::$lastId; }

You can reference it either using the class name or an existing instance, from anywhere in the code:

.. code-block:: php

    <?php
    Customer::getLastId(); $customer1::getLastId();


Namespaces
----------

You know that you cannot have two classes with the same name, since PHP would not know which one is being referred to when creating a new object. To solve this issue, PHP allows the use of namespaces, which act as paths in a filesystem. In this way, you can have as many classes with the same name as you need, as long as they are all defined in different namespaces. It is worth noting that, even though namespaces and the file path will usually be the same, this is enforced by the developer rather than by the language; you could actually use any namespace that has nothing to do with the filesystem.

Specifying a namespace has to be the first thing that you do in a file. In order to do that, use the namespace keyword followed by the namespace. Each section of the namespace is separated by \, as if it was a different directory. If you do not specify the namespace, the
class will belong to the base namespace, or root. At the beginning of both files—Book.php and Customer.php—add the following:

.. code-block:: php

    <?php 
    namespace Bookstore\Domain;

The preceding line of code sets the namespace of our classes as Bookstore\\Domain. The full name of our classes then is Bookstore\Domain\Book and Bookstore\Domain\Customer. If you try to access the init.php file from your browser, you will see an error saying that either the class Book or the class Customer were not found. But we included the files, right? That happens because PHP thinks that you are trying to access \Book and \Customer from the root. Do not worry, there are several ways to amend this.

One way would be to specify the full name of the classes when referencing them, that is, using $customer = new Bookstore\Domain\Book(); instead of $book = new Book();. 
But that does not sound practical, does it?

Another way would be to say that the init.php file belongs to the BookStore\Domain namespace. That means that all the references to classes inside init.php will have the BookStore\Domain prefixed to them, and you will be able to use Book and Customer. The downside of this solution is that you cannot easily reference other classes from
other namespaces, as any reference to a class will be prefixed with that
namespace.

The best solution is to use the keyword **use**. This keyword allows you to specify a full class name at the beginning of the file, and then use the simple name of the class in the rest of that file. Let's see an example:

.. code-block:: php

    <?php 
        use Bookstore\Domain\Book; 
        use Bookstore\Domain\Customer;
        require_once __DIR__ . '/Book.php'; 
        require_once __DIR__ . '/Customer.php'; 
        //...

In the preceding file, each time that we reference Book or Customer, PHP will know that we actually want to use the full class name, that is, with Bookstore\\Domain\\ prefixed to it. This solution allows you to have a clean code when referencing those classes, and at the same time, to be able to reference classes from other namespaces if needed.

But what if you want to include two different classes with the same name in the same file? If you set two use statements, PHP will not know which one to choose, so we still have the same problem as before! To fix that, either you use the full class name—with namespace—each time you want toreference any of the classes, or you use aliases.

Imagine that we have two Book classes, the first one in the namespace Bookstore\Domain and the second one in Library\Domain. To solve the conflict, you could do as follows:

.. code-block:: php

    <?php
    use Bookstore\Domain\Book; 
    use Library\Domain\Book as LibraryBook;

The keyword as sets an alias to that class. In that file, whenever you reference the class LibraryBook, you will actually be referencing the class Library\Domain\Book. And when referencing Book, PHP will just use the one from Bookstore. Problem solved!

Autoloading classes
-------------------

As you already know, in order to use a class, you need to include the file that defines it. So far, we have been including the files manually, as we only had a couple of classes and used them in one file. But what happens when we use several classes in several files? There must be a smarter way, right? Indeed there is. Autoloading to the rescue!

Autoloading is a PHP feature that allows your program to search and load files automatically given some set of predefined rules. Each time you reference a class that PHP does not know about, it will ask the autoloader. If the autoloader can figure out which file that class is in, it will load it, and the execution of the program will continue as normal. If it does not, PHP will stop the execution.

So, what is the autoloader? It is no more than a PHP function that gets a class name as a parameter, and it is expected to load a file. There are two ways of implementing an autoloader: either by using the __autoload function or the spl_autoload_register one.

Using the __autoload function
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Defining a function named __autoload tells PHP that the function is the autoloader that it must use. You could implement an easy solution:

.. code-block:: php

    <?php
    function __autoload($classname) { 
        $lastSlash = strpos($classname, '\\') + 1; 
        $classname = substr($classname, $lastSlash); 
        $directory = str_replace('\\', '/', $classname); 
        $filename = __DIR__ .'/' . $directory . '.php'; 
        require_once($filename); }

Our intention is to keep all PHP files in src, that is, the source. Inside this directory, the directory tree will emulate the namespace tree of the classes excluding the first section BookStore, which is useful as a namespace but not necessary as a directory. That means that our Book class, with full class name BookStore\Domain\Book, will be in src/Domain/Book.php.

In order to achieve that, our __autoload function tries to find the first occurrence of the backslash \ with strpos, and then extracts from that position until the end with substr. This, in practice, just removes the first section of the namespace, BookStore. After that, we replace all \ by / so that the filesystem can understand the path. Finally, we concatenate the current directory, the class name as a directory, and the .php extension.

Before trying that, remember to create the src/Domain directory and move the two classes inside it. Also, to make sure that we are testing the autoloader, save the following as your init.php, and go to http://localhost:8000/init.php:

.. code-block:: php

    <?php 
        use Bookstore\Domain\Book; 
        use Bookstore\Domain\Customer;

        function __autoload($classname) { 
            $lastSlash = strpos($classname, '\\') + 1; 
            $classname = substr($classname, $lastSlash); 
            $directory = str_replace('\\', '/', $classname); 
            $filename = __DIR__ .'/src/' . $directory . '.php'; 
            require_once($filename); 
        } 
        
        $book1 = new Book("1984", "George Orwell", 9785267006323, 12); $customer1 = new Customer(5, 'John', 'Doe', 'johndoe@mail.com');

The browser does not complain now, and there is no explicit require_once. Also remember that the __autoload function has to be defined only once, not in each file. So from now on, when you want touse your classes, as soon as the class is in a namespace and file that follows the convention, you only need to define the use statement. Way cleaner than before, right?

Using the spl_autoload_register function
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The __autoload solution looks pretty good, but it has a small problem: what if our code is so complex that we do not have only one convention, and we need more than one implementation of the __autoload function? As we cannot define two functions with the same name, we need a way to tell PHP to keep a list of possible implementations of the autoloader, so it can try all of them until one works.

That is the job of spl_autoload_register. You define your autoloader function with a valid name, and then invoke the function spl_autoload_register, sending the name of your autoloader as an argument. You can call this function as many times as the different autoloaders you have in your code. In fact, even if you have only one autoloader, using this system is still a better option than the __autoload one, as you make it easier for someone else who has to add a new autoloader later:

.. code-block:: php

    <?php
    function autoloader($classname) { 
        $lastSlash = strpos($classname, '\\') + 1; 
        $classname = substr($classname, $lastSlash); 
        $directory = str\_replace('\\', '/', $classname); 
        $filename = __DIR__ . '/' . $directory . '.php'; 
        require\_once($filename); }
        spl_autoload_register('autoloader');


Inheritance
===========

We have presented the object-oriented paradigm as the panacea for complex data structures, and even though we have shown that we can define objects with properties and methods, and it looks pretty and fancy, it is not something that we could not solve with arrays. Encapsulation was one feature that made objects more useful than arrays, but their true power lies in inheritance.


Introducing inheritance
-----------------------

Inheritance in OOP is the ability to pass the implementation of the class from parents to children. Yes, classes can have parents, and the technical way of referring to this feature is that a class *extends* from another class. When extending a class, we get all the properties and methods that are not defined as private, and the child class can use them as if they were its own. The limitation is that a class can only extend from one parent.

To show an example, let's consider our Customer class. It contains the properties firstname, surname, email, and id. A customer is actually a specific type of person, one that is registered in our system, so he/she can get books. But there can be other types of persons in our system, like librarian or guest. And all of them would have some common properties to all people, that is, firstname and surname. So it would make sense if we create a Person class, and make the Customer class extend from it. The hierarchic tree would look as follows:

|image17|

Note how Customer is connected to Person. The methods in Person are not defined in Customer, as they are implicit from the extension. Now save the new class in src/Domain/Person.php, following our convention:

.. code-block:: php

    <?php 
        namespace Bookstore\Domain; 
        class Person { 
            protected $firstname; 
            protected $surname; 
            
            public function __construct(string $firstname, string $surname) {
            $this->firstname = $firstname;
            $this->surname = $surname; 
        } 
        
        public function getFirstname(): string { 
            return $this->firstname; 
        } 
        
        public function getSurname(): string {
            return $this->surname; 
        } 
    }

The class defined in the preceding code snippet does not look special; we have just defined two properties, a constructor and two getters. Note though that we defined the properties as protected, because if we defined them as private, the children would not be able to access them. Now we can update our Customer class by removing the duplicate properties and its getters:

.. code-block:: php

    <?php 
        namespace Bookstore\Domain; 
        
        class Customer extends Person {
            private static $lastId = 0; 
            private $id; 
            private $email; 
           
            public function __construct( int $id, string $name, string $surname, string $email ) {
                if (empty($id)) { 
                    $this->id = ++self::$lastId; 
                }
                else { 
                    $this->id = $id; 
                    if ($id > self::$lastId) { 
                        self::$lastId = $id; 
                    } 
                } 
                $this->name = $name; 
                $this->surname = $surname;
                $this->email = $email; 
            } 
            
            public static function getLastId(): int {
                return self::$lastId; 
            } 
            
            public function getId(): int { 
                return $this->id; 
            } 
            
            public function getEmail(): string { 
                return $this->email; 
            } 
            
            public function setEmail($email): string {
                $this->email = $email; 
            } 
        }

Note the new keyword extends; it tells PHP that this class is a child of the Person class. As both Person and Customer are in the same namespace, you do not have to add any use statement, but if they were not, you should let it know how to find the parent. This code works fine, but we can see that there is a bit of duplication of code. The constructor of the Customer class is doing the same job as the constructor of the Person class! We will try to fix it really soon.

In order to reference a method or property of the parent class from the child, you can use $this as if the property or method was in the same class. In fact, you could say it actually is. But PHP allows you to redefine a method in the child class that was already present in the parent. If you want to reference the parent's implementation, you cannot use $this, as PHP will invoke the one in the child. To force PHP to use the parent's method, use the keyword parent:: instead of $this. Update the constructor of the Customer class as follows:

.. code-block:: php

    <?php
    public function __construct(int $id, string $firstname, string
    $surname, string $email ) { 
        parent::__construct($firstname, $surname); 
        if (empty($id)) { 
            $this->id = ++self::$lastId; 
        } 
        else {
            $this->id = $id; 
            if ($id > self::$lastId) {
                self::$lastId = $id; 
            } 
        }
        $this->email = $email; 
    }

This new constructor does not duplicate code. Instead, it calls the constructor of the parent class Person, sending $firstname and $surname, and letting the parent do what it already knows how to do. We avoid code duplication and, on top of that, we make it easier for any future changes to be made in the constructor of Person. If we need to change the implementation of the constructor of Person, we will change it in one place only, instead of in all the children.


Overriding methods
------------------

As said before, when extending from a class, we get all the methods of the parent class. That is implicit, so they are not actually written down inside the child's class. What would happen if you implement another method with the same signature and/or name? You will be *overriding the method*.

As we do not need this feature in our classes, let's just add some code in our init.php file to show this behavior, and then you can just remove it. Let's define a class Pops, a class Child that extends from the parent, and a sayHi method in both of them:

.. code-block:: php

    <?php
    class Pops { 
        public function sayHi() { 
            echo "Hi, I am pops."; 
        } 
    }
    
    class Child extends Pops{ 
        public function sayHi() { 
            echo "Hi, I am a child."; 
        } 
    } 
    
    $pops = new Pops(); 
    $child = new Child(); 
    echo $pops->sayHi(); // Hi, I am pops. 
    echo $child->sayHi(); // Hi, I am Child.

The highlighted code shows you that the method has been overridden, so when invoking it from a child's point of view, we will be using it rather than the one inherited from its father. But what happens if we want to reference the inherited one too? You can always reference it with the keyword parent. Let's see how it works:

.. code-block:: php

    <?php
    class Child extends Pops{ 
        public function sayHi() {
            echo "Hi, I am a child."; 
            parent::sayHi(); 
        } 
    } 
    
    $child = new Child(); 
    echo $child->sayHi(); // Hi, I am Child. Hi I am pops.

Now the child is saying hi for both himself and his father. It seems very easy and handy, right? Well, there is a restriction. Imagine that, as in real life, the child was very shy, and he would not say hi to everybody. We could try to set the visibility of the method as protected, but see what happens:

.. code-block:: php

    <?php
    class Child extends Pops{ 
        protected function sayHi() { 
            echo "Hi, I am a child."; 
        } 
    }

When trying this code, even without trying to instantiate it, you will get a fatal error complaining about the access level of that method. The reason is that when overriding, the method has to have at least as much visibility as the one inherited. That means that if we inherit a protected one, we can override it with another protected or a public one, but never with a private one.

Abstract classes
----------------

Remember that you can extend only from one parent class each time. That means that Customer can only extend from Person. But if we want to make this hierarchic tree more complex, we can create children classes that extend from Customer, and those classes will extend implicitly from Person too. Let's create two types of customer: basic and premium. These two customers will have the same properties and methods from Customer and from Person, plus the new ones that we implement in each one of them.

Save the following code as src/Domain/Customer/Basic.php:

.. code-block:: php

    <?php 
    namespace Bookstore\Domain\Customer; 
    use Bookstore\Domain\Customer; 
    
    class Basic extends Customer { 
    
        public function getMonthlyFee(): float { 
            return 5.0; 
        } 
        
        public function getAmountToBorrow(): int { 
            return 3; 
        } 
        
        public function getType(): string { 
            return 'Basic'; 
        } 
    }

And the following code as src/Domain/Customer/Premium.php:

.. code-block:: php

    <?php 
    namespace Bookstore\Domain\Customer; 
    use Bookstore\Domain\Customer; 
    
    class Premium extends Customer { 
    
        public function getMonthlyFee(): float {
            return 10.0; 
        } 
        
        public function getAmountToBorrow(): int { 
            return 10; 
        } 
        public function getType(): string { 
            return 'Premium'; 
        } 
    }

Things to note in the preceding two codes are that we extend from Customer in two different classes, and it is perfectly legal— we can extend from classes in different namespaces. With this addition, the hierarchic tree for Person would look as follows:

|image18|

We define the same methods in these two classes, but their implementations are different. The aim of this approach is to use both types of customers indistinctively, without knowing which one it is each time. For example, we could temporally have the following code in our init.php. Remember to add the use statement to import the class Customer if you do not have it.

.. code-block:: php

    <?php
    function checkIfValid(Customer $customer, array $books): bool {
        return $customer->getAmountToBorrow() >= count($books); 
    }

The preceding function would tell us if a given customer could borrow all the books in the array. Notice that the type hinting of the method says Customer, without specifying which one. This will accept objects that are instances of Customer or any class that extends from Customer, that is, Basic or Premium. Looks legit, right? Let's try to use it then:

.. code-block:: php

    <?php
    $customer1 = new Basic(5, 'John', 'Doe', 'johndoe@mail.com');
    var_dump(checkIfValid($customer1, [$book1])); // ok 
    $customer2 = new Customer(7, 'James', 'Bond', 'james@bond.com');
    var_dump(checkIfValid($customer2, [$book1])); // fails

The first invocation works as expected, but the second one fails, even though we are sending a Customer object. The problem arises because the parent does not know about any getAmountToBorrow method! It also looks dangerous that we rely on the children to always implement that method. The solution lies in using abstract classes.

An abstract class is a class that cannot be instantiated. Its sole purpose is to make sure that its children are correctly implemented. Declaring a class as abstract is done with the keyword abstract, followed by the definition of a normal class. We can also specify the methods that the children are forced to implement, without implementing them in the parent class. Those methods are called abstract methods, and are defined with the keyword abstract at the beginning. Of course, the rest of the normal methods can stay there too, and will be inherited by its children:

.. code-block:: php

    <?php 
    abstract class Customer extends Person { 
    //... 
    abstract public function getMonthlyFee(); 
    abstract public function getAmountToBorrow(); 
    abstract public function getType(); 
    //... 
    }

The preceding abstraction solves both problems. First, we will not be able to send any instance of the class Customer, because we cannot instantiate it. That means that all the objects that the checkIfValid method is going to accept are only the children from Customer. On the other hand, declaring abstract methods forces all the children that extend the class to implement them. With that, we make sure that all objects will implement getAmountToBorrow, and our code is safe.

The new hierarchic tree will define the three abstract methods in Customer, and will omit them for its children. It is true that we are implementing them in the children, but as they are enforced by Customer, and thanks to abstraction, we are sure that all classes extending from it will have to implement them, and that it is safe to do so. Let's see how this is done:

|image19|

With the last new addition, your init.php file should fail. The reason is that it is trying to instantiate the class Customer, but now it is abstract, so you cannot. Instantiate a concrete class, that is, one that is not abstract, to solve the problem.


OOP 3
=====

Interfaces
----------

An interface is an OOP element that groups a set of function declarations without implementing them, that is, it specifies the name, return type, and arguments, but not the block of code. Interfaces are different from abstract classes, since they cannot contain any implementation at all, whereas abstract classes could mix both method definitions and implemented ones. The purpose of interfaces is to state what a class can do, but not how it is done.

From our code, we can identify a potential usage of interfaces. Customers have an expected behavior, but its implementation changes depending on the type of customer. So, Customer could be an interface instead of an abstract class. But as an interface cannot implement any function, nor can it contain properties, we will have to move the concrete code from the Customer class to somewhere else. For now, let's move it up to the Person class. Edit the Person class as shown:

.. code-block:: php

    <?php 
    namespace Bookstore\Domain; 
    class Person { 
        private static
        $lastId = 0; 
        protected $id; 
        protected $firstname; 
        protected $surname; 
        protected $email; 
        
        public function __construct( int $id, string $firstname, string $surname, string $email ) {
            $this->firstname = $firstname; 
            $this->surname = $surname;
            $this->email = $email; 
            if (empty($id)) { 
                $this->id = ++self::$lastId; 
            } 
            else { 
                $this->id = $id; 
                if ($id > self::$lastId) { 
                    self::$lastId = $id; 
                } 
            } 
        } 
        
        public function getFirstname(): string { 
            return $this->firstname; 
        } 
        
        public function getSurname(): string {
            return $this->surname; 
        } 
        
        public static function getLastId(): int {
            return self::$lastId; 
        } 
        
        public function getId(): int { 
            return $this->id; 
        } 
        
        public function getEmail(): string { 
            return $this->email; 
        } 
    }

.. admonition::  Complicating things more than necessary

    Interfaces are very useful, but there is always a place and a time for everything. As our application is very simple due to its didactic nature, there is no real place for them. The abstract class already defined in the previous section is the best approach for our scenario. But just for the sake of showing how interfaces work, we will be adapting our code to them.

Do not worry though, as most of the code that we are going to introduce now will be replaced by better practices once we introduce databases and the MVC pattern in `*Chapter 5* <#Top_of_ch05_html>`__, *Using Databases*, and `*Chapter 6* <#Top_of_ch06_html>`__, *Adapting to MVC*.

When writing your own applications, do not try to complicate things more than necessary. It is a common pattern to see very complex code from developers that try to show up all the skills they have in a very simple scenario. Use only the necessary tools to leave clean code that is easy to maintain, and of course, that works as expected.

Change the content of Customer.php with the following:

.. code-block:: php

    <?php 
        namespace Bookstore\Domain; 
        interface Customer { 
            public function getMonthlyFee(): float; 
            public function getAmountToBorrow(): int; 
            public function getType(): string; 
        }

Note that an interface is very similar to an abstract class. The differences are that it is defined with the keyword interface, and that its methods do not have the word abstract. Interfaces cannot be instantiated, since their methods are not implemented as with abstract classes. The only thing you can do with them is make a class to implement them.

*Implementing* an interface means implementing all the methods defined in it, like when we extended an abstract class. It has all the benefits of the extension of abstract classes, such as belonging to that type—useful when type hinting. From the developer's point of view, using a class that implements an interface is like writing a contract: you ensure that your class will always have the methods declared in the interface, regardless of the implementation. Because of that, interfaces only care about public methods, which are the ones that other developers can use. The only change you need to make in your code is to replace the keywords extends by implements:

.. code-block:: php

    <?php
    class Basic implements Customer {
    
    }

So, why would someone use an interface if we could always use an abstract class that not only enforces the implementation of methods, but also allows inheriting code as well? The reason is that you can only extend from one class, but you can implement multiple instances at the same time. Imagine that you had another interface that defined payers. This could identify someone that has the ability to pay something, regardless of what it is. Save the following code in src/Domain/Payer.php:

.. code-block:: php

    <?php 
        namespace Bookstore\Domain; 
        interface Payer { 
            public function pay(float $amount); 
            public function isExtentOfTaxes(): bool; 
        }

Now our basic and premium customers can implement both the interfaces. The basic customer will look like the following:

.. code-block:: php

    <?php
    //... 
    use Bookstore\Domain\Customer; 
    use Bookstore\Domain\Person; 
    
    class Basic extends Person implements Customer { 
        public function getMonthlyFee(): float { 
        //...

And the premium customer will change in the same way:

.. code-block:: php

    <?php
    //... 
    use Bookstore\Domain\Customer; 
    use Bookstore\Domain\Person; 
    
    class Premium extends Person implements Customer { 
        public function getMonthlyFee(): float { 
    //...

You should see that this code would no longer work. The reason is that although we implement a second interface, the methods are not implemented. Add these two methods to the basic customer class:

.. code-block:: php

    <?php
    public function pay(float $amount) { echo "Paying $amount."; }
    public function isExtentOfTaxes(): bool { return false; }

Add these two methods to the premium customer class:

.. code-block:: php

    <?php
    public function pay(float $amount) { echo "Paying $amount."; }
    public function isExtentOfTaxes(): bool { return true; }

If you know that *all* customers will have to be payers, you could even
make the Customer interface to inherit from the Payer interface:

.. code-block:: php

    <?php
    interface Customer extends Payer {

This change does not affect the usage of our classes at all. Other developers will see that our basic and premium customers inherit from Payer and Customer, and so they contain all the necessary methods. That these interfaces are independent, or they extend from each other is something that will not affect too much.

Interfaces can only extend from other interfaces, and classes can only extend from other classes. The only way to mix them is when a class implements an interface, but neither does a class extend from an interface, nor does an interface extend from a class. But from the point of view of type hinting, they can be used interchangeably.

To summarize this section and make things clear, let's show what the hierarchic tree looks like after all the new additions. As in abstract classes, the methods declared in an interface are shown in the interface rather than in each of the classes that implement it.

|image20|


Polymorphism
------------

Polymorphism is an OOP feature that allows us to work with different classes that implement the same interface. It is one of the beauties of object-oriented programming. It allows the developer to create a complex system of classes and hierarchic trees, but offers a simple way of working with them.

Imagine that we have a function that, given a payer, checks whether it is exempt of taxes or not, and makes it pay some amount of money. This piece of code does not really mind if the payer is a customer, a librarian, or someone who has nothing to do with the bookstore. The only thing that it cares about is that the payer has the ability to pay. The function could be as follows:

.. code-block:: php

    <?php
    function processPayment(Payer $payer, float $amount) { 
        if($payer->isExtentOfTaxes()) { 
            echo "What a lucky one..."; 
        } 
        else {
            $amount *= 1.16; 
        } 
        $payer->pay($amount); 
    }

You could send basic or premium customers to this function, and the behavior will be different. But, as both implement the Payer interface, both objects provided are valid types, and both are capable of performing the actions needed.

The checkIfValid function takes a customer and a list of books. We already saw that sending any kind of customer makes the function work as expected. But what happens if we send an object of the class Librarian, which extends from Payer? As Payer does not know about Customer (it is rather the other way around), the function will complain as the type hinting is not accomplished.

One useful feature that comes with PHP is the ability to check whether an object is an instance of a specific class or interface. The way to use it is to specify the variable followed by the keyword instanceof and the name of the class or interface. It returns a Boolean, which is true if the object is from a class that extends or implements the specified one, or false otherwise. Let's see some examples:

.. code-block:: php

    <?php
    $basic = new Basic(1, "name", "surname", "email"); 
    $premium = new Premium(2, "name", "surname", "email"); 
    var_dump($basic instanceof Basic);     // true 
    var_dump($basic instanceof Premium);   // false
    var_dump($premium instanceof Basic);   // false 
    var_dump($premium instanceof Premium); // true 
    var_dump($basic instanceof Customer);  // true 
    var_dump($basic instanceof Person);    // true
    var_dump($basic instanceof Payer);     // true

Remember to add all the use statements for each of the class or interface, otherwise PHP will understand that the specified class name
is inside the namespace of the file.


Traits
------

So far, you have learned that extending from classes allows you to inherit code (properties and method implementations), but it has the limitation of extending only from one class each time. On the other hand, you can use interfaces to implement multiple behaviors from the same class, but you cannot inherit code in this way. To fill this gap, that is, to be able to inherit code from multiple places, you have traits.

Traits are mechanisms that allow you to reuse code, "inheriting", or rather copy-pasting code, from multiple sources at the same time. Traits, as abstract classes or interfaces, cannot be instantiated; they are just containers of functionality that can be used from other classes.

If you remember, we have some code in the Person class that manages the assignment of IDs. This code is not really part of a person, but rather part of an ID system that could be used by some other entity that has to be identified with IDs too. One way to extract this functionality from Person - and we are not saying that it is the best way to do so, but for the sake of seeing traits in action, we choose this one - is to move it to a trait.

To define a trait, do as if you were defining a class, just use the keyword trait instead of class. Define its namespace, add the use statements needed, declare its properties and implement its methods, and place everything in a file that follows the same conventions. Add the following code to the src/Utils/Unique.php file:

.. code-block:: php

    <?php 
        namespace Bookstore\Utils; 
        trait Unique { 
            private static $lastId = 0; 
            protected $id; 
            public function setId(int $id) { 
                if(empty($id)) { 
                    $this->id = ++self::$lastId; 
                } 
                else { 
                    $this->id = $id; 
                    if ($id > self::$lastId) { 
                        self::$lastId = $id; 
                    } 
                } 
            } 
            
            public static function getLastId(): int { 
                return self::$lastId; 
            } 
            
            public function getId(): int { 
                return $this->id; 
            } 
        }

Observe that the namespace is not the same as usual, since we are storing this code in a different file. This is a matter of conventions, but you are entirely free to use the file structure that you consider better for each case. In this case, we do not think that this trait represents "business logic" like customers and books do; instead, it represents a utility for managing the assignment of IDs.

We include all the code related to IDs from Person. That includes the properties, the getters, and the code inside the constructor. As the trait cannot be instantiated, we cannot add a constructor. Instead, we added a setId method that contains the code. When constructing a new instance that uses this trait, we can invoke this setId method to set the ID based on what the user sends as an argument.

The class Person will have to change too. We have to remove all references to IDs and we will have to define somehow that the class is using the trait. To do that, we use the keyword use, like in namespaces, but inside the class. Let's see what it would look like:

.. code-block:: php

    <?php 
        namespace Bookstore\Domain; 
        use Bookstore\Utils\Unique;

        class Person { 
            use Unique; 
            protected $firstname; 
            protected $surname;
            protected $email; 
            
            public function __construct( int $id, string $firstname, string $surname, string $email ) { 
                $this->firstname = $firstname; 
                $this->surname = $surname; 
                $this->email = $email;
                $this->setId($id); 
            } 
            
            public function getFirstname(): string { 
                return $this->firstname; 
            } 
            
            public function getSurname(): string { 
                return $this->surname; 
            } 
            
            public function getEmail(): string { 
                return $this->email; 
            } 
            
            public function setEmail(string $email) {
                $this->email = $email; 
            } 
        }

We add the use Unique; statement to let the class know that it is using the trait. We remove everything related to IDs, even inside the constructor. We still get an ID as the first argument of the constructor, but we ask the method setId from the trait to do everything for us. Note that we refer to that method with $this, as if the method was inside the class. The updated hierarchic tree would look like the following (note that we are not adding all the methods for all the classes or interfaces that are not involved in the recent changes in order to keep the diagram as small and readable as possible):

|image21|

Let's see how it works, even though it does so in the way that you probably expect. Add this code into your init.php file, include the necessary use statements, and execute it in your browser:

.. code-block:: php

    <?php
    $basic1 = new Basic(1, "name", "surname", "email"); 
    $basic2 = new Basic(null, "name", "surname", "email");
    var_dump($basic1->getId()); // 1 
    var_dump($basic2->getId()); // 2

The preceding code instantiates two customers. The first of them has a specific ID, whereas the second one lets the system choose an ID for it. The result is that the second basic customer has the ID 2. That is to be expected, as both customers are basic. But what would happen if the customers are of different types?

.. code-block:: php

    <?php
    $basic = new Basic(1, "name", "surname", "email"); 
    $premium = new Premium(null, "name", "surname", "email");
    var_dump($basic->getId());   // 1 
    var_dump($premium->getId()); // 2

The IDs are still the same. That is to be expected, as the trait is included in the Person class, so the static property $lastId will be shared across all the instances of the class Person, including Basic and Premium customers. If you used the trait from Basic and Premium customer instead of Person (but you should not), you would have the following result:

.. code-block:: php

    <?php
    var_dump($basic->getId());   // 1 
    var_dump($premium->getId()); // 1

Each class will have its own static property. All Basic instances will share the same $lastId, different from the $lastId of Premium instances. This should make clear that the static members in traits are linked to whichever class uses them, rather than the trait itself. That could also be reflected on testing the following code which uses our original scenario where the trait is used from Person:

.. code-block:: php

    <?php
    $basic = new Basic(1, "name", "surname", "email"); 
    $premium = new Premium(null, "name", "surname", "email");
    var_dump(Person::getLastId());   // 2 
    var_dump(Unique::getLastId());   // 0 
    var_dump(Basic::getLastId());    // 2
    var_dump(Premium::getLastId());  // 2

If you have a good eye for problems, you might start thinking about some potential issues around the usage of traits. What happens if we use two traits that contain the same method? Or what happens if you use a trait that contains a method that is already implemented in that class?

Ideally, you should avoid running into these kinds of situations; they are warning lights for possible bad design. But as there will always be extraordinary cases, let's see some isolated examples on how they would behave.

The scenario where the trait and the class implement the same method is easy. The method implemented explicitly in the class is the one with more precedence, followed by the method implemented in the trait, and finally, the method inherited from the parent class. Let's see how it works. Take for example the following trait and class definitions:

.. code-block:: php

    <?php 
        trait Contract { 
            public function sign() { 
                echo "Signing the contract."; 
            } 
        } 
        
        class Manager { 
            use Contract; 
            public function sign(){ 
                echo "Signing a new player."; 
            } 
        }

Both implement the sign method, which means that we have to apply the precedence rules defined previously. The method defined in the class takes precedence over the one from the trait, so in this case, the executed method will be the one from the class:

.. code-block:: php

    <?php
    $manager = new Manager(); 
    $manager->sign();           // Signing a new player.

The most complicated scenario would be one where a class uses two traits with the same method. There are no rules that solve the conflict automatically, so you have to solve it explicitly. Check the following code:

.. code-block:: php

    <?php 
        trait Contract { 
            public function sign() { 
                echo "Signing the contract."; 
            } 
        } 
        
        trait Communicator { 
            public function sign() { 
                echo "Signing to the waitress."; 
            } 
        } 
        
        class Manager { 
            use Contract, Communicator; 
        
        } 
        $manager = new Manager(); 
        $manager->sign();

The preceding code throws a fatal error, as both traits implement the same method. To choose the one you want to use, you have to use the operator insteadof. To use it, state the trait name and the method that you want to use, followed by insteadof and the trait that you are rejecting for use. Optionally, use the keyword as to add an alias like we did with namespaces so that you can use both the methods:

.. code-block:: php

    <?php
    class Manager { 
        use Contract, Communicator { 
            Contract::sign insteadof Communicator; 
            Communicator::sign as makeASign; 
        } 
    }
    
    $manager = new Manager(); 
    $manager->sign();       // Signing the contract.
    $manager->makeASign();  // Signing to the waitress.

You can see how we decided to use the method of Contract instead of Communicator, but added the alias so that both methods are available. Hopefully, you can see that even the conflicts can be solved, and there are specific cases where there is nothing to do but deal with them; in general, they look like a bad sign—no pun intended.



Handling exceptions
===================

It does not matter how easy and intuitive your application is designed to be, there will be bad usage from the user or just random errors of connectivity, and your code has to be ready to handle these scenarios so that the user experience is a good as possible. We call these scenarios exceptions: an element of the language that identifies a case that is not as we expected.

The try..catch block
--------------------

Your code can throw exceptions manually whenever you think it necessary. For example, take the setId method from the Unique trait. Thanks to type hinting, we are enforcing the ID to be a numeric one, but that is as far as it goes. What would happen if someone tries to set an ID that is a negative number? The code right now allows it to go through, but depending on your preferences, you would like to avoid it. That would be a good place for an exception to happen. Let's see how we would add this check and consequent exception:

.. code-block:: php

    <?php
    public function setId($id) { 
        if ($id < 0) { 
            throw new \Exception('Id cannot be negative.'); 
        } 
        if (empty($id)) { 
            $this->id = ++self::$lastId; 
        } 
        else { 
            $this->id = $id; 
            if ($id > self::$lastId) { 
                self::$lastId = $id; 
            } 
        } 
    }

As you can see, exceptions are objects of the class exception. Remember adding the backslash to the name of the class, unless you want to include it with use Exception; at the top of the file. The constructor of the Exception class takes some optional arguments, the first one of them being the message of the exception. Instances of the class Exception do nothing by themselves; they have to be thrown in order to be noticed by the program.

Let's try forcing our program to throw this exception. In order to do that, let's try to create a customer with a negative ID. In your init.php file, add the following:

.. code-block:: php

    <?php
    $basic = new Basic(-1, "name", "surname", "email");

If you try it now in your browser, PHP will throw a fatal error saying that there was an uncaught exception, which is the expected behavior. For PHP, an exception is something from what it cannot recover, so it will stop execution. That is far from ideal, as you would like to just display an error message to the user, and let them try again.

You can—and should—capture exceptions using the try…catch blocks. You insert the code that might throw an exception in the try block and if an exception happens, PHP will jump to the catch block. Let's see how it works:

.. code-block:: php

    <?php
    public function setId(int $id) { 
        try { 
            if ($id < 0) { 
                throw new Exception('Id cannot be negative.'); 
            } 
            if (empty($id)) { 
                $this->id = ++self::$lastId; 
            } 
            else { 
                $this->id = $id; 
                if ($id > self::$lastId) { 
                    self::$lastId = $id; 
                } 
            } 
        } 
        
        catch (Exception $e) { 
            echo $e->getMessage(); 
        } 
    }

If we test the last code snippet in our browser, we will see the message printed from the catch block. Calling the getMessage method on an exception instance will give us the message—the first argument when creating the object. But remember that the argument of the constructor is optional; so, do not rely on the message of the exception too much if you are not sure how it is generated, as it might be empty.

Note that after the exception is thrown, nothing else inside the try block is executed; PHP goes straight to the catch block. Additionally, the block gets an argument, which is the exception thrown. Here, type hinting is mandatory—you will see why very soon. Naming the argument as $e is a widely used convention, even though it is not a good practice to use poor descriptive names for variables.

Being a bit critical, so far, there is not any real advantage to be seen in using exceptions in this example. A simple if..else block would do exactly the same job, right? But the real power of exceptions lies in the ability to be propagated across methods. That is, the exception thrown on the setId method, if not captured, will be propagated to wherever the method was invoked, allowing us to capture it there. This is very useful, as different places in the code might want to handle the exception in a different way. To see how this is done, let's remove the try..catch inserted in setId, and place the following piece of code in your init.php file, instead:

.. code-block:: php

    <?php
    try { 
        $basic = new Basic(-1, "name", "surname", "email"); 
    } 
    catch (Exception $e) { 
        echo 'Something happened when creating the basic customer: ' $e->getMessage(); 
    }

The preceding example shows how useful it is to catch propagated exceptions: we can be more specific of what happens, as we know what the user was trying to do when the exception was thrown. In this case, we know that we were trying to create the customer, but this exception might have been thrown when trying to update the ID of an existing customer, which would need a different error message.

The finally block
-----------------

There is a third block that you can use when dealing with exceptions: the finally block. This block is added after the try…catch one, and it is optional. In fact, the catch block is optional too; the restriction is that a try must be followed by at least one of them. So you could have these three scenarios:

.. code-block:: php

    <?php
    // scenario 1: the whole try-catch-finally 
    try { 
        // code that might throw an exception 
    } 
    catch (Exception $e) { 
        // code that deals with the exception 
    } 
    
    finally { 
        // finally block 
    } 
    
    // scenario 2: try-finally without catch 
    
    try { 
        // code that might throw an exception 
    } 
    
    finally { 
        // finally block
     } 
     
     // scenario 3: try-catch without finally 
     try { 
        // code that might throw an exception 
    } 
    catch (Exception $e) { 
        // code that deals with the exception 
    }

The code inside the finally block is executed when either the try or the catch blocks are executed completely. So, if we have a scenario where there is no exception, after all the code inside the try block is executed, PHP will execute the code inside finally. On the other hand, if there is an exception thrown inside the try block, PHP will jump to the catch block, and after executing everything there, it will execute the finally block too.

In order to test this functionality, let's implement a function that contains a try…catch…finally block, trying to create a customer with a given ID (through an argument), and logging all the actions that take place. You can add the following code snippet into your init.php file:

.. code-block:: php

    <?php
    function createBasicCustomer($id) { 
        try { 
            echo "\\nTrying to create a new customer.\\n"; 
            return new Basic($id, "name", "surname", "email"); 
        } 
        
        catch (Exception $e) { 
            echo "Something happened when creating the basic customer: " . $e->getMessage() . "\\n"; 
        } 
        finally { 
            echo "End of function.\\n"; 
        } 
    } 
    
    createBasicCustomer(1);
    createBasicCustomer(-1);

If you try this, your browser will show you the following output; remember to display the source code of the page to see it formatted prettily:

|image22|

The result might not be the one you expected. The first time we invoke the function, we are able to create the object without an issue, and that means we execute the return statement. In a normal function, this should be the end of it, but since we are inside the try…catch..finally block, we still need to execute the finally code! The second example looks more intuitive, jumping from the try to the catch, and then to the finally block.

The finally block is very useful when dealing with expensive resources like database connections. In `*Chapter 5* <#Top_of_ch05_html>`__, *Using Databases*, you will see how to use them. Depending on the type of connection, you will have to close it after use for allowing other users to connect. The finally block is used for closing those connections, regardless of whether the function throws an exception or not.

Catching different types of exceptions
--------------------------------------

Exceptions have already been proven useful, but there is still one important feature to show: catching different types of exceptions. As you already know, exceptions are instances of the class Exception, and as with any other class, they can be extended. The main goal of extending from this class is to create different types of exceptions, but we will not add any logic inside—even though you can, of course. Let's create a class that extends from Exception, and which identifies exceptions related to invalid IDs. Put this code inside the src/Exceptions/InvalidIdException.php file:

.. code-block:: php

    <?php 
        namespace Bookstore\Exceptions; 
        use Exception; 
        
        class InvalidIdException extends Exception { 
            public function __construct($message = null) { 
                $message = $message ?: 'Invalid id provided.'; parent::__construct($message); 
            } 
        }

The InvalidIdException class extends from the class Exception, and so it can be thrown as one. The constructor of the class takes an optional argument, $message. The following two lines inside it contain interesting code:

-  The ?: operator is a shorter version of a conditional, and works like
   this: the expression on the left is returned if it does not evaluate
   to false, otherwise, the expression on the right will be returned.
   What we want here is to use the message given by the user, or a
   default one in case the user does not provide any. For more
   information and usages, you can visit the PHP documentation at
   `*http://php.net/manual/en/language.operators.comparison.php* <http://php.net/manual/en/language.operators.comparison.php>`__.

-  parent::__construct will invoke the parent's constructor, that is,
   the constructor of the class Exception. As you already know, this
   constructor gets the message of the exception as the first argument.
   You could argue that, as we are extending from the Exception class,
   we do not really need to call any functions, as we can edit the
   properties of the class straightaway. The reason for avoiding this is
   to let the parent class manage its own properties. Imagine that, for
   some reason, in a future version of PHP, Exception changes the name
   of the property for the message. If you modify it directly, you will
   have to change that in your code, but if you use the constructor, you
   have nothing to fear. Internal implementations are more likely to
   change than external interfaces.

We can use this new exception instead of the generic one. Replace it in your Unique trait as follows:

.. code-block:: php

    <?php
    throw new InvalidIdException('Id cannot be a negative number.');

You can see that we are still sending a message: that is because we want to be even more specific. But the exception would work as well without one. Try your code again, and you will see that nothing changes.

Now imagine that we have a very small database and we cannot allow more than 50 users. We can create a new exception that identifies this case, let's say, as src/Exceptions/ExceededMaxAllowedException.php:

.. code-block:: php

    <?php 
        namespace Bookstore\Exceptions; 
        use Exception; 
        class ExceededMaxAllowedException extends Exception { 
            public function __construct($message = null) { 
                $message = $message ?: 'Exceeded max allowed.'; parent::__construct($message); 
            } 
        }

Let's modify our trait in order to check for this case. When setting an
ID, if this ID is greater than 50, we can assume that we've reached the
maximum number of users:

.. code-block:: php

    <?php
    public function setId(int $id) { 
        if ($id < 0) { 
            throw new InvalidIdException( 'Id cannot be a negative number.' ); 
        } 
        if(empty($id)) { 
            $this->id = ++self::$lastId; 
        } 
        else { 
            $this->id = $id; 
            if ($id > self::$lastId) { 
                self::$lastId = $id; 
            } 
        } 
        if($this->id > 50) { 
            throw new ExceededMaxAllowedException( 'Max number of users is 50.' )
        } 
    }

Now the preceding function throws two different exceptions: InvalidIdException and ExceededMaxAllowedException. When catching them, you might want to behave in a different way depending on the type of exception caught. Remember how you have to declare an argument in your catch block? Well, you can add as many catch blocks as needed, specifying a different exception class in each of them. The code could look like this:

.. code-block:: php

    <?php
    function createBasicCustomer(int $id) { 
        try {
            echo "\\nTrying to create a new customer with id $id.\\n"; 
            return new Basic($id, "name", "surname", "email"); 
        } 
        
        catch (InvalidIdException $e) { 
            echo "You cannot provide a negative id.\\n"; 
        } 
        
        catch (ExceededMaxAllowedException $e) { 
            echo "No more customers are allowed.\\n"; 
        } 
        
        catch (Exception $e) { 
            echo "Unknown exception: " . $e->getMessage(); 
        } 
    } 
    
    createBasicCustomer(1);
    createBasicCustomer(-1); 
    createBasicCustomer(55);

If you try this code, you should see the following output:

|image23|

Note that we catch three exceptions here: our two new exceptions and the generic one. The reason for doing this is that it might happen that some other piece of code throws an exception of a different type than the ones we defined, and we need to define a catch block with the generic Exception class to get it, as all exceptions will extend from it. Of course, this is absolutely optional, and if you do not do it, the exception will be just propagated.

Bear in mind the order of the catch blocks. PHP tries to use the catch blocks in the order that you defined them. So, if your first catch is for Exception, the rest of the blocks will be never executed, as all exceptions extend from that class. Try it with the following code:


.. container:: toggle

    .. container:: header

        **Show/Hide Code**


    .. code-block:: php

        <?php
        try { 
            echo "\\nTrying to create a new customer with id $id.\\n";
            return new Basic($id, "name", "surname", "email"); 
        } 
        
        catch(Exception $e) { 
            echo 'Unknown exception: ' . $e->getMessage() . "\\n"; 
        } 
        catch (InvalidIdException $e) { 
            echo "You cannot provide a negative id.\\n"; 
        } 
        catch (ExceededMaxAllowedException $e) { 
            echo "No more customers are allowed.\\n"; 
        }

The result that you get from the browser will always be from the first catch:

|image24|