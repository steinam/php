Chapter 5. Using Databases
===========================

Data is probably the cornerstone of most web applications. Sure, your
application has to be pretty, fast, error-free, and so on, but if
something is essential to users, it is what data you can manage for
them. From this, we can extract that managing data is one of the most
important things you have to consider when designing your application.

Managing data implies not only storing read-only files and reading them
when needed, as we were doing so far, but also adding, fetching,
updating, and removing individual pieces of information. For this, we
need a tool that categorizes our data and makes these tasks easier for
us, and this is when databases come into play.

In this chapter, you will learn about:

-  Schemas and tables

-  Manipulating and querying data

-  Using PDO to connect your database with PHP

-  Indexing your data

-  Constructing complex queries in joining tables

Introducing databases
---------------------

Databases are tools to manage data. The basic functions of a database
are inserting, searching, updating, and deleting data, even though most
database systems do more than this. Databases are classified into two
different categories depending on how they store data: relational and
nonrelational databases.

Relational databases structure data in a very detailed way, forcing the
user to use a defined format and allowing the creation of
connections—that is, relations—between different pieces of information.
Nonrelational databases are systems that store data in a more relaxed
way, as though there were no apparent structure. Even though with these
very vague definitions you could assume that everybody would like to use
relational databases, both systems are very useful; it just depends on
how you want to use them.

In this book, we will focus on relational databases as they are widely
used in small web applications, in which there are not huge amounts of
data. The reason is that usually the application contains data that is
interrelated; for example, our application could store sales, which are
composed of customers and books.

MySQL
-----

MySQL has been the favorite choice of PHP developers for quite a long
time. It is a relational database system that uses SQL as the language
to communicate with the system. SQL is used in quite a few other
systems, which makes things easier in case you need to switch databases
or just need to understand an application with a different database than
the one you are used to. The rest of the chapter will be focused on
MySQL, but it will be helpful for you even if you choose a different SQL
system.

In order to use MySQL, you need to install two applications: the server
and the client. You might remember server-client applications from
`*Chapter 2* <#Top_of_ch02_html>`__, *Web Applications with PHP*. The
MySQL server is a program that listens for instructions or queries from
clients, executes them, and returns a result. You need to start the
server in order to access the database; take a look at `*Chapter
1* <#Top_of_ch01_html>`__, *Setting Up the Environment*, on how to do
this. The client is an application that allows you to construct
instructions and send them to the server, and it is the one that you
will use.

Note

GUI versus command line

The Graphical User Interface (GUI) is very common when using a database.
It helps you in constructing instructions, and you can even manage data
without them using just visual tables. On the other hand, command-line
clients force you to write all the commands by hand, but they are
lighter than GUIs, faster to start, and force you to remember how to
write SQL, which you need when you write your applications in PHP. Also,
in general, almost any machine with a database will have a MySQL client
but might not have a graphical application.

You can choose the one that you are more comfortable with as you will
usually work with your own machine. However, keep in mind that a basic
knowledge of the command line will save your life on several occasions.

In order to connect the client with a server, you need to provide some
information on where to connect and the credentials for the user to use.
If you do not customize your MySQL installation, you should at least
have a root user with no password, which is the one we will use. You
could think that this seems to be a horrible security hole, and it might
be so, but you should not be able to connect using this user if you do
not connect from the same machine on which the server is. The most
common arguments that you can use to provide information when starting
the client are:

-  -u <name>: This specifies the user—in our case, root.

-  -p<password>: Without a space, this specifies the password. As we do
   not have a password for our user, we do not need to provide this.

-  -h <host>: This specifies where to connect. By default, the client
   connects to the same machine. As this is our case, there is no need
   to specify any. If you had to, you could specify either an IP address
   or a hostname.

-  <schema name>: This specifies the name of the schema to use. We will
   explain in a bit what this means.

With these rules, you should be able to connect to your database with
the mysql -u root command. You should get an output very similar to the
following one:

    $ mysql -u root Welcome to the MySQL monitor. Commands end with ; or
    \\g. Your MySQL connection id is 2 Server version: 5.1.73 Source
    distribution Copyright (c) 2000, 2013, Oracle and/or its affiliates.
    All rights reserved. Oracle is a registered trademark of Oracle
    Corporation and/or its affiliates. Other names may be trademarks of
    their respective owners. Type 'help;' or '\\h' for help. Type '\\c'
    to clear the current input statement. mysql>

The terminal will show you the version of the server and some useful
information about how to use the client. From now on, the command line
will start with mysql> instead of your normal prompt, showing you that
you are using the MySQL client. In order to execute queries, just type
the query, end it with a semicolon, and press *Enter*. The client will
send the query to the server and will show the result of it. To exit the
client, you can either type \\q and press *Enter* or press *Ctrl* + *D*,
even though this last option will depend on your operating system.

Schemas and tables
------------------

Relational database systems usually have the same structure. They store
data in different databases or schemas, which separate the data from
different applications. These schemas are just collections of tables.
Tables are definitions of specific data structures and are composed of
fields. A field is a basic data type that defines the smallest component
of information as though they were the atoms of the data. So, schemas
are group of tables that are composed of fields. Let's look at each of
these elements.

Understanding schemas
---------------------

As defined before, schemas or databases— in MySQL, they are synonyms—are
collections of tables with a common context, usually belonging to the
same application. Actually, there are no restrictions around this, and
you could have several schemas belonging to the same application if
needed. However, for small web applications, as it is our case, we will
have just one schema.

Your server probably already has some schemas. They usually contain the
metadata needed for MySQL in order to operate, and we highly recommend
that you do not modify them. Instead, let's just create our own schema.
Schemas are quite simple elements, and they only have a mandatory name
and an optional charset. The name identifies the schema, and the charset
defines which type of codification or "alphabet" the strings should
follow. As the default charset is latin1, if you do not need to change
it, you do not need to specify it.

Use CREATE SCHEMA followed by the name of the schema in order to create
the schema that we will use for our bookstore. The name has to be
representative, so let's name it bookstore. Remember to end your line
with a semicolon. Take a look at the following:

    mysql> CREATE SCHEMA bookstore; Query OK, 1 row affected (0.00 sec)

If you need to remember how a schema was created, you can use SHOW
CREATE SCHEMA to see its description, as follows:

    mysql> SHOW CREATE SCHEMA bookstore \\G
    \*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\* 1. row
    \*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\* Database:
    bookstore Create Database: CREATE DATABASE \`bookstore\` /\*!40100
    DEFAULT CHARACTER SET latin1 \*/ 1 row in set (0.00 sec)

As you can see, we ended the query with \\G instead of a semicolon. This
tells the client to format the response in a different way than the
semicolon does. When using a command of the SHOW CREATE family, we
recommend that you end it with \\G to get a better understanding.

Tip

Should you use uppercase or lowercase?

When writing queries, you might note that we used uppercase for keywords
and lowercase for identifiers, such as names of schemas. This is just a
convention widely used in order to make it clear what is part of SQL and
what is your data. However, MySQL keywords are case-insensitive, so you
could use any case indistinctively.

All data must belong to a schema. There cannot be data floating around
outside all schemas. This way, you cannot do anything unless you specify
the schema you want to use. In order to do this, just after starting
your client, use the USE keyword followed by the name of the schema.
Optionally, you could tell the client which schema to use when
connecting to it, as follows:

    mysql> USE bookstore; Database changed

If you do not remember what the name of your schema is or want to check
which other schemas are in your server, you can run the SHOW SCHEMAS;
command to get a list of them, as follows:

    mysql> SHOW SCHEMAS; +--------------------+ \| Database \|
    +--------------------+ \| information\_schema \| \| bookstore \| \|
    mysql \| \| test \| +--------------------+ 4 rows in set (0.00 sec)

Database data types
-------------------

As in PHP, MySQL also has data types. They are used to define which kind
of data a field can contain. As in PHP, MySQL is quite flexible with
data types, transforming them from one type to the other if needed.
There are quite a few of them, but we will explain the most important
ones. We highly recommend that you visit the official documentation
related to data types at
`*http://dev.mysql.com/doc/refman/5.7/en/data-types.html* <http://dev.mysql.com/doc/refman/5.7/en/data-types.html>`__
if you want to build applications with more complex data structures.

Numeric data types
~~~~~~~~~~~~~~~~~~

Numeric data can be categorized as integers or decimal numbers. For
integers, MySQL uses the INT data type even though there are versions to
store smaller numbers, such as TINYINT, SMALLINT, or MEDIUMINT, or
bigger numbers, such as BIGINT. The following table shows what the sizes
of the different numeric types are, so you can choose which one to use
depending on your situation:

+-------------+-----------------------------------------------------------+
| Type        | Size/precision                                            |
+-------------+-----------------------------------------------------------+
| TINYINT     | -128 to 127                                               |
+-------------+-----------------------------------------------------------+
| SMALLINT    | -32,768 to 32,767                                         |
+-------------+-----------------------------------------------------------+
| MEDIUMINT   | -8,388,608 to 8,388,607                                   |
+-------------+-----------------------------------------------------------+
| INT         | -2,147,483,648 to 2,147,483,647                           |
+-------------+-----------------------------------------------------------+
| BIGINT      | -9,223,372,036,854,775,808 to 9,223,372,036,854,775,807   |
+-------------+-----------------------------------------------------------+

Numeric types can be defined as signed by default or unsigned; that is,
you can allow or not allow them to contain negative values. If a numeric
type is defined as UNSIGNED, the range of numbers that it can take is
doubled as it does not need to save space for negative numbers.

For decimal numbers we have two types: approximate values, which are
faster to process but are not exact sometimes, and exact values that
give you exact precision on the decimal value. For approximate values or
the floating-point type, we have FLOAT and DOUBLE. For exact values or
the fixed-point type we have DECIMAL.

MySQL allows you to specify the number of digits and decimal positions
that the number can take. For example, to specify a number that can
contains five digits and up to two of them can be decimal, we will use
the FLOAT(5,2) notation. This is useful as a constraint, as you will
note when we create tables with prices.

String data types
~~~~~~~~~~~~~~~~~

Even though there are several data types that allow you to store from
single characters to big chunks of text or binary code, it is outside
the scope of this chapter. In this section, we will introduce you to
three types: CHAR, VARCHAR, and TEXT.

CHAR is a data type that allows you to store an exact number of
characters. You need to specify how long the string will be once you
define the field, and from this point on, all values for this field have
to be of this length. One possible usage in our applications could be
when storing the ISBN of the book as we know it is always 13 characters
long.

VARCHAR or variable char is a data type that allows you to store strings
up to 65,535 characters long. You do not need to specify how long they
need to be, and you can insert strings of different lengths without an
issue. Of course, the fact that this type is dynamic makes it slower to
process compared with the previous one, but after a few times you know
how long a string will always be. You could tell MySQL that even if you
want to insert strings of different lengths, the maximum length will be
a determined number. This will help its performance. For example, names
are of different lengths, but you can safely assume that no name will be
longer than 64 characters, so your field could be defined as
VARCHAR(64).

Finally, TEXT is a data type for really big strings. You could use it if
you want to store long comments from users, articles, and so on. As with
INT, there are different versions of this data type: TINYTEXT, TEXT,
MEDIUMTEXT, and LONGTEXT. Even if they are very important in almost any
web application with user interaction, we will not use them in ours.

List of values
~~~~~~~~~~~~~~

In MySQL, you can force a field to have a set of valid values. There are
two types of them: ENUM, which allows exactly one of the possible
predefined values, and SET, which allows any number of the predefined
values.

For example, in our application, we have two types of customers: basic
and premium. If we want to store our customers in a database, there is a
chance that one of the fields will be the type of customer. As a
customer has to be either basic or premium, a good solution would be to
define the field as an enum as ENUM("basic", "premium"). In this way, we
will make sure that all customers stored in our database will be of a
correct type.

Although enums are quite common to use, the use of sets is less
widespread. It is usually a better idea to use an extra table to define
the values of the list, as you will note when we talk about foreign keys
in this chapter.

Date and time data types
~~~~~~~~~~~~~~~~~~~~~~~~

Date and time types are the most complex data types in MySQL. Even
though the idea is simple, there are several functions and edge cases
around these types. We cannot go through all of them, so we will just
explain the most common uses, which are the ones we will need for our
application.

DATE stores dates—that is, a combination of day, month, and year. TIME
stores times—that is, a combination of hour, minute, and second.
DATETIME are data types for both date and time. For any of these data
types, you can provide just a string specifying what the value is, but
you need to be careful with the format that you use. Even though you can
always specify the format that you are entering the data in, you can
just enter the dates or times in the default format—for example,
2014-12-31 for dates, 14:34:50 for time, and 2014-12-31 14:34:50 for the
date and time.

A fourth type is TIMESTAMP. This type stores an integer, which is the
representation of the seconds from January 1, 1970, which is also known
as the Unix timestamp. This is a very useful type as in PHP, it is
really easy to get the current Unix timestamp with the now() function,
and the format for this data type is always the same, so it is safer to
work with it. The downside is that the range of dates that it can
represent is limited as compared to other types.

There are some functions that help you manage these types. These
functions extract specific parts of the whole value, return the value
with a different format, add or subtract dates, and so on. Let's take a
look at a short list of them:

+---------------------------------------+---------------------------------------------------------------------------------------------------------+
| Function name                         | Description                                                                                             |
+---------------------------------------+---------------------------------------------------------------------------------------------------------+
| DAY(), MONTH(), and YEAR()            | Extracts the specific value for the day, month, or year from the DATE or DATETIME provided value.       |
+---------------------------------------+---------------------------------------------------------------------------------------------------------+
| HOUR(), MINUTE(), and SECOND()        | Extracts the specific value for the hour, minute, or second from the TIME or DATETIME provided value.   |
+---------------------------------------+---------------------------------------------------------------------------------------------------------+
| CURRENT\_DATE() and CURRENT\_TIME()   | Returns the current date or current time.                                                               |
+---------------------------------------+---------------------------------------------------------------------------------------------------------+
| NOW()                                 | Returns the current date and time.                                                                      |
+---------------------------------------+---------------------------------------------------------------------------------------------------------+
| DATE\_FORMAT()                        | Returns the DATE, TIME or DATETIME value with the specified format.                                     |
+---------------------------------------+---------------------------------------------------------------------------------------------------------+
| DATE\_ADD()                           | Adds the specified interval of time to a given date or time type.                                       |
+---------------------------------------+---------------------------------------------------------------------------------------------------------+

Do not worry if you are confused on how to use any of these functions;
we will use them during the rest of the book as part of our application.
Also, an extensive list of all the types can be found at
`*http://dev.mysql.com/doc/refman/5.7/en/date-and-time-functions.html* <http://dev.mysql.com/doc/refman/5.7/en/date-and-time-functions.html>`__.

Managing tables
---------------

Now that you understand the different types of data that fields can
take, it is time to introduce tables. As defined in the *Schemas and
tables* section, a table is a collection of fields that defines a type
of information. You could compare it with OOP and think of tables as
classes, fields being their properties. Each instance of the class would
be a row on the table.

When defining a table, you have to declare the list of fields that the
table contains. For each field, you need to specify its name, its type,
and some extra information depending on the type of the field. The most
common are:

-  NOT NULL: This is used if the field cannot be null—that is, if it
   needs a concrete valid value for each row. By default, a field can be
   null.

-  UNSIGNED: As mentioned earlier, this is used to forbid the use of
   negative numbers in this field. By default, a numeric field accepts
   negative numbers.

-  DEFAULT <value>: This defines a default value in case the user does
   not provide any. Usually, the default value is null if this clause is
   not specified.

Table definitions also need a name, as with schemas, and some optional
attributes. You can define the charset of the table or its engine.
Engines can be a quite large topic to cover, but for the scope of this
chapter, let's just note that we should use the InnoDB engine if we need
strong relationships between tables. For more advanced readers, you can
read more about MySQL engines at
`*https://dev.mysql.com/doc/refman/5.0/en/storage-engines.html* <https://dev.mysql.com/doc/refman/5.0/en/storage-engines.html>`__.

Knowing this, let's try to create a table that will keep our books. The
name of the table should be book, as each row will define a book. The
fields could have the same properties the Book class has. Let's take a
look at how the query to construct the table would look:

    mysql> CREATE TABLE book( -> isbn CHAR(13) NOT NULL, -> title
    VARCHAR(255) NOT NULL, -> author VARCHAR(255) NOT NULL, -> stock
    SMALLINT UNSIGNED NOT NULL DEFAULT 0, -> price FLOAT UNSIGNED -> )
    ENGINE=InnoDb; Query OK, 0 rows affected (0.01 sec)

As you can note, we can add more new lines until we end the query with a
semicolon. With this, we can format the query in a way that looks more
readable. MySQL will let us know that we are still writing the same
query showing the -> prompt. As this table contains five fields, it is
very likely that we will need to refresh our minds from time to time as
we will forget them. In order to display the structure of the table, you
could use the DESC command, as follows:

    mysql> DESC book;
    +--------+----------------------+------+-----+---------+-------+ \|
    Field \| Type \| Null \| Key \| Default \| Extra \|
    +--------+----------------------+------+-----+---------+-------+ \|
    isbn \| char(13) \| NO \| \| NULL \| \| \| title \| varchar(255) \|
    NO \| \| NULL \| \| \| author \| varchar(255) \| NO \| \| NULL \| \|
    \| stock \| smallint(5) unsigned \| NO \| \| 0 \| \| \| price \|
    float unsigned \| YES \| \| NULL \| \|
    +--------+----------------------+------+-----+---------+-------+ 5
    rows in set (0.00 sec)

We used SMALLINT for stock as it is very unlikely that we will have more
than thousands of copies of the same book. As we know that ISBN is 13
characters long, we enforced this when defining the field. Finally, both
stock and price are unsigned as negative values do not make sense. Let's
now create our customer table via the following script:

    mysql> CREATE TABLE customer( -> id INT UNSIGNED NOT NULL, ->
    firstname VARCHAR(255) NOT NULL, -> surname VARCHAR(255) NOT NULL,
    -> email VARCHAR(255) NOT NULL, -> type ENUM('basic', 'premium') ->
    ) ENGINE=InnoDb; Query OK, 0 rows affected (0.00 sec)

We already anticipated the use of enum for the field type as when
designing classes, we could draw a diagram identifying the content of
our database. On this, we could show the tables and their fields. Let's
take a look at how the diagram of tables would look so far:

|image25|

Note that even if we create tables similar to our classes, we will not
create a table for Person. The reason is that databases store data, and
there isn't any data that we could store for this class as the customer
table already contains everything we need. Also, sometimes, we may
create tables that do not exist as classes on our code, so the
class-table relationship is a very flexible one.

Keys and constraints
--------------------

Now that we have our main tables defined, let's try to think about how
the data inside would look. Each row inside a table would describe an
object, which may be either a book or a customer. What would happen if
our application has a bug and allows us to create books or customers
with the same data? How will the database differentiate them? In theory,
we will assign IDs to customers in order to avoid these scenarios, but
how do we enforce that the ID not be repeated?

MySQL has a mechanism that allows you to enforce certain restrictions on
your data. Other than attributes such as NOT NULL or UNSIGNED that you
already saw, you can tell MySQL that certain fields are more special
than others and instruct it to add some behavior to them. These
mechanisms are called keys, and there are four types: primary key,
unique key, foreign key, and index. Let's take a closer look at them.

Primary keys
~~~~~~~~~~~~

Primary keys are fields that identify a unique row from a table. There
cannot be two of the same value in the same table, and they cannot be
null. Adding a primary key to a table that defines *objects* is almost a
must as it will assure you that you will always be able to differentiate
two rows by this field.

Another part that makes primary keys so attractive is their ability to
set the primary key as an autoincremental numeric value; that is, you do
not have to assign a value to the ID, and MySQL will just pick up the
latest inserted ID and increment it by 1, as we did with our Unique
trait. Of course, for this to happen, your field has to be an integer
data type. In fact, we highly recommend that you always define your
primary key as an integer, even if the real-life object does not really
have this ID at all. The reason is that you should search a row by this
numeric ID, which is unique, and MySQL will add some performance
improvements that come by setting the field as a key.

Then, let's add an ID to our book table. In order to add a new field, we
need to alter our table. There is a command that allows you to do this:
ALTER TABLE. With this command, you can modify the definition of any
existing field, add new ones, or remove existing ones. As we add the
field that will be our primary key and be autoincremental, we can add
all these modifiers to the field definition. Execute the following code:

    mysql> ALTER TABLE book -> ADD id INT UNSIGNED NOT NULL
    AUTO\_INCREMENT -> PRIMARY KEY FIRST; Query OK, 0 rows affected
    (0.02 sec) Records: 0 Duplicates: 0 Warnings: 0

Note FIRST at the end of the command. When adding new fields, if you
want them to appear on a different position than at the end of the
table, you need to specify the position. It could be either FIRST or
AFTER <other field>. For convenience, the primary key of a table is the
first of its fields.

As the table customer already has an ID field, we do not have to add it
again but rather modify it. In order to do this, we will just use the
ALTER TABLE command with the MODIFY option, specifying the new
definition of an already existing field, as follows:

    mysql> ALTER TABLE customer -> MODIFY id INT UNSIGNED NOT NULL ->
    AUTO\_INCREMENT PRIMARY KEY; Query OK, 0 rows affected (0.00 sec)
    Records: 0 Duplicates: 0 Warnings: 0

Foreign keys
~~~~~~~~~~~~

Let's imagine that we need to keep track of the borrowed books. The
table should contain the borrowed book, who borrowed it, and when it was
borrowed. So, what kind of data would you use to identify the book or
the customer? Would you use the title or the name? Well, we should use
something that identifies a unique row from these tables, and this
"something" is the primary key. With this action, we will eliminate the
change of using a reference that can potentially point to two or more
rows at the same time.

We could then create a table that contains book\_id and customer\_id as
numeric fields, containing the IDs that reference these two tables. As
the first approach, it makes sense, but we can find some weaknesses. For
example, what happens if we insert wrong IDs and they do not exist in
book or customer? We could have some code in our PHP side to make sure
that when fetching information from borrowed\_books, we only displayed
the information that is correct. We could even have a routine that
periodically checks for wrong rows and removes them, solving the issue
of having wrong data wasting space in the disk. However, as with the
Unique trait versus adding primary keys in MySQL, it is usually better
to allow the database system to manage these things as the performance
will usually be better, and you do not need to write extra code.

MySQL allows you to create keys that enforce references to other tables.
These are called foreign keys, and they are the primary reason for which
we were forced to use the InnoDB table engine instead of any other. A
foreign key defines and enforces a reference between this field and
another row of a different table. If the ID supplied for the field with
a foreign key does not exist in the referenced table, the query will
fail. Furthermore, if you have a valid borrowed\_books row pointing to
an existing book and you remove the entry from the book table, MySQL
will complain about it—even though you will be able to customize this
behavior soon—as this action would leave wrong data in the system. As
you can note, this is way more useful than having to write code to
manage these cases.

Let's create the borrowed\_books table with the book, customer
references, and dates. Note that we have to define the foreign keys
after the definition of the fields as opposed to when we defined primary
keys, as follows:

    mysql> CREATE TABLE borrowed\_books( -> book\_id INT UNSIGNED NOT
    NULL, -> customer\_id INT UNSIGNED NOT NULL, -> start DATETIME NOT
    NULL, -> end DATETIME DEFAULT NULL, -> FOREIGN KEY (book\_id)
    REFERENCES book(id), -> FOREIGN KEY (customer\_id) REFERENCES
    customer(id) -> ) ENGINE=InnoDb; Query OK, 0 rows affected (0.00
    sec)

As with SHOW CREATE SCHEMA, you can also check how the table looks. This
command will also show you information about the keys as opposed to the
DESC command. Let's take a look at how it would work:

    mysql> SHOW CREATE TABLE borrowed\_books \\G
    \*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\* 1. row
    \*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\* Table:
    borrowed\_books Create Table: CREATE TABLE \`borrowed\_books\` (
    \`book\_id\` int(10) unsigned NOT NULL, \`customer\_id\` int(10)
    unsigned NOT NULL, \`start\` datetime NOT NULL, \`end\` datetime
    DEFAULT NULL, KEY \`book\_id\` (\`book\_id\`), KEY \`customer\_id\`
    (\`customer\_id\`), CONSTRAINT \`borrowed\_books\_ibfk\_1\` FOREIGN
    KEY (\`book\_id\`) REFERENCES \`book\` (\`id\`), CONSTRAINT
    \`borrowed\_books\_ibfk\_2\` FOREIGN KEY (\`customer\_id\`)
    REFERENCES \`customer\` (\`id\`) ) ENGINE=InnoDB DEFAULT
    CHARSET=latin1 1 row in set (0.00 sec)

Note two important things here. On one hand, we have two extra keys that
we did not define. The reason is that when defining a foreign key, MySQL
also defines the field as a key that will be used to improve performance
on the table; we will look into this in a moment. The other element to
note is the fact that MySQL defines names to the keys by itself. This is
necessary as we need to be able to reference them in case we want to
change or remove this key. You can let MySQL name the keys for you, or
you can specify the names you prefer when creating them.

We are running a bookstore, and even if we allow customers to borrow
books, we want to be able to sell them. A sale is a very important
element that we need to track down as customers may want to review them,
or you may just need to provide this information for taxation purposes.
As opposed to borrowing, in which knowing the book, customer, and date
was more than enough, here, we need to set IDs to the sales in order to
identify them to the customers.

However, this table is more difficult to design than the other ones and
not just because of the ID. Think about it: do customers buy books one
by one? Or do they rather buy any number of books at once? Thus, we need
to allow the table to contain an undefined amount of books. With PHP,
this is easy as we would just use an array, but we do not have arrays in
MySQL. There are two options to this problem.

One solution could be to set the ID of the sale as a normal integer
field and not as a primary key. In this way, we would be able to insert
several rows to the sales table, one for each borrowed book. However,
this solution is less than ideal as we miss the opportunity of defining
a very good primary key because it has the sales ID. Also, we are
duplicating the data about the customer and date since they will always
be the same.

The second solution, the one that we will implement, is the creation of
a separated table that acts as a "list". We will still have our sales
table, which will contain the ID of the sale as a primary key, the
customer ID as a foreign key, and the dates. However, we will create a
second table that we could name sale\_book, and we will define there the
ID of the sale, the ID of the book, and the amount of books of the same
copy that the customer bought. In this way, we will have at once the
information about the customer and date, and we will be able to insert
as many rows as needed in our sale\_book list-table without duplicating
any data. Let's take a look at how we would create these:

    mysql> CREATE TABLE sale( -> id INT UNSIGNED NOT NULL
    AUTO\_INCREMENT PRIMARY KEY, -> customer\_id INT UNSIGNED NOT NULL,
    -> date DATETIME NOT NULL, -> FOREIGN KEY (customer\_id) REFERENCES
    customer(id) -> ) ENGINE=InnoDb; Query OK, 0 rows affected (0.00
    sec) mysql> CREATE TABLE sale\_book( -> sale\_id INT UNSIGNED NOT
    NULL, -> book\_id INT UNSIGNED NOT NULL, -> amount SMALLINT UNSIGNED
    NOT NULL DEFAULT 1, -> FOREIGN KEY (sale\_id) REFERENCES sale(id),
    -> FOREIGN KEY (book\_id) REFERENCES book(id) -> ) ENGINE=InnoDb;
    Query OK, 0 rows affected (0.00 sec)

Keep in mind that you should always create the sales table first because
if you create the sale\_book table with a foreign key first, referencing
a table that does not exist yet, MySQL will complain.

We created three new tables in this section, and they are interrelated.
It is a good time to update the diagram of tables. Note that we link the
fields with the tables when there is a foreign key defined. Take a look:

|image26|

Unique keys
~~~~~~~~~~~

As you know, primary keys are extremely useful as they provide several
features with them. One of these is that the field has to be unique.
However, you can define only one primary key per table, even though you
might have several fields that are unique. In order to amend this
limitation, MySQL incorporates unique keys. Their job is to make sure
that the field is not repeated in multiple rows, but they do not come
with the rest of the functionalities of primary keys, such as being
autoincremental. Also, unique keys can be null.

Our book and customer tables contain good candidates for unique keys.
Books can potentially have the same title, and surely, there will be
more than one book by the same author. However, they also have an ISBN
which is unique; two different books should not have the same ISBN. In
the same way, even if two customers were to have the same name, their
e-mail addresses will be always different. Let's add the two keys with
the ALTER TABLE command, though you can also add them when creating the
table as we did with foreign keys, as follows:

    mysql> ALTER TABLE book ADD UNIQUE KEY (isbn); Query OK, 0 rows
    affected (0.01 sec) Records: 0 Duplicates: 0 Warnings: 0 mysql>
    ALTER TABLE customer ADD UNIQUE KEY (email); Query OK, 0 rows
    affected (0.01 sec) Records: 0 Duplicates: 0 Warnings: 0

Indexes
~~~~~~~

Indexes, which are a synonym for keys, are fields that do not need any
special behavior as do the rest of the keys but they are important
enough in our queries. So, we will ask MySQL to do some work with them
in order to perform better when querying by this field. Do you remember
when adding a foreign key that MySQL added extra keys to the table?
Those were indexes too.

Think about how the application will use the database. We want to show
the catalog of books to our customers, but we cannot show all of them at
once for sure. The customer will want to filter the results, and one of
the most common ways of filtering is by specifying the title of the book
that they are looking for. From this, we can extract that the title will
be used to filter books quite often, so we want to add an index to this
field. Let's add the index via the following code:

    mysql> ALTER TABLE book ADD INDEX (title); Query OK, 0 rows affected
    (0.01 sec) Records: 0 Duplicates: 0 Warnings: 0

Remember that all other keys also provide indexing. IDs of books,
customers and sales, ISBNs, and e-mails are already indexed, so there is
no need to add another index here. Also, try not to add indexes to every
single field as in doing so you will be overindexing, which would make
some types of queries even slower than if they were without indexes!

Inserting data
--------------

We have created the perfect tables to hold our data, but so far they are
empty. It is time that we populate them. We delayed this moment as
altering tables with data is more difficult than when they are empty.

In order to insert this data, we will use the INSERT INTO command. This
command will take the name of the table, the fields that you want to
populate, and the data for each field. Note that you can choose not to
specify the value for a field, and there are different reasons to do
this, which are as follows:

-  The field has a default value, and we are happy using it for this
   specific row

-  Even though the field does not have an explicit default value, the
   field can take null values; so, by not specifying the field, MySQL
   will automatically insert a null here

-  The field is a primary key and is autoincremental, and we want to let
   MySQL take the next ID for us

There are different reasons that can cause an INSERT INTO command to
fail:

-  If you do not specify the value of a field and MySQL cannot provide a
   valid default value

-  If the value provided is not of the type of the field and MySQL fails
   to find a valid conversion

-  If you specify that you want to set the value for a field but you
   fail to provide a value

-  If you provide a foreign key with an ID but the ID does not exist in
   the referenced table

Let's take a look at how to add rows. Let's start with our customer
table, adding one basic and one premium, as follows:

    mysql> INSERT INTO customer (firstname, surname, email, type) ->
    VALUES ("Han", "Solo", "han@tatooine.com", "premium"); Query OK, 1
    row affected (0.00 sec) mysql> INSERT INTO customer (firstname,
    surname, email, type) -> VALUES ("James", "Kirk", "enter@prise",
    "basic"); Query OK, 1 row affected (0.00 sec)

Note that MySQL shows you some return information; in this case, it
shows that there was one row affected, which is the row that we
inserted. We did not provide an ID, so MySQL just added the next ones in
the list. As it is the first time that we are adding data, MySQL used
the IDs 1 and 2.

Let's try to trick MySQL and add another customer, repeating the e-mail
address field that we set as unique in the previous section:

    mysql> INSERT INTO customer (firstname, surname, email, type) ->
    VALUES ("Mr", "Spock", "enter@prise", "basic"); ERROR 1062 (23000):
    Duplicate entry 'enter@prise' for key 'email'

An error is returned with an error code and an error message, and the
row was not inserted, of course. The error message usually contains
enough information in order to understand the issue and how to fix it.
If this is not the case, we can always try to search on the Internet
using the error code and note what either the official documentation or
other users have to say about it.

In case you need to introduce multiple rows to the same table and they
contain the same fields, there is a shorter version of the command, in
which you can specify the fields and then provide the groups of values
for each row. Let's take a look at how to use it when adding books to
our book table, as follows:

    mysql> INSERT INTO book (isbn,title,author,stock,price) VALUES ->
    ("9780882339726","1984","George Orwell",12,7.50), ->
    ("9789724621081","1Q84","Haruki Murakami",9,9.75), ->
    ("9780736692427","Animal Farm","George Orwell",8,3.50), ->
    ("9780307350169","Dracula","Bram Stoker",30,10.15), ->
    ("9780753179246","19 minutes","Jodi Picoult",0,10); Query OK, 5 rows
    affected (0.01 sec) Records: 5 Duplicates: 0 Warnings: 0

As with customers, we will not specify the ID and let MySQL choose the
appropriate one. Note also that now the amount of affected rows is 5 as
we inserted five rows.

How can we take advantage of the explicit defaults that we defined in
our tables? Well, we can do this in the same way as we did with the
primary keys: do not specify them in the fields list or in the values
list, and MySQL will just use the default value. For example, we defined
a default value of 1 for our book.stock field, which is a useful
notation for the book table and the stock field. Let's add another row
using this default, as follows:

    mysql> INSERT INTO book (isbn,title,author,price) VALUES ->
    ("9781416500360", "Odyssey", "Homer", 4.23); Query OK, 1 row
    affected (0.00 sec)

Now that we have books and customers, let's add some historic data about
customers borrowing books. For this, use the numeric IDs from book and
customer, as in the following code:

    mysql> INSERT INTO borrowed\_books(book\_id,customer\_id,start,end)
    -> VALUES -> (1, 1, "2014-12-12", "2014-12-28"), -> (4, 1,
    "2015-01-10", "2015-01-13"), -> (4, 2, "2015-02-01", "2015-02-10"),
    -> (1, 2, "2015-03-12", NULL); Query OK, 3 rows affected (0.00 sec)
    Records: 3 Duplicates: 0 Warnings: 0

Querying data
-------------

It took quite a lot of time, but we are finally in the most exciting—and
useful—section related to databases: querying data. Querying data refers
to asking MySQL to return rows from the specified table and optionally
filtering these results by a set of rules. You can also choose to get
specific fields instead of the whole row. In order to query data, we
will use the SELECT command, as follows:

    mysql> SELECT firstname, surname, type FROM customer;
    +-----------+---------+---------+ \| firstname \| surname \| type \|
    +-----------+---------+---------+ \| Han \| Solo \| premium \| \|
    James \| Kirk \| basic \| +-----------+---------+---------+ 2 rows
    in set (0.00 sec)

One of the simplest ways to query data is to specify the fields of
interest after SELECT and specify the table with the FROM keyword. As we
did not add any filters—mostly known as conditions—to the query, we got
all the rows there. Sometimes, this is the desired behavior, but the
most common thing to do is to add conditions to the query to retrieve
only the rows that we need. Use the WHERE keyword to achieve this.

    mysql> SELECT firstname, surname, type FROM customer -> WHERE id =
    1; +-----------+---------+---------+ \| firstname \| surname \| type
    \| +-----------+---------+---------+ \| Han \| Solo \| premium \|
    +-----------+---------+---------+ 1 row in set (0.00 sec)

Adding conditions is very similar to when we created Boolean expressions
in PHP. We will specify the name of the field, an operator, and a value,
and MySQL will retrieve only the rows that return true to this
expression. In this case, we asked for the customers that had the ID 1,
and MySQL returned one row: the one that had an ID of exactly 1.

A common query would be to get the books that start with some text. We
cannot construct this expression with any comparison operand that you
know, such as = and < or >, since we want to match only a part of the
string. For this, MySQL has the LIKE operator, which takes a string that
can contain wildcards. A wildcard is a character that represents a rule,
matching any number of characters that follows the rule. For example,
the % wildcard represents any number of characters, so using the 1%
string would match any string that starts with 1 and is followed by any
number or characters, matching strings such as 1984 or 1Q84. Let's
consider the following example:

    mysql> SELECT title, author, price FROM book -> WHERE title LIKE
    "1%"; +------------+-----------------+-------+ \| title \| author \|
    price \| +------------+-----------------+-------+ \| 1984 \| George
    Orwell \| 7.5 \| \| 1Q84 \| Haruki Murakami \| 9.75 \| \| 19 minutes
    \| Jodi Picoult \| 10 \| +------------+-----------------+-------+ 3
    rows in set (0.00 sec)

We asked for all the books whose title starts with 1, and we got three
rows. You can imagine how useful this operator is, especially when we
implement a search utility in our application.

As in PHP, MySQL also allows you to add logical operators—that is,
operators that take operands and perform a logical operation, returning
Boolean values as a result. The most common logical operators are, as in
PHP, AND and OR. AND returns true if both the expressions are true and
OR returns true if either of the operands is true. Let's consider an
example, as follows:

    mysql> SELECT title, author, price FROM book -> WHERE title LIKE
    "1%" AND stock > 0; +------------+-----------------+-------+ \|
    title \| author \| price \| +------------+-----------------+-------+
    \| 1984 \| George Orwell \| 7.5 \| \| 1Q84 \| Haruki Murakami \|
    9.75 \| +------------+-----------------+-------+ 2 rows in set (0.00
    sec)

This example is very similar to the previous one, but we added an extra
condition. We asked for all titles starting with 1 and whether there is
stock available. This is why one of the books does not show as it does
not satisfy both conditions. You can add as many conditions as you need
with logical operators but bear in mind that AND operators take
precedence over OR. If you want to change this precedence, you can
always wrap expressions with a parenthesis, as in PHP.

So far, we have retrieved specific fields when querying for data, but we
could ask for all the fields in a given table. To do this, we will just
use the \* wildcard in SELECT. Let's select all the fields for the
customers via the following code:

    mysql> SELECT \* FROM customer \\G
    \*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\* 1. row
    \*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\* id: 1
    firstname: Han surname: Solo email: han@tatooine.com type: premium
    \*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\* 2. row
    \*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\* id: 2
    firstname: James surname: Kirk email: enter@prise type: basic 2 rows
    in set (0.00 sec)

You can retrieve more information than just fields. For example, you can
use COUNT to retrieve the amount of rows that satisfy the given
conditions instead of retrieving all the columns. This way is faster
than retrieving all the columns and then counting them because you save
time in reducing the size of the response. Let's consider how it would
look:

    mysql> SELECT COUNT(\*) FROM borrowed\_books -> WHERE customer\_id =
    1 AND end IS NOT NULL; +----------+ \| COUNT(\*) \| +----------+ \|
    1 \| +----------+ 1 row in set (0.00 sec)

As you can note, the response says 1, which means that there is only one
borrowed book that satisfies the conditions. However, check the
conditions; you will note that we used another familiar logical
operator: NOT. NOT negates the expression, as ! does in PHP. Note also
that we do not use the equal sign to compare with null values. In MySQL,
you have to use IS instead of the equals sign in order to compare with
NULL. So, the second condition would be satisfied when a borrowed book
has an end date that is not null.

Let's finish this section by adding two more features when querying
data. The first one is the ability to specify in what order the rows
should be returned. To do this, just use the keyword ORDER BY followed
by the name of the field that you want to order by. You could also
specify whether you want to order in ascending mode, which is by
default, or in the descending mode, which can be done by appending DESC.
The other feature is the ability to limit the amount of rows to return
using LIMIT and the amount of rows to retrieve. Now, run the following:

    mysql> SELECT id, title, author, isbn FROM book -> ORDER BY title
    LIMIT 4; +----+-------------+-----------------+---------------+ \|
    id \| title \| author \| isbn \|
    +----+-------------+-----------------+---------------+ \| 5 \| 19
    minutes \| Jodi Picoult \| 9780753179246 \| \| 1 \| 1984 \| George
    Orwell \| 9780882339726 \| \| 2 \| 1Q84 \| Haruki Murakami \|
    9789724621081 \| \| 3 \| Animal Farm \| George Orwell \|
    9780736692427 \|
    +----+-------------+-----------------+---------------+ 4 rows in set
    (0.00 sec)

Using PDO
---------

So far, we have worked with MySQL, and you already have a good idea of
what you can do with it. However, connecting to the client and
performing queries manually is not our goal. What we want to achieve is
that our application can take advantage of the database in an automatic
way. In order to do this, we will use a set of classes that comes with
PHP and allows you to connect to the database and perform queries from
the code.

PHP Data Objects (PDO) is the class that connects to the database and
allows you to interact with it. This is the popular way to work with
databases for PHP developers, even though there are other ways that we
will not discuss here. PDO allows you to work with different database
systems, so you are not tied to MySQL only. In the following sections,
we will consider how to connect to a database, insert data, and retrieve
it using this class.

Connecting to the database
~~~~~~~~~~~~~~~~~~~~~~~~~~

In order to connect to the database, it is good practice to keep the
credentials—that is, the user and password—separated from the code in a
configuration file. We already have this file as config/app.json from
when we worked with the Config class. Let's add the correct credentials
for our database. If you have the configuration by default, the
configuration file should look similar to this:

    { "db": { "user": "root", "password": "" } }

Developers usually specify other information related to the connection,
such as the host, port, or name of the database. This will depend on how
your application is installed, whether MySQL is running on a different
server, and so on, and it is up to you how much information you want to
keep on your code and in your configuration files.

In order to connect to the database, we need to instantiate an object
from the PDO class. The constructor of this class expects three
arguments: Data Source Name (DSN), which is a string that represents the
type of database to use; the name of the user; and the password. We
already have the username and password from the Config class, but we
still need to build DSN.

One of the formats for MySQL databases is <database
type>:host=<host>;dbname=<schema name>. As our database system is MySQL,
it runs on the same server, and the schema name is bookstore, DSN will
be mysql:host=127.0.0.1;dbname=bookstore. Let's take a look at how we
will put everything together:

    $dbConfig = Config::getInstance()->get('db'); $db = new PDO(
    'mysql:host=127.0.0.1;dbname=bookstore', $dbConfig['user'],
    $dbConfig['password'] );
    $db->setAttribute(PDO::ATTR\_DEFAULT\_FETCH\_MODE,
    PDO::FETCH\_ASSOC);

Note also that we will invoke the setAttribute method from the PDO
instance. This method allows you to set some options to the connection;
in this case, it sets the format of the results coming from MySQL. This
option forces MySQL to return the arrays whose keys are the names of the
fields, which is way more useful than the default one, returning numeric
keys based on the order of the fields. Setting this option now will
affect all the queries performed with the $db instance, rather than
setting the option each time we perform a query.

Performing queries
~~~~~~~~~~~~~~~~~~

The easiest way to retrieve data from your database is to use the query
method. This method accepts the query as a string and returns a list of
rows as arrays. Let's consider an example: write the following after the
initialization of the database connection—for example, in the init.php
file:

    $rows = $db->query('SELECT \* FROM book ORDER BY title'); foreach
    ($rows as $row) { var\_dump($row); }

This query tries to get all the books in the database, ordering them by
the title. This could be the content of a function such as getAllBooks,
which is used when we display our catalog. Each row is an array that
contains all the fields as keys and the data as values.

If you run the application on your browser, you will get the following
result:

|image27|

The query function is useful when we want to retrieve data, but in order
to execute queries that insert rows, PDO provides the exec function.
This function also expects the first parameter as a string, defining the
query to execute, but it returns a Boolean specifying whether the
execution was successful or not. A good example would be to try to
insert books. Type the following:

    $query = <<<SQL INSERT INTO book (isbn, title, author, price) VALUES
    ("9788187981954", "Peter Pan", "J. M. Barrie", 2.34) SQL; $result =
    $db->exec($query); var\_dump($result); // true

This code also uses a new way of representing strings: heredoc. We will
enclose the string between <<<SQL and SQL;, both in different lines,
instead of quotes. The benefit of this is the ability to write strings
in multiple lines with tabulations or any other blank space, and PHP
will respect it. We can construct queries that are easy to read rather
than writing them on a single line or having to concatenate the
different strings. Note that SQL is a token to represent the start and
end of the string, but you could use any text that you consider.

The first time you run the application with this code, the query will be
executed successfully, and thus, the result will be the Boolean true.
However, if you run it again, it will return false as the ISBN that we
inserted is the same but we set its restriction to be unique.

It is useful to know that a query failed, but it is better if we know
why. The PDO instance has the errorInfo method that returns an array
with the information of the last error. The key 2 contains the
description, so it is probably the one that we will use more often.
Update the previous code with the following:

    $query = <<<SQL INSERT INTO book (isbn, title, author, price) VALUES
    ("9788187981954", "Peter Pan", "J. M. Barrie", 2.34) SQL; $result =
    $db->exec($query); var\_dump($result); // false $error =
    $db->errorInfo()[2]; var\_dump($error); // Duplicate entry
    '9788187981954' for key 'isbn'

The result is that the query failed because the ISBN entry was
duplicated. Now, we can build more meaningful error messages for our
customers or just for debugging purposes.

Prepared statements
~~~~~~~~~~~~~~~~~~~

The previous two functions are very useful when you need to run quick
queries that are always the same. However, in the second example you
might note that the string of the query is not very useful as it always
inserts the same book. Although it is true that you could just replace
the values by variables, it is not good practice as these variables
usually come from the user side and can contain malicious code. It is
always better to first sanitize these values.

PDO provides the ability to prepare a statement—that is, a query that is
parameterized. You can specify parameters for the fields that will
change in the query and then assign values to these parameters. Let's
consider first an example, as follows:

    $query = 'SELECT \* FROM book WHERE author = :author'; $statement =
    $db->prepare($query); $statement->bindValue('author', 'George
    Orwell'); $statement->execute(); $rows = $statement->fetchAll();
    var\_dump($rows);

The query is a normal one except that it has :author instead of the
string of the author that we want to find. This is a parameter, and we
will identify them using the prefix :. The prepare method gets the query
as an argument and returns a PDOStatement instance. This class contains
several methods to bind values, execute statements, fetch results, and
more. In this piece of code, we use only three of them, as follows:

-  bindValue: This takes two arguments: the name of the parameter as
   described in the query and the value to assign. If you provide a
   parameter name that is not in the query, this will throw an
   exception.

-  execute: This will send the query to MySQL with the replacement of
   the parameters by the provided values. If there is any parameter that
   is not assigned to a value, the method will throw an exception. As
   its brother exec, execute will return a Boolean, specifying whether
   the query was executed successfully or not.

-  fetchAll: This will retrieve the data from MySQL in case it was a
   SELECT query. As a query, fetchAll will return a list of all rows as
   arrays.

If you try this code, you will note that the result is very similar to
when using query; however, this time, the code is much more dynamic as
you can reuse it for any author that you need.

|image28|

There is another way to bind values to parameters of a query than using
the bindValue method. You could prepare an array where the key is the
name of the parameter and the value is the value you want to assign to
it, and then you can send it as the first argument of the execute
method. This way is quite useful as usually you already have this array
prepared and do not need to call bindValue several times with its
content. Add this code in order to test it:

    $query = <<<SQL INSERT INTO book (isbn, title, author, price) VALUES
    (:isbn, :title, :author, :price) SQL; $statement =
    $db->prepare($query); $params = [ 'isbn' => '9781412108614', 'title'
    => 'Iliad', 'author' => 'Homer', 'price' => 9.25 ];
    $statement->execute($params); echo $db->lastInsertId(); // 8

In this last example, we created a new book with almost all the
parameters, but we did not specify the ID, which is the desired behavior
as we want MySQL to choose a valid one for us. However, what happens if
you want to know the ID of the inserted row? Well, you could query MySQL
for the book with the same ISBN and the returned row would contain the
ID, but this seems like a lot of work. Instead, PDO has the lastInsertId
method, which returns the last ID inserted by a primary key, saving us
from one extra query.

Joining tables
--------------

Even though querying MySQL is quite fast, especially if it is in the
same server as our PHP application, we should try to reduce the number
of queries that we will execute to improve the performance of our
application. So far, we have queried data from just one table, but this
is rarely the case. Imagine that you want to retrieve information about
borrowed books: the table contains only IDs and dates, so if you query
it, you will not get very meaningful data, right? One approach would be
to query the data in borrowed\_books, and based on the returning IDs,
query the book and customer tables by filtering by the IDs we are
interested in. However, this approach consists of at least three queries
to MySQL and a lot of work with arrays in PHP. It seems as though there
should be a better option!

In SQL, you can execute join queries. A join query is a query that joins
two or more tables through a common field and, thus, allows you to
retrieve data from these tables, reducing the amount of queries needed.
Of course, the performance of a join query is not as good as the
performance of a normal query, but if you have the correct keys and
relationships defined, this option is way better than querying
separately.

In order to join tables, you need to link them using a common field.
Foreign keys are very useful in this matter as you know that both the
fields are the same. Let's take a look at how we would query for all the
important info related to the borrowed books:

    mysql> SELECT CONCAT(c.firstname, ' ', c.surname) AS name, ->
    b.title, -> b.author, -> DATE\_FORMAT(bb.start, '%d-%m-%y') AS
    start, -> DATE\_FORMAT(bb.end, '%d-%m-%y') AS end -> FROM
    borrowed\_books bb -> LEFT JOIN customer c ON bb.customer\_id = c.id
    -> LEFT JOIN book b ON b.id = bb.book\_id -> WHERE bb.start >=
    "2015-01-01";
    +------------+---------+---------------+----------+----------+ \|
    name \| title \| author \| start \| end \|
    +------------+---------+---------------+----------+----------+ \|
    Han Solo \| Dracula \| Bram Stoker \| 10-01-15 \| 13-01-15 \| \|
    James Kirk \| Dracula \| Bram Stoker \| 01-02-15 \| 10-02-15 \| \|
    James Kirk \| 1984 \| George Orwell \| 12-03-15 \| NULL \|
    +------------+---------+---------------+----------+----------+ 3
    rows in set (0.00 sec)

There are several new concepts introduced in this last query. Especially
with joining queries, as we joined the fields of different tables, it
might occur that two tables have the same field name, and MySQL needs us
to differentiate them. The way we will differentiate two fields of two
different tables is by prepending the name of the table. Imagine that we
want to differentiate the ID of a customer from the ID of the book; we
should use them as customer.id and book.id. However, writing the name of
the table each time would make our queries endless.

MySQL has the ability to add an alias to a table by just writing next to
the table's real name, as we did in borrowed\_books (bb), customer (c)
or book (b). Once you add an alias, you can use it to reference this
table, allowing us to write things such as bb.customer\_id instead of
borrowed\_books.customer\_id. It is also good practice to write the
table of the field even if the field is not duplicated anywhere else as
joining tables makes it a bit confusing to know where each field comes
from.

When joining tables, you need to write them in the FROM clause using
LEFT JOIN, followed by the name of the table, an optional alias, and the
fields that connect both tables. There are different joining types, but
let's focus on the most useful for our purposes. Left joins take each
row from the first table—the one on the left-hand side of the
definition—and search for the equivalent field in the right-hand side
table. Once it finds it, it will concatenate both rows as if they were
one. For example, when joining borrowed\_books with customer for each
borrowed\_books row, MySQL will search for an ID in customer that
matches the current customer\_id, and then it will add all the
information of this row in our current row in borrowed\_books as if they
were only one big table. As customer\_id is a foreign key, we are
certain that there will always be a customer to match.

You can join several tables, and MySQL will just resolve them from left
to right; that is, it will first join the two first tables as one, then
try to join this resulting one with the third table, and so on. This is,
in fact, what we did in our example: we first joined borrowed\_books
with customer and then joined these two with book.

As you can note, there are also aliases for fields. Sometimes, we do
more than just getting a field; an example was when we got how many rows
a query matched with COUNT(\*). However, the title of the column when
retrieving this information was also COUNT(\*), which is not always
useful. At other times, we used two tables with colliding field names,
and it makes everything confusing. When this happens, just add an alias
to the field in the same way we did with table names; AS is optional,
but it helps to understand what you are doing.

Let's move now to the usage of dates in this query. On one hand, we will
use DATE\_FORMAT for the first time. It accepts the date/time/datetime
value and the string with the format. In this case, we used %d-%m-%y,
which means day-month-year, but we could use %h-%i-%s to specify
hours-minutes-seconds or any other combination.

Note also how we compared dates in the WHERE clause. Given two dates or
time values of the same type, you can use the comparison operators as if
they were numbers. In this case, we will do bb.start >= "2015-01-01",
which will give us the borrowed books from January 1, 2015, onward.

The final thing to note about this complex query is the use of the
CONCAT function. Instead of returning two fields, one for the name and
one for the surname, we want to get the full name. To do this, we will
concatenate the fields using this function, sending as many strings as
we want as arguments of the function and getting back the concatenated
string. As you can see, you can send both fields and strings enclosed by
single quotes.

Well, if you fully understood this query, you should feel satisfied with
yourself; this was the most complex query we will see in this chapter.
We hope you can get a sense of how powerful a database system can be and
that from now on, you will try to process the data as much as you can on
the database side instead of the PHP side. If you set the correct
indexes, it will perform better.

Grouping queries
----------------

The last feature that we will discuss about querying is the GROUP BY
clause. This clause allows you to group rows of the same table with a
common field. For example, let's say we want to know how many books each
author has in just one query. Try the following:

    mysql> SELECT -> author, -> COUNT(\*) AS amount, ->
    GROUP\_CONCAT(title SEPARATOR ', ') AS titles -> FROM book -> GROUP
    BY author -> ORDER BY amount DESC, author;
    +-----------------+--------+-------------------+ \| author \| amount
    \| titles \| +-----------------+--------+-------------------+ \|
    George Orwell \| 2 \| 1984, Animal Farm \| \| Homer \| 2 \| Odyssey,
    Iliad \| \| Bram Stoker \| 1 \| Dracula \| \| Haruki Murakami \| 1
    \| 1Q84 \| \| J. M. Barrie \| 1 \| Peter Pan \| \| Jodi Picoult \| 1
    \| 19 minutes \| +-----------------+--------+-------------------+ 5
    rows in set (0.00 sec)

The GROUP BY clause, always after the WHERE clause, gets a field—or
many, separated by a coma—and treats all the rows with the same value
for this field, as though they were just one. Thus, selecting by author
will group all the rows that contain the same author. The feature might
not seem very useful, but there are several functions in MySQL that take
advantage of it. In this example:

-  COUNT(\*) is used in queries with GROUP BY and shows how many rows
   this field groups. In this case, we will use it to know how many
   books each author has. In fact, it always works like this; however,
   for queries without GROUP BY, MySQL treats the whole set of rows as
   one group.

-  GROUP\_CONCAT is similar to CONCAT, which we discussed earlier. The
   only difference is that this time the function will concatenate the
   fields of all the rows of a group. If you do not specify SEPARATOR,
   MySQL will use a single coma. However, in our case, we needed a coma
   and a space to make it readable, so we added SEPARATOR ', ' at the
   end. Note that you can add as many things to concatenate as you need
   in CONCAT, the separator will just separate the concatenations by
   rows.

Even though it is not about grouping, note the ORDER clause that we
added. We ordered by two fields instead of one. This means that MySQL
will order all the rows by the amount field; note that this is an alias,
but you can use it here as well. Then, MySQL will order each group of
rows with the same amount value by the title field.

There is one last thing to remember as we already presented all the
important clauses that a SELECT query can contain: MySQL expects the
clauses of the query to be always in the same order. If you write the
same query but change this order, you will get an error. The order is as
follows:

1. SELECT

2. FROM

3. WHERE

4. GROUP BY

5. ORDER BY

Updating and deleting data
--------------------------

We already know quite a lot about inserting and retrieving data, but if
applications could only do this, they would be quite static. Editing
this data as we need is what makes an application dynamic and what gives
to the user some value. In MySQL, and in most database systems, you have
two commands to change data: UPDATE and DELETE. Let's discuss them in
detail.

Updating data
~~~~~~~~~~~~~

When updating data in MySQL, the most important thing is to have a
unique reference of the row that you want to update. For this, primary
keys are very useful; however, if you have a table with no primary keys,
which should not be the case most of the time, you can still update the
rows based on other fields. Other than the reference, you will need the
new value and, of course, the table name and field to update. Let's take
a look at a very simple example:

    mysql> UPDATE book SET price = 12.75 WHERE id = 2; Query OK, 1 row
    affected (0.00 sec) Rows matched: 1 Changed: 1 Warnings: 0

In this UPDATE query, we set the price of the book with the ID 2 to
12.75. The SET clause does not need to specify only one change; you can
specify several changes on the same row as soon as you separate them by
commas—for example, SET price = 12.75, stock = 14. Also, note the WHERE
clause, in which we specify which rows we want to change. MySQL gets all
the rows of this table based on these conditions as though it were a
SELECT query and apply the change to this set of rows.

What MySQL will return is very important: the number of rows matched and
the number of rows changed. The first one is the number of rows that
match the conditions in the WHERE clause. The second one specifies the
amount of rows that can be changed. There are different reasons not to
change a row—for example when the row already has the same value. To see
this, let's run the same query again:

    mysql> UPDATE book SET price = 12.75 WHERE id = 2; Query OK, 0 rows
    affected (0.00 sec) Rows matched: 1 Changed: 0 Warnings: 0

The same row now says that there was 1 row matched, as expected, but 0
rows were changed. The reason is that we already set the price of this
book to 12.75, so MySQL does not need to do anything about this now.

As mentioned before, the WHERE clause is the most important bit in this
query. Way too many times, we find developers that run a priori innocent
UPDATE queries end up changing the whole table because they miss the
WHERE clause; thus, MySQL matches the whole table as valid rows to
update. This is usually not the intention of the developer, and it is
something not very pleasant, so try to make sure you always provide a
valid set of conditions. It is good practice to first write down the
SELECT query that returns the rows you need to edit, and once you are
sure that the conditions match the desired set of rows, you can write
the UPDATE query.

However, sometimes, affecting multiple rows is the intended scenario.
Imagine that we are going through tough times and need to increase the
price of all our books. We decide that we want to increase the price by
16%, which is the same as the current price times 1.16. We can run the
following query to perform these changes:

    mysql> UPDATE book SET price = price \* 1.16; Query OK, 8 rows
    affected (0.00 sec) Rows matched: 8 Changed: 8 Warnings: 0

This query does not contain any WHERE clause as we want to match all our
books. Also note that the SET clause uses the price field to get the
current value for the price, which is perfectly valid. Finally, note the
number of rows matched and changed, which is 8—the whole set of rows for
this table.

To finish with this subsection, let's consider how we can use UPDATE
queries from PHP through PDO. One very common scenario is when we want
to add copies of the already existing books to our inventory. Given a
book ID and an optional amount of books—by default, this value will be
1—we will increase the stock value of this book by these many copies.
Write this function in your init.php file:

    function addBook(int $id, int $amount = 1): void { $db = new PDO(
    'mysql:host=127.0.0.1;dbname=bookstore', 'root', '' ); $query =
    'UPDATE book SET stock = stock + :n WHERE id = :id'; $statement =
    $db->prepare($query); $statement->bindValue('id', $id);
    $statement->bindValue('n', $amount); if (!$statement->execute()) {
    throw new Exception($statement->errorInfo()[2]); } }

There are two arguments: $id and $amount. The first one will always be
mandatory, whereas the second one can be omitted, and the default value
will be 1. The function first prepares a query similar to the first one
of this section, in which we increased the amount of stock of a given
book, then binds both parameters to the statement, and finally executes
the query. If something happens and execute returns false, we will throw
an exception with the content of the error message from MySQL.

This function is very useful when we either buy more stock or a customer
returns a book. We could even use it to remove books by providing a
negative value to $amount, but this is very bad practice. The reason is
that even if we forced the stock field to be unsigned, setting it to a
negative value will not trigger any error, only a warning. MySQL will
not set the row to a negative value, but the execute invocation will
return true, and we will not know about it. It is better to just create
a second method, removeBook, and verify first that the amount of books
to remove is lower than or equal to the current stock.

Foreign key behaviors
~~~~~~~~~~~~~~~~~~~~~

One tricky thing to manage when updating or deleting rows is when the
row that we update is part of a foreign key somewhere else. For example,
our borrowed\_books table contains the IDs of customers and books, and
as you already know, MySQL enforces that these IDs are always valid and
exist on these respective tables. What would happen, then, if we changed
the ID of the book itself on the book table? Or even worse, what would
happen if we removed one of the books from book, and there is a row in
borrowed\_books that references this ID?

MySQL allows you to set the desired reaction when one of these scenarios
takes place. It has to be defined when adding the foreign key; so, in
our case, we will need to first remove the existing ones and then add
them again. To remove or drop a key, you need to know the name of this
key, which we can find using the SHOW CREATE TABLE command, as follows:

    mysql> SHOW CREATE TABLE borrowed\_books \\G
    \*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\* 1. row
    \*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\* Table:
    borrowed\_books Create Table: CREATE TABLE \`borrowed\_books\` (
    \`book\_id\` int(10) unsigned NOT NULL, \`customer\_id\` int(10)
    unsigned NOT NULL, \`start\` datetime NOT NULL, \`end\` datetime
    DEFAULT NULL, KEY \`book\_id\` (\`book\_id\`), KEY \`customer\_id\`
    (\`customer\_id\`), CONSTRAINT \`borrowed\_books\_ibfk\_1\` FOREIGN
    KEY (\`book\_id\`) REFERENCES \`book\` (\`id\`), CONSTRAINT
    \`borrowed\_books\_ibfk\_2\` FOREIGN KEY (\`customer\_id\`)
    REFERENCES \`customer\` (\`id\`) ) ENGINE=InnoDB DEFAULT
    CHARSET=latin1 1 row in set (0.00 sec)

The two foreign keys that we want to remove are borrowed\_books\_ibfk\_1
and borrowed\_books\_ibfk\_2. Let's remove them using the ALTER TABLE
command, as we did before:

    mysql> ALTER TABLE borrowed\_books -> DROP FOREIGN KEY
    borrowed\_books\_ibfk\_1; Query OK, 4 rows affected (0.02 sec)
    Records: 4 Duplicates: 0 Warnings: 0 mysql> ALTER TABLE
    borrowed\_books -> DROP FOREIGN KEY borrowed\_books\_ibfk\_2; Query
    OK, 4 rows affected (0.01 sec) Records: 4 Duplicates: 0 Warnings: 0

Now, we need to add the foreign keys again. The format of the command
will be the same as when we added them, but appending the new desired
behavior. In our case, if we remove a customer or book from our tables,
we want to remove the rows referencing these books and customers from
borrowed\_books; so, we need to use the CASCADE option. Let's consider
what they would look like:

    mysql> ALTER TABLE borrowed\_books -> ADD FOREIGN KEY (book\_id)
    REFERENCES book (id) -> ON DELETE CASCADE ON UPDATE CASCADE, -> ADD
    FOREIGN KEY (customer\_id) REFERENCES customer (id) -> ON DELETE
    CASCADE ON UPDATE CASCADE; Query OK, 4 rows affected (0.01 sec)
    Records: 4 Duplicates: 0 Warnings: 0

Note that we can define the CASCADE behavior for both actions: when
updating and when deleting rows. There are other options instead of
CASCADE—for example SET NULL, which sets the foreign keys columns to
NULL and allows the original row to be deleted, or the default one,
RESTRICT, which rejects the update/delete commands.

Deleting data
~~~~~~~~~~~~~

Deleting data is almost the same as updating it. You need to provide a
WHERE clause that will match the rows that you want to delete. Also, as
with when updating data, it is highly recommended to first build the
SELECT query that will retrieve the rows that you want to delete before
performing the DELETE command. Do not think that you are wasting time
with this methodology; as the saying goes, measure twice, cut once. Not
always is it possible to recover data after deleting rows!

Let's try to delete a book by observing how the CASCADE option we set
earlier behaves. For this, let's first query for the existing borrowed
books list via the following:

    mysql> SELECT book\_id, customer\_id FROM borrowed\_books;
    +---------+-------------+ \| book\_id \| customer\_id \|
    +---------+-------------+ \| 1 \| 1 \| \| 4 \| 1 \| \| 4 \| 2 \| \|
    1 \| 2 \| +---------+-------------+ 4 rows in set (0.00 sec)

There are two different books, 1 and 4, with each of them borrowed
twice. Let's try to delete the book with the ID 4. First, build a query
such as SELECT \* FROM book WHERE id = 4 to make sure that the condition
in the WHERE clause is the appropriate one. Once you are sure, perform
the following query:

    mysql> DELETE FROM book WHERE id = 4; Query OK, 1 row affected (0.02
    sec)

As you can note, we only specified the DELETE FROM command followed by
the name of the table and the WHERE clause. MySQL tells us that there
was 1 row affected, which makes sense, given the previous SELECT
statement we made.

If we go back to our borrowed\_books table and query for the existing
ones, we will note that all the rows referencing the book with the ID 4
are gone. This is because when deleting them from the book table, MySQL
noticed the foreign key reference, checked what it needed to do while
deleting—in this case, CASCADE—and deleted also the rows in
borrowed\_books. Take a look at the following:

    mysql> SELECT book\_id, customer\_id FROM borrowed\_books;
    +---------+-------------+ \| book\_id \| customer\_id \|
    +---------+-------------+ \| 1 \| 1 \| \| 1 \| 2 \|
    +---------+-------------+ 2 rows in set (0.00 sec)

Working with transactions
-------------------------

In the previous section, we reiterated how important it is to make sure
that an update or delete query contain the desirable matching set of
rows. Even though this will always apply, there is a way to revert the
changes that you just made, which is working with transactions.

A transaction is a state where MySQL keeps track of all the changes that
you make in your data in order to be able to revert all of them if
needed. You need to explicitly start a transaction, and before you close
the connection to the server, you need to commit your changes. This
means that MySQL does not really perform these changes until you tell it
to do so. If during a transaction you want to revert the changes, you
should roll back instead of making a commit.

PDO allows you to do this with three functions:

-  beginTransaction: This will start the transaction.

-  commit: This will commit your changes. Keep in mind that if you do
   not commit and the PHP script finishes or you close the connection
   explicitly, MySQL will reject all the changes you made during this
   transaction.

-  rollBack: This will roll back all the changes that were made during
   this transaction.

One possible use of transactions in your application is when you need to
perform multiple queries and all of them have to be successful and the
whole set of queries should not be performed otherwise. This would be
the case when adding a sale into the database. Remember that our sales
are stored in two tables: one for the sale itself and one for the list
of books related to this sale. When adding a new one, you need to make
sure that all the books are added to this database; otherwise, the sale
will be corrupted. What you should do is execute all the queries,
checking for their returning values. If any of them returns false, the
whole sale should be rolled back.

Let's create an addSale function in your init.php file in order to
emulate this behavior. The content should be as follows:

    function addSale(int $userId, array $bookIds): void { $db = new PDO(
    'mysql:host=127.0.0.1;dbname=bookstore', 'root', '' );
    $db->beginTransaction(); try { $query = 'INSERT INTO sale
    (customer\_id, date) ' . 'VALUES(:id, NOW())'; $statement =
    $db->prepare($query); if (!$statement->execute(['id' => $userId])) {
    throw new Exception($statement->errorInfo()[2]); } $saleId =
    $db->lastInsertId(); $query = 'INSERT INTO sale\_book (book\_id,
    sale\_id) ' . 'VALUES(:book, :sale)'; $statement =
    $db->prepare($query); $statement->bindValue('sale', $saleId);
    foreach ($bookIds as $bookId) { $statement->bindValue('book',
    $bookId); if (!$statement->execute()) { throw new
    Exception($statement->errorInfo()[2]); } } $db->commit(); } catch
    (Exception $e) { $db->rollBack(); throw $e; } }

This function is quite complex. It gets as arguments the ID of the
customer and the list of books as we assume that the date of the sale is
the current date. The first thing we will do is connect to the database,
instantiating the PDO class. Right after this, we will begin our
transaction, which will last only during the course of this function.
Once we begin the transaction, we will open a try…catch block that will
enclose the rest of the code of the function. The reason is that if we
throw an exception, the catch block will capture it, rolling back the
transaction and propagating the exception. The code inside the try block
just adds first the sale and then iterates the list of books, inserting
them into the database too. At all times, we will check the response of
the execute function, and if it's false, we will throw an exception with
the information of the error.

Let's try to use this function. Write the following code that tries to
add a sale for three books; however, one of them does not exist, which
is the one with the ID 200:

    try { addSale(1, [1, 2, 200]); } catch (Exception $e) { echo 'Error
    adding sale: ' . $e->getMessage(); }

This code will echo the error message, complaining about the nonexistent
book. If you check in MySQL, there will be no rows in the sales table as
the function rolled back when the exception was thrown.

Finally, let's try the following code instead. This one will add three
valid books so that the queries are always successful and the try block
can go until the end, where we will commit the changes:

    try { addSale(1, [1, 2, 3]); } catch (Exception $e) { echo 'Error
    adding sale: ' . $e->getMessage(); }

Test it, and you will see how there is no message printed on your
browser. Then, go to your database to make sure that there is a new
sales row and there are three books linked to it.

Summary
-------

In this chapter, we learned the importance of databases and how to use
them from our web application: from setting up the connection using PDO
and creating and fetching data on demand to constructing more complex
queries that fulfill our needs. With all of this, our application looks
way more useful now than when it was completely static.

In the next chapter, we will discover how to apply the most important
design patterns for web applications through Model View Controller
(MVC). You will gain a sense of clarity in your code when you organize
your application in this way.