.. include:: images.txt


Chapter 9. Building REST APIs
=============================

Most non-developers probably think that creating applications means
building either software for your PC or Mac, games, or web pages,
because that is what they can see and use. But once you join the
developers' community, either by your own or professionally, you will
eventually realize how much work is done for applications and tools that
do not have a user interface.

Have you ever wondered how someone's website can access your Facebook
profile, and later on, post an automatic message on your wall? Or how
websites manage to send/receive information in order to update the
content of the page, without refreshing or submitting any form? All of
these features, and many more interesting ones, are possible thanks to
the integration of applications working "behind the scenes". Knowing how
to use them will open the doors for creating more interesting and useful
web applications.

In this chapter, you will learn the following:

-  Introduction to APIs and REST APIs, and their use

-  The foundation of REST APIs

-  Using third-party APIs

-  Tools for REST API developers

-  Designing and writing REST APIs with Laravel

-  Different ways of testing your REST APIs

Introducing APIs
----------------

API stands for Application Program Interface. Its goal is to provide an
interface so that other programs can send commands that will trigger
some process inside the application, possibly returning some output. The
concept might seem a bit abstract, but in fact, there are APIs virtually
in everything which is somehow related to computers. Let's see some real
life examples:

-  Operating systems or OS, like Windows or Linux, are the programs that
   allow you to use computers. When you use any application from your
   computer, it most probably needs to talk to the OS in one way or
   another, for example by requesting a certain file, sending some audio
   to the speakers, and so on. All these interactions between the
   application and the OS are possible thanks to the APIs that the OS
   provides. In this way, the application need not interact with the
   hardware straight away, which is a very tiring task.

-  To interact with the user, a mobile application provides a GUI. The
   interface captures all the events that the user triggers, like
   clicking or typing, in order to send them to the server. The GUI
   communicates with the server using an API in the same way the program
   communicates with the OS as explained earlier.

-  When you create a website that needs to display tweets from the
   user's Twitter account, you need to communicate with Twitter. They
   provide an API that can be accessed via HTTP. Once authenticated, by
   sending the correct HTTP requests, you can update and/or retrieve
   data from their application.

As you can see, there are different places where APIs are useful. In
general, when you have a system that should be accessed externally, you
need to provide potential users an API. When we say externally, we mean
from another application or library, but it can very well be inside the
same machine.

Introducing REST APIs
---------------------

REST APIs are a specific type of APIs. They use HTTP as the protocol to
communicate with them, so you can imagine that they will be the most
used ones by web applications. In fact, they are not very different from
the websites that you've already built, since the client sends an HTTP
request, and the server replies with an HTTP response. The difference
here is that REST APIs make heavy use of HTTP status codes to understand
what the response is, and instead of returning HTML resources with CSS
and JS, the response uses JSON, XML, or any other document format with
just information, and not a graphic user interface.

Let's take an example. The Twitter API, once authenticated, allows
developers to get the tweets of a given user by sending an HTTP GET
request to https://api.twitter.com/1.1/statuses/user\_timeline.json. The
response to this request is an HTTP message with a JSON map of tweets as
the body and the status code 200. We've already mentioned status code in
`*Chapter 2* <#Top_of_ch02_html>`__, *Web Applications with PHP*, but we
will review them shortly.

The REST API also allows developers to post tweets on behalf of the
user. If you were already authenticated, as in the previous example, you
just need to send a POST request to
https://api.twitter.com/1.1/statuses/update.json with the appropriate
POST parameters in the body, like the text that you want to tweet. Even
though this request is not a GET, and thus, you are not requesting data
but rather sending it, the response of this request is quite important
too. The server will use the status codes of the response to let the
requester know if the tweet was posted successfully, or if they could
not understand the request, there was an internal server error, the
authentication was not valid, and so on. Each of these scenarios has a
different status code, which is the same across all applications. This
makes it very easy to communicate with different APIs, since you will
not need to learn a new list of status code each time. The server can
also add some extra information to the body in order to throw some light
on why the error happened, but that will depend on the application.

You can imagine that these REST APIs are provided to developers so they
can integrate them with their applications. They are not user-friendly,
but HTTP-friendly.

The foundations of REST APIs
----------------------------

Even though REST APIs do not have an official standard, most developers
agree on the same foundation. It helps that HTTP, which is the protocol
that this technology uses to communicate, does have a standard. In this
section, we will try to describe how REST APIs should work.

HTTP request methods
~~~~~~~~~~~~~~~~~~~~

We've already introduced the idea of HTTP methods in `*Chapter
2* <#Top_of_ch02_html>`__, *Web Applications with PHP*. We explained
that an HTTP method is just the verb of the request, which defines what
kind of action it is trying to perform. We've already defined this
method when working with HTML forms: the form tag can get an optional
attribute, method, which will make the form submit with that specific
HTTP method.

You will not use forms when working with REST APIs, but you can still
specify the method of the request. In fact, two requests can go to the
same endpoint with the same parameters, headers, and so on, and yet have
completely different behaviors due to their methods, which makes them a
very important part of the request.

As we are giving so much importance to HTTP methods in order to identify
what a request is trying to do, it is natural that we will need a
handful of them. So far, we have introduced GET and POST, but there are
actually eight different methods: GET, POST, PUT, DELETE, OPTIONS, HEAD,
TRACE, and CONNECT. You will usually work with just four of them. Let's
look at them in detail.

GET
+++

When a request uses the GET method, it means that it is requesting for
information about a given entity. The endpoint should contain
information of what that entity is, like the ID of a book. GET can also
be used to query for a list of objects, either all of them, filtered, or
paginated.

GET requests can add extra information to the request when needed. For
example, if we are try to retrieve all the books that contain the string
"rings", or if we want the page number 2 of the full list of books. As
you already know, this extra information is added to the query string as
GET parameters, which is a list of key-value pairs concatenated by an
ampersand (&). So, that means that the request for
http://bookstore.com/books?year=2001&page3 is probably used for getting
the second page of the list of books published during 2001.

REST APIs have extensive documentation on the available endpoints and
parameters, so it should be easy for you to learn to query properly.
Still, even though it will be documented, you should expect parameters
with intuitive names, like the ones in the example.

POST and PUT
++++++++++++

POST is the second type of HTTP method that you already know about. You
used it in forms with the intention of "posting" data, that is, trying
to update a resource on the server side. When you wanted to add or
update a new book, you sent a POST request with the data of the book as
the POST parameters.

POST parameters are sent in a format similar to the GET parameters, but
instead of being part of the query string, they are included as part of
the request's body. Forms in HTML are already doing that for you, but
when you need to talk to a REST API, you should know how to do this by
yourself. In the next section, we will show you how to perform POST
using tools other than forms. Also note that you can add any data to the
body of the request; it is quite common to send JSON in the body instead
of POST parameters.

The PUT method is quite similar to the POST method. This too tries to
add or update data on the server side, and for this purpose, it also
adds extra information on the body of the request. Why should we have
two different methods that do the same thing? There are actually two
main differences between these methods:

-  PUT requests either create a resource or update it, but the affected
   resource is the one defined by the endpoint and nothing else. That
   means that if we want to update a book, the endpoint should state
   that the resource is a book, and specify it, for example,
   http://bookstore.com/books/8734. On the other hand, if you do not
   identify the resource to be created or updated in the endpoint, or
   you affect other resources at the same time, you should use POST
   requests.

-  Idempotent is a complicated word for a simple concept. An idempotent
   HTTP method is one that can be called many times, and the result will
   always be the same. For example, if you are trying to update the
   title of a book to "Don Quixote", it does not matter how many times
   you call it, the result will always be the same: the resource will
   have the title "Don Quixote". On the other hand, non-idempotent
   methods might return different results when executing the same
   request. An example could be an endpoint that increases the stock of
   some book. Each time you call it, you will increase the stock more
   and more, and thus, the result is not the same. PUT requests are
   idempotent, whereas POST requests are not.

Even with this explanation in mind, misusing POST and PUT is quite a
common mistake among developers, especially when they lack enough
experience in developing REST APIs. Since forms in HTML only send data
with POST and not PUT, the first one is more popular. You might find
REST APIs where all the endpoints that update data are POST, even though
some of them should be PUT.

DELETE
++++++

The DELETE HTTP method is quite self-explanatory. It is used when you
want to delete a resource on the server. As with PUT requests, DELETE
endpoints should identify the specific resource to be deleted. An
example would be when we want to remove one book from our database. We
could send a DELETE request to an endpoint similar to
http://bookstore.com/books/23942.

DELETE requests just delete resources, and they are already determined
by the URL. Still, if you need to send extra information to the server,
you could use the body of the request as you do with POST or PUT. In
fact, you can always send information within the body of the request,
including GET requests, but that does not mean it is a good practice to
do so.

Status codes in responses
~~~~~~~~~~~~~~~~~~~~~~~~~

If HTTP methods are very important for requests, status codes are almost
indispensable for responses. With just one number, the client will know
what happened with the request. This is especially useful when you know
that status codes are a standard, and they are extensively documented on
the Internet.

We've already described the most important ones in `*Chapter
2* <#Top_of_ch02_html>`__, *Web Applications with PHP*, but let's list
them again, adding a few more that are important for REST APIs. For the
full list of status codes, you can visit
`*https://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html* <https://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html>`__.

2xx – success
+++++++++++++

All the status codes that start with 2 are used for responses where the
request was processed successfully, regardless of whether it was a GET
or POST. Some of the most commonly used ones in this category are as
follows:

-  200 OK: It is the generic "everything was OK" response. If you were
   asking for a resource, you will get it in the body of the response,
   and if you were updating a resource, this will mean that the new data
   has been successfully saved.

-  201 created: It is the response used when resources are created
   successfully with POST or PUT.

-  202 accepted: This response means that the request has been accepted,
   but it has not been processed yet. This might be useful when the
   client needs a straightforward response for a very heavy operation:
   the server sends the accepted response, and then starts processing
   it.

3xx – redirection
+++++++++++++++++

Even though you might think there is only one type of redirection, there
are a few refinements:

-  301 moved permanently: This means that the resource has been moved to
   a different URL, so from then on, you should try to access it through
   the URL provided in the body of the response.

-  303 see other: This means that the request has been processed but, in
   order to see the response, you need to access the URL provided in the
   body of the response.

4xx – client error
++++++++++++++++++

This category has status codes describing what went wrong due to the
client's request:

-  400 bad request: This is a generic response to a malformed request,
   that is, there is a syntax error in the endpoint, or some of the
   expected parameters were not provided.

-  401 unauthorized: This means the client has not been authenticated
   successfully yet, and the resource that it is trying to access needs
   this authentication.

-  403 forbidden: This error message means that even though the client
   has been authenticated, it does not have enough permissions to access
   that resource.

-  404 not found: The specific resource has not been found.

-  405 method not allowed: This means that the endpoint exists, but it
   does not accept the HTTP method used on the request, for example, we
   were trying to use PUT, but the endpoint only accepts POST requests.

5xx – server error
++++++++++++++++++

There are up to 11 different errors on the server side, but we are only
interested in one: the 500 internal server error. You could use this
status code when something unexpected, like a database error, happens
while processing the request.

REST API security
~~~~~~~~~~~~~~~~~

REST APIs are a powerful tool since they allow developers to retrieve
and/or update data from the server. But with great power comes great
responsibility, and when designing a REST API, you should think about
making your data as secure as possible. Imagine— anyone could post
tweets on your behalf with a simple HTTP request!

Similar to using web applications, there are two concepts here:
authentication and authorization. Authenticating someone is identifying
who he or she is, that is, linking his or her request to a user in the
database. On the other hand, authorizing someone is to allow that
specific user to perform certain actions. You could think of
authentication as the login of the user, and authorization as giving
permissions.

REST APIs need to manage these two concepts very carefully. Just because
a developer has been authenticated does not mean he can access all the
data on the server. Sometimes, users can access only their own data,
whereas sometimes you would like to implement a roles system where each
role has different access levels. It always depends on the type of
application you are building.

Although authorization happens on the server side, that is, it's the
server's database that will decide whether a given user can access a
certain resource or not, authentications have to be triggered by the
client. This means that the client has to know what authentication
system the REST API is using in order to proceed with the
authentication. Each REST API will implement its own authentication
system, but there are some well known implementations.

Basic access authentication
+++++++++++++++++++++++++++

Basic access authentication—BA for short—is, as its name suggests,
basic. The client adds the information about the user in the headers of
each request, that is, username and password. The problem is that this
information is only encoded using BASE64 but not encrypted, making it
extremely easy for an intruder to decode the header and obtain the
password in plain text. If you ever have to use it, since, to be honest,
it is a very easy way of implementing some sort of authentication, we
would recommend you to use it with HTTPS.

In order to use this method, you need to concatenate the username and
password like username:password, encode the resultant string using
Base64, and add the authorization header as:

    Authorization: Basic <encoded-string>

OAuth 2.0
+++++++++

If basic authentication was very simple, and insecure, OAuth 2.0 is the
most secure system that REST APIs use in order to authenticate, and so
was the previous OAuth 1.0. There are actually different versions of
this standard, but all of them work on the same foundation:

1. There are no usernames and passwords. Instead, the provider of the
   REST API assigns a pair of credentials—a token and the secret—to the
   developer.

2. In order to authenticate, the developer needs to send a POST request
   to the "token" endpoint, which is different in each REST API but has
   the same concept. This request has to include the encoded developer
   credentials.

3. The server replies to the previous request with a session token. This
   (and not the credentials mentioned in the first step) is to be
   included in each request that you make to the REST API. The session
   token expires for security reasons, so you will have to repeat the
   second step again when that happens.

Even though this standard is kind of recent (2012 onwards), several big
companies like Google or Facebook have already implemented it for their
REST APIs. It might look a bit overcomplicated, but you will soon get to
use it, and even implement it.

Using third-party APIs
----------------------

That was enough theory about REST APIs; it is time to dive into a real
world example. In this section, we will write a small PHP application
that interacts with Twitter's REST API; that includes requesting
developer credentials, authenticating, and sending requests. The goal is
to give you your first experience in working with REST APIs, and showing
you that it is easier than you could expect. It will also help you to
understand better how they work, so it will be easier to build your own
later.

Getting the application's credentials
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

REST APIs usually have the concept of application. An application is
like an account on their development site that identifies who uses the
API. The credentials that you will use to access the API will be linked
to this application, which means that you can have multiple applications
linked to the same account.

Assuming that you have a Twitter account, go to
`*https://apps.twitter.com* <https://apps.twitter.com>`__ in order to
create a new application. Click on the Create New App button in order to
access the form for application details. The fields are very
self-explanatory—just a name for the application, the description, and
the website URL. The callback URL is not necessary here, since that will
be used only for applications that require access to someone else's
account. Agree with the terms and conditions in order to proceed.

Once you have been redirected to your application's page, you will see
all sort of information that you can edit. Since this is just an
example, let's go straight to what matters: the credentials. Click on
the Keys and Access Tokens tab to see the values of Consumer key (API
key) and Consumer Secret (API secret). There is nothing else that we
need from here. You can save them on your filesystem, as
~/.twitter\_php7.json, for example:

    { "key": "iTh4Mzl0EAPn9HAm98hEhAmVEXS", "secret":
    "PfoWM9yq4Bh6rGbzzJhr893j4r4sMIAeVRaPMYbkDer5N6F" }

Tip

Securing your credentials

Securing your REST API credentials should be taken seriously. In fact,
you should take care of all kinds of credentials, like the database
ones. But the difference is that you will usually host your database in
your server, which makes things slightly more difficult to whoever wants
to attack. On the other hand, the third-party REST API is not part of
your system, and someone with your credentials can use your account
freely on your behalf.

Never include your credentials in your code base, especially if you have
your code in GitHub or some other repository. One solution would be to
have a file in your server, outside your code, with the credentials; if
that file is encrypted, that is even better. And try to refresh your
credentials regularly, which you can probably do on the provider's
website.

Setting up the application
~~~~~~~~~~~~~~~~~~~~~~~~~~

Our application will be extremely simple. It will consist of one class
that will allow us to fetch tweets. This will be managed by our app.php
script.

As we have to make HTTP requests, we can either write our own functions
that use cURL (a set of PHP native functions), or make use of the famous
PHP library, Guzzle. This library can be found in Packagist, so we will
use Composer to include it:

    $ composer require guzzlehttp/guzzle

We will have a Twitter class, which will get the credentials from the
constructor, and one public method: fetchTwits. For now, just create the
skeleton so that we can work with it; we will implement such methods in
later sections. Add the following code to src/Twitter.php:

    <?php namespace TwitterApp; class Twitter { private $key; private
    $secret; public function \_\_construct(String $key, String $secret)
    { $this->key = $key; $this->secret = $secret; } public function
    fetchTwits(string name, int $count): array { return []; } }

Since we set the namespace TwitterApp, we need to update our
composer.json file with the following addition. Remember to run composer
update to update the autoloader.

    "autoload": { "psr-4": {"TwitterApp\\\\": "src"} }

Finally, we will create a basic app.php file, which includes the
Composer autoloader, reads the credentials file, and creates a Twitter
instance:

    <?php use TwitterApp\\Twitter; require \_\_DIR\_\_ .
    '/vendor/autoload.php'; $path = $\_SERVER['HOME'] .
    '/.twitter\_php7.json'; $jsonCredentials =
    file\_get\_contents($path); $credentials =
    json\_decode($jsonCredentials, true); $twitter = new
    Twitter($credentials['key'], $credentials['secret']);

Requesting an access token
~~~~~~~~~~~~~~~~~~~~~~~~~~

In a real world application, you would probably want to separate the
code related to authentication from the one that deals with operations
like fetching or posting data. To keep things simple here, we will let
the Twitter class know how to authenticate by itself.

Let's start by adding a $client property to the class which will contain
an instance of Guzzle's Client class. This instance will contain the
base URI of the Twitter API, which we can have as the constant
TWITTER\_API\_BASE\_URI. Instantiate this property in the constructor so
that the rest of the methods can make use of it. You can also add an
$accessToken property which will contain the access token returned by
the Twitter API when authenticating. All these changes are highlighted
here:

    <?php namespace TwitterApp; use Exception; use GuzzleHttp\\Client;
    class Twitter { const TWITTER\_API\_BASE\_URI =
    'https://api.twitter.com'; private $key; private $secret; private
    $accessToken; private $client; public function \_\_construct(String
    $key, String $secret) { $this->key = $key; $this->secret = $secret;
    $this->client = new Client( ['base\_uri' =>
    self::TWITTER\_API\_BASE\_URI] ); } //... }

The next step would be to write a method that, given the key and secret
are provided, requests an access token to the provider. More
specifically:

-  Concatenate the key and the secret with a :. Encode the result using
   Base64.

-  Send a POST request to /oauth2/token with the encoded credentials as
   the Authorization header. Also include a Content-Type header and a
   body (check the code for more information).

We now invoke the post method of Guzzle's client instance sending two
arguments: the endpoint string (/oauth2/token) and an array with
options. These options include the headers and the body of the request,
as you will see shortly. The response of this invocation is an object
that identifies the HTTP response. You can extract the content (body) of
the response with getBody. Twitter's API response is a JSON with some
arguments. The one that you care about the most is the access\_token,
the token that you will need to include in each subsequent request to
the API. Extract it and save it. The full method looks as follows:

    private function requestAccessToken() { $encodedString =
    base64\_encode( $this->key . ':' . $this->secret ); $headers = [
    'Authorization' => 'Basic ' . $encodedString, 'Content-Type' =>
    'application/x-www-form-urlencoded;charset=UTF-8' ]; $options = [
    'headers' => $headers, 'body' => 'grant\_type=client\_credentials'
    ]; $response = $this->client->post(self:: OAUTH\_ENDPOINT,
    $options); $body = json\_decode($response->getBody(), true);
    $this->accessToken = $body['access\_token']; }

You can already try this code by adding these two lines at the end of
the constructor:

    $this->requestAccessToken(); var\_dump($this->accessToken);

Run the application in order to see the access token given by the
provider using the following command. Remember to remove the preceding
two lines in order to proceed with the section.

    $ php app.php

Keep in mind that, even though having a key and secret and getting an
access token is the same across all OAuth authentications, the specific
way of encoding, the endpoint used, and the response received from the
provider are exclusive from Twitter's API. It could be that several
others are exactly the same, but always check the documentation for each
one.

Fetching tweets
~~~~~~~~~~~~~~~

We finally arrive to the section where we actually make use of the API.
We will implement the fetchTwits method in order to get a list of the
last *N* number of tweets for a given user. In order to perform
requests, we need to add the Authorization header to each one, this time
with the access token. Since we want to make this class as reusable as
possible, let's extract this to a private method:

    private function getAccessTokenHeaders(): array { if
    (empty($this->accessToken)) { $this->requestAccessToken(); } return
    ['Authorization' => 'Bearer ' . $this->accessToken]; }

As you can see, the preceding method also allows us to fetch the access
token from the provider. This is useful, since if we make more than one
request, we will just request the access token once, and we have one
unique place to do so. Add now the following method implementation:

    const GET\_TWITS = '/1.1/statuses/user\_timeline.json'; //... public
    function fetchTwits(string $name, int $count): array { $options = [
    'headers' => $this->getAccessTokenHeaders(), 'query' => [ 'count' =>
    $count, 'screen\_name' => $name ] ]; $response =
    $this->client->get(self::GET\_TWITS, $options); $responseTwits =
    json\_decode($response->getBody(), true); $twits = []; foreach
    ($responseTwits as $twit) { $twits[] = [ 'created\_at' =>
    $twit['created\_at'], 'text' => $twit['text'], 'user' =>
    $twit['user']['name'] ]; } return $twits; }

The first part of the preceding method builds the options array with the
access token headers and the query string arguments—in this case, with
the number of tweets to retrieve and the user. We perform the GET
request and decode the JSON response into an array. This array contains
a lot of information that we might not need, so we iterate it in order
to extract those fields that we really want—in this example, the date,
the text, and the user.

In order to test the application, just invoke the fetchTwits method at
the end of the app.php file, specifying the Twitter ID of one of the
people you are following, or yourself.

    $twits = $twitter->fetchTwits('neiltyson', 10); var\_dump($twits);

You should get a response similar to ours, shown in the following
screenshot:

|image44|

One thing to keep in mind is that access tokens expire after some time,
returning an HTTP response with a 4xx status code (usually, 401
unauthorized). Guzzle throws an exception when the status code is either
4xx or 5xx, so it is easy manage these scenarios. You could add this
code when performing the GET request:

    try { $response = $this->client->get(self::GET\_TWITS, $options); }
    catch (ClientException $e) { if ($e->getCode() == 401) {
    $this->requestAccessToken(); $response =
    $this->client->get(self::GET\_TWITS, $options); } else { throw $e; }
    }

The toolkit of the REST API developer
-------------------------------------

While you are developing your own REST API, or writing an integration
for a third-party one, you might want to test it before you start
writing your code. There are a handful of tools that will help you with
this task, whether you want to use your browser, or you are a fan of the
command line.

Testing APIs with browsers
~~~~~~~~~~~~~~~~~~~~~~~~~~

There are actually several add-ons that allow you to perform HTTP
requests from browsers, depending on which one you use. Some famous
names are *Advanced Rest Client* for Chrome and *RESTClient* for
Firefox. At the end of the day, all those clients allow you to perform
the same HTTP requests, where you can specify the URL, the method, the
headers, the body, and so on. These clients will also show you all the
details you can imagine from the response, including the status code,
the time spent, and the body. The following screenshot displays an
example of a request using Chrome's *Advanced Rest Client*:

|image45|

If you want to test GET requests with your own API, and all that you
need is the URL, that is, you do not need to send any headers, you can
just use your browser as if you were trying to access any other website.
If you do so, and if you are working with JSON responses, you can
install another add-on to your browser that will help you in viewing
your JSON in a more "beautiful" way. Look for *JSONView* on any browser
for a really handy one.

Testing APIs using the command line
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Some people feel more comfortable using the command line; so luckily,
for them there are tools that allow them to perform any HTTP request
from their consoles. We will give a brief introduction to one of the
most famous ones: cURL. This tool has quite a lot of features, but we
will focus only on the ones that you will be using more often: the HTTP
method, post parameters, and headers:

-  -X <method>: This specifies the HTTP method to use

-  --data: This adds the parameters specified, which can be added as
   key-value pairs, JSON, plain text, and so on

-  --header: This adds a header to the request

The following is an example of the way to send a POST request with cURL:

    curl -X POST --data "text=This is sparta!" \\ > --header
    "Authorization: Bearer 8s8d7bf8asdbf8sbdf8bsa" \\ >
    https://api.twitter.com/1.1/statuses/update.json
    {"errors":[{"code":89,"message":"Invalid or expired token."}]}

If you are using a Unix system, you will probably be able to format the
resulting JSON by appending \| python -m json.tool so that it gets
easier to read:

    $ curl -X POST --data "text=This is sparta!" \\ > --header
    "Authorization: Bearer 8s8d7bf8asdbf8sbdf8bsa" \\ >
    https://api.twitter.com/1.1/statuses/update.json \\ > \| python -m
    json.tool { "errors": [ { "code": 89, "message": "Invalid or expired
    token." } ] }

cURL is quite a powerful tool that lets you do quite a few tricks. If
you are interested, go ahead and check the documentation or some
tutorial on how to use all its features.

Best practices with REST APIs
-----------------------------

We've already gone through some of the best practices when writing REST
APIs, like using HTTP methods properly, or choosing the correct status
code for your responses. We also described two of the most used
authentication systems. But there is still a lot to learn about creating
proper REST APIs. Remember that they are meant to be used by developers
like yourself, so they will always be grateful if you do things
properly, and make their lives easier. Ready?

Consistency in your endpoints
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

When deciding how to name your endpoints, try keeping them consistent.
Even though you are free to choose, there is a set of spoken rules that
will make your endpoints more intuitive and easy to understand. Let's
list some of them:

-  For starters, an endpoint should point to a specific resource (for
   example, books or tweets), and you should make that clear in your
   endpoint. If you have an endpoint that returns the list of all books,
   do not name it /library, as it is not obvious what it will be
   returning. Instead, name it /books or /books/all.

-  The name of the resource can be either plural or singular, but make
   it consistent. If sometimes you use /books and sometimes /user, it
   might be confusing, and people will probably make mistakes. We
   personally prefer to use the plural form, but that is totally up to
   you.

-  When you want to retrieve a specific resource, do it by specifying
   the ID whenever possible. IDs must be unique in your system, and any
   other parameter might point to two different entities. Specify the ID
   next to the name of the resource, such as /books/249234-234-23-42.

-  If you can understand what an endpoint does by just the HTTP method,
   there is no need to add this information as part of the endpoint. For
   example, if you want to get a book, or you want to delete it,
   /books/249234-234-23-42 along with the HTTP methods GET and DELETE
   are more than enough. If it is not obvious, state it as a verb at the
   end of the endpoint, like /employee/9218379182/promote.

Document as much as you can
~~~~~~~~~~~~~~~~~~~~~~~~~~~

The title says everything. You are probably not going to be the one
using the REST API, others will. Obviously, even if you design a very
intuitive set of endpoints, developers will still need to know the whole
set of available endpoints, what each of them does, what optional
parameters are available, and so on.

Write as much documentation as possible, and keep it up to date. Take a
look at other documented APIs to gather ideas on how to display the
information. There are plenty of templates and tools that will help you
deliver a well-presented documentation, but you are the one that has to
be consistent and methodical. Developers have a special hate towards
documenting anything, but we also like to find clear and beautifully
presented documentation when we need to use someone else's APIs.

Filters and pagination
~~~~~~~~~~~~~~~~~~~~~~

One of the common usages of an API is to list resources and filter them
by some criteria. We already saw an example when we were building our
own bookstore; we wanted to get the list of books that contained a
certain string in their titles or authors.

Some developers try to have beautiful endpoints, which a priori is a
good thing to do. Imagine that you want to filter just by title, you
might end up having an endpoint like /books/title/<string>. We add also
the ability to filter by author, and we now get two more endpoints:
/books/title/<string>/author/<string> and /books/author/<string>. Now
let's add the description too—do you see where we are going?

Even though some developers do not like to use query strings as
arguments, there is nothing wrong with it. In fact, if you use them
properly, you will end up with cleaner endpoints. You want to get books?
Fine, just use /books, and add whichever filter you need using the query
string.

Pagination occurs when you have way too many resources of the same type
to retrieve all at once. You should think of pagination as another
optional filter to be specified as a GET parameter. You should have
pages with a default size, let's say 10 books, but it is a good idea to
give the developers the ability to define their own size. In this case,
developers can specify the length and the number of pages to retrieve.

API versioning
-~~~~~~~~~~~~~

Your API is a reflection of what your application can do. Chances are
that your code will evolve, improving the already existing features or
adding new ones. Your API should be updated too, exposing those new
features, updating existing endpoints, or even removing some of them.

Imagine now that someone else is using your REST API, and their whole
website relies on it. If you change your existing endpoints, their
website will stop working! They will not be happy at all, and will try
to find someone else that can do what you were doing. Not a good
scenario, but then, how do you improve your API?

The solution is to use versioning. When you release a new version of the
API, do not nuke down the existing one; you should give some time to the
users to upgrade their integrations. And how can two different versions
of the API coexist? You already saw one of the options—the one that we
recommend you: by specifying the version of the API to use as part of
the endpoint. Do you remember the endpoint of the Twitter API
/1.1/statuses/user\_timeline.json? The 1.1 refers to the version that we
want to use.

Using HTTP cache
~~~~~~~~~~~~~~~~

If the main feature of REST APIs is that they make heavy use of HTTP,
why not take advantage of HTTP cache? Well, there are actual reasons for
not using it, but most of them are due to a lack of knowledge about
using it properly. It is out of the scope of this book to explain every
single detail of its implementation, but let's try to give a short
introduction to the topic. Plenty of resources on the Internet can help
you to understand the parts that you are more interested in.

HTTP responses can be divided as public and private. Public responses
are shared between all users of the API, whereas the private ones are
meant to be unique for each user. You can specify which type of response
is yours using the Cache-Control header, allowing the response to be
cached if the method of the request was a GET. This header can also
expose the expiration of the cache, that is, you can specify the
duration for which your response will remain the same, and thus, can be
cached.

Other systems rely on generating a hash of the representation of a
resource, and add it as the ETag (Entity tag) header in order to know if
the resource has changed or not. In a similar way, you can set the
Last-Modified header to let the client know when was the last time that
the given resource changed. The idea behind those systems is to identify
when the client already contains valid data. If so, the provider does
not process the request, but returns an empty response with the status
code 304 (not modified) instead. When the client gets that response, it
uses its cached content.

Creating a REST API with Laravel
--------------------------------

In this section, we will build a REST API with Laravel from scratch.
This REST API will allow you to manage different clients at your
bookstore, not only via the browser, but via the UI as well. You will be
able to perform pretty much the same actions as before, that is, listing
books, buying them, borrowing for free, and so on.

Once the REST API is done, you should remove all the business logic from
the bookstore that you built during the previous chapters. The reason is
that you should have one unique place where you can actually manipulate
your databases and the REST API, and the rest of the applications, like
the web one, should able to communicate with the REST API for managing
data. In doing so, you will be able to create other applications for
different platforms, like mobile apps, that will use the REST API too,
and both the website and the mobile app will always be synchronized,
since they will be using the same sources.

As with our previous Laravel example, in order to create a new project,
you just need to run the following command:

    $ laravel new bookstore\_api

Setting OAuth2 authentication
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The first thing that we are going to implement is the authentication
layer. We will use OAuth2 in order to make our application more secure
than basic authentication. Laravel does not provide support for OAuth2
out of the box, but there is a service provider which does that for us.

Installing OAuth2Server
+++++++++++++++++++++++

To install OAuth2, add it as a dependency to your project using
Composer:

    $ composer require "lucadegasperi/oauth2-server-laravel:5.1.\*"

This service provider needs quite a few changes. We will go through them
without going into too much detail on how things work exactly. If you
are more interested in the topic, or if you want to create your own
service providers for Laravel, we recommend you to go though the
extensive official documentation.

To start with, we need to add the new OAuth2Server service provider to
the array of providers in the config/app.php file. Add the following
lines at the end of the providers array:

    /\* \* OAuth2 Server Service Providers... \*/
    LucaDegasperi\\OAuth2Server\\Storage\\FluentStorageServiceProvider::class,
    LucaDegasperi\\OAuth2Server\\OAuth2ServerServiceProvider::class,

In the same way, you need to add a new alias to the aliases array in the
same file:

    'Authorizer' =>
    LucaDegasperi\\OAuth2Server\\Facades\\Authorizer::class,

Let's move to the app/Http/Kernel.php file, where we need to make some
changes too. Add the following entry to the $middleware array property
of the Kernel class:

    \\LucaDegasperi\\OAuth2Server\\Middleware\\OAuthExceptionHandlerMiddleware::class,

Add the following key-value pairs to the $routeMiddleware array property
of the same class:

    'oauth' =>
    \\LucaDegasperi\\OAuth2Server\\Middleware\\OAuthMiddleware::class,
    'oauth-user' =>
    \\LucaDegasperi\\OAuth2Server\\Middleware\\OAuthUserOwnerMiddleware::class,
    'oauth-client' =>
    \\LucaDegasperi\\OAuth2Server\\Middleware\\OAuthClientOwnerMiddleware::class,
    'check-authorization-params' =>
    \\LucaDegasperi\\OAuth2Server\\Middleware\\CheckAuthCodeRequestMiddleware::class,
    'csrf' => \\App\\Http\\Middleware\\VerifyCsrfToken::class,

We added a CSRF token verifier to the $routeMiddleware, so we need to
remove the one already defined in $middlewareGroups, since they are
incompatible. Use the following line to do so:

    \\App\\Http\\Middleware\\VerifyCsrfToken::class,

Setting up the database
+++++++++++++++++++++++

Let's set up the database now. In this section, we will assume that you
already have the bookstore database in your environment. If you do not
have it, go back to `*Chapter 5* <#Top_of_ch05_html>`__, *Using
Databases*, to create it in order to proceed with this setup.

The first thing to do is to update the database credentials in the .env
file. They should look something similar to the following lines, but
with your username and password:

    DB\_HOST=localhost DB\_DATABASE=bookstore DB\_USERNAME=root
    DB\_PASSWORD=

In order to prepare the configuration and database migration files from
the OAuth2Server service provider, we need to publish it. In Laravel,
you do it by executing the following command:

    $ php artisan vendor:publish

Now the database/migrations directory contains all the necessary
migration files that will create the necessary tables related to OAuth2
in our database. To execute them, we run the following command:

    $ php artisan migrate

We need to add at least one client to the oauth\_clients table, which is
the table that stores the key and secrets for all clients that want to
connect to our REST API. This new client will be the one that you will
use during the development process in order to test what you have done.
We can set a random ID—the key—and the secret as follows:

    mysql> INSERT INTO oauth\_clients(id, secret, name) ->
    VALUES('iTh4Mzl0EAPn90sK4EhAmVEXS', ->
    'PfoWM9yq4Bh6rGbzzJhr8oDDsNZwGlsMIAeVRaPM', -> 'Toni'); Query OK, 1
    row affected, 1 warning (0.00 sec)

Enabling client-credentials authentication
++++++++++++++++++++++++++++++++++++++++++

Since we published the plugins in vendor in the previous step, now we
have the configuration files for the OAuth2Server. This plugin allows us
different authentication systems (all of them with OAuth2), depending on
our necessities. The one that we are interested in for our project is
the client\_credentials type. To let Laravel know, add the following
lines at the end of the array in the config/oauth2.php file:

    'grant\_types' => [ 'client\_credentials' => [ 'class' =>
    '\\League\\OAuth2\\Server\\Grant\\ClientCredentialsGrant',
    'access\_token\_ttl' => 3600 ] ]

These preceding lines grant access to the client\_credentials type,
which are managed by the ClientCredentialsGrant class. The
access\_token\_ttl value refers to the time period of the access token,
that is, for how long someone can use it. In this case, it is set to 1
hour, that is, 3,600 seconds.

Finally, we need to enable a route so we can post our credentials in
exchange for an access token. Add the following route to the routes file
in app/Http/routes.php:

    Route::post('oauth/access\_token', function() { return
    Response::json(Authorizer::issueAccessToken()); });

Requesting an access token
++++++++++++++++++++++++++

It is time to test what we have done so far. To do so, we need to send a
POST request to the /oauth/access\_token endpoint that we enabled just
now. This request needs the following POST parameters:

-  client\_id with the key from the database

-  client\_secret with the secret from the database

-  grant\_type to specify the type of authentication that we are trying
   to perform, in this case client\_credentials

The request issued using the *Advanced REST Client* add-on from Chrome
looks as follows:

|image46|

The response that you should get should have the same format as this
one:

    { "access\_token": "MPCovQda354d10zzUXpZVOFzqe491E7ZHQAhSAax"
    "token\_type": "Bearer" "expires\_in": 3600 }

Note that this is a different way of requesting for an access token than
what the Twitter API does, but the idea is still the same: given a key
and a secret, the provider gives us an access token that will allow us
to use the API for some time.

Preparing the database
~~~~~~~~~~~~~~~~~~~~~~

Even though we've already done the same in the previous chapter, you
might think: "Why do we start by preparing the database?". We could
argue that you first need to know the kind of endpoints you want to
expose in your REST API, and only then you can start thinking about what
your database should look like. But you could also think that, since we
are working with an API, each endpoint should manage one resource, so
first you need to define the resources you are dealing with. This *code
first versus database/model first* is an ongoing war on the Internet.
But whichever way you think is better, the fact is that we already know
what the users will need to do with our REST API, since we already built
the UI previously; so it does not really matter.

We need to create four tables: books, sales, sales\_books, and
borrowed\_books. Remember that Laravel already provides a users table,
which we can use as our customers. Run the following four commands to
create the migrations files:

    $ php artisan make:migration create\_books\_table --create=books $
    php artisan make:migration create\_sales\_table --create=sales $ php
    artisan make:migration create\_borrowed\_books\_table \\
    --create=borrowed\_books $ php artisan make:migration
    create\_sales\_books\_table \\ --create=sales\_books

Now we have to go file by file to define what each table should look
like. We will try to replicate the data structure from `*Chapter
5* <#Top_of_ch05_html>`__, *Using Databases*, as much as possible.
Remember that the migration files can be found inside the
database/migrations directory. The first file that we can edit is the
create\_books\_table.php. Replace the existing empty up method by the
following one:

    public function up() { Schema::create('books', function (Blueprint
    $table) { $table->increments('id');
    $table->string('isbn')->unique(); $table->string('title');
    $table->string('author'); $table->smallInteger('stock')->unsigned();
    $table->float('price')->unsigned(); }); }

The next one in the list is create\_sales\_table.php. Remember that this
one has a foreign key pointing to the users table. You can use
references(field)->on(tablename) to define this constraint.

    public function up() { Schema::create('sales', function (Blueprint
    $table) { $table->increments('id');
    $table->string('user\_id')->references('id')->on('users');
    $table->timestamps(); }); }

The create\_sales\_books\_table.php file contains two foreign keys: one
pointing to the ID of the sale, and one to the ID of the book. Replace
the existing up method by the following one:

    public function up() { Schema::create('sales\_books', function
    (Blueprint $table) { $table->increments('id');
    $table->integer('sale\_id')->references('id')->on('sales');
    $table->integer('book\_id')->references('id')->on('books');
    $table->smallInteger('amount')->unsigned(); }); }

Finally, edit the create\_borrowed\_books\_table.php file, which has the
book\_id foreign key and the start and end timestamps:

    public function up() { Schema::create('borrowed\_books', function
    (Blueprint $table) { $table->increments('id');
    $table->integer('book\_id')->references('id')->on('books');
    $table->string('user\_id')->references('id')->on('users');
    $table->timestamp('start'); $table->timestamp('end'); }); }

The migration files are ready so we just need to migrate them in order
to create the database tables. Run the following command:

    $ php artisan migrate

Also, add some books to the database manually so that you can test
later. For example:

    mysql> INSERT INTO books (isbn,title,author,stock,price) VALUES ->
    ("9780882339726","1984","George Orwell",12,7.50), ->
    ("9789724621081","1Q84","Haruki Murakami",9,9.75), ->
    ("9780736692427","Animal Farm","George Orwell",8,3.50), ->
    ("9780307350169","Dracula","Bram Stoker",30,10.15), ->
    ("9780753179246","19 minutes","Jodi Picoult",0,10); Query OK, 5 rows
    affected (0.01 sec) Records: 5 Duplicates: 0 Warnings: 0

Setting up the models
~~~~~~~~~~~~~~~~~~~~~

The next thing to do on the list is to add the relationships that our
data has, that is, to translate the foreign keys from the database to
the models. First of all, we need to create those models, and for that
we just run the following commands:

    $ php artisan make:model Book $ php artisan make:model Sale $ php
    artisan make:model BorrowedBook $ php artisan make:model SalesBook

Now we have to go model by model, and add the one to one and one to many
relationships as we did in the previous chapter. For BookModel, we will
only specify that the model does not have timestamps, since they come by
default. To do so, add the following highlighted line to your
app/Book.php file:

    <?php namespace App; use Illuminate\\Database\\Eloquent\\Model;
    class Book extends Model { public $timestamps = false; }

For the BorrowedBook model, we need to specify that it has one book, and
it belongs to a user. We also need to specify the fields we will fill
once we need to create the object—in this case, book\_id and start. Add
the following two methods in app/BorrowedBook.php:

    <?php namespace App; use Illuminate\\Database\\Eloquent\\Model;
    class BorrowedBook extends Model { protected $fillable =
    ['user\_id', 'book\_id', 'start']; public $timestamps = false;
    public function user() { return $this->belongsTo('App\\User'); }
    public function book() { return $this->hasOne('App\\Book'); } }

Sales can have many "sale books" (we know it might sound a little
awkward), and they also belong to just one user. Add the following to
your app/Sale.php:

    <?php namespace App; use Illuminate\\Database\\Eloquent\\Model;
    class Sale extends Model { protected $fillable = ['user\_id'];
    public function books() { return $this->hasMany('App\\SalesBook'); }
    public function user() { return $this->belongsTo('App\\User'); } }

Like borrowed books, sale books can have one book and belong to one sale
instead of to one user. The following lines should be added to
app/SalesBook.php:

    <?php namespace App; use Illuminate\\Database\\Eloquent\\Model;
    class SaleBook extends Model { public $timestamps = false; protected
    $fillable = ['book\_id', 'sale\_id', 'amount']; public function
    sale() { return $this->belongsTo('App\\Sale'); } public function
    books() { return $this->hasOne('App\\Book'); } }

Finally, the last model that we need to update is the User model. We
need to add the opposite relationship to the belongs we used earlier in
Sale and BorrowedBook. Add these two functions, and leave the rest of
the class intact:

    <?php namespace App; use Illuminate\\Foundation\\Auth\\User as
    Authenticatable; class User extends Authenticatable { //... public
    function sales() { return $this->hasMany('App\\Sale'); } public
    function borrowedBooks() { return
    $this->hasMany('App\\BorrowedBook'); } }

Designing endpoints
~~~~~~~~~~~~~~~~~~~

In this section, we need to come up with the list of endpoints that we
want to expose to the REST API clients. Keep in mind the "rules"
explained in the *Best practices with REST APIs* section. In short, keep
the following rules in mind:

-  One endpoint interacts with one resource

-  A possible schema could be <API version>/<resource name>/<optional
   id>/<optional action>

-  Use GET parameters for filtering and pagination

So what will the user need to do? We already have a good idea about
that, since we created the UI. A brief summary would be as follows:

-  List all the available books with some filtering (by title and
   author), and paginated when necessary. Also retrieve the information
   on a specific book, given the ID.

-  Allow the user to borrow a specific book if available. In the same
   way, the user should be able to return books, and list the history of
   borrowed books too (filtered by date and paginated).

-  Allow the user to buy a list of books. This could be improved, but
   for now let's force the user to buy books with just one request,
   including the full list of books in the body. Also, list the sales of
   the user following the same rules as that with borrowed books.

We will start straightaway with our list of endpoints, specifying the
path, the HTTP method, and the optional parameters. It will also give
you an idea on how to document your REST APIs.

-  GET /books

   -  title: Optional and filters by title

   -  author: Optional and filters by author

   -  page: Optional, default is 1, and specifies the page to return

   -  page-size: Optional, default is 50, and specifies the page size to
      return

-  GET /books/<book id>

-  POST /borrowed-books

   -  book-id: Mandatory and specifies the ID of the book to borrow

-  GET /borrowed-books

   -  from: Optional and returns borrowed books from the specified date

   -  page: Optional, default is 1, and specifies the page to return

   -  page-size: Optional, default is 50, and specifies the number of
      borrowed books per page

-  PUT /borrowed-books/<borrowed book id>/return

-  POST /sales

   -  books: Mandatory and it is an array listing the book IDs to buy
      and their amounts, that is, *{"book-id-1": amount, "book-id-2":
      amount, ...}*

-  GET /sales

   -  from: Optional and returns borrowed books from the specified date

   -  page: Optional, default is 1, and specifies the page to return

   -  page-size: Optional, default is 50, and specifies the number of
      sales per page

-  GET /sales/<sales id>

We use POST requests when creating sales and borrowed books, since we do
not know the ID of the resource that we want to create a priori, and
posting the same request will create multiple resources. On the other
hand, when returning a book, we do know the ID of the borrowed book, and
sending the same request multiple times will leave the database in the
same state. Let's translate these endpoints to routes in
app/Http/routes.php:

    /\* \* Books endpoints. \*/ Route::get('books', ['middleware' =>
    'oauth', 'uses' => 'BookController@getAll']);
    Route::get('books/{id}', ['middleware' => 'oauth', 'uses' =>
    'BookController@get']); /\* \* Borrowed books endpoints. \*/
    Route::post('borrowed-books', ['middleware' => 'oauth', 'uses' =>
    'BorrowedBookController@borrow']); Route::get('borrowed-books',
    ['middleware' => 'oauth', 'uses' => 'BorrowedBookController@get']);
    Route::put('borrowed-books/{id}/return', ['middleware' => 'oauth',
    'uses' => 'BorrowedBookController@returnBook']); /\* \* Sales
    endpoints. \*/ Route::post('sales', ['middleware' => 'oauth', 'uses'
    => 'SalesController@buy]); Route::get('sales', ['middleware' =>
    'oauth', 'uses' => 'SalesController@getAll']);
    Route::get('sales/{id}', ['middleware' => 'oauth', 'uses' =>
    'SalesController@get']);

In the preceding code, note how we added the middleware oauth to all the
endpoints. This will require the user to provide a valid access token in
order to access them.

Adding the controllers
~~~~~~~~~~~~~~~~~~~~~~

From the previous section, you can imagine that we need to create three
controllers: BookController, BorrowedBookController, and
SalesController. Let's start with the easiest one: returning the
information of a book given the ID. Create the file
app/Http/Controllers/BookController.php, and add the following code:

    <?php namespace App\\Http\\Controllers; use App\\Book; use
    Illuminate\\Http\\JsonResponse; use Illuminate\\Http\\Response;
    class BookController extends Controller { public function get(string
    $id): JsonResponse { $book = Book::find($id); if (empty($book)) {
    return new JsonResponse ( null, JsonResponse::HTTP\_NOT\_FOUND ); }
    return response()->json(['book' => $book]); } }

Even though this preceding example is quite easy, it contains most of
what we will need for the rest of the endpoints. We try to fetch a book
given the ID from the URL, and when not found, we reply with a 404 (not
found) empty response—the constant Response::HTTP\_NOT\_FOUND is 404. In
case we have the book, we return it as JSON with response->json(). Note
how we add the seemingly unnecessary key book; it is true that we do not
return anything else and, since we ask for the book, the user will know
what we are talking about, but as it does not really hurt, it is good to
be as explicit as possible.

Let's test it! You already know how to get an access token—check the
*Requesting an access token* section. So get one, and try to access the
following URLs:

-  http://localhost/books/0?access\_token=12345

-  http://localhost/books/1?access\_token=12345

Assuming that 12345 is your access token, that you have a book in the
database with ID 1, and you do not have a book with ID 0, the first URL
should return a 404 response, and the second one, a response something
similar to the following:

    { "book": { "id": 1 "isbn": "9780882339726" "title": "1984"
    "author": "George Orwell" "stock": 12 "price": 7.5 } }

Let's now add the method to get all the books with filters and
pagination. It looks quite verbose, but the logic that we use is quite
simple:

    public function getAll(Request $request): JsonResponse { $title =
    $request->get('title', ''); $author = $request->get('author', '');
    $page = $request->get('page', 1); $pageSize =
    $request->get('page-size', 50); $books = Book::where('title',
    'like', "%$title%") ->where('author', 'like', "%$author%")
    ->take($pageSize) ->skip(($page - 1) \* $pageSize) ->get(); return
    response()->json(['books' => $books]); }

We get all the parameters that can come from the request, and set the
default values of each one in case the user does not include them (since
they are optional). Then, we use the Eloquent ORM to filter by title and
author using where(), and limiting the results with take()->skip(). We
return the JSON in the same way we did with the previous method. In this
one though, we do not need any extra check; if the query does not return
any book, it is not really a problem.

You can now play with your REST API, sending different requests with
different filters. The following are some examples:

-  http://localhost/books?access\_token=12345

-  http://localhost/books?access\_token=12345&title=19&page-size=1

-  http://localhost/books?access\_token=12345&page=2

The next controller in the list is BorrowedBookController. We need to
add three methods: borrow, get, and returnBook. As you already know how
to work with requests, responses, status codes, and the Eloquent ORM, we
will write the entire class straightaway:

    <?php namespace App\\Http\\Controllers; use App\\Book; use
    App\\BorrowedBook; use Illuminate\\Http\\JsonResponse; use
    Illuminate\\Http\\Request; use
    LucaDegasperi\\OAuth2Server\\Facades\\Authorizer; class
    BorrowedBookController extends Controller { public function get():
    JsonResponse { $borrowedBooks = BorrowedBook::where( 'user\_id',
    '=', Authorizer::getResourceOwnerId() )->get(); return
    response()->json( ['borrowed-books' => $borrowedBooks] ); } public
    function borrow(Request $request): JsonResponse { $id =
    $request->get('book-id'); if (empty($id)) { return new JsonResponse(
    ['error' => 'Expecting book-id parameter.'],
    JsonResponse::HTTP\_BAD\_REQUEST ); } $book = Book::find($id); if
    (empty($book)) { return new JsonResponse( ['error' => 'Book not
    found.'], JsonResponse::HTTP\_BAD\_REQUEST ); } else if
    ($book->stock < 1) { return new JsonResponse( ['error' => 'Not
    enough stock.'], JsonResponse::HTTP\_BAD\_REQUEST ); }
    $book->stock--; $book->save(); $borrowedBook = BorrowedBook::create(
    [ 'book\_id' => $book->id, 'start' => date('Y-m-d H:i:s'),
    'user\_id' => Authorizer::getResourceOwnerId() ] ); return
    response()->json(['borrowed-book' => $borrowedBook]); } public
    function returnBook(string $id): JsonResponse { $borrowedBook =
    BorrowedBook::find($id); if (empty($borrowedBook)) { return new
    JsonResponse( ['error' => 'Borrowed book not found.'],
    JsonResponse::HTTP\_BAD\_REQUEST ); } $book =
    Book::find($borrowedBook->book\_id); $book->stock++; $book->save();
    $borrowedBook->end = date('Y-m-d H:m:s'); $borrowedBook->save();
    return response()->json(['borrowed-book' => $borrowedBook]); } }

The only thing to note in the preceding code is how we also update the
stock of the book by increasing or decreasing the stock, and invoke the
save method to save the changes in the database. We also return the
borrowed book object as the response when borrowing a book so that the
user can know the borrowed book ID, and use it when querying or
returning the book.

You can test how this set of endpoints works with the following use
cases:

-  Borrow a book. Check that you get a valid response.

-  Get the list of borrowed books. The one that you just created should
   be there with a valid starting date and an empty end date.

-  Get the information of the book you borrowed. The stock should be one
   less.

-  Return the book. Fetch the list of borrowed books to check the end
   date and the returned book to check the stock.

Of course, you can always try to trick the API and ask for books without
stock, non-existing borrowed books, and the like. All these edge cases
should respond with the correct status codes and error messages.

We finish this section, and the REST API, by creating the
SalesController. This controller is the one that contains more logic,
since creating a sale implies adding entries to the sales books table,
prior to checking for enough stock for each one. Add the following code
to app/Html/SalesController.php:

    <?php namespace App\\Http\\Controllers; use App\\Book; use
    App\\Sale; use App\\SalesBook; use Illuminate\\Http\\JsonResponse;
    use Illuminate\\Http\\Request; use
    LucaDegasperi\\OAuth2Server\\Facades\\Authorizer; class
    SalesController extends Controller { public function get(string
    $id): JsonResponse { $sale = Sale::find($id); if (empty($sale)) {
    return new JsonResponse( null, JsonResponse::HTTP\_NOT\_FOUND ); }
    $sale->books = $sale->books()->getResults(); return
    response()->json(['sale' => $sale]); } public function buy(Request
    $request): JsonResponse { $books =
    json\_decode($request->get('books'), true); if (empty($books) \|\|
    !is\_array($books)) { return new JsonResponse( ['error' => 'Books
    array is malformed.'], JsonResponse::HTTP\_BAD\_REQUEST ); }
    $saleBooks = []; $bookObjects = []; foreach ($books as $bookId =>
    $amount) { $book = Book::find($bookId); if (empty($book) \|\|
    $book->stock < $amount) { return new JsonResponse( ['error' => "Book
    $bookId not valid."], JsonResponse::HTTP\_BAD\_REQUEST ); }
    $bookObjects[] = $book; $saleBooks[] = [ 'book\_id' => $bookId,
    'amount' => $amount ]; } $sale = Sale::create( ['user\_id' =>
    Authorizer::getResourceOwnerId()] ); foreach ($bookObjects as $key
    => $book) { $book->stock -= $saleBooks[$key]['amount'];
    $saleBooks[$key]['sale\_id'] = $sale->id;
    SalesBook::create($saleBooks[$key]); } $sale->books =
    $sale->books()->getResults(); return response()->json(['sale' =>
    $sale]); } public function getAll(Request $request): JsonResponse {
    $page = $request->get('page', 1); $pageSize =
    $request->get('page-size', 50); $sales = Sale::where( 'user\_id',
    '=', Authorizer::getResourceOwnerId() ) ->take($pageSize)
    ->skip(($page - 1) \* $pageSize) ->get(); foreach ($sales as $sale)
    { $sale->books = $sale->books()->getResults(); } return
    response()->json(['sales' => $sales]); } }

In the preceding code, note how we first check the availability of all
the books before creating the sales entry. This way, we make sure that
we do not leave any unfinished sale in the database when returning an
error to the user. You could change this, and use transactions instead,
and if a book is not valid, just roll back the transaction.

In order to test this, we can follow similar steps as we did with
borrowed books. Just remember that the books parameter, when posting a
sale, is a JSON map; for example, {"1": 2, "4": 1} means that I am
trying to buy two books with ID 1 and one book with ID 4.

Testing your REST APIs
----------------------

You have already been testing your REST API after finishing each
controller by making some request and expecting a response. As you might
imagine, this can be handy sometimes, but it is for sure not the way to
go. Testing should be automatic, and should cover as much as possible.
We will have to think of a solution similar to unit testing.

In `*Chapter 10* <#Top_of_ch10_html>`__, *Behavioral Testing*, you will
learn more methodologies and tools for testing an application end to
end, and that will include REST APIs. However, due to the simplicity of
our REST API, we can add some pretty good tests with what Laravel
provides us as well. Actually, the idea is very similar to the tests
that we wrote in `*Chapter 8* <#Top_of_ch08_html>`__, *Using Existing
PHP Frameworks*, where we made a request to some endpoint, and expected
a response. The only difference will be in the kind of assertions that
we use (which can check if a JSON response is OK), and the way we
perform requests.

Let's add some tests to the set of endpoints related to books. We need
some books in the database in order to query them, so we will have to
populate the database before each test, that is, use the setUp method.
Remember that in order to leave the database clean of test data, we need
to use the trait DatabaseTransactions. Add the following code to
tests/BooksTest.php:

    <?php use Illuminate\\Foundation\\Testing\\DatabaseTransactions; use
    App\\Book; class BooksTest extends TestCase { use
    DatabaseTransactions; private $books = []; public function setUp() {
    parent::setUp(); $this->addBooks(); } private function addBooks() {
    $this->books[0] = Book::create( [ 'isbn' => '293842983648273',
    'title' => 'Iliad', 'author' => 'Homer', 'stock' => 12, 'price' =>
    7.40 ] ); $this->books[0]->save(); $this->books[0] =
    $this->books[0]->fresh(); $this->books[1] = Book::create( [ 'isbn'
    => '9879287342342', 'title' => 'Odyssey', 'author' => 'Homer',
    'stock' => 8, 'price' => 10.60 ] ); $this->books[1]->save();
    $this->books[1] = $this->books[1]->fresh(); $this->books[2] =
    Book::create( [ 'isbn' => '312312314235324', 'title' => 'The
    Illuminati', 'author' => 'Larry Burkett', 'stock' => 22, 'price' =>
    5.10 ] ); $this->books[2]->save(); $this->books[2] =
    $this->books[2]->fresh(); } }

As you can see in the preceding code, we add three books to the
database, and to the class property $books too. We will need them when
we want to assert that a response is valid. Also note the use of the
fresh method; this method synchronizes the model that we have with the
content in the database. We need to do this in order to get the ID
inserted in the database, since we do not know it a priori.

There is another thing we need to do before we run each test:
authenticating our client. We will need to make a POST request to the
access token generation endpoint sending valid credentials, and storing
the access token that we receive so that it can be used in the remaining
requests. You are free to choose how to provide the credentials, since
there are different ways to do it. In our case, we just provide the
credentials of a client test that we know exists in the database, but
you might prefer to insert that client into the database each time.
Update the test with the following code:

    <?php use Illuminate\\Foundation\\Testing\\DatabaseTransactions; use
    App\\Book; class BooksTest extends TestCase { use
    DatabaseTransactions; private $books = []; private $accessToken;
    public function setUp() { parent::setUp(); $this->addBooks();
    $this->authenticate(); } //... private function authenticate() {
    $this->post( 'oauth/access\_token', [ 'client\_id' =>
    'iTh4Mzl0EAPn90sK4EhAmVEXS', 'client\_secret' =>
    'PfoWM9yq4Bh6rhr8oDDsNZM', 'grant\_type' => 'client\_credentials' ]
    ); $response = json\_decode( $this->response->getContent(), true );
    $this->accessToken = $response['access\_token']; } }

In the preceding code, we use the post method in order to send a POST
request. This method accepts a string with the endpoint, and an array
with the parameters to be included. After making a request, Laravel
saves the response object into the $response property. We can
JSON-decode it, and extract the access token that we need.

It is time to add some tests. Let's start with an easy one: requesting a
book given an ID. The ID is used to make the GET requests with the ID of
the book (do not forget the access token), and check if the response
matches the expected one. Remember that we have the $books array
already, so it will be pretty easy to perform these checks.

We will be using two assertions: seeJson, which compares the received
JSON response with the one that we provide, and assertResponseOk, which
you already know from previous tests—it just checks that the response
has a 200 status code. Add this test to the class:

    public function testGetBook() { $expectedResponse = [ 'book' =>
    json\_decode($this->books[1], true) ]; $url = 'books/' .
    $this->books[1]->id . '?' . $this->getCredentials();
    $this->get($url) ->seeJson($expectedResponse) ->assertResponseOk();
    } private function getCredentials(): string { return
    'grant\_access=client\_credentials&access\_token=' .
    $this->accessToken; }

We use the get method instead of post, since this is a GET request. Also
note that we use the getCredentials helper, since we will have to use it
in each test. To see another example, let's add a test that checks the
response when requesting the books that contain the given title:

    public function testGetBooksByTitle() { $expectedResponse = [
    'books' => [ json\_decode($this->books[0], true),
    json\_decode($this->books[2], true) ] ]; $url = 'books/?title=Il&' .
    $this->getCredentials(); $this->get($url)
    ->seeJson($expectedResponse) ->assertResponseOk(); }

The preceding test is pretty much the same as the previous one, isn't
it? The only changes are the endpoint and the expected response. Well,
the remaining tests will all follow the same pattern, since so far, we
can only fetch books and filter them.

To see something different, let's check how to test an endpoint that
creates resources. There are different options, one of them being to
first make the request, and then going to the database to check that the
resource has been created. Another option, the one that we prefer, is to
first send the request that creates the resource, and then, with the
information in the response, send a request to fetch the newly created
resource. This is preferable, since we are testing only the REST API,
and we do not need to know the specific schema that the database is
using. Also, if the REST API changes its database, the tests will keep
passing—and they should—since we test through the interface only.

One good example could be borrowing a book. The test should first send a
POST in order to borrow the book, specifying the book ID, then extract
the borrowed book ID from the response, and finally send a GET request
asking for that borrowed book. To save time, you can add the following
test to the already existing tests/BooksTest.php:

    public function testBorrowBook() { $params = ['book-id' =>
    $this->books[1]->id]; $params = array\_merge($params,
    $this->postCredentials()); $this->post('borrowed-books', $params)
    ->seeJsonContains(['book\_id' => $this->books[1]->id])
    ->assertResponseOk(); $response =
    json\_decode($this->response->getContent(), true); $url =
    'borrowed-books' . '?' . $this->getCredentials(); $this->get($url)
    ->seeJsonContains(['id' => $response['borrowed-book']['id']])
    ->assertResponseOk(); } private function postCredentials(): array {
    return [ 'grant\_access' => 'client\_credentials', 'access\_token'
    => $this->accessToken ]; }

Summary
-------

In this chapter, you learned the importance of REST APIs in the web
world. Now you are able not only to use them, but also write your own
REST APIs, which has turned you into a more resourceful developer. You
can also integrate your applications with third-party APIs to give more
features to your users, and for making your websites more interesting
and useful.

In the next and last chapter, we will end this book discovering a type
of testing other than unit testing: behavioral testing, which improves
the quality and reliability of your web applications.
