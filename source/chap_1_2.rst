.. include:: images.txt



Setting up the environment on Windows
=====================================

Even though it is not very professional to pick sides based on personal opinions, it is well known among developers how hard it can be to use Windows as a developer machine. They prove to be extremely tricky when it comes to installing all the software since the installation mode is always very different from OS X and Linux systems, and quite often, there are dependency or configuration problems. In addition, the command line has different interpreters than Unix systems, which makes things a bit more confusing. This is why most developers would recommend you use a virtual machine with Linux if you only have a Windows machine at your disposal.

However, to be fair, PHP 7 is the exception to the rule. It is surprisingly simple to install it, so if you are really comfortable with your Windows and would prefer not to use Vagrant, here you have a short explanation on how to set up your environment.

Installing PHP
--------------

In order to install PHP 7, you will first download the installer from the official website. For this, go to
`*http://windows.php.net/download* <http://windows.php.net/download>`__. The options should be similar to the following screenshot:

|image8|

Choose x86 Thread Safe for Windows 32-bit or x64 Thread Safe for the 64-bit one. Once downloaded, uncompress it in C:\\php7. Yes, that is it!

Installing MySQL
----------------

Installing MySQL is a little more complex. Download the installer from `*http://dev.mysql.com/downloads/installer/* <http://dev.mysql.com/downloads/installer/>`__ and execute it. After accepting the license agreement, you will get a window similar to the following one:

|image9|

For the purposes of the book—and actually for any development environment—you should go for the first option: Developer Default. Keep going forward, leaving all the default options, until you get a window similar to this:

|image10|

Depending on your preferences, you can either just set a password for the root user, which is enough as it is only a development machine, or you can add an extra user by clicking on Add User. Make sure to set the correct name, password, and permissions. A user named test with administration permissions should look similar to the following screenshot:

|image11|

For the rest of the installation process, you can select all the default options.

Installing Nginx
----------------

The installation for Nginx is almost identical to the PHP 7 one. First, download the ZIP file from `*http://nginx.org/en/download.html* <http://nginx.org/en/download.html>`__. At the time of writing, the versions available are as follows:

|image12|

You can safely download the mainline version 1.9.10 or a later one if it is stable. Once the file is downloaded, uncompress it in C:\\nginx and run the following commands to start the web server:

    $ cd nginx $ start nginx

Installing Composer
-------------------

To finish with the setup, we need to install Composer. To go for the automatic installation, just download the installer from `*https://getcomposer.org/Composer-Setup.exe* <https://getcomposer.org/Composer-Setup.exe>`__. Once downloaded, execute it in order to install Composer on your system and to update your PATH environment variable.


chap The HTTP protocol
======================

If you check the RFC2068 standard at `*https://tools.ietf.org/html/rfc2068* <https://tools.ietf.org/html/rfc2068>`__, you will see that its description is almost endless. Luckily, what you need to know about this protocol, at least for starters, is way shorter.

HTTP stands for HyperText Transfer Protocol. As any other protocol, the goal is to allow two entities or nodes to communicate with each other. In order to achieve this, the messages need to be formatted in a way that they both understand, and the entities must follow some pre-established rules.

A simple example
----------------

The following diagram shows a very basic interchange of messages:

|A simple example|

A simple GET request

Do not worry if you do not understand all the elements in this diagram; we will describe them shortly. In this representation, there are two entities: sender and receiver. The sender sends a message to the receiver. This message, which starts the communication, is called the request. In this case, the message is a GET request. The receiver receives the message, processes it, and generates a second message: the response. In this case, the response shows a 200 status code, meaning that the request was processed successfully.

HTTP is stateless; that is, it treats each request independently, unrelated to any previous one. This means that with this request and response sequence, the communication is finished. Any new requests will not be aware of this specific interchange of messages.

Parts of the message
--------------------

An HTTP message contains several parts. We will define only the most important of them.

URL
~~~

The URL of the message is the destination of the message. The request will contain the receiver's URL, and the response will contain the sender's.

As you might know, the URL can contain extra parameters, known as a query string. This is used when the sender wants to add extra data. For example, consider this URL: http://myserver.com/greeting?name=Alex. This URL contains one parameter: name with the value Alex. It could not be represented as part of the URL http://myserver.com/greeting, so the sender chose to add it at the end of it. You will see later that this is not the only way that we can add extra information into a message.

The HTTP method
~~~~~~~~~~~~~~~

The HTTP method is the verb of the message. It identifies what kind of action the sender wants to perform with this message. The most common ones are GET and POST.

-  GET: This asks the receiver about something, and the receiver usually
   sends this information back. The most common example is asking for a
   web page, where the receiver will respond with the HTML code of the
   requested page.

-  POST: This means that the sender wants to perform an action that will
   update the data that the receiver is holding. For example, the sender
   can ask the receiver to update his profile name.

There are other methods, such as PUT, DELETE, or OPTION, but they are less used in web development, although they play a crucial role in REST APIs, which will be explained in `*Chapter 9* <#Top_of_ch09_html>`__, *Building REST APIs*.

Body
~~~~

The body part is usually present in response messages even though a request message can contain it too. The body of the message contains the content of the message itself; for example, if the user requested a web page, the body of the response would consist of the HTML code that represents this page.

Soon, we will discuss how the request can also contain a body, which is used to send extra information as part of the request, such as form
parameters.

The body can contain text in any format; it can be an HTML text that represents a web page, plain text, the content of an image, JSON, and so on.

Headers
~~~~~~~

The headers on an HTTP message are the metadata that the receiver needs in order to understand the content of the message. There are a lot of headers, and you will see some of them in this book.

Headers consist of a map of key-value pairs. The following could be the headers of a request:

    Accept: text/html Cookie: name=Richard

This request tells the receiver, which is a server, that it will accept text as HTML, which is the common way of representing a web page; and that it has a cookie named Richard.

The status code
~~~~~~~~~~~~~~~

The status code is present in responses. It identifies the status of the request with a numeric code so that browsers and other tools know how to react. For example, if we try to access a URL that does not exist, the server should reply with a status code 404. In this way, the browser knows what happened without even looking at the content of the response.

Common status codes are:

-  200: The request was successful

-  401: Unauthorized; the user does not have permission to see this
   resource

-  404: Page not found

-  500: Internal server error; something wrong happened on the server
   side and it could not be recovered

A more complex example
----------------------

The following diagram shows a POST request and its response:

|A more complex example|

A more complex POST request

In this exchange of messages, we can see the other important method, POST, in action. In this case, the sender tries to send a request in order to update some entity's data. The message contains a cookie ID with the value 84, which may identify the entity to update. It also contains two parameters in the body: name and age. This is the data that the receiver has to update.

.. admonition:: Submitting web forms

    Representing the parameters as part of the body is a common way to send information when submitting a form, but not the only one. You can add a query string to the URL, add JSON to the body of the message, and so on.

    The response has a status code of 200, meaning that the request was processed successfully. In addition, the response also contains a body, this time formatted as JSON, which represents the new status of the updated entity.


chap Web applications
=====================

Maybe you have noticed that in the previous sections, I used the not very intuitive terms of sender and receiver as they do not represent any specific scenario that you might know but rather all of them in a generic way. The main reason for this choice of terminology is to try to separate HTTP from web applications. You will see at the end of the book that HTTP is used for more than just websites.

If you are reading this book, you already know what a web application is. Alternatively, maybe you know it by other terms, such as website or web page. Let's try to give some definitions.

A web page is a single document with content. It contains links that open other web pages with different content.

A website is the set of web pages that usually live in the same server and are related to each other.

A web application is just a piece of software that runs on a client, which is usually a browser, and communicates with a *server*. A server is a remote machine that receives requests from a client, processes them, and generates a response. This response will go back to the client, generally rendered by the browser in order to display it to the
user.

Even though this is out of the scope of this book, you may be interested to know that not only browsers can act as clients, generating requests and sending them to the servers; even servers can be the ones taking the initiative of sending messages to the browsers.

So, what is the difference between a website and a web application? Well, the web application can be a small part of a bigger website with a specific functionality. Also, not all websites are web applications as a web application always does something but a website can just display information.

HTML, CSS, and JavaScript
-------------------------

Web applications are rendered by the browser so that the user can see its content. To do this, the server needs to send the content of the page or document. The document uses HTML to describe its elements and how they are organized. Elements can be links, buttons, input fields, and so on. A simple example of a web page looks like this:

.. code-block:: html

    <!DOCTYPE html> 
    <html lang="en"> 
        <head> 
            <meta charset="UTF-8">
            <title>Your first app</title> 
        </head> 
        <body> 
            <a id="special" class="link" href="http://yourpage.com">Your page</a> 
            <a class="link" href="http://theirpage.com">Their page</a> 
        </body>
    </html>

Let's focus on the highlighted code. As you can see, we are describing two <a> links with some properties. Both links have a class, a destination, and a text. The first one also contains an ID. Save this code into a file named index.html and execute it. You will see how your default browser opens a very simple page with two links.

If we want to add some styles, or change the color, size, and position of the links, we need to add CSS. CSS describes how elements from the HTML are displayed. There are several ways to include CSS, but the best approach is to have it in a separated file and then reference it from the HTML. Let's update our <head> section as shown in the following code:

.. code-block:: html

    <head> 
        <meta charset="UTF-8"> 
        <title>Your first app</title> 
        <link rel="stylesheet" type="text/css" href="mystyle.css"> 
    </head>

Now, let's create a new mystyle.css file in the same folder with the following content:

.. code-block:: css

    .link { color: green; font-weight: bold; } 
    #special { font-size: 30px; }

This CSS file contains two style definitions: one for the link class and one for the special ID. The class style will be applied to both the links as they both define this class, and it sets them as green and bold. The ID style that increases the font of the link is only applied to the first link.

Finally, in order to add behavior to our web page, we need to add JS or JavaScript. JS is a programming language that would need an entire book for itself, and in fact, there are quite a lot of them. If you want to give it a chance, we recommend the free online book *Eloquent JavaScript*, *Marijn Haverbeke*, which you can find at `*http://eloquentjavascript.net/* <http://eloquentjavascript.net/>`__. As with CSS, the best approach would be to add a separate file and then reference it from our HTML. Update the <body> section with the following highlighted code:

.. code-block:: html

    <body> 
        <a id="special" class="link" href="http://yourpage.com">Your page</a> 
        <a class="link" href="http://theirpage.com">Their page</a>
        <script src="myactions.js"></script> </body>

Now, create a myactions.js file with the following content:

.. code-block:: js

    document.getElementById("special").onclick = function() { alert("You
    clicked me?"); }

The JS file adds a function that will be called when the special link is
clicked on. This function just pops up an alert. You can save all your
changes and refresh the browser to see how it looks now and how the
links behave.

.. admonition:: Different ways of including JS

    You might notice that we included the CSS file reference at the end of the <head> section and JS at the end of <body>. You can actually include JS in both the <head> and the <body>; just bear in mind that the script will be executed as soon as it is included. If your script references fields that are not yet defined or other JS files that will be included later, JS will fail.

Congratulations! You just wrote your very first web page. Not impressed? Well, then you are reading the correct book! You will have the chance to work with more HTML, CSS, and JS during the book, even though the book focuses especially on PHP.

chap Web servers
================

So, it is about time that you learn what those famous web servers are. A web server is no more than a piece of software running on a machine and listening to requests from a specific port. Usually, this port is 80, but it can be any other that is available.

How they work
-------------

The following diagram represents the flow of request-response on the server side:

|How they work|

Request-response flow on the server side

The job of a web server is to route external requests to the correct application so that they can be processed. Once the application returns a response, the web server will send this response to the client. Let's take a close look at all the steps:

1. The client, which is a browser, sends a request. This can be of any
   type—GET or POST—and contain anything as long as it is valid.

2. The server receives the request, which points to a port. If there is
   a web server listening on this port, the web server will then take
   control of the situation.

3. The web server decides which web application—usually a file in the
   filesystem—needs to process the request. In order to decide, the web
   server usually considers the path of the URL; for example,
   http://myserver.com/app1/hi would try to pass the request to the app1
   application, wherever it is in the filesystem. However, another
   scenario would be http://app1.myserver.com/hi, which would also go to
   the same application. The rules are very flexible, and it is up to
   both the web server and the user as to how to set them.

4. The web application, after receiving a request from the web server,
   generates a response and sends it to the web server.

5. The web server sends the response to the indicated port.

6. The response finally arrives to the client.

The PHP built-in server
-----------------------

There are powerful web servers that support high loads of traffic, such as Apache or Nginx, which are fairly simple to install and manage. For the purpose of this book, though, we will use something even simpler: a PHP built-in server. The reason to use this is that you will not need extra package installations, configurations, and headaches as it comes with PHP. With just one command, you will have a web server running on your machine.

.. admonition:: Production web servers

    Note that the PHP built-in web server is good for testing purposes, but it is highly recommended not to use it in production environments. If you have to set up a server that needs to be public and your application is written in PHP, I highly recommend you to choose either of the classics: Apache (`*http://httpd.apache.org* <http://httpd.apache.org>`__) or Nginx (`*https://www.nginx.com* <https://www.nginx.com>`__). Both can run almost on any server, are free and easy to install and configure, and, more importantly, have a huge community that will support you on virtually any problem you might encounter.

Finally, hands on! Let's try to create our very first web page using the built-in server. For this, create an index.php file inside your
workspace directory—for example, Documents/workspace/index.php. The content of this file should be:

    <?php echo 'hello world';

Now, open your command line, go to your workspace directory, probably by running the cd Documents/workspace command, and run the following command:

    $ php -S localhost:8000

The command line will prompt you with some information, the most important one being what is listening, which should be localhost:8000 as specified, and how to stop it, usually by pressing *Ctrl* + *C*. Do not close the command line as it will stop the web server too.

Now, let's open a browser and go to http://localhost:8000. You should see a hello world message on a white page. Yay, success! If you are interested, you can check your command line, and you will see log entries of each request you are sending via your browser.

So, how does it really work? Well, if you check again in the previous diagram, the php -S command started a web server—in our case, listening to port 8000 instead of 80. Also, PHP knows that the web application code will be on the same directory that you started the web server: your workspace. There are more specific options, but by default, PHP will try to execute the index.php file in your workspace.

Putting things together
-----------------------

Let's try to include our first project (index.html with its CSS and JS files) as part of the built-in server. To do this, you just need to open the command line and go to the directory in which these files are and start the web server with php -S localhost:8000. If you check localhost:8000 in your browser, you will see our two-link page, as is expected.

Let's now move our new index.php file to the same directory. You do not need to restart your web server; PHP will know about the changes automatically. Go to your browser and refresh the page. You should now see the hello world message instead of the links. What happened here?

If you do not change the default options, PHP will always try to find an index.php file in the directory in which you started the web server. If this is not found, PHP will try to find an index.html file. Previously, we only had the index.html file, so PHP failed to find index.php. Now that it can find its first option, index.php, it will load it.

If we want to see our index.html file from the browser, we can always specify it in the URL like http://localhost:8000/index.html. If the web
server notices that you are trying to access a specific file, it will try to load it instead of the default options.

Finally, if we try to access a file that is not on our filesystem, the web server will return a response with status code 404—that is, not found. We can see this code if we open the Developer tools section of our browser and go to the Network section.

.. admonition:: Developer tools are your friends

    As a web developer, you will find very few tools more useful than the developer tools of your browser. It changes from browser to browser, but all of the big names, such as Chrome or Firefox, have it. It is very important that you get familiar with how to use it as it allows you to debug your applications from the client side.

    I will introduce you to some of these tools during the course of this book.

Summary
-------

In this chapter, you learned what HTTP is and how web applications use it in order to interact with the server. You also now know how web
servers work and how to launch a light built-in server with PHP. Finally, you took the first steps toward building your first web
application. Congratulations!

In the next chapter, we will take a look at the basics of PHP so that you can start building simple applications.
