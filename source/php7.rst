

Design patterns
===============

Developers have been creating code since way before the appearance of
with Internet, and they have been working on a number of different
areas, not just web applications. Because of that, a lot of people have
already had to confront similar scenarios, carrying the experience of
previous attempts for fixing the same thing. In short, it means that
almost surely, someone has already designed a good way of solving the
problem that you are facing now.

A lot of books have been written trying to group solutions to common
problems, also known as design patterns. Design patterns are not
algorithms that you copy and paste into your program, showing how to fix
something step-by-step, but rather recipes that show you, in a heuristic
way, how to look for the answer.

Studying them is essential if you want to become a professional
developer, not only for solving problems, but also for communicating
with other developers. It is very common to get an answer like "You
could use a factory here", when discussing your program design. It saves
a lot of time knowing what a factory is, rather than explaining the
pattern each time someone mentions it.

As we said, there are entire books that talk about design patterns, and
we highly recommend you to have a look at some of them. The goal of this
section is to show you what a design pattern is and how you can use it.
Additionally, we will show you some of the most common design patterns
used with PHP when writing web applications, excluding the MVC pattern,
which we will study in `*Chapter 6* <#Top_of_ch06_html>`__, *Adapting to
MVC*.

Other than books, you could also visit the open source project
DesignPatternsPHP at
`*http://designpatternsphp.readthedocs.org/en/latest/README.html* <http://designpatternsphp.readthedocs.org/en/latest/README.html>`__.
There is a good collection of them, and they are implemented in PHP, so
it would be easier for you to adapt.

Factory
-------

A factory is a design pattern of the creational group, which means that
it allows you to create objects. You might think that we do not need
such a thing, as creating an object is as easy as using the new keyword,
the class, and its arguments. But letting the user do that is dangerous
for different reasons. Apart from the increased difficulty caused by
using new when unit testing our code (you will learn about unit testing
in `*Chapter 7* <#Top_of_ch07_html>`__, *Testing Web Applications*), a
lot of coupling too gets added into our code.

When we discussed encapsulation, you learned that it is better to hide
the internal implementation of a class, and you could consider the
constructor as part of it. The reason is that the user needs to know at
all times how to create objects, including what the arguments of the
constructor are. And what if we want to change our constructor to accept
different arguments? We need to go one by one to all the places where we
have created objects and update them.

Another reason for using factories is to manage different classes that
inherit a super class or implement the same interface. As you know,
thanks to polymorphism, you can use one object without knowing the
specific class that it instantiates, as long as you know the interface
being implemented. It might so happen that your code needs to
instantiate an object that implements an interface and use it, but the
concrete class of the object may not be important at all.

Think about our bookstore example. We have two types of customers: basic
and premium. But for most of the code, we do not really care what type
of customer a specific instance is. In fact, we should implement our
code to use objects that implement the Customer interface, being unaware
of the specific type. So, if we decide in the future to add a new type,
as long as it implements the correct interface, our code will work
without an issue. But, if that is the case, what do we do when we need
to create a new customer? We cannot instantiate an interface, so let's
use the factory pattern. Add the following code into
src/Domain/Customer/CustomerFactory.php:

    <?php namespace Bookstore\\Domain\\Customer; use
    Bookstore\\Domain\\Customer; class CustomerFactory { public static
    function factory( string $type, int $id, string $firstname, string
    $surname, string $email ): Customer { switch ($type) { case 'basic':
    return new Basic($id, $firstname, $surname, $email); case 'premium':
    return new Premium($id, $firstname, $surname, $email); } } }

The factory in the preceding code is less than ideal for different
reasons. In the first one, we use a switch, and add a case for all the
existing customer types. Two types do not make much difference, but what
if we have 19? Let's try to make this factory method a bit more dynamic.

    public static function factory( string $type, int $id, string
    $firstname, string $surname, string $email ): Customer { $classname
    = \_\_NAMESPACE\_\_ . '\\\\' . ucfirst($type); if
    (!class\_exists($classname)) { throw new
    \\InvalidArgumentException('Wrong type.'); } return new
    $classname($id, $firstname, $surname, $email); }

Yes, you can do what we did in the preceding code in PHP. Instantiating
classes dynamically, that is, using the content of a variable as the
name of the class, is one of the things that makes PHP so flexible… and
dangerous. Used wrongly, it will make your code horribly difficult to
read and maintain, so be careful about it. Note too the constant
\_\_NAMESPACE\_\_, which contains the namespace of the current file.

Now this factory looks cleaner, and it is also very dynamic. You could
add more customer types and, as long as they are inside the correct
namespace and implement the interface, there is nothing to change on the
factory side, nor in the usage of the factory.

In order to use it, let's change our init.php file. You can remove all
our tests, and just leave the autoloader code. Then, add the following:

    CustomerFactory::factory('basic', 2, 'mary', 'poppins',
    'mary@poppins.com'); CustomerFactory::factory('premium', null,
    'james', 'bond', 'james@bond.com');

The factory design pattern can be as complex as you need. There are
different variants of it, and each one has its own place and time, but
the general idea is always the same.

Singleton
---------

If someone with a bit of experience with design patterns, or web
development in general, reads the title of this section, they will
probably start tearing their hair out and claiming that singleton is the
worst example of a design pattern. But just bear with me.

When explaining interfaces, I added a note about how developers tend to
complicate their code too much just so they can use all the tools they
know. Using design patterns is one of the cases where this happens. They
have been so famous, and people claimed that good use of them is
directly linked to great developers, that everybody that learns them
tries to use them absolutely everywhere.

The singleton pattern is probably the most infamous of the design
patterns used in PHP for web development. This pattern has a very
specific purpose, and when that is the case, the pattern proves to be
very useful. But this pattern is so easy to implement that developers
continuously try to add singletons everywhere, turning their code into
something unmaintainable. It is for this reason that people call this an
anti-pattern, something that should be avoided rather than used.

I do agree with this point of view, but I still think that you should be
very familiar with this design pattern. Even though you should avoid its
overuse, people still use it everywhere, and they refer to it countless
times, so you should be in a position to either agree with them or
rather have enough reasons to discourage them to use it. Having said
that, let's see what the aim of the singleton pattern is.

The idea is simple: singletons are used when you want one class to
always have one unique instance. Every time, and everywhere you use that
class, it has to be through the same instance. The reason is to avoid
having too many instances of some heavy resource, or to keep always the
same state everywhere—to be global. Examples of this are database
connections or configuration handlers.

Imagine that in order to run, our application needs some configuration,
such as credentials for the database, URLs of special endpoints,
directory paths for finding libraries or important files, and so on.
When you receive a request, the first thing you do is to load this
configuration from the filesystem, and then you store it as an array or
some other data structure. Save the following code as your
src/Utils/Config.php file:

    <?php namespace Bookstore\\Utils; use
    Bookstore\\Exceptions\\NotFoundException; class Config { private
    $data; public function \_\_construct() { $json =
    file\_get\_contents(\_\_DIR\_\_ . '/../../config/app.json');
    $this->data = json\_decode($json, true); } public function get($key)
    { if (!isset($this->data[$key])) { throw new NotFoundException("Key
    $key not in config."); } return $this->data[$key]; } }

As you can see, this class uses a new exception. Create it under
src/Utils/NotFoundException.php:

    <?php namespace Bookstore\\Exceptions; use Exception; class
    NotFoundException extends Exception { }

Also, the class reads a file, config/app.json. You could add the
following JSON map inside it:

    { "db": { "user": "Luke", "password": "Skywalker" } }

In order to use this configuration, let's add the following code into
your init.php file.

    $config = new Config(); $dbConfig = $config->get('db');
    var\_dump($dbConfig);

That seems a very good way to read configuration, right? But pay
attention to the highlighted line. We instantiate the Config object,
hence, we read a file, transform its contents from JSON to array, and
store it. What if the file contains hundreds of lines instead of just
six? You should notice then that instantiating this class is very
expensive.

You do not want to read the files and transform them into arrays each
time you ask for some data from your configuration. That is way too
expensive! But, for sure, you will need the configuration array in very
different places of your code, and you cannot carry this array
everywhere you go. If you understood static properties and methods, you
could argue that implementing a static array inside the object should
fix the problem. You instantiate it once, and then just call a static
method that will access an already populated static property.
Theoretically, we skip the instantiation, right?

    <?php namespace Bookstore\\Utils; use
    Bookstore\\Exceptions\\NotFoundException; class Config { private
    static $data; public function \_\_construct() { $json =
    file\_get\_contents(\_\_DIR\_\_ . '/../config/app.json');
    self::$data = json\_decode($json, true); } public static function
    get($key) { if (!isset(self::$data[$key])) { throw new
    NotFoundException("Key $key not in config."); } return
    self::$data[$key]; } }

This seems to be a good idea, but it is highly dangerous. How can you be
absolutely sure that the array has already been populated? And how can
you be sure that, even using a static context, the user will not keep
instantiating this class again and again? That is where singletons come
in handy.

Implementing a singleton implies the following points:

1. Make the constructor of the class private, so absolutely no one from
   outside the class can ever instantiate that class.

2. Create a static property named $instance, which will contain an
   instance of itself—that is, in our Config class, the $instance
   property will contain an instance of the class Config.

3. Create a static method, getInstance, which will check if $instance is
   null, and if it is, it will create a new instance using the private
   constructor. Either way, it will return the $instance property.

Let's see what the singleton class would look like:

    <?php namespace Bookstore\\Utils; use
    Bookstore\\Exceptions\\NotFoundException; class Config { private
    $data; private static $instance; private function \_\_construct() {
    $json = file\_get\_contents(\_\_DIR\_\_ . '/../config/app.json');
    $this->data = json\_decode($json, true); } public static function
    getInstance(){ if (self::$instance == null) { self::$instance = new
    Config(); } return self::$instance; } public function get($key) { if
    (!isset($this->data[$key])) { throw new NotFoundException("Key $key
    not in config."); } return $this->data[$key]; } }

If you run this code right now, it will throw you an error, as the
constructor of this class is private. First achievement unlocked! Let's
use this class properly:

    $config = Config::getInstance(); $dbConfig = $config->get('db');
    var\_dump($dbConfig);

Does it convince you? It proves to be very handy indeed. But I cannot
emphasize this enough: be careful when you use this design pattern, as
it has very, very, specific use cases. Avoid falling into the trap of
implementing it everywhere!

Anonymous functions
-------------------

Anonymous functions, or lambda functions, are functions without a name.
As they do not have a name, in order to be able to invoke them, we need
to store them as variables. It might be strange at the beginning, but
the idea is quite simple. At this point of time, we do not really need
any anonymous function, so let's just add the code into init.php, and
then remove it:

    $addTaxes = function (array &$book, $index, $percentage) {
    $book['price'] += round($percentage \* $book['price'], 2); };

This preceding anonymous function gets assigned to the variable
$addTaxes. It expects three arguments: $book (an array as a reference),
$index (not used), and $percentage. The function adds taxes to the price
key of the book, rounded to 2 decimal places (round is a native PHP
function). Do not mind the argument $index, it is not used in this
function, but forced by how we will use it, as you will see.

You could instantiate a list of books as an array, iterate them, and
then call this function each time. An example could be as follows:

    $books = [ ['title' => '1984', 'price' => 8.15], ['title' => 'Don
    Quijote', 'price' => 12.00], ['title' => 'Odyssey', 'price' => 3.55]
    ]; foreach ($books as $index => $book) { $addTaxes($book, $index,
    0.16); } var\_dump($books);

In order to use the function, you just invoke it as if $addTaxes
contained the name of the function to be invoked. The rest of the
function works as if it was a normal function: it receives arguments, it
can return a value, and it has a scope. What is the benefit of defining
it in this way? One possible application would be to use it as a
callable. A callable is a variable type that identifies a function that
PHP can call. You send this callable variable as an argument, and the
function that receives it can invoke it. Take the PHP native function,
array\_walk. It gets an array, a callable, and some extra arguments. PHP
will iterate the array, and for each element, it will invoke the
callable function (just like the foreach loop). So, you can replace the
whole loop by just the following:

    array\_walk($books, $addTaxes, 0.16);

The callable that array\_walk receives needs to take at least two
arguments: the value and the index of the current element of the array,
and thus, the $index argument that we were forced to implement
previously. It can optionally take extra arguments, which will be the
extra arguments sent to array\_walk—in this case, the 0.16 as
$percentage.

Actually, anonymous functions are not the only callable in PHP. You can
send normal functions and even class methods. Let's see how:

    function addTaxes(array &$book, $index, $percentage) { if
    (isset($book['price'])) { $book['price'] += round($percentage \*
    $book['price'], 2); } } class Taxes { public static function
    add(array &$book, $index, $percentage) { if (isset($book['price']))
    { $book['price'] += round($percentage \* $book['price'], 2); } }
    public function addTaxes(array &$book, $index, $percentage) { if
    (isset($book['price'])) { $book['price'] += round($percentage \*
    $book['price'], 2); } } } // using normal function
    array\_walk($books, 'addTaxes', 0.16); var\_dump($books); // using
    static class method array\_walk($books, ['Taxes', 'add'], 0.16);
    var\_dump($books); // using class method array\_walk($books, [new
    Taxes(), 'addTaxes'], 0.16); var\_dump($books);

In the preceding example, you can see how we can use each case as a
callable. For normal methods, just send the name of the method as a
string. For static methods of a class, send an array with the name of
the class in a way that PHP understands (either the full name including
namespace, or adding the use keyword beforehand), and the name of the
method, both as strings. To use a normal method of a class, you need to
send an array with an instance of that class and the method name as a
string.

OK, so anonymous functions can be used as callable, just as any other
function or method can. So what is so special about them? One of the
things is that anonymous functions are variables, and so they have all
the advantages—or disadvantages—that a variable has. That includes
scope—that is, the function is defined inside a scope, and as soon as
this scope ends, the function will no longer be accessible. That can be
useful if your function is extremely specific to that bit of code, and
there is no way you will want to reuse it somewhere else. Moreover, as
it is nameless, you will not have conflicts with any other existing
function.

There is another benefit in using anonymous functions: inheriting
variables from the parent scope. When you define an anonymous function,
you can specify some variable from the scope where it is defined with
the keyword use, and use it inside the function. The value of the
variable will be the one it had at the moment of declaring the function,
even if it is updated later. Let's see an example:

    $percentage = 0.16; $addTaxes = function (array &$book, $index) use
    ($percentage) { if (isset($book['price'])) { $book['price'] +=
    round($percentage \* $book['price'], 2); } }; $percentage = 100000;
    array\_walk($books, $addTaxes); var\_dump($books);

The preceding example shows you how to use the keyword use. Even when we
update $percentage after defining the function, the result shows you
that the taxes were only 16%. This is useful, as it liberates you from
having to send $percentage everywhere you want to use the function
$addTaxes. If there is a scenario where you really need to have the
updated value of the used variables, you can declare them as a reference
as you would with a normal function's argument:

    $percentage = 0.16; $addTaxes = function (array &$book, $index) use
    (&$percentage) { if (isset($book['price'])) { $book['price'] +=
    round($percentage \* $book['price'], 2); } }; array\_walk($books,
    $addTaxes, 0.16); var\_dump($books); $percentage = 100000;
    array\_walk($books, $addTaxes, 0.16); var\_dump($books);

In this last example, the first array\_walk used the original value
0.16, as that was still the value of the variable. But on the second
call, $percentage had already changed, and it affected the result of the
anonymous function.

Summary
-------

In this chapter, you have learned what object-oriented programming is,
and how to apply it to our web application for creating a clean code,
which is easy to maintain. You also know how to manage exceptions
properly, the design patterns that are used the most, and how to use
anonymous functions when necessary.

In the next chapter, we will explain how to manage the data of your
application using databases so that you can completely separate data
from code.

Chapter 5. Using Databases
===========================

Data is probably the cornerstone of most web applications. Sure, your
application has to be pretty, fast, error-free, and so on, but if
something is essential to users, it is what data you can manage for
them. From this, we can extract that managing data is one of the most
important things you have to consider when designing your application.

Managing data implies not only storing read-only files and reading them
when needed, as we were doing so far, but also adding, fetching,
updating, and removing individual pieces of information. For this, we
need a tool that categorizes our data and makes these tasks easier for
us, and this is when databases come into play.

In this chapter, you will learn about:

-  Schemas and tables

-  Manipulating and querying data

-  Using PDO to connect your database with PHP

-  Indexing your data

-  Constructing complex queries in joining tables

Introducing databases
=====================

Databases are tools to manage data. The basic functions of a database
are inserting, searching, updating, and deleting data, even though most
database systems do more than this. Databases are classified into two
different categories depending on how they store data: relational and
nonrelational databases.

Relational databases structure data in a very detailed way, forcing the
user to use a defined format and allowing the creation of
connections—that is, relations—between different pieces of information.
Nonrelational databases are systems that store data in a more relaxed
way, as though there were no apparent structure. Even though with these
very vague definitions you could assume that everybody would like to use
relational databases, both systems are very useful; it just depends on
how you want to use them.

In this book, we will focus on relational databases as they are widely
used in small web applications, in which there are not huge amounts of
data. The reason is that usually the application contains data that is
interrelated; for example, our application could store sales, which are
composed of customers and books.

MySQL
-----

MySQL has been the favorite choice of PHP developers for quite a long
time. It is a relational database system that uses SQL as the language
to communicate with the system. SQL is used in quite a few other
systems, which makes things easier in case you need to switch databases
or just need to understand an application with a different database than
the one you are used to. The rest of the chapter will be focused on
MySQL, but it will be helpful for you even if you choose a different SQL
system.

In order to use MySQL, you need to install two applications: the server
and the client. You might remember server-client applications from
`*Chapter 2* <#Top_of_ch02_html>`__, *Web Applications with PHP*. The
MySQL server is a program that listens for instructions or queries from
clients, executes them, and returns a result. You need to start the
server in order to access the database; take a look at `*Chapter
1* <#Top_of_ch01_html>`__, *Setting Up the Environment*, on how to do
this. The client is an application that allows you to construct
instructions and send them to the server, and it is the one that you
will use.

Note

GUI versus command line

The Graphical User Interface (GUI) is very common when using a database.
It helps you in constructing instructions, and you can even manage data
without them using just visual tables. On the other hand, command-line
clients force you to write all the commands by hand, but they are
lighter than GUIs, faster to start, and force you to remember how to
write SQL, which you need when you write your applications in PHP. Also,
in general, almost any machine with a database will have a MySQL client
but might not have a graphical application.

You can choose the one that you are more comfortable with as you will
usually work with your own machine. However, keep in mind that a basic
knowledge of the command line will save your life on several occasions.

In order to connect the client with a server, you need to provide some
information on where to connect and the credentials for the user to use.
If you do not customize your MySQL installation, you should at least
have a root user with no password, which is the one we will use. You
could think that this seems to be a horrible security hole, and it might
be so, but you should not be able to connect using this user if you do
not connect from the same machine on which the server is. The most
common arguments that you can use to provide information when starting
the client are:

-  -u <name>: This specifies the user—in our case, root.

-  -p<password>: Without a space, this specifies the password. As we do
   not have a password for our user, we do not need to provide this.

-  -h <host>: This specifies where to connect. By default, the client
   connects to the same machine. As this is our case, there is no need
   to specify any. If you had to, you could specify either an IP address
   or a hostname.

-  <schema name>: This specifies the name of the schema to use. We will
   explain in a bit what this means.

With these rules, you should be able to connect to your database with
the mysql -u root command. You should get an output very similar to the
following one:

    $ mysql -u root Welcome to the MySQL monitor. Commands end with ; or
    \\g. Your MySQL connection id is 2 Server version: 5.1.73 Source
    distribution Copyright (c) 2000, 2013, Oracle and/or its affiliates.
    All rights reserved. Oracle is a registered trademark of Oracle
    Corporation and/or its affiliates. Other names may be trademarks of
    their respective owners. Type 'help;' or '\\h' for help. Type '\\c'
    to clear the current input statement. mysql>

The terminal will show you the version of the server and some useful
information about how to use the client. From now on, the command line
will start with mysql> instead of your normal prompt, showing you that
you are using the MySQL client. In order to execute queries, just type
the query, end it with a semicolon, and press *Enter*. The client will
send the query to the server and will show the result of it. To exit the
client, you can either type \\q and press *Enter* or press *Ctrl* + *D*,
even though this last option will depend on your operating system.

Schemas and tables
==================

Relational database systems usually have the same structure. They store
data in different databases or schemas, which separate the data from
different applications. These schemas are just collections of tables.
Tables are definitions of specific data structures and are composed of
fields. A field is a basic data type that defines the smallest component
of information as though they were the atoms of the data. So, schemas
are group of tables that are composed of fields. Let's look at each of
these elements.

Understanding schemas
---------------------

As defined before, schemas or databases— in MySQL, they are synonyms—are
collections of tables with a common context, usually belonging to the
same application. Actually, there are no restrictions around this, and
you could have several schemas belonging to the same application if
needed. However, for small web applications, as it is our case, we will
have just one schema.

Your server probably already has some schemas. They usually contain the
metadata needed for MySQL in order to operate, and we highly recommend
that you do not modify them. Instead, let's just create our own schema.
Schemas are quite simple elements, and they only have a mandatory name
and an optional charset. The name identifies the schema, and the charset
defines which type of codification or "alphabet" the strings should
follow. As the default charset is latin1, if you do not need to change
it, you do not need to specify it.

Use CREATE SCHEMA followed by the name of the schema in order to create
the schema that we will use for our bookstore. The name has to be
representative, so let's name it bookstore. Remember to end your line
with a semicolon. Take a look at the following:

    mysql> CREATE SCHEMA bookstore; Query OK, 1 row affected (0.00 sec)

If you need to remember how a schema was created, you can use SHOW
CREATE SCHEMA to see its description, as follows:

    mysql> SHOW CREATE SCHEMA bookstore \\G
    \*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\* 1. row
    \*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\* Database:
    bookstore Create Database: CREATE DATABASE \`bookstore\` /\*!40100
    DEFAULT CHARACTER SET latin1 \*/ 1 row in set (0.00 sec)

As you can see, we ended the query with \\G instead of a semicolon. This
tells the client to format the response in a different way than the
semicolon does. When using a command of the SHOW CREATE family, we
recommend that you end it with \\G to get a better understanding.

Tip

Should you use uppercase or lowercase?

When writing queries, you might note that we used uppercase for keywords
and lowercase for identifiers, such as names of schemas. This is just a
convention widely used in order to make it clear what is part of SQL and
what is your data. However, MySQL keywords are case-insensitive, so you
could use any case indistinctively.

All data must belong to a schema. There cannot be data floating around
outside all schemas. This way, you cannot do anything unless you specify
the schema you want to use. In order to do this, just after starting
your client, use the USE keyword followed by the name of the schema.
Optionally, you could tell the client which schema to use when
connecting to it, as follows:

    mysql> USE bookstore; Database changed

If you do not remember what the name of your schema is or want to check
which other schemas are in your server, you can run the SHOW SCHEMAS;
command to get a list of them, as follows:

    mysql> SHOW SCHEMAS; +--------------------+ \| Database \|
    +--------------------+ \| information\_schema \| \| bookstore \| \|
    mysql \| \| test \| +--------------------+ 4 rows in set (0.00 sec)

Database data types
-------------------

As in PHP, MySQL also has data types. They are used to define which kind
of data a field can contain. As in PHP, MySQL is quite flexible with
data types, transforming them from one type to the other if needed.
There are quite a few of them, but we will explain the most important
ones. We highly recommend that you visit the official documentation
related to data types at
`*http://dev.mysql.com/doc/refman/5.7/en/data-types.html* <http://dev.mysql.com/doc/refman/5.7/en/data-types.html>`__
if you want to build applications with more complex data structures.

Numeric data types
~~~~~~~~~~~~~~~~~~

Numeric data can be categorized as integers or decimal numbers. For
integers, MySQL uses the INT data type even though there are versions to
store smaller numbers, such as TINYINT, SMALLINT, or MEDIUMINT, or
bigger numbers, such as BIGINT. The following table shows what the sizes
of the different numeric types are, so you can choose which one to use
depending on your situation:

+-------------+-----------------------------------------------------------+
| Type        | Size/precision                                            |
+-------------+-----------------------------------------------------------+
| TINYINT     | -128 to 127                                               |
+-------------+-----------------------------------------------------------+
| SMALLINT    | -32,768 to 32,767                                         |
+-------------+-----------------------------------------------------------+
| MEDIUMINT   | -8,388,608 to 8,388,607                                   |
+-------------+-----------------------------------------------------------+
| INT         | -2,147,483,648 to 2,147,483,647                           |
+-------------+-----------------------------------------------------------+
| BIGINT      | -9,223,372,036,854,775,808 to 9,223,372,036,854,775,807   |
+-------------+-----------------------------------------------------------+

Numeric types can be defined as signed by default or unsigned; that is,
you can allow or not allow them to contain negative values. If a numeric
type is defined as UNSIGNED, the range of numbers that it can take is
doubled as it does not need to save space for negative numbers.

For decimal numbers we have two types: approximate values, which are
faster to process but are not exact sometimes, and exact values that
give you exact precision on the decimal value. For approximate values or
the floating-point type, we have FLOAT and DOUBLE. For exact values or
the fixed-point type we have DECIMAL.

MySQL allows you to specify the number of digits and decimal positions
that the number can take. For example, to specify a number that can
contains five digits and up to two of them can be decimal, we will use
the FLOAT(5,2) notation. This is useful as a constraint, as you will
note when we create tables with prices.

String data types
~~~~~~~~~~~~~~~~~

Even though there are several data types that allow you to store from
single characters to big chunks of text or binary code, it is outside
the scope of this chapter. In this section, we will introduce you to
three types: CHAR, VARCHAR, and TEXT.

CHAR is a data type that allows you to store an exact number of
characters. You need to specify how long the string will be once you
define the field, and from this point on, all values for this field have
to be of this length. One possible usage in our applications could be
when storing the ISBN of the book as we know it is always 13 characters
long.

VARCHAR or variable char is a data type that allows you to store strings
up to 65,535 characters long. You do not need to specify how long they
need to be, and you can insert strings of different lengths without an
issue. Of course, the fact that this type is dynamic makes it slower to
process compared with the previous one, but after a few times you know
how long a string will always be. You could tell MySQL that even if you
want to insert strings of different lengths, the maximum length will be
a determined number. This will help its performance. For example, names
are of different lengths, but you can safely assume that no name will be
longer than 64 characters, so your field could be defined as
VARCHAR(64).

Finally, TEXT is a data type for really big strings. You could use it if
you want to store long comments from users, articles, and so on. As with
INT, there are different versions of this data type: TINYTEXT, TEXT,
MEDIUMTEXT, and LONGTEXT. Even if they are very important in almost any
web application with user interaction, we will not use them in ours.

List of values
~~~~~~~~~~~~~~

In MySQL, you can force a field to have a set of valid values. There are
two types of them: ENUM, which allows exactly one of the possible
predefined values, and SET, which allows any number of the predefined
values.

For example, in our application, we have two types of customers: basic
and premium. If we want to store our customers in a database, there is a
chance that one of the fields will be the type of customer. As a
customer has to be either basic or premium, a good solution would be to
define the field as an enum as ENUM("basic", "premium"). In this way, we
will make sure that all customers stored in our database will be of a
correct type.

Although enums are quite common to use, the use of sets is less
widespread. It is usually a better idea to use an extra table to define
the values of the list, as you will note when we talk about foreign keys
in this chapter.

Date and time data types
~~~~~~~~~~~~~~~~~~~~~~~~

Date and time types are the most complex data types in MySQL. Even
though the idea is simple, there are several functions and edge cases
around these types. We cannot go through all of them, so we will just
explain the most common uses, which are the ones we will need for our
application.

DATE stores dates—that is, a combination of day, month, and year. TIME
stores times—that is, a combination of hour, minute, and second.
DATETIME are data types for both date and time. For any of these data
types, you can provide just a string specifying what the value is, but
you need to be careful with the format that you use. Even though you can
always specify the format that you are entering the data in, you can
just enter the dates or times in the default format—for example,
2014-12-31 for dates, 14:34:50 for time, and 2014-12-31 14:34:50 for the
date and time.

A fourth type is TIMESTAMP. This type stores an integer, which is the
representation of the seconds from January 1, 1970, which is also known
as the Unix timestamp. This is a very useful type as in PHP, it is
really easy to get the current Unix timestamp with the now() function,
and the format for this data type is always the same, so it is safer to
work with it. The downside is that the range of dates that it can
represent is limited as compared to other types.

There are some functions that help you manage these types. These
functions extract specific parts of the whole value, return the value
with a different format, add or subtract dates, and so on. Let's take a
look at a short list of them:

+---------------------------------------+---------------------------------------------------------------------------------------------------------+
| Function name                         | Description                                                                                             |
+---------------------------------------+---------------------------------------------------------------------------------------------------------+
| DAY(), MONTH(), and YEAR()            | Extracts the specific value for the day, month, or year from the DATE or DATETIME provided value.       |
+---------------------------------------+---------------------------------------------------------------------------------------------------------+
| HOUR(), MINUTE(), and SECOND()        | Extracts the specific value for the hour, minute, or second from the TIME or DATETIME provided value.   |
+---------------------------------------+---------------------------------------------------------------------------------------------------------+
| CURRENT\_DATE() and CURRENT\_TIME()   | Returns the current date or current time.                                                               |
+---------------------------------------+---------------------------------------------------------------------------------------------------------+
| NOW()                                 | Returns the current date and time.                                                                      |
+---------------------------------------+---------------------------------------------------------------------------------------------------------+
| DATE\_FORMAT()                        | Returns the DATE, TIME or DATETIME value with the specified format.                                     |
+---------------------------------------+---------------------------------------------------------------------------------------------------------+
| DATE\_ADD()                           | Adds the specified interval of time to a given date or time type.                                       |
+---------------------------------------+---------------------------------------------------------------------------------------------------------+

Do not worry if you are confused on how to use any of these functions;
we will use them during the rest of the book as part of our application.
Also, an extensive list of all the types can be found at
`*http://dev.mysql.com/doc/refman/5.7/en/date-and-time-functions.html* <http://dev.mysql.com/doc/refman/5.7/en/date-and-time-functions.html>`__.

Managing tables
---------------

Now that you understand the different types of data that fields can
take, it is time to introduce tables. As defined in the *Schemas and
tables* section, a table is a collection of fields that defines a type
of information. You could compare it with OOP and think of tables as
classes, fields being their properties. Each instance of the class would
be a row on the table.

When defining a table, you have to declare the list of fields that the
table contains. For each field, you need to specify its name, its type,
and some extra information depending on the type of the field. The most
common are:

-  NOT NULL: This is used if the field cannot be null—that is, if it
   needs a concrete valid value for each row. By default, a field can be
   null.

-  UNSIGNED: As mentioned earlier, this is used to forbid the use of
   negative numbers in this field. By default, a numeric field accepts
   negative numbers.

-  DEFAULT <value>: This defines a default value in case the user does
   not provide any. Usually, the default value is null if this clause is
   not specified.

Table definitions also need a name, as with schemas, and some optional
attributes. You can define the charset of the table or its engine.
Engines can be a quite large topic to cover, but for the scope of this
chapter, let's just note that we should use the InnoDB engine if we need
strong relationships between tables. For more advanced readers, you can
read more about MySQL engines at
`*https://dev.mysql.com/doc/refman/5.0/en/storage-engines.html* <https://dev.mysql.com/doc/refman/5.0/en/storage-engines.html>`__.

Knowing this, let's try to create a table that will keep our books. The
name of the table should be book, as each row will define a book. The
fields could have the same properties the Book class has. Let's take a
look at how the query to construct the table would look:

    mysql> CREATE TABLE book( -> isbn CHAR(13) NOT NULL, -> title
    VARCHAR(255) NOT NULL, -> author VARCHAR(255) NOT NULL, -> stock
    SMALLINT UNSIGNED NOT NULL DEFAULT 0, -> price FLOAT UNSIGNED -> )
    ENGINE=InnoDb; Query OK, 0 rows affected (0.01 sec)

As you can note, we can add more new lines until we end the query with a
semicolon. With this, we can format the query in a way that looks more
readable. MySQL will let us know that we are still writing the same
query showing the -> prompt. As this table contains five fields, it is
very likely that we will need to refresh our minds from time to time as
we will forget them. In order to display the structure of the table, you
could use the DESC command, as follows:

    mysql> DESC book;
    +--------+----------------------+------+-----+---------+-------+ \|
    Field \| Type \| Null \| Key \| Default \| Extra \|
    +--------+----------------------+------+-----+---------+-------+ \|
    isbn \| char(13) \| NO \| \| NULL \| \| \| title \| varchar(255) \|
    NO \| \| NULL \| \| \| author \| varchar(255) \| NO \| \| NULL \| \|
    \| stock \| smallint(5) unsigned \| NO \| \| 0 \| \| \| price \|
    float unsigned \| YES \| \| NULL \| \|
    +--------+----------------------+------+-----+---------+-------+ 5
    rows in set (0.00 sec)

We used SMALLINT for stock as it is very unlikely that we will have more
than thousands of copies of the same book. As we know that ISBN is 13
characters long, we enforced this when defining the field. Finally, both
stock and price are unsigned as negative values do not make sense. Let's
now create our customer table via the following script:

    mysql> CREATE TABLE customer( -> id INT UNSIGNED NOT NULL, ->
    firstname VARCHAR(255) NOT NULL, -> surname VARCHAR(255) NOT NULL,
    -> email VARCHAR(255) NOT NULL, -> type ENUM('basic', 'premium') ->
    ) ENGINE=InnoDb; Query OK, 0 rows affected (0.00 sec)

We already anticipated the use of enum for the field type as when
designing classes, we could draw a diagram identifying the content of
our database. On this, we could show the tables and their fields. Let's
take a look at how the diagram of tables would look so far:

|image25|

Note that even if we create tables similar to our classes, we will not
create a table for Person. The reason is that databases store data, and
there isn't any data that we could store for this class as the customer
table already contains everything we need. Also, sometimes, we may
create tables that do not exist as classes on our code, so the
class-table relationship is a very flexible one.

Keys and constraints
====================

Now that we have our main tables defined, let's try to think about how
the data inside would look. Each row inside a table would describe an
object, which may be either a book or a customer. What would happen if
our application has a bug and allows us to create books or customers
with the same data? How will the database differentiate them? In theory,
we will assign IDs to customers in order to avoid these scenarios, but
how do we enforce that the ID not be repeated?

MySQL has a mechanism that allows you to enforce certain restrictions on
your data. Other than attributes such as NOT NULL or UNSIGNED that you
already saw, you can tell MySQL that certain fields are more special
than others and instruct it to add some behavior to them. These
mechanisms are called keys, and there are four types: primary key,
unique key, foreign key, and index. Let's take a closer look at them.

Primary keys
------------

Primary keys are fields that identify a unique row from a table. There
cannot be two of the same value in the same table, and they cannot be
null. Adding a primary key to a table that defines *objects* is almost a
must as it will assure you that you will always be able to differentiate
two rows by this field.

Another part that makes primary keys so attractive is their ability to
set the primary key as an autoincremental numeric value; that is, you do
not have to assign a value to the ID, and MySQL will just pick up the
latest inserted ID and increment it by 1, as we did with our Unique
trait. Of course, for this to happen, your field has to be an integer
data type. In fact, we highly recommend that you always define your
primary key as an integer, even if the real-life object does not really
have this ID at all. The reason is that you should search a row by this
numeric ID, which is unique, and MySQL will add some performance
improvements that come by setting the field as a key.

Then, let's add an ID to our book table. In order to add a new field, we
need to alter our table. There is a command that allows you to do this:
ALTER TABLE. With this command, you can modify the definition of any
existing field, add new ones, or remove existing ones. As we add the
field that will be our primary key and be autoincremental, we can add
all these modifiers to the field definition. Execute the following code:

    mysql> ALTER TABLE book -> ADD id INT UNSIGNED NOT NULL
    AUTO\_INCREMENT -> PRIMARY KEY FIRST; Query OK, 0 rows affected
    (0.02 sec) Records: 0 Duplicates: 0 Warnings: 0

Note FIRST at the end of the command. When adding new fields, if you
want them to appear on a different position than at the end of the
table, you need to specify the position. It could be either FIRST or
AFTER <other field>. For convenience, the primary key of a table is the
first of its fields.

As the table customer already has an ID field, we do not have to add it
again but rather modify it. In order to do this, we will just use the
ALTER TABLE command with the MODIFY option, specifying the new
definition of an already existing field, as follows:

    mysql> ALTER TABLE customer -> MODIFY id INT UNSIGNED NOT NULL ->
    AUTO\_INCREMENT PRIMARY KEY; Query OK, 0 rows affected (0.00 sec)
    Records: 0 Duplicates: 0 Warnings: 0

Foreign keys
------------

Let's imagine that we need to keep track of the borrowed books. The
table should contain the borrowed book, who borrowed it, and when it was
borrowed. So, what kind of data would you use to identify the book or
the customer? Would you use the title or the name? Well, we should use
something that identifies a unique row from these tables, and this
"something" is the primary key. With this action, we will eliminate the
change of using a reference that can potentially point to two or more
rows at the same time.

We could then create a table that contains book\_id and customer\_id as
numeric fields, containing the IDs that reference these two tables. As
the first approach, it makes sense, but we can find some weaknesses. For
example, what happens if we insert wrong IDs and they do not exist in
book or customer? We could have some code in our PHP side to make sure
that when fetching information from borrowed\_books, we only displayed
the information that is correct. We could even have a routine that
periodically checks for wrong rows and removes them, solving the issue
of having wrong data wasting space in the disk. However, as with the
Unique trait versus adding primary keys in MySQL, it is usually better
to allow the database system to manage these things as the performance
will usually be better, and you do not need to write extra code.

MySQL allows you to create keys that enforce references to other tables.
These are called foreign keys, and they are the primary reason for which
we were forced to use the InnoDB table engine instead of any other. A
foreign key defines and enforces a reference between this field and
another row of a different table. If the ID supplied for the field with
a foreign key does not exist in the referenced table, the query will
fail. Furthermore, if you have a valid borrowed\_books row pointing to
an existing book and you remove the entry from the book table, MySQL
will complain about it—even though you will be able to customize this
behavior soon—as this action would leave wrong data in the system. As
you can note, this is way more useful than having to write code to
manage these cases.

Let's create the borrowed\_books table with the book, customer
references, and dates. Note that we have to define the foreign keys
after the definition of the fields as opposed to when we defined primary
keys, as follows:

    mysql> CREATE TABLE borrowed\_books( -> book\_id INT UNSIGNED NOT
    NULL, -> customer\_id INT UNSIGNED NOT NULL, -> start DATETIME NOT
    NULL, -> end DATETIME DEFAULT NULL, -> FOREIGN KEY (book\_id)
    REFERENCES book(id), -> FOREIGN KEY (customer\_id) REFERENCES
    customer(id) -> ) ENGINE=InnoDb; Query OK, 0 rows affected (0.00
    sec)

As with SHOW CREATE SCHEMA, you can also check how the table looks. This
command will also show you information about the keys as opposed to the
DESC command. Let's take a look at how it would work:

    mysql> SHOW CREATE TABLE borrowed\_books \\G
    \*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\* 1. row
    \*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\* Table:
    borrowed\_books Create Table: CREATE TABLE \`borrowed\_books\` (
    \`book\_id\` int(10) unsigned NOT NULL, \`customer\_id\` int(10)
    unsigned NOT NULL, \`start\` datetime NOT NULL, \`end\` datetime
    DEFAULT NULL, KEY \`book\_id\` (\`book\_id\`), KEY \`customer\_id\`
    (\`customer\_id\`), CONSTRAINT \`borrowed\_books\_ibfk\_1\` FOREIGN
    KEY (\`book\_id\`) REFERENCES \`book\` (\`id\`), CONSTRAINT
    \`borrowed\_books\_ibfk\_2\` FOREIGN KEY (\`customer\_id\`)
    REFERENCES \`customer\` (\`id\`) ) ENGINE=InnoDB DEFAULT
    CHARSET=latin1 1 row in set (0.00 sec)

Note two important things here. On one hand, we have two extra keys that
we did not define. The reason is that when defining a foreign key, MySQL
also defines the field as a key that will be used to improve performance
on the table; we will look into this in a moment. The other element to
note is the fact that MySQL defines names to the keys by itself. This is
necessary as we need to be able to reference them in case we want to
change or remove this key. You can let MySQL name the keys for you, or
you can specify the names you prefer when creating them.

We are running a bookstore, and even if we allow customers to borrow
books, we want to be able to sell them. A sale is a very important
element that we need to track down as customers may want to review them,
or you may just need to provide this information for taxation purposes.
As opposed to borrowing, in which knowing the book, customer, and date
was more than enough, here, we need to set IDs to the sales in order to
identify them to the customers.

However, this table is more difficult to design than the other ones and
not just because of the ID. Think about it: do customers buy books one
by one? Or do they rather buy any number of books at once? Thus, we need
to allow the table to contain an undefined amount of books. With PHP,
this is easy as we would just use an array, but we do not have arrays in
MySQL. There are two options to this problem.

One solution could be to set the ID of the sale as a normal integer
field and not as a primary key. In this way, we would be able to insert
several rows to the sales table, one for each borrowed book. However,
this solution is less than ideal as we miss the opportunity of defining
a very good primary key because it has the sales ID. Also, we are
duplicating the data about the customer and date since they will always
be the same.

The second solution, the one that we will implement, is the creation of
a separated table that acts as a "list". We will still have our sales
table, which will contain the ID of the sale as a primary key, the
customer ID as a foreign key, and the dates. However, we will create a
second table that we could name sale\_book, and we will define there the
ID of the sale, the ID of the book, and the amount of books of the same
copy that the customer bought. In this way, we will have at once the
information about the customer and date, and we will be able to insert
as many rows as needed in our sale\_book list-table without duplicating
any data. Let's take a look at how we would create these:

    mysql> CREATE TABLE sale( -> id INT UNSIGNED NOT NULL
    AUTO\_INCREMENT PRIMARY KEY, -> customer\_id INT UNSIGNED NOT NULL,
    -> date DATETIME NOT NULL, -> FOREIGN KEY (customer\_id) REFERENCES
    customer(id) -> ) ENGINE=InnoDb; Query OK, 0 rows affected (0.00
    sec) mysql> CREATE TABLE sale\_book( -> sale\_id INT UNSIGNED NOT
    NULL, -> book\_id INT UNSIGNED NOT NULL, -> amount SMALLINT UNSIGNED
    NOT NULL DEFAULT 1, -> FOREIGN KEY (sale\_id) REFERENCES sale(id),
    -> FOREIGN KEY (book\_id) REFERENCES book(id) -> ) ENGINE=InnoDb;
    Query OK, 0 rows affected (0.00 sec)

Keep in mind that you should always create the sales table first because
if you create the sale\_book table with a foreign key first, referencing
a table that does not exist yet, MySQL will complain.

We created three new tables in this section, and they are interrelated.
It is a good time to update the diagram of tables. Note that we link the
fields with the tables when there is a foreign key defined. Take a look:

|image26|

Unique keys
-----------

As you know, primary keys are extremely useful as they provide several
features with them. One of these is that the field has to be unique.
However, you can define only one primary key per table, even though you
might have several fields that are unique. In order to amend this
limitation, MySQL incorporates unique keys. Their job is to make sure
that the field is not repeated in multiple rows, but they do not come
with the rest of the functionalities of primary keys, such as being
autoincremental. Also, unique keys can be null.

Our book and customer tables contain good candidates for unique keys.
Books can potentially have the same title, and surely, there will be
more than one book by the same author. However, they also have an ISBN
which is unique; two different books should not have the same ISBN. In
the same way, even if two customers were to have the same name, their
e-mail addresses will be always different. Let's add the two keys with
the ALTER TABLE command, though you can also add them when creating the
table as we did with foreign keys, as follows:

    mysql> ALTER TABLE book ADD UNIQUE KEY (isbn); Query OK, 0 rows
    affected (0.01 sec) Records: 0 Duplicates: 0 Warnings: 0 mysql>
    ALTER TABLE customer ADD UNIQUE KEY (email); Query OK, 0 rows
    affected (0.01 sec) Records: 0 Duplicates: 0 Warnings: 0

Indexes
-------

Indexes, which are a synonym for keys, are fields that do not need any
special behavior as do the rest of the keys but they are important
enough in our queries. So, we will ask MySQL to do some work with them
in order to perform better when querying by this field. Do you remember
when adding a foreign key that MySQL added extra keys to the table?
Those were indexes too.

Think about how the application will use the database. We want to show
the catalog of books to our customers, but we cannot show all of them at
once for sure. The customer will want to filter the results, and one of
the most common ways of filtering is by specifying the title of the book
that they are looking for. From this, we can extract that the title will
be used to filter books quite often, so we want to add an index to this
field. Let's add the index via the following code:

    mysql> ALTER TABLE book ADD INDEX (title); Query OK, 0 rows affected
    (0.01 sec) Records: 0 Duplicates: 0 Warnings: 0

Remember that all other keys also provide indexing. IDs of books,
customers and sales, ISBNs, and e-mails are already indexed, so there is
no need to add another index here. Also, try not to add indexes to every
single field as in doing so you will be overindexing, which would make
some types of queries even slower than if they were without indexes!

Inserting data
==============

We have created the perfect tables to hold our data, but so far they are
empty. It is time that we populate them. We delayed this moment as
altering tables with data is more difficult than when they are empty.

In order to insert this data, we will use the INSERT INTO command. This
command will take the name of the table, the fields that you want to
populate, and the data for each field. Note that you can choose not to
specify the value for a field, and there are different reasons to do
this, which are as follows:

-  The field has a default value, and we are happy using it for this
   specific row

-  Even though the field does not have an explicit default value, the
   field can take null values; so, by not specifying the field, MySQL
   will automatically insert a null here

-  The field is a primary key and is autoincremental, and we want to let
   MySQL take the next ID for us

There are different reasons that can cause an INSERT INTO command to
fail:

-  If you do not specify the value of a field and MySQL cannot provide a
   valid default value

-  If the value provided is not of the type of the field and MySQL fails
   to find a valid conversion

-  If you specify that you want to set the value for a field but you
   fail to provide a value

-  If you provide a foreign key with an ID but the ID does not exist in
   the referenced table

Let's take a look at how to add rows. Let's start with our customer
table, adding one basic and one premium, as follows:

    mysql> INSERT INTO customer (firstname, surname, email, type) ->
    VALUES ("Han", "Solo", "han@tatooine.com", "premium"); Query OK, 1
    row affected (0.00 sec) mysql> INSERT INTO customer (firstname,
    surname, email, type) -> VALUES ("James", "Kirk", "enter@prise",
    "basic"); Query OK, 1 row affected (0.00 sec)

Note that MySQL shows you some return information; in this case, it
shows that there was one row affected, which is the row that we
inserted. We did not provide an ID, so MySQL just added the next ones in
the list. As it is the first time that we are adding data, MySQL used
the IDs 1 and 2.

Let's try to trick MySQL and add another customer, repeating the e-mail
address field that we set as unique in the previous section:

    mysql> INSERT INTO customer (firstname, surname, email, type) ->
    VALUES ("Mr", "Spock", "enter@prise", "basic"); ERROR 1062 (23000):
    Duplicate entry 'enter@prise' for key 'email'

An error is returned with an error code and an error message, and the
row was not inserted, of course. The error message usually contains
enough information in order to understand the issue and how to fix it.
If this is not the case, we can always try to search on the Internet
using the error code and note what either the official documentation or
other users have to say about it.

In case you need to introduce multiple rows to the same table and they
contain the same fields, there is a shorter version of the command, in
which you can specify the fields and then provide the groups of values
for each row. Let's take a look at how to use it when adding books to
our book table, as follows:

    mysql> INSERT INTO book (isbn,title,author,stock,price) VALUES ->
    ("9780882339726","1984","George Orwell",12,7.50), ->
    ("9789724621081","1Q84","Haruki Murakami",9,9.75), ->
    ("9780736692427","Animal Farm","George Orwell",8,3.50), ->
    ("9780307350169","Dracula","Bram Stoker",30,10.15), ->
    ("9780753179246","19 minutes","Jodi Picoult",0,10); Query OK, 5 rows
    affected (0.01 sec) Records: 5 Duplicates: 0 Warnings: 0

As with customers, we will not specify the ID and let MySQL choose the
appropriate one. Note also that now the amount of affected rows is 5 as
we inserted five rows.

How can we take advantage of the explicit defaults that we defined in
our tables? Well, we can do this in the same way as we did with the
primary keys: do not specify them in the fields list or in the values
list, and MySQL will just use the default value. For example, we defined
a default value of 1 for our book.stock field, which is a useful
notation for the book table and the stock field. Let's add another row
using this default, as follows:

    mysql> INSERT INTO book (isbn,title,author,price) VALUES ->
    ("9781416500360", "Odyssey", "Homer", 4.23); Query OK, 1 row
    affected (0.00 sec)

Now that we have books and customers, let's add some historic data about
customers borrowing books. For this, use the numeric IDs from book and
customer, as in the following code:

    mysql> INSERT INTO borrowed\_books(book\_id,customer\_id,start,end)
    -> VALUES -> (1, 1, "2014-12-12", "2014-12-28"), -> (4, 1,
    "2015-01-10", "2015-01-13"), -> (4, 2, "2015-02-01", "2015-02-10"),
    -> (1, 2, "2015-03-12", NULL); Query OK, 3 rows affected (0.00 sec)
    Records: 3 Duplicates: 0 Warnings: 0

Querying data
=============

It took quite a lot of time, but we are finally in the most exciting—and
useful—section related to databases: querying data. Querying data refers
to asking MySQL to return rows from the specified table and optionally
filtering these results by a set of rules. You can also choose to get
specific fields instead of the whole row. In order to query data, we
will use the SELECT command, as follows:

    mysql> SELECT firstname, surname, type FROM customer;
    +-----------+---------+---------+ \| firstname \| surname \| type \|
    +-----------+---------+---------+ \| Han \| Solo \| premium \| \|
    James \| Kirk \| basic \| +-----------+---------+---------+ 2 rows
    in set (0.00 sec)

One of the simplest ways to query data is to specify the fields of
interest after SELECT and specify the table with the FROM keyword. As we
did not add any filters—mostly known as conditions—to the query, we got
all the rows there. Sometimes, this is the desired behavior, but the
most common thing to do is to add conditions to the query to retrieve
only the rows that we need. Use the WHERE keyword to achieve this.

    mysql> SELECT firstname, surname, type FROM customer -> WHERE id =
    1; +-----------+---------+---------+ \| firstname \| surname \| type
    \| +-----------+---------+---------+ \| Han \| Solo \| premium \|
    +-----------+---------+---------+ 1 row in set (0.00 sec)

Adding conditions is very similar to when we created Boolean expressions
in PHP. We will specify the name of the field, an operator, and a value,
and MySQL will retrieve only the rows that return true to this
expression. In this case, we asked for the customers that had the ID 1,
and MySQL returned one row: the one that had an ID of exactly 1.

A common query would be to get the books that start with some text. We
cannot construct this expression with any comparison operand that you
know, such as = and < or >, since we want to match only a part of the
string. For this, MySQL has the LIKE operator, which takes a string that
can contain wildcards. A wildcard is a character that represents a rule,
matching any number of characters that follows the rule. For example,
the % wildcard represents any number of characters, so using the 1%
string would match any string that starts with 1 and is followed by any
number or characters, matching strings such as 1984 or 1Q84. Let's
consider the following example:

    mysql> SELECT title, author, price FROM book -> WHERE title LIKE
    "1%"; +------------+-----------------+-------+ \| title \| author \|
    price \| +------------+-----------------+-------+ \| 1984 \| George
    Orwell \| 7.5 \| \| 1Q84 \| Haruki Murakami \| 9.75 \| \| 19 minutes
    \| Jodi Picoult \| 10 \| +------------+-----------------+-------+ 3
    rows in set (0.00 sec)

We asked for all the books whose title starts with 1, and we got three
rows. You can imagine how useful this operator is, especially when we
implement a search utility in our application.

As in PHP, MySQL also allows you to add logical operators—that is,
operators that take operands and perform a logical operation, returning
Boolean values as a result. The most common logical operators are, as in
PHP, AND and OR. AND returns true if both the expressions are true and
OR returns true if either of the operands is true. Let's consider an
example, as follows:

    mysql> SELECT title, author, price FROM book -> WHERE title LIKE
    "1%" AND stock > 0; +------------+-----------------+-------+ \|
    title \| author \| price \| +------------+-----------------+-------+
    \| 1984 \| George Orwell \| 7.5 \| \| 1Q84 \| Haruki Murakami \|
    9.75 \| +------------+-----------------+-------+ 2 rows in set (0.00
    sec)

This example is very similar to the previous one, but we added an extra
condition. We asked for all titles starting with 1 and whether there is
stock available. This is why one of the books does not show as it does
not satisfy both conditions. You can add as many conditions as you need
with logical operators but bear in mind that AND operators take
precedence over OR. If you want to change this precedence, you can
always wrap expressions with a parenthesis, as in PHP.

So far, we have retrieved specific fields when querying for data, but we
could ask for all the fields in a given table. To do this, we will just
use the \* wildcard in SELECT. Let's select all the fields for the
customers via the following code:

    mysql> SELECT \* FROM customer \\G
    \*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\* 1. row
    \*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\* id: 1
    firstname: Han surname: Solo email: han@tatooine.com type: premium
    \*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\* 2. row
    \*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\* id: 2
    firstname: James surname: Kirk email: enter@prise type: basic 2 rows
    in set (0.00 sec)

You can retrieve more information than just fields. For example, you can
use COUNT to retrieve the amount of rows that satisfy the given
conditions instead of retrieving all the columns. This way is faster
than retrieving all the columns and then counting them because you save
time in reducing the size of the response. Let's consider how it would
look:

    mysql> SELECT COUNT(\*) FROM borrowed\_books -> WHERE customer\_id =
    1 AND end IS NOT NULL; +----------+ \| COUNT(\*) \| +----------+ \|
    1 \| +----------+ 1 row in set (0.00 sec)

As you can note, the response says 1, which means that there is only one
borrowed book that satisfies the conditions. However, check the
conditions; you will note that we used another familiar logical
operator: NOT. NOT negates the expression, as ! does in PHP. Note also
that we do not use the equal sign to compare with null values. In MySQL,
you have to use IS instead of the equals sign in order to compare with
NULL. So, the second condition would be satisfied when a borrowed book
has an end date that is not null.

Let's finish this section by adding two more features when querying
data. The first one is the ability to specify in what order the rows
should be returned. To do this, just use the keyword ORDER BY followed
by the name of the field that you want to order by. You could also
specify whether you want to order in ascending mode, which is by
default, or in the descending mode, which can be done by appending DESC.
The other feature is the ability to limit the amount of rows to return
using LIMIT and the amount of rows to retrieve. Now, run the following:

    mysql> SELECT id, title, author, isbn FROM book -> ORDER BY title
    LIMIT 4; +----+-------------+-----------------+---------------+ \|
    id \| title \| author \| isbn \|
    +----+-------------+-----------------+---------------+ \| 5 \| 19
    minutes \| Jodi Picoult \| 9780753179246 \| \| 1 \| 1984 \| George
    Orwell \| 9780882339726 \| \| 2 \| 1Q84 \| Haruki Murakami \|
    9789724621081 \| \| 3 \| Animal Farm \| George Orwell \|
    9780736692427 \|
    +----+-------------+-----------------+---------------+ 4 rows in set
    (0.00 sec)

Using PDO
=========

So far, we have worked with MySQL, and you already have a good idea of
what you can do with it. However, connecting to the client and
performing queries manually is not our goal. What we want to achieve is
that our application can take advantage of the database in an automatic
way. In order to do this, we will use a set of classes that comes with
PHP and allows you to connect to the database and perform queries from
the code.

PHP Data Objects (PDO) is the class that connects to the database and
allows you to interact with it. This is the popular way to work with
databases for PHP developers, even though there are other ways that we
will not discuss here. PDO allows you to work with different database
systems, so you are not tied to MySQL only. In the following sections,
we will consider how to connect to a database, insert data, and retrieve
it using this class.

Connecting to the database
--------------------------

In order to connect to the database, it is good practice to keep the
credentials—that is, the user and password—separated from the code in a
configuration file. We already have this file as config/app.json from
when we worked with the Config class. Let's add the correct credentials
for our database. If you have the configuration by default, the
configuration file should look similar to this:

    { "db": { "user": "root", "password": "" } }

Developers usually specify other information related to the connection,
such as the host, port, or name of the database. This will depend on how
your application is installed, whether MySQL is running on a different
server, and so on, and it is up to you how much information you want to
keep on your code and in your configuration files.

In order to connect to the database, we need to instantiate an object
from the PDO class. The constructor of this class expects three
arguments: Data Source Name (DSN), which is a string that represents the
type of database to use; the name of the user; and the password. We
already have the username and password from the Config class, but we
still need to build DSN.

One of the formats for MySQL databases is <database
type>:host=<host>;dbname=<schema name>. As our database system is MySQL,
it runs on the same server, and the schema name is bookstore, DSN will
be mysql:host=127.0.0.1;dbname=bookstore. Let's take a look at how we
will put everything together:

    $dbConfig = Config::getInstance()->get('db'); $db = new PDO(
    'mysql:host=127.0.0.1;dbname=bookstore', $dbConfig['user'],
    $dbConfig['password'] );
    $db->setAttribute(PDO::ATTR\_DEFAULT\_FETCH\_MODE,
    PDO::FETCH\_ASSOC);

Note also that we will invoke the setAttribute method from the PDO
instance. This method allows you to set some options to the connection;
in this case, it sets the format of the results coming from MySQL. This
option forces MySQL to return the arrays whose keys are the names of the
fields, which is way more useful than the default one, returning numeric
keys based on the order of the fields. Setting this option now will
affect all the queries performed with the $db instance, rather than
setting the option each time we perform a query.

Performing queries
------------------

The easiest way to retrieve data from your database is to use the query
method. This method accepts the query as a string and returns a list of
rows as arrays. Let's consider an example: write the following after the
initialization of the database connection—for example, in the init.php
file:

    $rows = $db->query('SELECT \* FROM book ORDER BY title'); foreach
    ($rows as $row) { var\_dump($row); }

This query tries to get all the books in the database, ordering them by
the title. This could be the content of a function such as getAllBooks,
which is used when we display our catalog. Each row is an array that
contains all the fields as keys and the data as values.

If you run the application on your browser, you will get the following
result:

|image27|

The query function is useful when we want to retrieve data, but in order
to execute queries that insert rows, PDO provides the exec function.
This function also expects the first parameter as a string, defining the
query to execute, but it returns a Boolean specifying whether the
execution was successful or not. A good example would be to try to
insert books. Type the following:

    $query = <<<SQL INSERT INTO book (isbn, title, author, price) VALUES
    ("9788187981954", "Peter Pan", "J. M. Barrie", 2.34) SQL; $result =
    $db->exec($query); var\_dump($result); // true

This code also uses a new way of representing strings: heredoc. We will
enclose the string between <<<SQL and SQL;, both in different lines,
instead of quotes. The benefit of this is the ability to write strings
in multiple lines with tabulations or any other blank space, and PHP
will respect it. We can construct queries that are easy to read rather
than writing them on a single line or having to concatenate the
different strings. Note that SQL is a token to represent the start and
end of the string, but you could use any text that you consider.

The first time you run the application with this code, the query will be
executed successfully, and thus, the result will be the Boolean true.
However, if you run it again, it will return false as the ISBN that we
inserted is the same but we set its restriction to be unique.

It is useful to know that a query failed, but it is better if we know
why. The PDO instance has the errorInfo method that returns an array
with the information of the last error. The key 2 contains the
description, so it is probably the one that we will use more often.
Update the previous code with the following:

    $query = <<<SQL INSERT INTO book (isbn, title, author, price) VALUES
    ("9788187981954", "Peter Pan", "J. M. Barrie", 2.34) SQL; $result =
    $db->exec($query); var\_dump($result); // false $error =
    $db->errorInfo()[2]; var\_dump($error); // Duplicate entry
    '9788187981954' for key 'isbn'

The result is that the query failed because the ISBN entry was
duplicated. Now, we can build more meaningful error messages for our
customers or just for debugging purposes.

Prepared statements
-------------------

The previous two functions are very useful when you need to run quick
queries that are always the same. However, in the second example you
might note that the string of the query is not very useful as it always
inserts the same book. Although it is true that you could just replace
the values by variables, it is not good practice as these variables
usually come from the user side and can contain malicious code. It is
always better to first sanitize these values.

PDO provides the ability to prepare a statement—that is, a query that is
parameterized. You can specify parameters for the fields that will
change in the query and then assign values to these parameters. Let's
consider first an example, as follows:

    $query = 'SELECT \* FROM book WHERE author = :author'; $statement =
    $db->prepare($query); $statement->bindValue('author', 'George
    Orwell'); $statement->execute(); $rows = $statement->fetchAll();
    var\_dump($rows);

The query is a normal one except that it has :author instead of the
string of the author that we want to find. This is a parameter, and we
will identify them using the prefix :. The prepare method gets the query
as an argument and returns a PDOStatement instance. This class contains
several methods to bind values, execute statements, fetch results, and
more. In this piece of code, we use only three of them, as follows:

-  bindValue: This takes two arguments: the name of the parameter as
   described in the query and the value to assign. If you provide a
   parameter name that is not in the query, this will throw an
   exception.

-  execute: This will send the query to MySQL with the replacement of
   the parameters by the provided values. If there is any parameter that
   is not assigned to a value, the method will throw an exception. As
   its brother exec, execute will return a Boolean, specifying whether
   the query was executed successfully or not.

-  fetchAll: This will retrieve the data from MySQL in case it was a
   SELECT query. As a query, fetchAll will return a list of all rows as
   arrays.

If you try this code, you will note that the result is very similar to
when using query; however, this time, the code is much more dynamic as
you can reuse it for any author that you need.

|image28|

There is another way to bind values to parameters of a query than using
the bindValue method. You could prepare an array where the key is the
name of the parameter and the value is the value you want to assign to
it, and then you can send it as the first argument of the execute
method. This way is quite useful as usually you already have this array
prepared and do not need to call bindValue several times with its
content. Add this code in order to test it:

    $query = <<<SQL INSERT INTO book (isbn, title, author, price) VALUES
    (:isbn, :title, :author, :price) SQL; $statement =
    $db->prepare($query); $params = [ 'isbn' => '9781412108614', 'title'
    => 'Iliad', 'author' => 'Homer', 'price' => 9.25 ];
    $statement->execute($params); echo $db->lastInsertId(); // 8

In this last example, we created a new book with almost all the
parameters, but we did not specify the ID, which is the desired behavior
as we want MySQL to choose a valid one for us. However, what happens if
you want to know the ID of the inserted row? Well, you could query MySQL
for the book with the same ISBN and the returned row would contain the
ID, but this seems like a lot of work. Instead, PDO has the lastInsertId
method, which returns the last ID inserted by a primary key, saving us
from one extra query.

Joining tables
==============

Even though querying MySQL is quite fast, especially if it is in the
same server as our PHP application, we should try to reduce the number
of queries that we will execute to improve the performance of our
application. So far, we have queried data from just one table, but this
is rarely the case. Imagine that you want to retrieve information about
borrowed books: the table contains only IDs and dates, so if you query
it, you will not get very meaningful data, right? One approach would be
to query the data in borrowed\_books, and based on the returning IDs,
query the book and customer tables by filtering by the IDs we are
interested in. However, this approach consists of at least three queries
to MySQL and a lot of work with arrays in PHP. It seems as though there
should be a better option!

In SQL, you can execute join queries. A join query is a query that joins
two or more tables through a common field and, thus, allows you to
retrieve data from these tables, reducing the amount of queries needed.
Of course, the performance of a join query is not as good as the
performance of a normal query, but if you have the correct keys and
relationships defined, this option is way better than querying
separately.

In order to join tables, you need to link them using a common field.
Foreign keys are very useful in this matter as you know that both the
fields are the same. Let's take a look at how we would query for all the
important info related to the borrowed books:

    mysql> SELECT CONCAT(c.firstname, ' ', c.surname) AS name, ->
    b.title, -> b.author, -> DATE\_FORMAT(bb.start, '%d-%m-%y') AS
    start, -> DATE\_FORMAT(bb.end, '%d-%m-%y') AS end -> FROM
    borrowed\_books bb -> LEFT JOIN customer c ON bb.customer\_id = c.id
    -> LEFT JOIN book b ON b.id = bb.book\_id -> WHERE bb.start >=
    "2015-01-01";
    +------------+---------+---------------+----------+----------+ \|
    name \| title \| author \| start \| end \|
    +------------+---------+---------------+----------+----------+ \|
    Han Solo \| Dracula \| Bram Stoker \| 10-01-15 \| 13-01-15 \| \|
    James Kirk \| Dracula \| Bram Stoker \| 01-02-15 \| 10-02-15 \| \|
    James Kirk \| 1984 \| George Orwell \| 12-03-15 \| NULL \|
    +------------+---------+---------------+----------+----------+ 3
    rows in set (0.00 sec)

There are several new concepts introduced in this last query. Especially
with joining queries, as we joined the fields of different tables, it
might occur that two tables have the same field name, and MySQL needs us
to differentiate them. The way we will differentiate two fields of two
different tables is by prepending the name of the table. Imagine that we
want to differentiate the ID of a customer from the ID of the book; we
should use them as customer.id and book.id. However, writing the name of
the table each time would make our queries endless.

MySQL has the ability to add an alias to a table by just writing next to
the table's real name, as we did in borrowed\_books (bb), customer (c)
or book (b). Once you add an alias, you can use it to reference this
table, allowing us to write things such as bb.customer\_id instead of
borrowed\_books.customer\_id. It is also good practice to write the
table of the field even if the field is not duplicated anywhere else as
joining tables makes it a bit confusing to know where each field comes
from.

When joining tables, you need to write them in the FROM clause using
LEFT JOIN, followed by the name of the table, an optional alias, and the
fields that connect both tables. There are different joining types, but
let's focus on the most useful for our purposes. Left joins take each
row from the first table—the one on the left-hand side of the
definition—and search for the equivalent field in the right-hand side
table. Once it finds it, it will concatenate both rows as if they were
one. For example, when joining borrowed\_books with customer for each
borrowed\_books row, MySQL will search for an ID in customer that
matches the current customer\_id, and then it will add all the
information of this row in our current row in borrowed\_books as if they
were only one big table. As customer\_id is a foreign key, we are
certain that there will always be a customer to match.

You can join several tables, and MySQL will just resolve them from left
to right; that is, it will first join the two first tables as one, then
try to join this resulting one with the third table, and so on. This is,
in fact, what we did in our example: we first joined borrowed\_books
with customer and then joined these two with book.

As you can note, there are also aliases for fields. Sometimes, we do
more than just getting a field; an example was when we got how many rows
a query matched with COUNT(\*). However, the title of the column when
retrieving this information was also COUNT(\*), which is not always
useful. At other times, we used two tables with colliding field names,
and it makes everything confusing. When this happens, just add an alias
to the field in the same way we did with table names; AS is optional,
but it helps to understand what you are doing.

Let's move now to the usage of dates in this query. On one hand, we will
use DATE\_FORMAT for the first time. It accepts the date/time/datetime
value and the string with the format. In this case, we used %d-%m-%y,
which means day-month-year, but we could use %h-%i-%s to specify
hours-minutes-seconds or any other combination.

Note also how we compared dates in the WHERE clause. Given two dates or
time values of the same type, you can use the comparison operators as if
they were numbers. In this case, we will do bb.start >= "2015-01-01",
which will give us the borrowed books from January 1, 2015, onward.

The final thing to note about this complex query is the use of the
CONCAT function. Instead of returning two fields, one for the name and
one for the surname, we want to get the full name. To do this, we will
concatenate the fields using this function, sending as many strings as
we want as arguments of the function and getting back the concatenated
string. As you can see, you can send both fields and strings enclosed by
single quotes.

Well, if you fully understood this query, you should feel satisfied with
yourself; this was the most complex query we will see in this chapter.
We hope you can get a sense of how powerful a database system can be and
that from now on, you will try to process the data as much as you can on
the database side instead of the PHP side. If you set the correct
indexes, it will perform better.

Grouping queries
================

The last feature that we will discuss about querying is the GROUP BY
clause. This clause allows you to group rows of the same table with a
common field. For example, let's say we want to know how many books each
author has in just one query. Try the following:

    mysql> SELECT -> author, -> COUNT(\*) AS amount, ->
    GROUP\_CONCAT(title SEPARATOR ', ') AS titles -> FROM book -> GROUP
    BY author -> ORDER BY amount DESC, author;
    +-----------------+--------+-------------------+ \| author \| amount
    \| titles \| +-----------------+--------+-------------------+ \|
    George Orwell \| 2 \| 1984, Animal Farm \| \| Homer \| 2 \| Odyssey,
    Iliad \| \| Bram Stoker \| 1 \| Dracula \| \| Haruki Murakami \| 1
    \| 1Q84 \| \| J. M. Barrie \| 1 \| Peter Pan \| \| Jodi Picoult \| 1
    \| 19 minutes \| +-----------------+--------+-------------------+ 5
    rows in set (0.00 sec)

The GROUP BY clause, always after the WHERE clause, gets a field—or
many, separated by a coma—and treats all the rows with the same value
for this field, as though they were just one. Thus, selecting by author
will group all the rows that contain the same author. The feature might
not seem very useful, but there are several functions in MySQL that take
advantage of it. In this example:

-  COUNT(\*) is used in queries with GROUP BY and shows how many rows
   this field groups. In this case, we will use it to know how many
   books each author has. In fact, it always works like this; however,
   for queries without GROUP BY, MySQL treats the whole set of rows as
   one group.

-  GROUP\_CONCAT is similar to CONCAT, which we discussed earlier. The
   only difference is that this time the function will concatenate the
   fields of all the rows of a group. If you do not specify SEPARATOR,
   MySQL will use a single coma. However, in our case, we needed a coma
   and a space to make it readable, so we added SEPARATOR ', ' at the
   end. Note that you can add as many things to concatenate as you need
   in CONCAT, the separator will just separate the concatenations by
   rows.

Even though it is not about grouping, note the ORDER clause that we
added. We ordered by two fields instead of one. This means that MySQL
will order all the rows by the amount field; note that this is an alias,
but you can use it here as well. Then, MySQL will order each group of
rows with the same amount value by the title field.

There is one last thing to remember as we already presented all the
important clauses that a SELECT query can contain: MySQL expects the
clauses of the query to be always in the same order. If you write the
same query but change this order, you will get an error. The order is as
follows:

1. SELECT

2. FROM

3. WHERE

4. GROUP BY

5. ORDER BY

Updating and deleting data
==========================

We already know quite a lot about inserting and retrieving data, but if
applications could only do this, they would be quite static. Editing
this data as we need is what makes an application dynamic and what gives
to the user some value. In MySQL, and in most database systems, you have
two commands to change data: UPDATE and DELETE. Let's discuss them in
detail.

Updating data
-------------

When updating data in MySQL, the most important thing is to have a
unique reference of the row that you want to update. For this, primary
keys are very useful; however, if you have a table with no primary keys,
which should not be the case most of the time, you can still update the
rows based on other fields. Other than the reference, you will need the
new value and, of course, the table name and field to update. Let's take
a look at a very simple example:

    mysql> UPDATE book SET price = 12.75 WHERE id = 2; Query OK, 1 row
    affected (0.00 sec) Rows matched: 1 Changed: 1 Warnings: 0

In this UPDATE query, we set the price of the book with the ID 2 to
12.75. The SET clause does not need to specify only one change; you can
specify several changes on the same row as soon as you separate them by
commas—for example, SET price = 12.75, stock = 14. Also, note the WHERE
clause, in which we specify which rows we want to change. MySQL gets all
the rows of this table based on these conditions as though it were a
SELECT query and apply the change to this set of rows.

What MySQL will return is very important: the number of rows matched and
the number of rows changed. The first one is the number of rows that
match the conditions in the WHERE clause. The second one specifies the
amount of rows that can be changed. There are different reasons not to
change a row—for example when the row already has the same value. To see
this, let's run the same query again:

    mysql> UPDATE book SET price = 12.75 WHERE id = 2; Query OK, 0 rows
    affected (0.00 sec) Rows matched: 1 Changed: 0 Warnings: 0

The same row now says that there was 1 row matched, as expected, but 0
rows were changed. The reason is that we already set the price of this
book to 12.75, so MySQL does not need to do anything about this now.

As mentioned before, the WHERE clause is the most important bit in this
query. Way too many times, we find developers that run a priori innocent
UPDATE queries end up changing the whole table because they miss the
WHERE clause; thus, MySQL matches the whole table as valid rows to
update. This is usually not the intention of the developer, and it is
something not very pleasant, so try to make sure you always provide a
valid set of conditions. It is good practice to first write down the
SELECT query that returns the rows you need to edit, and once you are
sure that the conditions match the desired set of rows, you can write
the UPDATE query.

However, sometimes, affecting multiple rows is the intended scenario.
Imagine that we are going through tough times and need to increase the
price of all our books. We decide that we want to increase the price by
16%, which is the same as the current price times 1.16. We can run the
following query to perform these changes:

    mysql> UPDATE book SET price = price \* 1.16; Query OK, 8 rows
    affected (0.00 sec) Rows matched: 8 Changed: 8 Warnings: 0

This query does not contain any WHERE clause as we want to match all our
books. Also note that the SET clause uses the price field to get the
current value for the price, which is perfectly valid. Finally, note the
number of rows matched and changed, which is 8—the whole set of rows for
this table.

To finish with this subsection, let's consider how we can use UPDATE
queries from PHP through PDO. One very common scenario is when we want
to add copies of the already existing books to our inventory. Given a
book ID and an optional amount of books—by default, this value will be
1—we will increase the stock value of this book by these many copies.
Write this function in your init.php file:

    function addBook(int $id, int $amount = 1): void { $db = new PDO(
    'mysql:host=127.0.0.1;dbname=bookstore', 'root', '' ); $query =
    'UPDATE book SET stock = stock + :n WHERE id = :id'; $statement =
    $db->prepare($query); $statement->bindValue('id', $id);
    $statement->bindValue('n', $amount); if (!$statement->execute()) {
    throw new Exception($statement->errorInfo()[2]); } }

There are two arguments: $id and $amount. The first one will always be
mandatory, whereas the second one can be omitted, and the default value
will be 1. The function first prepares a query similar to the first one
of this section, in which we increased the amount of stock of a given
book, then binds both parameters to the statement, and finally executes
the query. If something happens and execute returns false, we will throw
an exception with the content of the error message from MySQL.

This function is very useful when we either buy more stock or a customer
returns a book. We could even use it to remove books by providing a
negative value to $amount, but this is very bad practice. The reason is
that even if we forced the stock field to be unsigned, setting it to a
negative value will not trigger any error, only a warning. MySQL will
not set the row to a negative value, but the execute invocation will
return true, and we will not know about it. It is better to just create
a second method, removeBook, and verify first that the amount of books
to remove is lower than or equal to the current stock.

Foreign key behaviors
---------------------

One tricky thing to manage when updating or deleting rows is when the
row that we update is part of a foreign key somewhere else. For example,
our borrowed\_books table contains the IDs of customers and books, and
as you already know, MySQL enforces that these IDs are always valid and
exist on these respective tables. What would happen, then, if we changed
the ID of the book itself on the book table? Or even worse, what would
happen if we removed one of the books from book, and there is a row in
borrowed\_books that references this ID?

MySQL allows you to set the desired reaction when one of these scenarios
takes place. It has to be defined when adding the foreign key; so, in
our case, we will need to first remove the existing ones and then add
them again. To remove or drop a key, you need to know the name of this
key, which we can find using the SHOW CREATE TABLE command, as follows:

    mysql> SHOW CREATE TABLE borrowed\_books \\G
    \*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\* 1. row
    \*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\* Table:
    borrowed\_books Create Table: CREATE TABLE \`borrowed\_books\` (
    \`book\_id\` int(10) unsigned NOT NULL, \`customer\_id\` int(10)
    unsigned NOT NULL, \`start\` datetime NOT NULL, \`end\` datetime
    DEFAULT NULL, KEY \`book\_id\` (\`book\_id\`), KEY \`customer\_id\`
    (\`customer\_id\`), CONSTRAINT \`borrowed\_books\_ibfk\_1\` FOREIGN
    KEY (\`book\_id\`) REFERENCES \`book\` (\`id\`), CONSTRAINT
    \`borrowed\_books\_ibfk\_2\` FOREIGN KEY (\`customer\_id\`)
    REFERENCES \`customer\` (\`id\`) ) ENGINE=InnoDB DEFAULT
    CHARSET=latin1 1 row in set (0.00 sec)

The two foreign keys that we want to remove are borrowed\_books\_ibfk\_1
and borrowed\_books\_ibfk\_2. Let's remove them using the ALTER TABLE
command, as we did before:

    mysql> ALTER TABLE borrowed\_books -> DROP FOREIGN KEY
    borrowed\_books\_ibfk\_1; Query OK, 4 rows affected (0.02 sec)
    Records: 4 Duplicates: 0 Warnings: 0 mysql> ALTER TABLE
    borrowed\_books -> DROP FOREIGN KEY borrowed\_books\_ibfk\_2; Query
    OK, 4 rows affected (0.01 sec) Records: 4 Duplicates: 0 Warnings: 0

Now, we need to add the foreign keys again. The format of the command
will be the same as when we added them, but appending the new desired
behavior. In our case, if we remove a customer or book from our tables,
we want to remove the rows referencing these books and customers from
borrowed\_books; so, we need to use the CASCADE option. Let's consider
what they would look like:

    mysql> ALTER TABLE borrowed\_books -> ADD FOREIGN KEY (book\_id)
    REFERENCES book (id) -> ON DELETE CASCADE ON UPDATE CASCADE, -> ADD
    FOREIGN KEY (customer\_id) REFERENCES customer (id) -> ON DELETE
    CASCADE ON UPDATE CASCADE; Query OK, 4 rows affected (0.01 sec)
    Records: 4 Duplicates: 0 Warnings: 0

Note that we can define the CASCADE behavior for both actions: when
updating and when deleting rows. There are other options instead of
CASCADE—for example SET NULL, which sets the foreign keys columns to
NULL and allows the original row to be deleted, or the default one,
RESTRICT, which rejects the update/delete commands.

Deleting data
-------------

Deleting data is almost the same as updating it. You need to provide a
WHERE clause that will match the rows that you want to delete. Also, as
with when updating data, it is highly recommended to first build the
SELECT query that will retrieve the rows that you want to delete before
performing the DELETE command. Do not think that you are wasting time
with this methodology; as the saying goes, measure twice, cut once. Not
always is it possible to recover data after deleting rows!

Let's try to delete a book by observing how the CASCADE option we set
earlier behaves. For this, let's first query for the existing borrowed
books list via the following:

    mysql> SELECT book\_id, customer\_id FROM borrowed\_books;
    +---------+-------------+ \| book\_id \| customer\_id \|
    +---------+-------------+ \| 1 \| 1 \| \| 4 \| 1 \| \| 4 \| 2 \| \|
    1 \| 2 \| +---------+-------------+ 4 rows in set (0.00 sec)

There are two different books, 1 and 4, with each of them borrowed
twice. Let's try to delete the book with the ID 4. First, build a query
such as SELECT \* FROM book WHERE id = 4 to make sure that the condition
in the WHERE clause is the appropriate one. Once you are sure, perform
the following query:

    mysql> DELETE FROM book WHERE id = 4; Query OK, 1 row affected (0.02
    sec)

As you can note, we only specified the DELETE FROM command followed by
the name of the table and the WHERE clause. MySQL tells us that there
was 1 row affected, which makes sense, given the previous SELECT
statement we made.

If we go back to our borrowed\_books table and query for the existing
ones, we will note that all the rows referencing the book with the ID 4
are gone. This is because when deleting them from the book table, MySQL
noticed the foreign key reference, checked what it needed to do while
deleting—in this case, CASCADE—and deleted also the rows in
borrowed\_books. Take a look at the following:

    mysql> SELECT book\_id, customer\_id FROM borrowed\_books;
    +---------+-------------+ \| book\_id \| customer\_id \|
    +---------+-------------+ \| 1 \| 1 \| \| 1 \| 2 \|
    +---------+-------------+ 2 rows in set (0.00 sec)

Working with transactions
=========================

In the previous section, we reiterated how important it is to make sure
that an update or delete query contain the desirable matching set of
rows. Even though this will always apply, there is a way to revert the
changes that you just made, which is working with transactions.

A transaction is a state where MySQL keeps track of all the changes that
you make in your data in order to be able to revert all of them if
needed. You need to explicitly start a transaction, and before you close
the connection to the server, you need to commit your changes. This
means that MySQL does not really perform these changes until you tell it
to do so. If during a transaction you want to revert the changes, you
should roll back instead of making a commit.

PDO allows you to do this with three functions:

-  beginTransaction: This will start the transaction.

-  commit: This will commit your changes. Keep in mind that if you do
   not commit and the PHP script finishes or you close the connection
   explicitly, MySQL will reject all the changes you made during this
   transaction.

-  rollBack: This will roll back all the changes that were made during
   this transaction.

One possible use of transactions in your application is when you need to
perform multiple queries and all of them have to be successful and the
whole set of queries should not be performed otherwise. This would be
the case when adding a sale into the database. Remember that our sales
are stored in two tables: one for the sale itself and one for the list
of books related to this sale. When adding a new one, you need to make
sure that all the books are added to this database; otherwise, the sale
will be corrupted. What you should do is execute all the queries,
checking for their returning values. If any of them returns false, the
whole sale should be rolled back.

Let's create an addSale function in your init.php file in order to
emulate this behavior. The content should be as follows:

    function addSale(int $userId, array $bookIds): void { $db = new PDO(
    'mysql:host=127.0.0.1;dbname=bookstore', 'root', '' );
    $db->beginTransaction(); try { $query = 'INSERT INTO sale
    (customer\_id, date) ' . 'VALUES(:id, NOW())'; $statement =
    $db->prepare($query); if (!$statement->execute(['id' => $userId])) {
    throw new Exception($statement->errorInfo()[2]); } $saleId =
    $db->lastInsertId(); $query = 'INSERT INTO sale\_book (book\_id,
    sale\_id) ' . 'VALUES(:book, :sale)'; $statement =
    $db->prepare($query); $statement->bindValue('sale', $saleId);
    foreach ($bookIds as $bookId) { $statement->bindValue('book',
    $bookId); if (!$statement->execute()) { throw new
    Exception($statement->errorInfo()[2]); } } $db->commit(); } catch
    (Exception $e) { $db->rollBack(); throw $e; } }

This function is quite complex. It gets as arguments the ID of the
customer and the list of books as we assume that the date of the sale is
the current date. The first thing we will do is connect to the database,
instantiating the PDO class. Right after this, we will begin our
transaction, which will last only during the course of this function.
Once we begin the transaction, we will open a try…catch block that will
enclose the rest of the code of the function. The reason is that if we
throw an exception, the catch block will capture it, rolling back the
transaction and propagating the exception. The code inside the try block
just adds first the sale and then iterates the list of books, inserting
them into the database too. At all times, we will check the response of
the execute function, and if it's false, we will throw an exception with
the information of the error.

Let's try to use this function. Write the following code that tries to
add a sale for three books; however, one of them does not exist, which
is the one with the ID 200:

    try { addSale(1, [1, 2, 200]); } catch (Exception $e) { echo 'Error
    adding sale: ' . $e->getMessage(); }

This code will echo the error message, complaining about the nonexistent
book. If you check in MySQL, there will be no rows in the sales table as
the function rolled back when the exception was thrown.

Finally, let's try the following code instead. This one will add three
valid books so that the queries are always successful and the try block
can go until the end, where we will commit the changes:

    try { addSale(1, [1, 2, 3]); } catch (Exception $e) { echo 'Error
    adding sale: ' . $e->getMessage(); }

Test it, and you will see how there is no message printed on your
browser. Then, go to your database to make sure that there is a new
sales row and there are three books linked to it.

Summary
=======

In this chapter, we learned the importance of databases and how to use
them from our web application: from setting up the connection using PDO
and creating and fetching data on demand to constructing more complex
queries that fulfill our needs. With all of this, our application looks
way more useful now than when it was completely static.

In the next chapter, we will discover how to apply the most important
design patterns for web applications through Model View Controller
(MVC). You will gain a sense of clarity in your code when you organize
your application in this way.

Chapter 6. Adapting to MVC

Web applications are more complex than what we have built so far. The
more functionality you add, the more difficult the code is to maintain
and understand. It is for this reason that structuring your code in an
organized way is crucial. You could design your own structure, but as
with OOP, there already exist some design patterns that try to solve
this problem.

MVC (model-view-controller) has been the favorite pattern for web
developers. It helps us separate the different parts of a web
application, leaving the code easy to understand even for beginners. We
will try to refactor our bookstore example to use the MVC pattern, and
you will realize how quickly you can add new functionality after that.

In this chapter, you will learn the following:

-  Using Composer to manage dependencies

-  Designing a router for your application

-  Organizing your code into models, views, and controllers

-  Twig as the template engine

-  Dependency injection

The MVC pattern
===============

So far, each time we have had to add a feature, we added a new PHP file
with a mixture of PHP and HTML for that specific page. For chunks of
code with a single purpose, and which we have to reuse, we created
functions and added them to the functions file. Even for very small web
applications like ours, the code starts becoming very confusing, and the
ability to reuse code is not as helpful as it could be. Now imagine an
application with a large number of features: that would be pretty much
chaos itself.

The problems do not stop here. In our code, we have mixed HTML and PHP
code in a single file. That will give us a lot of trouble when trying to
change the design of the web application, or even if we want to perform
a very small change across all pages, such as changing the menu or
footer of the page. The more complex the application, the more problems
we will encounter.

MVC came up as a pattern to help us divide the different parts of the
application. These parts are known as models, views, and controllers.
Models manage the data and/or the business logic, views contain the
templates for our responses (for example, HTML pages), and controllers
orchestrate requests, deciding what data to use and how to render the
appropriate template. We will go through them in later sections of this
chapter.

Using Composer
==============

Even though this is not a necessary component when implementing the MVC
pattern, Composer has been an indispensable tool for any PHP web
application over the last few years. The main goal of this tool is to
help you manage the dependencies of your application, that is, the
third-party libraries (of code) that we need to use in our application.
We can achieve that by just creating a configuration file that lists
them, and by running a command in your command line.

You need to install Composer on your development machine (see `*Chapter
1* <#Top_of_ch01_html>`__, *Setting Up the Environment*). Make sure that
you have it by executing the following command:

    $ composer –version

This should return the version of your Composer installation. If it does
not, return to the installation section to fix the problem.

Managing dependencies
---------------------

As we stated earlier, the main goal of Composer is to manage
dependencies. For example, we've already implemented our configuration
reader, the Config class, but if we knew of someone that implemented a
better version of it, we could just use theirs instead of reinventing
the wheel; just make sure that they allow you to do so!

Note

Open source

Open source refers to the code that developers write and share with the
community in order to be used by others without restrictions. There are
actually different types of licenses, and some give you more flexibility
than others, but the basic idea is that we can reuse the libraries that
other developers have written in our applications. That helps the
community to grow in knowledge, as we can learn what others have done,
improve it, and share it afterwards.

We've already implemented a decent configuration reader, but there are
other elements of our application that need to be done. Let's take
advantage of Composer to reuse someone else's libraries. There are a
couple of ways of adding a dependency to our project: executing a
command in our command line, or editing the configuration file manually.
As we still do not have Composer's configuration file, let's use the
first option. Execute the following command in the root directory of
your application:

    $ composer require monolog/monolog

This command will show the following result:

    Using version ^1.17 for monolog/monolog ./composer.json has been
    created Loading composer repositories with package information
    Updating dependencies (including require-dev) - Installing psr/log
    (1.0.0) Downloading: 100% - Installing monolog/monolog (1.17.2)
    Downloading: 100% ... Writing lock file Generating autoload files

With this command, we asked Composer to add the library monolog/monolog
as a dependency of our application. Having executed that, we can now see
some changes in our directory:

-  We have a new file named composer.json. This is the configuration
   file where we can add our dependencies.

-  We have a new file named composer.lock. This is a file that Composer
   uses in order to track the dependencies that have already been
   installed and their versions.

-  We have a new directory named vendor. This directory contains the
   code of the dependencies that Composer downloaded.

The output of the command also shows us some extra information. In this
case, it says that it downloaded two libraries or packages, even though
we asked for only one. The reason is that the package that we needed
also contained other dependencies that were resolved by Composer. Also
note the version that Composer downloaded; as we did not specify any
version, Composer took the most recent one available, but you can always
try to write the specific version that you need.

We will need another library, in this case twig/twig. Let's add it to
our dependencies list with the following command:

    $ composer require twig/twig

This command will show the following result:

    Using version ^1.23 for twig/twig ./composer.json has been updated
    Loading composer repositories with package information Updating
    dependencies (including require-dev) - Installing twig/twig
    (v1.23.1) Downloading: 100% Writing lock file Generating autoload
    files

If we check the composer.json file, we will see the following content:

    { "require": { "monolog/monolog": "^1.17", "twig/twig": "^1.23" } }

The file is just a JSON map that contains the configuration of our
application; in this case, the list of the two dependencies that we
installed. As you can see, the dependencies' name follows a pattern: two
words separated by a slash. The first of the words refers to the vendor
that developed the library. The second of them is the name of the
library itself. The dependency has a version, which could be the exact
version number—as in this case—or it could contain wildcard characters
or tag names. You can read more about this at
`*https://getcomposer.org/doc/articles/aliases.md* <https://getcomposer.org/doc/articles/aliases.md>`__.

Finally, if you would like to add another dependency, or edit the
composer.json file in any other way, you should run composer update in
your command line, or wherever the composer.json file is, in order to
update the dependencies.

Autoloader with PSR-4
---------------------

In the previous chapters, we also added an autoloader to our
application. As we are now using someone else's code, we need to know
how to load their classes too. Soon, developers realized that this
scenario without a standard would be virtually impossible to manage, and
they came out with some standards that most developers follow. You can
find a lot of information on this topic at
`*http://www.php-fig.org* <http://www.php-fig.org>`__.

Nowadays, PHP has two main standards for autoloading: PSR-0 and PSR-4.
They are very similar, but we will be implementing the latter, as it is
the most recent standard published. This standard basically follows what
we've already introduced when talking about namespaces: the namespace of
a class must be the same as the directory where it is, and the name of
the class should be the name of the file, followed by the extension
.php. For example, the file in src/Domain/Book.php contains the class
Book inside the namespace Bookstore\\Domain.

Applications using Composer should follow one of those standards, and
they should note in their respective composer.json file which one they
are using. This means that Composer knows how to autoload its own
application files, so we will not need to take care of it when we
download external libraries. To specify that, we edit our composer.json
file, and add the following content:

    { "require": { "monolog/monolog": "^1.17", "twig/twig": "^1.23" },
    "autoload": { "psr-4": { "Bookstore\\\\": "src" } } }

The preceding code means that we will use PSR-4 in our application, and
that all the namespaces that start with Bookstore should be found inside
the src/ directory. This is exactly what our autoloader was doing
already, but reduced to a couple of lines in a configuration file. We
can safely remove our autoloader and any reference to it now.

Composer generates some mappings that help to speed up the loading of
classes. In order to update those maps with the new information added to
the configuration file, we need to run the composer update command that
we ran earlier. This time, the output will tell us that there is no
package to update, but the autoload files will be generated again:

    $ composer update Loading composer repositories with package
    information Updating dependencies (including require-dev) Nothing to
    install or update Writing lock file Generating autoload files

Adding metadata
---------------

In order to know where to find the libraries that you define as
dependencies, Composer keeps a repository of packages and versions,
known as Packagist. This repository keeps a lot of useful information
for developers, such as all the versions available for a given package,
the authors, some description of what the package does (or a website
pointing to that information), and the dependencies that this package
will download. You can also browse the packages, searching by name or
categories.

But how does Packagist know about this? It is all thanks to the
composer.json file itself. In there, you can define all the metadata of
your application in a format that Composer understands. Let's see an
example. Add the following content to your composer.json file:

    { "name": "picahielos/bookstore", "description": "Manages an online
    bookstore.", "minimum-stability": "stable", "license": "Apache-2.0",
    "type": "project", "authors": [ { "name": "Antonio Lopez", "email":
    "antonio.lopez.zapata@gmail.com" } ], // ... }

The configuration file now contains the name of the package following
the Composer convention: vendor name, slash, and the package name—in
this case, picahielos/bookstore. We also add a description, license,
authors, and other metadata. If you have your code in a pubic repository
such as GitHub, adding this composer.json file will allow you to go to
Packagist and insert the URL of your repository. Packagist will add your
code as a new package, extracting the info from your composer.json file.
It will show the available versions based on your tags or branches. In
order to learn more about it, we encourage you to visit the official
documentation at
`*https://getcomposer.org/doc/04-schema.md* <https://getcomposer.org/doc/04-schema.md>`__.

The index.php file
------------------

In MVC applications, we usually have one file that gets all the
requests, and routes them to the specific controller depending on the
URL. This logic can generally be found in the index.php file in our root
directory. We already have one, but as we are adapting our features to
the MVC pattern, we will not need the current index.php anymore. Hence,
you can safely replace it with the following:

    <?php require\_once \_\_DIR\_\_ . '/vendor/autoload.php';

The only thing that this file will do now is include the file that
handles all the autoloading from the Composer code. Later, we will
initialize everything here, such as database connections, configuration
readers, and so on, but right now, let's leave it empty.

Working with requests
=====================

As you might recall from previous chapters, the main purpose of a web
application is to process HTTP requests coming from the client and
return a response. If that is the main goal of your application,
managing requests and responses should be an important part of your
code.

PHP is a language that can be used for scripts, but its main usage is in
web applications. Due to this, the language comes ready with a lot of
helpers for managing requests and responses. Still, the native way is
not ideal, and as good OOP developers, we should come up with a set of
classes that help with that. The main elements for this small
project—still inside your application—are the request and the router.
Let's start!

The request object
------------------

As we start our mini framework, we need to change our directory
structure a bit. We will create the src/Core directory for all the
classes related to the framework. As the configuration reader from the
previous chapters is also part of the framework (rather than
functionality for the user), we should move the Config.php file to this
directory too.

The first thing to consider is what a request looks like. If you
remember `*Chapter 2* <#Top_of_ch02_html>`__, *Web Applications with
PHP*, a request is basically a message that goes to a URL, and has a
method—GET or POST for now. The URL is at the same time composed of two
parts: the domain of the web application, that is, the name of your
server, and the path of the request inside the server. For example, if
you try to access http://bookstore.com/my-books, the first part,
http://bookstore.com, would be the domain and /my-books would be the
path. In fact, http would not be part of the domain, but we do not need
that level of granularity for our application. You can get this
information from the global array $\_SERVER that PHP populates for each
request.

Our Request class should have a property for each of those three
elements, followed by a set of getters and some other helpers that will
be useful for the user. Also, we should initialize all the properties
from $\_SERVER in the constructor. Let's see what it would look like:

    <?php namespace Bookstore\\Core; class Request { const GET = 'GET';
    const POST = 'POST'; private $domain; private $path; private
    $method; public function \_\_construct() { $this->domain =
    $\_SERVER['HTTP\_HOST']; $this->path = $\_SERVER['REQUEST\_URI'];
    $this->method = $\_SERVER['REQUEST\_METHOD']; } public function
    getUrl(): string { return $this->domain . $this->path; } public
    function getDomain(): string { return $this->domain; } public
    function getPath(): string { return $this->path; } public function
    getMethod(): string { return $this->method; } public function
    isPost(): bool { return $this->method === self::POST; } public
    function isGet(): bool { return $this->method === self::GET; } }

We can see in the preceding code that other than the getters for each
property, we added the methods getUrl, isPost, and isGet. The user could
find the same information using the already existing getters, but as
they will be needed a lot, it is always good to make it easier for the
user. Also note that the properties are coming from the values of the
$\_SERVER array: HTTP\_HOST, REQUEST\_URI, and REQUEST\_METHOD.

Filtering parameters from requests
----------------------------------

Another important part of a request is the information that comes from
the user, that is, the GET and POST parameters, and the cookies. As with
the $\_SERVER global array, this information comes from $\_POST, $\_GET,
and $\_COOKIE, but it is always good to avoid using them directly,
without filtering, as the user could send malicious code.

We will now implement a class that will represent a map—key-value
pairs—that can be filtered. We will call it FilteredMap, and will
include it in our namespace, Bookstore\\Core. We will use it to contain
the parameters GET and POST and the cookies as two new properties in our
Request class. The map will contain only one property, the array of
data, and will have some methods to fetch information from it. To
construct the object, we need to send the array of data as an argument
to the constructor:

    <?php namespace Bookstore\\Core; class FilteredMap { private $map;
    public function \_\_construct(array $baseMap) { $this->map =
    $baseMap; } public function has(string $name): bool { return
    isset($this->map[$name]); } public function get(string $name) {
    return $this->map[$name] ?? null; } }

This class does not do much so far. We could have the same functionality
with a normal array. The utility of this class comes when we add filters
while fetching data. We will implement three filters, but you can add as
many as you need:

    public function getInt(string $name) { return (int)
    $this->get($name); } public function getNumber(string $name) {
    return (float) $this->get($name); } public function getString(string
    $name, bool $filter = true) { $value = (string) $this->get($name);
    return $filter ? addslashes($value) : $value; }

These three methods in the preceding code allow the user to get
parameters of a specific type. Let's say that the developer needs to get
the ID of the book from the request. The best option is to use the
getInt method to make sure that the returned value is a valid integer,
and not some malicious code that can mess up our database. Also note the
function getString, where we use the addSlashed method. This method adds
slashes to some of the suspicious characters, such as slashes or quotes,
trying to prevent malicious code with it.

Now we are ready to get the GET and POST parameters as well as the
cookies from our Request class using our FilteredMap. The new code would
look like the following:

    <?php namespace Bookstore\\Core; class Request { // ... private
    $params; private $cookies; public function \_\_construct() {
    $this->domain = $\_SERVER['HTTP\_HOST']; $this->path = explode('?',
    $\_SERVER['REQUEST\_URI'])[0]; $this->method =
    $\_SERVER['REQUEST\_METHOD']; $this->params = new FilteredMap(
    array\_merge($\_POST, $\_GET) ); $this->cookies = new
    FilteredMap($\_COOKIE); } // ... public function getParams():
    FilteredMap { return $this->params; } public function getCookies():
    FilteredMap { return $this->cookies; } }

With this new addition, a developer could get the POST parameter price
with the following line of code:

    $price = $request->getParams()->getNumber('price');

This is way safer than the usual call to the global array:

    $price = $\_POST['price'];

Mapping routes to controllers
-----------------------------

If you can recall from any URL that you use daily, you will probably not
see any PHP file as part of the path, like we have with
http://localhost:8000/init.php. Websites try to format their URLs to
make them easier to remember instead of depending on the file that
should handle that request. Also, as we've already mentioned, all our
requests go through the same file, index.php, regardless of their path.
Because of this, we need to keep a map of the URL paths, and who should
handle them.

Sometimes, we have URLs that contain parameters as part of their path,
which is different from when they contain the GET or POST parameters.
For example, to get the page that shows a specific book, we might
include the ID of the book as part of the URL, such as /book/12 or
/book/3. The ID will change for each different book, but the same
controller should handle all of these requests. To achieve this, we say
that the URL contains an argument, and we could represent it by
/book/:id, where id is the argument that identifies the ID of the book.
Optionally, we could specify the kind of value this argument can take,
for example, number, string, and so on.

Controllers, the ones in charge of processing requests, are defined by a
method's class. This method takes as arguments all the arguments that
the URL's path defines, such as the ID of the book. We group controllers
by their functionality, that is, a BookController class will contain the
methods related to requests about books.

Having defined all the elements of a route—a URL-controller
relationship—we are ready to create our routes.json file, a
configuration file that will keep this map. Each entry of this file
should contain a route, the key being the URL, and the value, a map of
information about the controller. Let's see an example:

    { "books/:page": { "controller": "Book", "method": "getAllWithPage",
    "params": { "page": "number" } } }

The route in the preceding example refers to all the URLs that follow
the pattern /books/:page, with page being any number. Thus, this route
will match URLs such as /books/23 or /books/2, but it should not match
/books/one or /books. The controller that will handle this request
should be the getAllWithPage method from BookController; we will append
Controller to all the class names. Given the parameters that we defined,
the definition of the method should be something like the following:

    public function getAllWithPage(int $page): string { //... }

There is one last thing we should consider when defining a route. For
some endpoints, we should enforce the user to be authenticated, such as
when the user is trying to access their own sales. We could define this
rule in several ways, but we chose to do it as part of the route, adding
the entry "login": true as part of the controller's information. With
that in mind, let's add the rest of the routes that define all the views
that we expect to have:

    { //... "books": { "controller": "Book", "method": "getAll" },
    "book/:id": { "controller": "Book", "method": "get", "params": {
    "id": "number" } }, "books/search": { "controller": "Book",
    "method": "search" }, "login": { "controller": "Customer", "method":
    "login" }, "sales": { "controller": "Sales", "method": "getByUser" ,
    "login": true }, "sales/:id": { "controller": "Sales", "method":
    "get", "login": true, "params": { "id": "number" } }, "my-books": {
    "controller": "Book", "method": "getByUser", "login": true } }

These routes define all the pages we need; we can get all the books in a
paginated way or specific books by their ID, we can search books, list
the sales of the user, show a specific sale by its ID, and list all the
books that a certain user has borrowed. However, we are still lacking
some of the endpoints that our application should be able to handle. For
all those actions that are trying to modify data rather than requesting
it, that is, borrowing a book or buying it, we need to add endpoints
too. Add the following to your routes.json file:

    { // ... "book/:id/buy": { "controller": "Sales", "method": "add",
    "login": true "params": { "id": "number" } }, "book/:id/borrow": {
    "controller": "Book", "method": "borrow", "login": true "params": {
    "id": "number" } }, "book/:id/return": { "controller": "Book",
    "method": "returnBook", "login": true "params": { "id": "number" } }
    }

The router
----------

The router will be by far the most complicated piece of code in our
application. The main goal is to receive a Request object, decide which
controller should handle it, invoke it with the necessary parameters,
and return the response from that controller. The main goal of this
section is to understand the importance of the router rather than its
detailed implementation, but we will try to describe each of its parts.
Copy the following content as your src/Core/Router.php file:

    <?php namespace Bookstore\\Core; use
    Bookstore\\Controllers\\ErrorController; use
    Bookstore\\Controllers\\CustomerController; class Router { private
    $routeMap; private static $regexPatters = [ 'number' => '\\d+',
    'string' => '\\w' ]; public function \_\_construct() { $json =
    file\_get\_contents( \_\_DIR\_\_ . '/../../config/routes.json' );
    $this->routeMap = json\_decode($json, true); } public function
    route(Request $request): string { $path = $request->getPath();
    foreach ($this->routeMap as $route => $info) { $regexRoute =
    $this->getRegexRoute($route, $info); if
    (preg\_match("@^/$regexRoute$@", $path)) { return
    $this->executeController( $route, $path, $info, $request ); } }
    $errorController = new ErrorController($request); return
    $errorController->notFound(); } }

The constructor of this class reads from the routes.json file, and
stores the content as an array. Its main method, route, takes a Request
object and returns a string, which is what we will send as output to the
client. This method iterates all the routes from the array, trying to
match each with the path of the given request. Once it finds one, it
tries to execute the controller related to that route. If none of the
routes are a good match to the request, the router will execute the
notFound method of the ErrorController, which will then return an error
page.

URLs matching with regular expressions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

While matching a URL with the route, we need to take care of the
arguments for dynamic URLs, as they do not let us perform a simple
string comparison. PHP—and other languages—has a very strong tool for
performing string comparisons with dynamic content: regular expressions.
Being an expert in regular expressions takes time, and it is outside the
scope of this book, but we will give you a brief introduction to them.

A regular expression is a string that contains some wildcard characters
that will match the dynamic content. Some of the most important ones are
as follows:

-  ^: This is used to specify that the matching part should be the start
   of the whole string

-  $: This is used to specify that the matching part should be the end
   of the whole string

-  \\d: This is used to match a digit

-  \\w: This is used to match a word

-  +: This is used for following a character or expression, to let that
   character or expression to appear at least once or many times

-  \*: This is used for following a character or expression, to let that
   character or expression to appear zero or many times

-  .: This is used to match any single character

Let's see some examples:

-  The pattern .\* will match anything, even an empty string

-  The pattern .+ will match anything that contains at least one
   character

-  The pattern ^\\d+$ will match any number that has at least one digit

In PHP, we have different functions to work with regular expressions.
The easiest of them, and the one that we will use, is pregmatch. This
function takes a pattern as its first argument (delimited by two
characters, usually @ or /), the string that we are trying to match as
the second argument, and optionally, an array where PHP stores the
occurrences found. The function returns a Boolean value, being true if
there was a match, false otherwise. We use it as follows in our Route
class:

    preg\_match("@^/$regexRoute$@", $path)

The $path variable contains the path of the request, for example,
/books/2. We match using a pattern that is delimited by @, has the ^ and
$ wildcards to force the pattern to match the whole string, and contains
the concatenation of / and the variable $regexRoute. The content of this
variable is given by the following method; add this as well to your
Router class:

    private function getRegexRoute( string $route, array $info ): string
    { if (isset($info['params'])) { foreach ($info['params'] as $name =>
    $type) { $route = str\_replace( ':' . $name,
    self::$regexPatters[$type], $route ); } } return $route; }

The preceding method iterates the parameters list coming from the
information of the route. For each parameter, the function replaces the
name of the parameter inside the route by the wildcard character
corresponding to the type of parameter—check the static array,
$regexPatterns. To illustrate the usage of this function, let's see some
examples:

-  The route /books will be returned without a change, as it does not
   contain any argument

-  The route books/:id/borrow will be changed to books/\\d+/borrow, as
   the URL argument, id, is a number

Extracting the arguments of the URL
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In order to execute the controller, we need three pieces of data: the
name of the class to instantiate, the name of the method to execute, and
the arguments that the method needs to receive. We already have the
first two as part of the route $info array, so let's focus our efforts
on finding the third one. Add the following method to the Router class:

    private function extractParams( string $route, string $path ): array
    { $params = []; $pathParts = explode('/', $path); $routeParts =
    explode('/', $route); foreach ($routeParts as $key => $routePart) {
    if (strpos($routePart, ':') === 0) { $name = substr($routePart, 1);
    $params[$name] = $pathParts[$key+1]; } } return $params; }

This last method expects that both the path of the request and the URL
of the route follow the same pattern. With the explode method, we get
two arrays that should match each of their entries. We iterate them, and
for each entry in the route array that looks like a parameter, we fetch
its value in the URL. For example, if we had the route /books/:id/borrow
and the path /books/12/borrow, the result of this method would be the
array *['id' => 12]*.

Executing the controller
~~~~~~~~~~~~~~~~~~~~~~~~

We end this section by implementing the method that executes the
controller in charge of a given route. We already have the name of the
class, the method, and the arguments that the method needs, so we could
make use of the call\_user\_func\_array native function that, given an
object, a method name, and the arguments for the method, invokes the
method of the object passing the arguments. We have to make use of it as
the number of arguments is not fixed, and we cannot perform a normal
invocation.

But we are still missing a behavior introduced when creating our
routes.json file. There are some routes that force the user to be logged
in, which, in our case, means that the user has a cookie with the user
ID. Given a route that enforces authorization, we will check whether our
request contains the cookie, in which case we will set it to the
controller class through setCustomerId. If the user does not have a
cookie, instead of executing the controller for the current route, we
will execute the showLogin method of the CustomerController class, which
will render the template for the login form. Let's see how everything
would look on adding the last method of our Router class:

    private function executeController( string $route, string $path,
    array $info, Request $request ): string { $controllerName =
    '\\Bookstore\\Controllers\\\\' . $info['controller'] . 'Controller';
    $controller = new $controllerName($request); if
    (isset($info['login']) && $info['login']) { if
    ($request->getCookies()->has('user')) { $customerId =
    $request->getCookies()->get('user');
    $controller->setCustomerId($customerId); } else { $errorController =
    new CustomerController($request); return $errorController->login();
    } } $params = $this->extractParams($route, $path); return
    call\_user\_func\_array( [$controller, $info['method']], $params );
    }

We have already warned you about the lack of security in our
application, as this is just a project with didactic purposes. So, avoid
copying the authorization system implemented here.

M for model
===========

Imagine for a moment that our bookstore website is quite successful, so
we think of building a mobile app to increase our market. Of course, we
would want to use the same database that we use for our website, as we
need to sync the books that people borrow or buy from both apps. We do
not want to be in a position where two people buy the same last copy of
a book!

Not only the database, but the queries used to get books, update them,
and so on, have to be the same too, otherwise we would end up with
unexpected behavior. Of course, one apparently easy option would be to
replicate the queries in both codebases, but that has a huge
maintainability problem. What if we change one single field of our
database? We need to apply the same change to at least two different
codebases. That does not seem to be useful at all.

Business logic plays an important role here too. Think of it as
decisions you need to take that affect your business. In our case, that
a premium customer is able to borrow 10 books and a normal one only 3,
is business logic. This logic should be put in a common place too,
because, if we want to change it, we will have the same problems as with
our database queries.

We hope that by now we've convinced you that data and business logic
should be separated from the rest of the code in order to make it
reusable. Do not worry if it is hard for you to define what should go as
part of the model or as part of the controller; a lot of people struggle
with this distinction. As our application is very simple, and it does
not have a lot of business logic, we will just focus on adding all the
code related to MySQL queries.

As you can imagine, for an application integrated with MySQL, or any
other database system, the database connection is an important element
of a model. We chose to use PDO in order to interact with MySQL, and as
you might remember, instantiating that class was a bit of a pain. Let's
create a singleton class that returns an instance of PDO to make things
easier. Add this code to src/Core/Db.php:

    <?php namespace Bookstore\\Core; use PDO; class Db { private static
    $instance; private static function connect(): PDO { $dbConfig =
    Config::getInstance()->get('db'); return new PDO(
    'mysql:host=127.0.0.1;dbname=bookstore', $dbConfig['user'],
    $dbConfig['password'] ); } public static function getInstance(){ if
    (self::$instance == null) { self::$instance = self::connect(); }
    return self::$instance; } }

This class, defined in the preceding code snippet, just implements the
singleton pattern and wraps the creation of a PDO instance. From now on,
in order to get a database connection, we just need to write
Db::getInstance().

Although it might not be true for all models, in our application, they
will always have to access the database. We could create an abstract
class where all models extend. This class could contain a $db protected
property that will be set on the constructor. With this, we avoid
duplicating the same constructor and property definition across all our
models. Copy the following class into src/Models/AbstractModel.php:

    <?php namespace Bookstore\\Models; use PDO; abstract class
    AbstractModel { private $db; public function \_\_construct(PDO $db)
    { $this->db = $db; } }

Finally, to finish the setup of the models, we could create a new
exception (as we did with the NotFoundException class) that represents
an error from the database. It will not contain any code, but we will be
able to differentiate where an exception is coming from. We will save it
in src/Exceptions/DbException.php:

    <?php namespace Bookstore\\Exceptions; use Exception; class
    DbException extends Exception { }

Now that we've set the ground, we can start writing our models. It is up
to you to organize your models, but it is a good idea to mimic the
domain objects structure. In this case, we would have three models:
CustomerModel, BookModel, and SalesModel. In the following sections, we
will explain the contents of each of them.

The customer model
------------------

Let's start with the easiest one. As our application is still very
primitive, we will not allow the creation of new costumers, and work
with the ones we inserted manually into the database instead. That means
that the only thing we need to do with customers is to query them. Let's
create a CustomerModel class in src/Models/CustomerModel.php with the
following content:

    <?php namespace Bookstore\\Models; use Bookstore\\Domain\\Customer;
    use Bookstore\\Domain\\Customer\\CustomerFactory; use
    Bookstore\\Exceptions\\NotFoundException; class CustomerModel
    extends AbstractModel { public function get(int $userId): Customer {
    $query = 'SELECT \* FROM customer WHERE customer\_id = :user'; $sth
    = $this->db->prepare($query); $sth->execute(['user' => $userId]);
    $row = $sth->fetch(); if (empty($row)) { throw new
    NotFoundException(); } return CustomerFactory::factory(
    $row['type'], $row['id'], $row['firstname'], $row['surname'],
    $row['email'] ); } public function getByEmail(string $email):
    Customer { $query = 'SELECT \* FROM customer WHERE email = :user';
    $sth = $this->db->prepare($query); $sth->execute(['user' =>
    $email]); $row = $sth->fetch(); if (empty($row)) { throw new
    NotFoundException(); } return CustomerFactory::factory(
    $row['type'], $row['id'], $row['firstname'], $row['surname'],
    $row['email'] ); } }

The CustomerModel class, which extends from the AbstractModel class,
contains two methods; both of them return a Customer instance, one of
them when providing the ID of the customer, and the other one when
providing the e-mail. As we already have the database connection as the
$db property, we just need to prepare the statement with the given
query, execute the statement with the arguments, and fetch the result.
As we expect to get a customer, if the user provided an ID or an e-mail
that does not belong to any customer, we will need to throw an
exception—in this case, a NotFoundException is just fine. If we find a
customer, we use our factory to create the object and return it.

The book model
--------------

Our BookModel class gives us a bit more of work. Customers had a
factory, but it is not worth having one for books. What we use for
creating them from MySQL rows is not the constructor, but a fetch mode
that PDO has, and that allows us to map a row into an object. To do so,
we need to adapt the Book domain object a bit:

-  The names of the properties have to be the same as the names of the
   fields in the database

-  There is no need for a constructor or setters, unless we need them
   for other purposes

-  To go with encapsulation, properties should be private, so we will
   need getters for all of them

The new Book class should look like the following:

    <?php namespace Bookstore\\Domain; class Book { private $id; private
    $isbn; private $title; private $author; private $stock; private
    $price; public function getId(): int { return $this->id; } public
    function getIsbn(): string { return $this->isbn; } public function
    getTitle(): string { return $this->title; } public function
    getAuthor(): string { return $this->author; } public function
    getStock(): int { return $this->stock; } public function getCopy():
    bool { if ($this->stock < 1) { return false; } else {
    $this->stock--; return true; } } public function addCopy() {
    $this->stock++; } public function getPrice(): float { return
    $this->price; } }

We retained the getCopy and addCopy methods even though they are not
getters, as we will need them later. Now, when fetching a group of rows
from MySQL with the fetchAll method, we can send two parameters: the
constant PDO::FETCH\_CLASS that tells PDO to map rows to a class, and
the name of the class that we want to map to. Let's create the BookModel
class with a simple get method that fetches a book from the database
with a given ID. This method will return either a Book object or throw
an exception in case the ID does not exist. Save it as
src/Models/BookModel.php:

    <?php namespace Bookstore\\Models; use Bookstore\\Domain\\Book; use
    Bookstore\\Exceptions\\DbException; use
    Bookstore\\Exceptions\\NotFoundException; use PDO; class BookModel
    extends AbstractModel { const CLASSNAME =
    '\\Bookstore\\Domain\\Book'; public function get(int $bookId): Book
    { $query = 'SELECT \* FROM book WHERE id = :id'; $sth =
    $this->db->prepare($query); $sth->execute(['id' => $bookId]); $books
    = $sth->fetchAll( PDO::FETCH\_CLASS, self::CLASSNAME ); if
    (empty($books)) { throw new NotFoundException(); } return $books[0];
    } }

There are advantages and disadvantages of using this fetch mode. On one
hand, we avoid a lot of dull code when creating objects from rows.
Usually, we either just send all the elements of the row array to the
constructor of the class, or use setters for all its properties. If we
add more fields to the MySQL table, we just need to add the properties
to our domain class, instead of changing everywhere where we were
instantiating the objects. On the other hand, you are forced to use the
same names for the fields in both the table's as well as the class'
properties, which means high coupling (always a bad idea). This also
causes some conflicts when following conventions, because in MySQL, it
is common to use book\_id, but in PHP, the property is $bookId.

Now that we know how this fetch mode works, let's add three other
methods that fetch data from MySQL. Add the following code to your
model:

    public function getAll(int $page, int $pageLength): array { $start =
    $pageLength \* ($page - 1); $query = 'SELECT \* FROM book LIMIT
    :page, :length'; $sth = $this->db->prepare($query);
    $sth->bindParam('page', $start, PDO::PARAM\_INT);
    $sth->bindParam('length', $pageLength, PDO::PARAM\_INT);
    $sth->execute(); return $sth->fetchAll(PDO::FETCH\_CLASS,
    self::CLASSNAME); } public function getByUser(int $userId): array {
    $query = <<<SQL SELECT b.\* FROM borrowed\_books bb LEFT JOIN book b
    ON bb.book\_id = b.id WHERE bb.customer\_id = :id SQL; $sth =
    $this->db->prepare($query); $sth->execute(['id' => $userId]); return
    $sth->fetchAll(PDO::FETCH\_CLASS, self::CLASSNAME); } public
    function search(string $title, string $author): array { $query =
    <<<SQL SELECT \* FROM book WHERE title LIKE :title AND author LIKE
    :author SQL; $sth = $this->db->prepare($query);
    $sth->bindValue('title', "%$title%"); $sth->bindValue('author',
    "%$author%"); $sth->execute(); return
    $sth->fetchAll(PDO::FETCH\_CLASS, self::CLASSNAME); }

The methods added are as follows:

-  getAll returns an array of all the books for a given page. Remember
   that LIMIT allows you to return a specific number of rows with an
   offset, which can work as a paginator.

-  getByUser returns all the books that a given customer has borrowed—we
   will need to use a join query for this. Note that we return b.\*,
   that is, only the fields of the book table, skipping the rest of the
   fields.

-  Finally, there is a method to search by either title or author, or
   both. We can do that using the operator LIKE and enclosing the
   patterns with %. If we do not specify one of the parameters, we will
   try to match the field with %%, which matches everything.

So far, we have been adding methods to fetch data. Let's add methods
that will allow us to modify the data in our database. For the book
model, we will need to be able to borrow books and return them. Here is
the code for those two actions:

    public function borrow(Book $book, int $userId) { $query = <<<SQL
    INSERT INTO borrowed\_books (book\_id, customer\_id, start)
    VALUES(:book, :user, NOW()) SQL; $sth = $this->db->prepare($query);
    $sth->bindValue('book', $book->getId()); $sth->bindValue('user',
    $userId); if (!$sth->execute()) { throw new
    DbException($sth->errorInfo()[2]); } $this->updateBookStock($book);
    } public function returnBook(Book $book, int $userId) { $query =
    <<<SQL UPDATE borrowed\_books SET end = NOW() WHERE book\_id = :book
    AND customer\_id = :user AND end IS NULL SQL; $sth =
    $this->db->prepare($query); $sth->bindValue('book', $book->getId());
    $sth->bindValue('user', $userId); if (!$sth->execute()) { throw new
    DbException($sth->errorInfo()[2]); } $this->updateBookStock($book);
    } private function updateBookStock(Book $book) { $query = 'UPDATE
    book SET stock = :stock WHERE id = :id'; $sth =
    $this->db->prepare($query); $sth->bindValue('id', $book->getId());
    $sth->bindValue('stock', $book->getStock()); if (!$sth->execute()) {
    throw new DbException($sth->errorInfo()[2]); } }

When borrowing a book, you are adding a row to the borrower\_books
table. When returning books, you do not want to remove that row, but
rather to set the end date in order to keep a history of the books that
a user has been borrowing. Both methods need to change the stock of the
borrowed book: when borrowing it, reducing the stock by one, and when
returning it, increasing the stock. That is why, in the last code
snippet, we created a private method to update the stock of a given
book, which will be used from both the borrow and returnBook methods.

The sales model
---------------

Now we need to add the last model to our application: the SalesModel.
Using the same fetch mode that we used with books, we need to adapt the
domain class as well. We need to think a bit more in this case, as we
will be doing more than just fetching. Our application has to be able to
create new sales on demand, containing the ID of the customer and the
books. We can already add books with the current implementation, but we
need to add a setter for the customer ID. The ID of the sale will be
given by the autoincrement ID in MySQL, so there is no need to add a
setter for it. The final implementation would look as follows:

    <?php namespace Bookstore\\Domain; class Sale { private $id; private
    $customer\_id; private $books; private $date; public function
    setCustomerId(int $customerId) { $this->customer\_id = $customerId;
    } public function getId(): int { return $this->id; } public function
    getCustomerId(): int { return $this->customer\_id; } public function
    getBooks(): array { return $this->books; } public function
    getDate(): string { return $this->date; } public function
    addBook(int $bookId, int $amount = 1) { if
    (!isset($this->books[$bookId])) { $this->books[$bookId] = 0; }
    $this->books[$bookId] += $amount; } public function setBooks(array
    $books) { $this->books = $books; } }

The SalesModel will be the most difficult one to write. The problem with
this model is that it includes manipulating different tables: sale and
sale\_book. For example, when getting the information of a sale, we need
to get the information from the sale table, and then the information of
all the books in the sale\_book table. You could argue about whether to
have one unique method that fetches all the necessary information
related to a sale, or to have two different methods, one to fetch the
sale and the other to fetch the books, and let the controller to decide
which one to use.

This actually starts a very interesting discussion. On one hand, we want
to make things easier for the controller—having one unique method to
fetch the entire Sale object. This makes sense as the controller does
not need to know about the internal implementation of the Sale object,
which lowers coupling. On the other hand, forcing the model to always
fetch the whole object, even if we only need the information in the sale
table, is a bad idea. Imagine if the sale contains a lot of books;
fetching them from MySQL will decrease performance unnecessarily.

You should think how your controllers need to manage sales. If you will
always need the entire object, you can have one method without being
concerned about performance. If you only need to fetch the entire object
sometimes, maybe you could add both methods. For our application, we
will have one method to rule them all, since that is what we will always
need.

Note

Lazy loading

As with any other design challenge, other developers have already given
a lot of thought to this problem. They came up with a design pattern
named lazy load. This pattern basically lets the controller think that
there is only one method to fetch the whole domain object, but we will
actually be fetching only what we need from database.

The model fetches the most used information for the object and leaves
the rest of the properties that need extra database queries empty. Once
the controller uses a getter of a property that is empty, the model
automatically fetches that data from the database. We get the best of
both worlds: there is simplicity for the controller, but we do not spend
more time than necessary querying unused data.

Add the following as your src/Models/SaleModel.php file:

    <?php namespace Bookstore\\Models; use Bookstore\\Domain\\Sale; use
    Bookstore\\Exceptions\\DbException; use PDO; class SaleModel extends
    AbstractModel { const CLASSNAME = '\\Bookstore\\Domain\\Sale';
    public function getByUser(int $userId): array { $query = 'SELECT \*
    FROM sale WHERE s.customer\_id = :user'; $sth =
    $this->db->prepare($query); $sth->execute(['user' => $userId]);
    return $sth->fetchAll(PDO::FETCH\_CLASS, self::CLASSNAME); } public
    function get(int $saleId): Sale { $query = 'SELECT \* FROM sale
    WHERE id = :id'; $sth = $this->db->prepare($query);
    $sth->execute(['id' => $saleId]); $sales =
    $sth->fetchAll(PDO::FETCH\_CLASS, self::CLASSNAME); if
    (empty($sales)) { throw new NotFoundException('Sale not found.'); }
    $sale = array\_pop($sales); $query = <<<SQL SELECT b.id, b.title,
    b.author, b.price, sb.amount as stock, b.isbn FROM sale s LEFT JOIN
    sale\_book sb ON s.id = sb.sale\_id LEFT JOIN book b ON sb.book\_id
    = b.id WHERE s.id = :id SQL; $sth = $this->db->prepare($query);
    $sth->execute(['id' => $saleId]); $books = $sth->fetchAll(
    PDO::FETCH\_CLASS, BookModel::CLASSNAME ); $sale->setBooks($books);
    return $sale; } }

Another tricky method in this model is the one that takes care of
creating a sale in the database. This method has to create a sale in the
sale table, and then add all the books for that sale to the sale\_book
table. What would happen if we have a problem when adding one of the
books? We would leave a corrupted sale in the database. To avoid that,
we need to use transactions, starting with one at the beginning of the
model's or the controller's method, and either rolling back in case of
error, or committing it at the end of the method.

In the same method, we also need to take care of the ID of the sale. We
do not set the ID of the sale when creating the sale object, because we
rely on the autoincremental field in the database. But when inserting
the books into sale\_book, we do need the ID of the sale. For that, we
need to request the PDO for the last inserted ID with the lastInsertId
method. Let's add then the create method into your SaleModel:

    public function create(Sale $sale) { $this->db->beginTransaction();
    $query = <<<SQL INSERT INTO sale(customer\_id, date) VALUES(:id,
    NOW()) SQL; $sth = $this->db->prepare($query); if
    (!$sth->execute(['id' => $sale->getCustomerId()])) {
    $this->db->rollBack(); throw new DbException($sth->errorInfo()[2]);
    } $saleId = $this->db->lastInsertId(); $query = <<<SQL INSERT INTO
    sale\_book(sale\_id, book\_id, amount) VALUES(:sale, :book, :amount)
    SQL; $sth = $this->db->prepare($query); $sth->bindValue('sale',
    $saleId); foreach ($sale->getBooks() as $bookId => $amount) {
    $sth->bindValue('book', $bookId); $sth->bindValue('amount',
    $amount); if (!$sth->execute()) { $this->db->rollBack(); throw new
    DbException($sth->errorInfo()[2]); } } $this->db->commit(); }

One last thing to note from this method is that we prepare a statement,
bind a value to it (the sale ID), and then bind and execute the same
statement as many times as the books in the array. Once you have a
statement, you can bind the values as many times as you want. Also, you
can execute the same statement as many times as you want, and the values
stay the same.

V for view
==========

The view is the layer that takes care of the… view. In this layer, you
find all the templates that render the HTML that the user gets. Although
the separation between views and the rest of the application is easy to
see, that does not make views an easy part. In fact, you will have to
learn a new technology in order to write views properly. Let's get into
the details.

Introduction to Twig
--------------------

In our first attempt at writing views, we mixed up PHP and HTML code. We
already know that the logic should not be mixed in the same place as
HTML, but that is not the end of the story. When rendering HTML, we need
some logic there too. For example, if we want to print a list of books,
we need to repeat a certain block of HTML for each book. And since a
priori we do not know the number of books to print, the best option
would be a foreach loop.

One option that a lot of people take is minimizing the amount of logic
that you can include in a view. You could set some rules, such as *we
should only include conditionals and loops*, which is a reasonable
amount of logic needed to render basic views. The problem is that there
is not a way of enforcing this kind of rule, and other developers can
easily start adding heavy logic in there. While some people are OK with
that, assuming that no one will do it, others prefer to implement more
restrictive systems. That was the beginning of template engines.

You could think of a template engine as another language that you need
to learn. Why would you do that? Because this new "language" is more
limited than PHP. These languages usually allow you to perform
conditionals and simple loops, and that is it. The developer is not able
to add PHP to that file, since the template engine will not treat it as
PHP code. Instead, it will just print the code to the output—the
response' body—as if it was plain text. Also, as it is specially
oriented to write templates, the syntax is usually easier to read when
mixed with HTML. Almost everything is an advantage.

The inconvenience of using a template engine is that it takes some time
to translate the new language to PHP, and then to HTML. This can be
quite time consuming, so it is very important that you choose a good
template engine. Most of them also allow you to cache templates,
improving the performance. Our choice is a quite light and widely used
one: Twig. As we've already added the dependency in our Composer file,
we can use it straight away.

Setting up Twig is quite easy. On the PHP side, you just need to specify
the location of the templates. A common convention is to use the views
directory for that. Create the directory, and add the following two
lines into your index.php:

    $loader = new Twig\_Loader\_Filesystem(\_\_DIR\_\_ . '/views');
    $twig = new Twig\_Environment($loader);

The book view
-------------

In these sections, as we work with templates, it would be nice to see
the result of your work. We have not yet implemented any controllers, so
we will force our index.php to render a specific template, regardless of
the request. We can start rendering the view of a single book. For that,
let's add the following code at the end of your index.php, after
creating your twig object:

    $bookModel = new BookModel(Db::getInstance()); $book =
    $bookModel->get(1); $params = ['book' => $book]; echo
    $twig->loadTemplate('book.twig')->render($params);

In the preceding code, we request the book with ID 1 to the BookModel,
get the book object, and create an array where the book key has the
value of the book object. After that, we tell Twig to load the template
book.twig and to render it by sending the array. This takes the template
and injects the $book object, so that you are able to use it inside the
template.

Let's now create our first template. Write the following code into
view/book.twig. By convention, all Twig templates should have the .twig
extension:

    <h2>{{ book.title }}</h2> <h3>{{ book.author }}</h3> <hr> <p>
    <strong>ISBN</strong> {{ book.isbn }} </p> <p>
    <strong>Stock</strong> {{ book.stock }} </p> <p>
    <strong>Price</strong> {{ book.price\|number\_format(2) }} € </p>
    <hr> <h3>Actions</h3> <form method="post" action="/book/{{ book.id
    }}/borrow"> <input type="submit" value="Borrow"> </form> <form
    method="post" action="/book/{{ book.id }}/buy"> <input type="submit"
    value="Buy"> </form>

Since this is your first Twig template, let's go step by step. You can
see that most of the content is HTML: some headers, a couple of
paragraphs, and two forms with two buttons. You can recognize the Twig
part, since it is enclosed by {{ }}. In Twig, everything that is between
those curly brackets will be printed out. The first one that we find
contains book.title. Do you remember that we injected the book object
when rendering the template? We can access it here, just not with the
usual PHP syntax. To access an object's property, use . instead of ->.
So, this book.title will return the value of the title property of the
book object, and the {{ }} will make Twig print it out. The same applies
to the rest of the template.

There is one that does a bit more than just access an object's property.
The book.price\|number\_format(2) gets the price of the book and sends
it as an argument (using the pipe symbol) to the function
number\_format, which has already got 2 as another argument. This bit of
code basically formats the price to two digital figures. In Twig, you
also have some functions, but they are mostly reduced to formatting the
output, which is an acceptable amount of logic.

Are you convinced now about how clean it is to use a template engine for
your views? You can try it in your browser: accessing any path, your web
server should execute the index.php file, forcing the template book.twig
to be rendered.

Layouts and blocks
------------------

When you design your web application, usually you would want to share a
common layout across most of your views. In our case, we want to always
have a menu at the top of the view that allows us to go to the different
sections of the website, or even to search books from wherever the user
is. As with models, we want to avoid code duplication, since if we were
to copy and paste the layout everywhere, updating it would be a
nightmare. Instead, Twig comes with the ability to define layouts.

A layout in Twig is just another template file. Its content is just the
common HTML code that we want to display across all views (in our case,
the menu and search bar), and contains some tagged gaps (blocks in
Twig's world), where you will be able to inject the specific HTML of
each view. You can define one of those blocks with the tag {% block %}.
Let's see what our views/layout.twig file would look like:

    <html> <head> <title>{% block title %}{% endblock %}</title> </head>
    <body> <div style="border: solid 1px"> <a href="/books">Books</a> <a
    href="/sales">My Sales</a> <a href="/my-books">My Books</a> <hr>
    <form action="/books/search" method="get"> <label>Title</label>
    <input type="text" name="title"> <label>Author</label> <input
    type="text" name="author"> <input type="submit" value="Search">
    </form> </div> {% block content %}{% endblock %} </body> </html>

As you can see in the preceding code, blocks have a name so that
templates using the layout can refer to them. In our layout, we defined
two blocks: one for the title of the view and the other for the content
itself. When a template uses the layout, we just need to write the HTML
code for each of the blocks defined in the layout, and Twig will do the
rest. Also, to let Twig know that our template wants to use the layout,
we use the tag {% extends %} with the layout filename. Let's update
views/book.twig to use our new layout:

    {% extends 'layout.twig' %} {% block title %} {{ book.title }} {%
    endblock %} {% block content %} <h2>{{ book.title }}</h2> //...
    </form> {% endblock %}

At the top of the file, we add the layout that we need to use. Then, we
open a block tag with the reference name, and we write inside it the
HTML that we want to use. You can use anything valid inside a block,
either Twig code or plain HTML. In our template, we used the title of
the book as the title block, which refers to the title of the view, and
we put all the previous HTML inside the content block. Note that
everything in the file is inside a block now. Try it in your browser now
to see the changes.

Paginated book list
-------------------

Let's add another view, this time for a paginated list of books. In
order to see the result of your work, update the content of index.php,
replacing the code of the previous section with the following:

    $bookModel = new BookModel(Db::getInstance()); $books =
    $bookModel->getAll(1, 3); $params = ['books' => $books,
    'currentPage' => 2]; echo
    $twig->loadTemplate('books.twig')->render($params);

In the preceding snippet, we force the application to render the
books.twig template, sending an array of books from page number 1, and
showing 3 books per page. This array, though, might not always return 3
books, maybe because there are only 2 books in the database. We should
then use a loop to iterate the list instead of assuming the size of the
array. In Twig, you can emulate a foreach loop using {% for <element> in
<array> %} in order to iterate an array. Let's use it for your
views/books.twig:

    {% extends 'layout.twig' %} {% block title %} Books {% endblock %}
    {% block content %} <table> <thead> <th>Title</th> <th>Author</th>
    <th></th> </thead> {% for book in books %} <tr> <td>{{ book.title
    }}</td> <td>{{ book.author }}</td> <td><a href="/book/{{ book.id
    }}">View</a></td> </tr> {% endfor %} </table> {% endblock %}

We can also use conditionals in a Twig template, which work the same as
the conditionals in PHP. The syntax is {% if <boolean expression> %}.
Let's use it to decide if we should show the previous and/or following
links on our page. Add the following code at the end of the content
block:

    {% if currentPage != 1 %} <a href="/books/{{ currentPage - 1
    }}">Previous</a> {% endif %} {% if not lastPage %} <a
    href="/books/{{ currentPage + 1 }}">Next</a> {% endif %}

The last thing to note from this template is that we are not restricted
to using only variables when printing out content with {{ }}. We can add
any valid Twig expression that returns a value, as we did with {{
currentPage + 1 }}.

The sales view
--------------

We have already shown you everything that you will need for using
templates, and now we just have to finish adding all of them. The next
one in the list is the template that shows the list of sales for a given
user. Update your index.php file with the following hack:

    $saleModel = new SaleModel(Db::getInstance()); $sales =
    $saleModel->getByUser(1); $params = ['sales' => $sales]; echo
    $twig->loadTemplate('sales.twig')->render($params);

The template for this view will be very similar to the one listing the
books: a table populated with the content of an array. The following is
the content of views/sales.twig:

    {% extends 'layout.twig' %} {% block title %} My sales {% endblock
    %} {% block content %} <table> <thead> <th>Id</th> <th>Date</th>
    </thead> {% for sale in sales %} <tr> <td>{{ sale.id}}</td> <td>{{
    sale.date }}</td> <td><a href="/sales/{{ sale.id }}">View</a></td>
    </tr> {% endfor %} </table> {% endblock %}

The other view related to sales is where we want to display all the
content of a specific one. This sale, again, will be similar to the
books list, as we will be listing the books related to that sale. The
hack to force the rendering of this template is as follows:

    $saleModel = new SaleModel(Db::getInstance()); $sale =
    $saleModel->get(1); $params = ['sale' => $sale]; echo
    $twig->loadTemplate('sale.twig')->render($params);

And the Twig template should be placed in views/sale.twig:

    {% extends 'layout.twig' %} {% block title %} Sale {{ sale.id }} {%
    endblock %} {% block content %} <table> <thead> <th>Title</th>
    <th>Author</th> <th>Amount</th> <th>Price</th> <th></th> </thead> {%
    for book in sale.books %} <tr> <td>{{ book.title }}</td> <td>{{
    book.author }}</td> <td>{{ book.stock }}</td> <td>{{ (book.price \*
    book.stock)\|number\_format(2) }} €</td> <td><a href="/book/{{
    book.id }}">View</a></td> </tr> {% endfor %} </table> {% endblock %}

The error template
------------------

We should add a very simple template that will be shown to the user when
there is an error in our application, rather than showing a PHP error
message. This template will just expect the errorMessage variable, and
it could look like the following. Save it as views/error.twig:

    {% extends 'layout.twig' %} {% block title %} Error {% endblock %}
    {% block content %} <h2>Error: {{ errorMessage }}</h2> {% endblock
    %}

Note that even the error page extends from the layout, as we want the
user to be able to do something else when this happens.

The login template
------------------

Our last template will be the one that allows the user to log in. This
template is a bit different from the others, as it will be used in two
different scenarios. In the first one, the user accesses the login view
for the first time, so we need to show the form. In the second one, the
user has already tried to log in, and there was an error when doing so,
that is, the e-mail address was not found. In this case, we will add an
extra variable to the template, errorMessage, and we will add a
conditional to show its contents only when this variable is defined. You
can use the operator is defined to check that. Add the following
template as views/login.twig:

    {% extends 'layout.twig' %} {% block title %} Login {% endblock %}
    {% block content %} {% if errorMessage is defined %} <strong>{{
    errorMessage }}</strong> {% endif %} <form action="/login"
    method="post"> <label>Email</label> <input type="text" name="email">
    <input type="submit"> </form> {% endblock %}

C for controller
================

It is finally time for the director of the orchestra. Controllers
represent the layer in our application that, given a request, talks to
the models and builds the views. They act like the manager of a team:
they decide what resources to use depending on the situation.

As we stated when explaining models, it is sometimes difficult to decide
if some piece of logic should go into the controller or the model. At
the end of the day, MVC is a pattern, like a recipe that guides you,
rather than an exact algorithm that you need to follow step by step.
There will be scenarios where the answer is not straightforward, so it
will be up to you; in these cases, just try to be consistent. The
following are some common scenarios that might be difficult to localize:

-  The request points to a path that we do not support. This scenario is
   already covered in our application, and it is the router that should
   take care of it, not the controller.

-  The request tries to access an element that does not exist, for
   example, a book ID that is not in the database. In this case, the
   controller should ask the model if the book exists, and depending on
   the response, render a template with the book's contents, or another
   with a "Not found" message.

-  The user tries to perform an action, such as buying a book, but the
   parameters coming from the request are not valid. This is a tricky
   one. One option is to get all the parameters from the request without
   checking them, sending them straight to the model, and leaving the
   task of sanitizing the information to the model. Another option is
   that the controller checks that the parameters provided make sense,
   and then gives them to the model. There are other solutions, like
   building a class that checks if the parameters are valid, which can
   be reused in different controllers. In this case, it will depend on
   the amount of parameters and logic involved in the sanitization. For
   requests receiving a lot of data, the third option looks like the
   best of them, as we will be able to reuse the code in different
   endpoints, and we are not writing controllers that are too long. But
   in requests where the user sends one or two parameters, sanitizing
   them in the controller might be good enough.

Now that we've set the ground, let's prepare our application to use
controllers. The first thing to do is to update our index.php, which has
been forcing the application to always render the same template.
Instead, we should be giving this task to the router, which will return
the response as a string that we can just print with echo. Update your
index.php file with the following content:

    <?php use Bookstore\\Core\\Router; use Bookstore\\Core\\Request;
    require\_once \_\_DIR\_\_ . '/vendor/autoload.php'; $router = new
    Router(); $response = $router->route(new Request()); echo $response;

As you might remember, the router instantiates a controller class,
sending the request object to the constructor. But controllers have
other dependencies as well, such as the template engine, the database
connection, or the configuration reader. Even though this is not the
best solution (you will improve it once we cover dependency injection in
the next section), we could create an AbstractController that would be
the parent of all controllers, and will set those dependencies. Copy the
following as src/Controllers/AbstractController.php:

    <?php namespace Bookstore\\Controllers; use Bookstore\\Core\\Config;
    use Bookstore\\Core\\Db; use Bookstore\\Core\\Request; use
    Monolog\\Logger; use Twig\_Environment; use
    Twig\_Loader\_Filesystem; use Monolog\\Handler\\StreamHandler;
    abstract class AbstractController { protected $request; protected
    $db; protected $config; protected $view; protected $log; public
    function \_\_construct(Request $request) { $this->request =
    $request; $this->db = Db::getInstance(); $this->config =
    Config::getInstance(); $loader = new Twig\_Loader\_Filesystem(
    \_\_DIR\_\_ . '/../../views' ); $this->view = new
    Twig\_Environment($loader); $this->log = new Logger('bookstore');
    $logFile = $this->config->get('log'); $this->log->pushHandler( new
    StreamHandler($logFile, Logger::DEBUG) ); } public function
    setCustomerId(int $customerId) { $this->customerId = $customerId; }
    }

When instantiating a controller, we will set some properties that will
be useful when handling requests. We already know how to instantiate the
database connection, the configuration reader, and the template engine.
The fourth property, $log, will allow the developer to write logs to a
given file when necessary. We will use the Monolog library for that, but
there are many other options. Notice that in order to instantiate the
logger, we get the value of log from the configuration, which should be
the path to the log file. The convention is to use the /var/log/
directory, so create the /var/log/bookstore.log file, and add "log":
"/var/log/bookstore.log" to your configuration file.

Another thing that is useful to some controllers—but not all of them—is
the information about the user performing the action. As this is only
going to be available for certain routes, we should not set it when
constructing the controller. Instead, we have a setter for the router to
set the customer ID when available; in fact, the router does that
already.

Finally, a handy helper method that we could use is one that renders a
given template with parameters, as all the controllers will end up
rendering one template or the other. Let's add the following protected
method to the AbstractController class:

    protected function render(string $template, array $params): string {
    return $this->view->loadTemplate($template)->render($params); }

The error controller
--------------------

Let's start by creating the easiest of the controllers: the
ErrorController. This controller does not do much; it just renders the
error.twig template sending the "Page not found!" message. As you might
remember, the router uses this controller when it cannot match the
request to any of the other defined routes. Save the following class in
src/Controllers/ErrorController.php:

    <?php namespace Bookstore\\Controllers; class ErrorController
    extends AbstractController { public function notFound(): string {
    $properties = ['errorMessage' => 'Page not found!']; return
    $this->render('error.twig', $properties); } }

The login controller
--------------------

The second controller that we have to add is the one that manages the
login of the customers. If we think about the flow when a user wants to
authenticate, we have the following scenarios:

-  The user wants to get the login form in order to submit the necessary
   information and log in.

-  The user tries to submit the form, but we could not get the e-mail
   address. We should render the form again, letting them know about the
   problem.

-  The user submits the form with an e-mail, but it is not a valid one.
   In this case, we should show the login form again with an error
   message explaining the situation.

-  The user submits a valid e-mail, we set the cookie, and we show the
   list of books so the user can start searching. This is absolutely
   arbitrary; you could choose to send them to their borrowed books
   page, their sales, and so on. The important thing here is to notice
   that we will be redirecting the request to another controller.

There are up to four possible paths. We will use the request object to
decide which of them to use in each case, returning the corresponding
response. Let's create, then, the CustomerController class in
src/Controllers/CustomerController.php with the login method, as
follows:

    <?php namespace Bookstore\\Controllers; use
    Bookstore\\Exceptions\\NotFoundException; use
    Bookstore\\Models\\CustomerModel; class CustomerController extends
    AbstractController { public function login(string $email): string {
    if (!$this->request->isPost()) { return $this->render('login.twig',
    []); } $params = $this->request->getParams(); if
    (!$params->has('email')) { $params = ['errorMessage' => 'No info
    provided.']; return $this->render('login.twig', $params); } $email =
    $params->getString('email'); $customerModel = new
    CustomerModel($this->db); try { $customer =
    $customerModel->getByEmail($email); } catch (NotFoundException $e) {
    $this->log->warn('Customer email not found: ' . $email); $params =
    ['errorMessage' => 'Email not found.']; return
    $this->render('login.twig', $params); } setcookie('user',
    $customer->getId()); $newController = new
    BookController($this->request); return $newController->getAll(); } }

As you can see, there are four different returns for the four different
cases. The controller itself does not do anything, but orchestrates the
rest of the components, and makes decisions. First, we check if the
request is a POST, and if it is not, we will assume that the user wants
to get the form. If it is, we will check for the e-mail in the
parameters, returning an error if the e-mail is not there. If it is, we
will try to find the customer with that e-mail, using our model. If we
get an exception saying that there is no such customer, we will render
the form with a "Not found" error message. If the login is successful,
we will set the cookie with the ID of the customer, and will execute the
getAll method of BookController (still to be written), returning the
list of books.

At this point, you should be able to test the login feature of your
application end to end with the browser. Try to access
http://localhost:8000/login to see the form, adding random e-mails to
get the error message, and adding a valid e-mail (check your customer
table in MySQL) to log in successfully. After this, you should see the
cookie with the customer ID.

The book controller
-------------------

The BookController class will be the largest of our controllers, as most
of the application relies on it. Let's start by adding the easiest
methods, the ones that just retrieve information from the database. Save
this as src/Controllers/BookController.php:

    <?php namespace Bookstore\\Controllers; use
    Bookstore\\Models\\BookModel; class BookController extends
    AbstractController { const PAGE\_LENGTH = 10; public function
    getAllWithPage($page): string { $page = (int)$page; $bookModel = new
    BookModel($this->db); $books = $bookModel->getAll($page,
    self::PAGE\_LENGTH); $properties = [ 'books' => $books,
    'currentPage' => $page, 'lastPage' => count($books) <
    self::PAGE\_LENGTH ]; return $this->render('books.twig',
    $properties); } public function getAll(): string { return
    $this->getAllWithPage(1); } public function get(int $bookId): string
    { $bookModel = new BookModel($this->db); try { $book =
    $bookModel->get($bookId); } catch (\\Exception $e) {
    $this->log->error( 'Error getting book: ' . $e->getMessage() );
    $properties = ['errorMessage' => 'Book not found!']; return
    $this->render('error.twig', $properties); } $properties = ['book' =>
    $book]; return $this->render('book.twig', $properties); } public
    function getByUser(): string { $bookModel = new
    BookModel($this->db); $books =
    $bookModel->getByUser($this->customerId); $properties = [ 'books' =>
    $books, 'currentPage' => 1, 'lastPage' => true ]; return
    $this->render('books.twig', $properties); } }

There's nothing too special in this preceding code so far. The
getAllWithPage and getAll methods do the same thing, one with the page
number given by the user as a URL argument, and the other setting the
page number as 1—the default case. They ask the model for the list of
books to be displayed and passed to the view. The information of the
current page—and whether or not we are on the last page—is also sent to
the template in order to add the "previous" and "next" page links.

The get method will get the ID of the book that the customer is
interested in. It will try to fetch it using the model. If the model
throws an exception, we will render the error template with a "Book not
found" message. Instead, if the book ID is valid, we will render the
book template as expected.

The getByUser method will return all the books that the authenticated
customer has borrowed. We will make use of the customerId property that
we set from the router. There is no sanity check here, since we are not
trying to get a specific book, but rather a list, which could be empty
if the user has not borrowed any books yet—but that is not an issue.

Another getter controller is the one that searches for a book by its
title and/or author. This method will be triggered when the user submits
the form in the layout template. The form sends both the title and the
author fields, so the controller will ask for both. The model is ready
to use the arguments that are empty, so we will not perform any extra
checking here. Add the method to the BookController class:

    public function search(): string { $title =
    $this->request->getParams()->getString('title'); $author =
    $this->request->getParams()->getString('author'); $bookModel = new
    BookModel($this->db); $books = $bookModel->search($title, $author);
    $properties = [ 'books' => $books, 'currentPage' => 1, 'lastPage' =>
    true ]; return $this->render('books.twig', $properties); }

Your application cannot perform any actions, but at least you can
finally browse the list of books, and click on any of them to view the
details. We are finally getting something here!

Borrowing books
---------------

Borrowing and returning books are probably the actions that involve the
most logic, together with buying a book, which will be covered by a
different controller. This is a good place to start logging the user's
actions, since it will be useful later for debugging purposes. Let's see
the code first, and then discuss it briefly. Add the following two
methods to your BookController class:

    public function borrow(int $bookId): string { $bookModel = new
    BookModel($this->db); try { $book = $bookModel->get($bookId); }
    catch (NotFoundException $e) { $this->log->warn('Book not found: ' .
    $bookId); $params = ['errorMessage' => 'Book not found.']; return
    $this->render('error.twig', $params); } if (!$book->getCopy()) {
    $params = [ 'errorMessage' => 'There are no copies left.' ]; return
    $this->render('error.twig', $params); } try {
    $bookModel->borrow($book, $this->customerId); } catch (DbException
    $e) { $this->log->error( 'Error borrowing book: ' . $e->getMessage()
    ); $params = ['errorMessage' => 'Error borrowing book.']; return
    $this->render('error.twig', $params); } return $this->getByUser(); }
    public function returnBook(int $bookId): string { $bookModel = new
    BookModel($this->db); try { $book = $bookModel->get($bookId); }
    catch (NotFoundException $e) { $this->log->warn('Book not found: ' .
    $bookId); $params = ['errorMessage' => 'Book not found.']; return
    $this->render('error.twig', $params); } $book->addCopy(); try {
    $bookModel->returnBook($book, $this->customerId); } catch
    (DbException $e) { $this->log->error( 'Error returning book: ' .
    $e->getMessage() ); $params = ['errorMessage' => 'Error returning
    book.']; return $this->render('error.twig', $params); } return
    $this->getByUser(); }

As we mentioned earlier, one of the new things here is that we are
logging user actions, like when trying to borrow or return a book that
is not valid. Monolog allows you to write logs with different priority
levels: error, warning, and notices. You can invoke methods such as
error, warn, or notice to refer to each of them. We use warnings when
something unexpected, yet not critical, happens, for example, trying to
borrow a book that is not there. Errors are used when there is an
unknown problem from which we cannot recover, like an error from the
database.

The modus operandi of these two methods is as follows: we get the book
object from the 3database with the given book ID. As usual, if there is
no such book, we return an error page. Once we have the book domain
object, we make use of the helpers addCopy and getCopy in order to
update the stock of the book, and send it to the model, together with
the customer ID, to store the information in the database. There is also
a sanity check when borrowing a book, just in case there are no more
books available. In both cases, we return the list of books that the
user has borrowed as the response of the controller.

The sales controller
--------------------

We arrive at the last of our controllers: the SalesController. With a
different model, it will end up doing pretty much the same as the
methods related to borrowed books. But we need to create the sale domain
object in the controller instead of getting it from the model. Let's add
the following code, which contains a method for buying a book, add, and
two getters: one that gets all the sales of a given user and one that
gets the info of a specific sale, that is, getByUser and get
respectively. Following the convention, the file will be
src/Controllers/SalesController.php:

    <?php namespace Bookstore\\Controllers; use Bookstore\\Domain\\Sale;
    use Bookstore\\Models\\SaleModel; class SalesController extends
    AbstractController { public function add($id): string { $bookId =
    (int)$id; $salesModel = new SaleModel($this->db); $sale = new
    Sale(); $sale->setCustomerId($this->customerId);
    $sale->addBook($bookId); try { $salesModel->create($sale); } catch
    (\\Exception $e) { $properties = [ 'errorMessage' => 'Error buying
    the book.' ]; $this->log->error( 'Error buying book: ' .
    $e->getMessage() ); return $this->render('error.twig', $properties);
    } return $this->getByUser(); } public function getByUser(): string {
    $salesModel = new SaleModel($this->db); $sales =
    $salesModel->getByUser($this->customerId); $properties = ['sales' =>
    $sales]; return $this->render('sales.twig', $properties); } public
    function get($saleId): string { $salesModel = new
    SaleModel($this->db); $sale = $salesModel->get($saleId); $properties
    = ['sale' => $sale]; return $this->render('sale.twig', $properties);
    } }

Dependency injection
====================

At the end of the chapter, we will cover one of the most interesting and
controversial of the topics that come with, not only the MVC pattern,
but OOP in general: dependency injection. We will show you why it is so
important, and how to implement a solution that suits our specific
application, even though there are quite a few different implementations
that can cover different necessities.

Why is dependency injection necessary?
--------------------------------------

We still need to cover the way to unit test your code, hence you have
not experienced it by yourself yet. But one of the signs of a potential
source of problems is when you use the new statement in your code to
create an instance of a class that does not belong to your code
base—also known as a dependency. Using new to create a domain object
like Book or Sale is fine. Using it to instantiate models is also
acceptable. But manually instantiating, which something else, such as
the template engine, the database connection, or the logger, is
something that you should avoid. There are different reasons that
support this idea:

-  If you want to use a controller from two different places, and each
   of these places needs a different database connection or log file,
   instantiating those dependencies inside the controller will not allow
   us to do that. The same controller will always use the same
   dependency.

-  Instantiating the dependencies inside the controller means that the
   controller is fully aware of the concrete implementation of each of
   its dependencies, that is, the controller knows that we are using PDO
   with the MySQL driver and the location of the credentials for the
   connection. This means a high level of coupling in your
   application—so, bad news.

-  Replacing one dependency with another that implements the same
   interface is not easy if you are instantiating the dependency
   explicitly everywhere, as you will have to search all these places,
   and change the instantiation manually.

For all these reasons, and more, it is always good to provide the
dependencies that a class such as a controller needs instead of letting
it create its own. This is something that everybody agrees with. The
problem comes when implementing a solution. There are different options:

-  We have a constructor that expects (through arguments) all the
   dependencies that the controller, or any other class, needs. The
   constructor will assign each of the arguments to the properties of
   the class.

-  We have an empty constructor, and instead, we add as many setter
   methods as the dependencies of the class.

-  A hybrid of both, where we set the main dependencies through a
   constructor, and set the rest of the dependencies via setters.

-  Sending an object that contains all the dependencies as a unique
   argument for the constructor, and the controller gets the
   dependencies that it needs from that container.

Each solution has its pros and cons. If we have a class with a lot of
dependencies, injecting all of them via the constructor would make it
counterintuitive, so it would be better if we inject them using setters,
even though a class with a lot of dependencies looks like bad design. If
we have just one or two dependencies, using the constructor could be
acceptable, and we will write less code. For classes with several
dependencies, but not all of them mandatory, using the hybrid version
could be a good solution. The fourth option makes it easier when
injecting the dependencies as we do not need to know what each object
expects. The problem is that each class should know how to fetch its
dependency, that is, the dependency name, which is not ideal.

Implementing our own dependency injector
----------------------------------------

Open source solutions for dependency injectors are already available,
but we think that it would be a good experience to implement a simple
one by yourself. The idea of our dependency injector is a class that
contains instances of the dependencies that your code needs. This class,
which is basically a map of dependency names to dependency instances,
will have two methods: a getter and a setter of dependencies. We do not
want to use a static property for the dependencies array, as one of the
goals is to be able to have more than one dependency injector with a
different set of dependencies. Add the following class to
src/Utils/DependencyInjector.php:

    <?php namespace Bookstore\\Utils; use
    Bookstore\\Exceptions\\NotFoundException; class DependencyInjector {
    private $dependencies = []; public function set(string $name,
    $object) { $this->dependencies[$name] = $object; } public function
    get(string $name) { if (isset($this->dependencies[$name])) { return
    $this->dependencies[$name]; } throw new NotFoundException( $name . '
    dependency not found.' ); } }

Having a dependency injector means that we will always use the same
instance of a given class every time we ask for it, instead of creating
one each time. That means that singleton implementations are not needed
anymore; in fact, as mentioned in `*Chapter 4* <#Top_of_ch04_html>`__,
*Creating Clean Code with OOP*, it is preferable to avoid them. Let's
get rid of them, then. One of the places where we were using it was in
our configuration reader. Replace the existing code with the following
in the src/Core/Config.php file:

    <?php namespace Bookstore\\Core; use
    Bookstore\\Exceptions\\NotFoundException; class Config { private
    $data; public function \_\_construct() { $json =
    file\_get\_contents( \_\_DIR\_\_ . '/../../config/app.json' );
    $this->data = json\_decode($json, true); } public function get($key)
    { if (!isset($this->data[$key])) { throw new NotFoundException("Key
    $key not in config."); } return $this->data[$key]; } }

The other place where we were making use of the singleton pattern was in
the DB class. In fact, the purpose of the class was only to have a
singleton for our database connection, but if we are not making use of
it, we can remove the entire class. So, delete your src/Core/DB.php
file.

Now we need to define all these dependencies and add them to our
dependency injector. The index.php file is a good place to have the
dependency injector before we route the request. Add the following code
just before instantiating the Router class:

    $config = new Config(); $dbConfig = $config->get('db'); $db = new
    PDO( 'mysql:host=127.0.0.1;dbname=bookstore', $dbConfig['user'],
    $dbConfig['password'] ); $loader = new
    Twig\_Loader\_Filesystem(\_\_DIR\_\_ . '/../../views'); $view = new
    Twig\_Environment($loader); $log = new Logger('bookstore'); $logFile
    = $config->get('log'); $log->pushHandler(new StreamHandler($logFile,
    Logger::DEBUG)); $di = new DependencyInjector(); $di->set('PDO',
    $db); $di->set('Utils\\Config', $config);
    $di->set('Twig\_Environment', $view); $di->set('Logger', $log);
    $router = new Router($di); //...

There are a few changes that we need to make now. The most important of
them refers to the AbstractController, the class that will make heavy
use of the dependency injector. Add a property named $di to that class,
and replace the constructor with the following:

    public function \_\_construct( DependencyInjector $di, Request
    $request ) { $this->request = $request; $this->di = $di; $this->db =
    $di->get('PDO'); $this->log = $di->get('Logger'); $this->view =
    $di->get('Twig\_Environment'); $this->config =
    $di->get('Utils\\Config'); $this->customerId = $\_COOKIE['id']; }

The other changes refer to the Router class, as we are sending it now as
part of the constructor, and we need to inject it to the controllers
that we create. Add a $di property to that class as well, and change the
constructor to the following one:

    public function \_\_construct(DependencyInjector $di) { $this->di =
    $di; $json = file\_get\_contents(\_\_DIR\_\_ .
    '/../../config/routes.json'); $this->routeMap = json\_decode($json,
    true); }

Also change the content of the executeController and route methods:

    public function route(Request $request): string { $path =
    $request->getPath(); foreach ($this->routeMap as $route => $info) {
    $regexRoute = $this->getRegexRoute($route, $info); if
    (preg\_match("@^/$regexRoute$@", $path)) { return
    $this->executeController( $route, $path, $info, $request ); } }
    $errorController = new ErrorController( $this->di, $request );
    return $errorController->notFound(); } private function
    executeController( string $route, string $path, array $info, Request
    $request ): string { $controllerName =
    '\\Bookstore\\Controllers\\\\' . $info['controller'] . 'Controller';
    $controller = new $controllerName($this->di, $request); if
    (isset($info['login']) && $info['login']) { if
    ($request->getCookies()->has('user')) { $customerId =
    $request->getCookies()->get('user');
    $controller->setCustomerId($customerId); } else { $errorController =
    new CustomerController( $this->di, $request ); return
    $errorController->login(); } } $params =
    $this->extractParams($route, $path); return call\_user\_func\_array(
    [$controller, $info['method']], $params ); }

There is one last place that you need to change. The login method of
CustomerController was instantiating a controller too, so we need to
inject the dependency injector there as well:

    $newController = new BookController($this->di, $this->request);

Summary
=======

In this chapter, you learned what MVC is, and how to write an
application that follows that pattern. You also know how to use a router
to route requests to controllers, Twig to write templates, and Composer
to manage your dependencies and autoloader. You were introduced to
dependency injection, and you even built your own implementation, even
though it is a very controversial topic with many different points of
view.

In the next chapter, we will go through one of the most important parts
needed when writing good code and good applications: unit testing your
code to get quick feedback from it.

Chapter 7. Testing Web Applications

We are pretty sure you have heard the term "bug" when speaking about
applications. Sentences such as "We found a bug in the application
that…" followed by some very undesirable behavior are more common than
you think. Writing code is not the only task of a developer; testing it
is crucial too. You should not release a version of your application
that has not been tested. However, could you imagine having to test your
entire application every time you change a line? It would be a
nightmare!

Well, we are not the first ones to have this issue, so, luckily enough,
developers have already found a pretty good solution to this problem. In
fact, they found more than one solution, turning testing into a very hot
topic of discussion. Even being a test developer has become quite a
common role. In this chapter, we will introduce you to one of the
approaches of testing your code: unit tests.

In this chapter, you will learn about:

-  How unit tests work

-  Configuring PHPUnit to test your code

-  Writing tests with assertions, data providers, and mocks

-  Good and bad practices when writing unit tests

The necessity for tests
=======================

When you work on a project, chances are that you are not the only
developer who will work with this code. Even in the case where you are
the only one who will ever change it, if you do this a few weeks after
creating it, you will probably not remember all the places that this
piece of code is affected. Okay, let's assume that you are the only
developer and your memory is beyond limits; would you be able to verify
that a change on a frequently used object, such as a request, will
always work as expected? More importantly, would you like to do it every
single time you make a tiny change?

Types of tests
--------------

While writing your application, making changes to the existing code, or
adding new features, it is very important to get good *feedback*. How do
you know that the feedback you get is good enough? It should accomplish
the AEIOU principles:

-  Automatic: Getting the feedback should be as painless as possible.
   Getting it by running just one command is always preferable to having
   to test your application manually.

-  Extensive: We should be able to cover as many use cases as possible,
   including edge cases that are difficult to foresee when writing code.

-  Immediate: You should get it as soon as possible. This means that the
   feedback that you get just after introducing a change is way better
   than the feedback that you get after your code is in production.

-  Open: The results should be transparent, and also, the tests should
   give us insight to other developers as to how to integrate or operate
   with the code.

-  Useful: It should answer questions such as "Will this change work?",
   "Will it break the application unexpectedly?", or "Is there any edge
   case that does not work properly?".

So, even though the concept is quite weird at the beginning, the best
way to test your code is… with more code. Exactly! We will write code
with the goal of testing the code of our application. Why? Well, it is
the best way we know to satisfy all the AEIU principles, and it has the
following advantages:

-  We can execute the tests by just running one command from our command
   line or even from our favorite IDE. There is no need to manually test
   your application via a browser continually.

-  We need to write the test just once. At the beginning, it may be a
   bit painful, but once the code is written, you will not need to
   repeat it again and again. This means that after some work, we will
   be able to test every single case effortlessly. If we had to test it
   manually, along with all the use cases and edge cases, it would be a
   nightmare.

-  You do not need to have the whole application working in order to
   know whether your code works. Imagine that you are writing your
   router: in order to know whether it works, you will have to wait
   until your application works in a browser. Instead, you can write
   your tests and run them as soon as you finish your class.

-  When writing your tests, you will be provided with feedback on what
   is failing. This is very useful to know when a specific function of
   the router does not work and the reason for the failure, which is
   better than getting a 500 error on our browser.

We hope that by now we have sold you on the idea that writing tests is
indispensable. This was the easy part, though. The problem is that we
know several different approaches. Do we write tests that test the
entire application or tests that test specific parts? Do we isolate the
tested area from the rest? Do we want to interact with the database or
with other external resources while testing? Depending on your answers,
you will decide on which type of tests you want to write. Let's discuss
the three main approaches that developers agree with:

-  Unit tests: These are tests that have a very focused scope. Their aim
   is to test a single class or method, isolating them from the rest of
   code. Take your Sale domain class as an example: it has some logic
   regarding the addition of books, right? A unit test might just
   instantiate a new sale, add books to the object, and verify that the
   array of books is valid. Unit tests are super fast due to their
   reduced scope, so you can have several different scenarios of the
   same functionality easily, covering all the edge cases you can
   imagine. They are also isolated, which means that we will not care
   too much about how all the pieces of our application are integrated.
   Instead, we will make sure that each piece works perfectly fine.

-  Integration tests: These are tests with a wider scope. Their aim is
   to verify that all the pieces of your application work together, so
   their scope is not limited to a class or function but rather includes
   a set of classes or the whole application. There is still some
   isolation in case we do not want to use a real database or depend on
   some other external web service. An example in our application would
   be to simulate a Request object, send it to the router, and verify
   that the response is as expected.

-  Acceptance tests: These are tests with an even wider scope. They try
   to test a whole functionality from the user's point of view. In web
   applications, this means that we can launch a browser and simulate
   the clicks that the user would make, asserting the response in the
   browser each time. And yes, all of this through code! These tests are
   slower to run, as you can imagine, because their scope is larger and
   working with a browser slows them down quite a lot too.

So, with all these types of tests, which one should you write? The
answer is all of them. The trick is to know when and how many of each
type you should write. One good approach is to write a lot of unit
tests, covering absolutely everything in your code, then writing fewer
integration tests to make sure that all the components of your
application work together, and finally writing acceptance tests but
testing only the main flows of your application. The following test
pyramid represents this idea:

|image29|

The reason is simple: your real feedback will come from your unit tests.
They will tell you if you messed up something with your changes as soon
as you finish writing them because executing unit tests is easy and
fast. Once you know that all your classes and functions behave as
expected, you need to verify that they can work together. However, for
this, you do not need to test all the edge cases again; you already did
this when writing unit tests. Here, you need to write just a few
integration tests that confirm that all the pieces communicate properly.
Finally, to make sure that not only that the code works but also the
user experience is the desired one, we will write acceptance tests that
emulate a user going through the different views. Here, tests are very
slow and only possible once the flow is complete, so the feedback comes
later. We will add acceptance tests to make sure that the main flows
work, but we do not need to test every single scenario as we already did
this with integration and unit tests.

Unit tests and code coverage
----------------------------

Now that you know what tests are, why we need them, and which types of
tests we have, we will focus the rest of the chapter on writing good
unit tests as they will be the ones that will occupy most of your time.

As we explained before, the idea of a unit test is to make sure that a
piece of code, usually a class or method, works as expected. As the
amount of code that a method contains should be small, running the test
should take almost no time. Taking advantage of this, we will run
several tests, trying to cover as many use cases as possible.

If this is not the first time you've heard about unit tests, you might
know the concept of code coverage. This concept refers to the amount of
code that our tests execute, that is, the percentage of tested code. For
example, if your application has 10,000 lines and your tests test a
total of 7,500 lines, your code coverage is 75%. There are tools that
show marks on your code to indicate whether a certain line is tested or
not, which is very useful in order to identify which parts of your
application are not tested and thus warn you that it is more dangerous
to change them.

However, code coverage is a double-edge sword. Why is this so? This is
because developers tend to get obsessed with code coverage, aiming for a
100% coverage. However, you should be aware that code coverage is just a
consequence, not your goal. Your goal is to write unit tests that verify
all the use cases of certain pieces of code in order to make you feel
safer each time that you have to change this code. This means that for a
given method, it might not be enough to write one test because the same
line with different input values may behave differently. However, if
your focus was on code coverage, writing one test would satisfy it, and
you might not need to write any more tests.

Integrating PHPUnit
===================

Writing tests is a task that you could do by yourself; you just need to
write code that throws exceptions when conditions are not met and then
run the script any time you need. Luckily, other developers were not
satisfied with this manual process, so they implemented tools to help us
automate this process and get good feedback. The most used in PHP is
PHPUnit. PHPUnit is a framework that provides a set of tools to write
tests in an easier manner, gives us the ability to run tests
automatically, and delivers useful feedback to the developer.

In order to use PHPUnit, traditionally, we installed it on our laptop.
In doing so, we added the classes of the framework to include the path
of PHP and also the executable to run the tests. This was less than
ideal as we forced developers to install one more tool on their
development machine. Nowadays, Composer (refer to `*Chapter
6* <#Top_of_ch06_html>`__, *Adapting to MVC*, in order to refresh your
memory) helps us in including PHPUnit as a dependency of the project.
This means that running Composer, which you will do for sure in order to
get the rest of the dependencies, will get PHPUnit too. Add, then, the
following into composer.json:

    { //... "require": { "monolog/monolog": "^1.17", "twig/twig":
    "^1.23" }, "require-dev": { "phpunit/phpunit": "5.1.3" },
    "autoload": { "psr-4": { "Bookstore\\\\": "src" } } }

Note that this dependency is added as require-dev. This means that the
dependency will be downloaded only when we are on a development
environment, but it will not be part of the application that we will
deploy on production as we do not need to run tests there. To get the
dependency, as always, run composer update.

A different approach is to install PHPUnit globally so that all the
projects on your development environment can use it instead of
installing it locally each time. You can read about how to install tools
globally with Composer at
`*https://akrabat.com/global-installation-of-php-tools-with-composer/* <https://akrabat.com/global-installation-of-php-tools-with-composer/>`__.

The phpunit.xml file
--------------------

PHPUnit needs a phpunit.xml file in order to define the way we want to
run the tests. This file defines a set of rules like where the tests
are, what code are the tests testing, and so on. Add the following file
in your root directory:

    <?xml version="1.0" encoding="UTF-8"?> <phpunit
    backupGlobals="false" backupStaticAttributes="false" colors="true"
    convertErrorsToExceptions="true" convertNoticesToExceptions="true"
    convertWarningsToExceptions="true" processIsolation="false"
    stopOnFailure="false" syntaxCheck="false"
    bootstrap="vendor/autoload.php" > <testsuites> <testsuite
    name="Bookstore Test Suite"> <directory>./tests/</directory>
    </testsuite> </testsuites> <filter> <whitelist>
    <directory>./src</directory> </whitelist> </filter> </phpunit>

This file defines quite a lot of things. The most important are
explained as follows:

-  Setting convertErrorsToExceptions, convertNoticesToExceptions, and
   convertWarningsToExceptions to true will make your tests fail if
   there is a PHP error, warning, or notice. The goal is to make sure
   that your code does not contain minor errors on edge cases, which are
   always the source of potential problems.

-  The stopOnFailure tells PHPUnit whether it should continue executing
   the rest of tests or not when there is a failed test. In this case,
   we want to run all of them to know how many tests are failing and
   why.

-  The bootstrap defines which file we should execute before starting to
   run the tests. The most common usage is to include the autoloader,
   but you could also include a file that initializes some dependencies,
   such as databases or configuration readers.

-  The testsuites defines the directories where PHPUnit will look for
   tests. In our case, we defined ./tests, but we could add more if we
   had them in different directories.

-  The whitelist defines the list of directories that contain the code
   that we are testing. This can be useful to generate output related to
   the code coverage.

When running the tests with PHPUnit, just make sure that you run the
command from the same directory where the phpunit.xml file is. We will
show you how in the next section.

Your first test
---------------

Right, that's enough preparations and theory; let's write some code. We
will write tests for the basic customer, which is a domain object with
little logic. First of all, we need to refactor the Unique trait as it
still contains some unnecessary code after integrating our application
with MySQL. We are talking about the ability to assign the next
available ID, which is now handled by the autoincremental field. Remove
it, leaving the code as follows:

    <?php namespace Bookstore\\Utils; trait Unique { protected $id;
    public function setId(int $id) { $this->id = $id; } public function
    getId(): int { return $this->id; } }

The tests will be inside the tests/ directory. The structure of
directories should be the same as in the src/ directory so that it is
easier to identify where each test should be. The file and the class
names need to end with Test so that PHPUnit knows that a file contains
tests. Knowing this, our test should be in
tests/Domain/Customer/BasicTest.php, as follows:

    <?php namespace Bookstore\\Tests\\Domain\\Customer; use
    Bookstore\\Domain\\Customer\\Basic; use
    PHPUnit\_Framework\_TestCase; class BasicTest extends
    PHPUnit\_Framework\_TestCase { public function testAmountToBorrow()
    { $customer = new Basic(1, 'han', 'solo', 'han@solo.com');
    $this->assertSame( 3, $customer->getAmountToBorrow(), 'Basic
    customer should borrow up to 3 books.' ); } }

As you can note, the BasicTest class extends from
PHPUnit\_Framework\_TestCase. All test classes have to extend from this
class. This class comes with a set of methods that allow you to make
assertions. An assertion in PHPUnit is just a check performed on a
value. Assertions can be comparisons to other values, a verification of
some attributes of the values, and so on. If an assertion is not true,
the test will be marked as failed, outputting the proper error message
to the developer. The example shows an assertion using the assertSame
method, which will compare two values, expecting that both of them are
exactly the same. The third argument is an error message that the
assertion will show in case it fails.

Also, note that the function names that start with test are the ones
executed with PHPUnit. In this example, we have one unique test named
testAmountToBorrow that instantiates a basic customer and verifies that
the amount of books that the customer can borrow is 3. In the next
section, we will show you how to run this test and get feedback from it.

Optionally, you could use any function name if you add the @test
annotation in the method's DocBlock, as follows:

    /\*\* \* @test \*/ public function thisIsATestToo() { //... }

Running tests
-------------

In order to run the tests you wrote, you need to execute the script that
Composer generated in vendor/bin. Remember always to run from the root
directory of the project so that PHPUnit can find your phpunit.xml
configuration file. Then, type ./vendor/bin/phpunit.

|image30|

When executing this program, we will get the feedback given by the
tests. The output shows us that there is one test (one method) and one
assertion and whether these were satisfactory. This output is what you
would like to see every time you run your tests, but you will get more
failed tests than you would like. Let's take a look at them by adding
the following test:

    public function testFail() { $customer = new Basic(1, 'han', 'solo',
    'han@solo.com'); $this->assertSame( 4,
    $customer->getAmountToBorrow(), 'Basic customer should borrow up to
    3 books.' ); }

This test will fail as we are checking whether getAmountToBorrow returns
4, but you know that it always returns 3. Let's run the tests and take a
look at what kind of output we get.

|image31|

We can quickly note that the output is not good due to the red color. It
shows us that there is a failure, pointing to the class and test method
that failed. The feedback points out the type of failure (as 3 is not
identical to 4) and optionally, the error message we added when invoking
the assert method.

Writing unit tests
==================

Let's start digging into all the features that PHPUnit offers us in
order to write tests. We will divide these features in different
subsections: setting up a test, assertions, exceptions, and data
providers. Of course, you do not need to use all of these tools each
time you write a test.

The start and end of a test
---------------------------

PHPUnit gives you the opportunity to set up a common scenario for each
test in a class. For this, you need to use the setUp method, which, if
present, is executed each time that a test of this class is executed.
The instance of the class that invokes the setUp and test methods is the
same, so you can use the properties of the class to save the context.
One common use would be to create the object that we will use for our
tests in case this is always the same. For an example, write the
following code in tests/Domain/Customer/BasicTest.php:

    <?php namespace Bookstore\\Tests\\Domain\\Customer; use
    Bookstore\\Domain\\Customer\\Basic; use
    PHPUnit\_Framework\_TestCase; class BasicTest extends
    PHPUnit\_Framework\_TestCase { private $customer; public function
    setUp() { $this->customer = new Basic( 1, 'han', 'solo',
    'han@solo.com' ); } public function testAmountToBorrow() {
    $this->assertSame( 3, $this->customer->getAmountToBorrow(), 'Basic
    customer should borrow up to 3 books.' ); } }

When testAmountToBorrow is invoked, the $customer property is already
initialized through the execution of the setUp method. If the class had
more than one test, the setUp method would be executed each time.

Even though it is less common to use, there is another method used to
clean up the scenario after the test is executed: tearDown. This works
in the same way, but it is executed after each test of this class is
executed. Possible uses would be to clean up database data, close
connections, delete files, and so on.

Assertions
----------

You have already been introduced to the concept of assertions, so let's
just list the most common ones in this section. For the full list, we
recommend you to visit the official documentation at
`*https://phpunit.de/manual/current/en/appendixes.assertions.html* <https://phpunit.de/manual/current/en/appendixes.assertions.html>`__
as it is quite extensive; however, to be honest, you will probably not
use many of them.

The first type of assertion that we will see is the Boolean assertion,
that is, the one that checks whether a value is true or false. The
methods are as simple as assertTrue and assertFalse, and they expect one
parameter, which is the value to assert, and optionally, a text to
display in case of failure. In the same BasicTest class, add the
following test:

    public function testIsExemptOfTaxes() { $this->assertFalse(
    $this->customer->isExemptOfTaxes(), 'Basic customer should be exempt
    of taxes.' ); }

This test makes sure that a basic customer is never exempt of taxes.
Note that we could do the same assertion by writing the following:

    $this->assertSame( $this->customer->isExemptOfTaxes(), false, 'Basic
    customer should be exempt of taxes.' );

A second group of assertions would be the comparison assertions. The
most famous ones are assertSame and assertEquals. You have already used
the first one, but are you sure of its meaning? Let's add another test
and run it:

    public function testGetMonthlyFee() { $this->assertSame( 5,
    $this->customer->getMonthlyFee(), 'Basic customer should pay 5 a
    month.' ); }

The result of the test is shown in the following screenshot:

|image32|

The test failed! The reason is that assertSame is the equivalent to
comparing using identity, that is, without using type juggling. The
result of the getMonthlyFee method is always a float, and we will
compare it with an integer, so it will never be the same, as the error
message tells us. Change the assertion to assertEquals, which compares
using equality, and the test will pass now.

When working with objects, we can use an assertion to check whether a
given object is an instance of the expected class or not. When doing so,
remember to send the full name of the class as this is a quite common
mistake. Even better, you could get the class name using ::class, for
example, Basic::class. Add the following test in
tests/Domain/Customer/CustomerFactoryTest.php:

    <?php namespace Bookstore\\Tests\\Domain\\Customer; use
    Bookstore\\Domain\\Customer\\CustomerFactory; use
    PHPUnit\_Framework\_TestCase; class CustomerFactoryTest extends
    PHPUnit\_Framework\_TestCase { public function testFactoryBasic() {
    $customer = CustomerFactory::factory( 'basic', 1, 'han', 'solo',
    'han@solo.com' ); $this->assertInstanceOf( Basic::class, $customer,
    'basic should create a Customer\\Basic object.' ); } }

This test creates a customer using the customer factory. As the type of
customer was basic, the result should be an instance of Basic, which is
what we are testing with assertInstanceOf. The first argument is the
expected class, the second is the object that we are testing, and the
third is the error message. This test also helps us to note the behavior
of comparison assertions with objects. Let's create a basic customer
object as expected and compare it with the result of the factory. Then,
run the test, as follows:

    $expectedBasicCustomer = new Basic(1, 'han', 'solo',
    'han@solo.com'); $this->assertSame( $customer,
    $expectedBasicCustomer, 'Customer object is not as expected.' );

The result of this test is shown in the following screenshot:

|image33|

The test failed because when you compare two objects with identity
comparison, you comparing the object reference, and it will only be the
same if the two objects are exactly the same instance. If you create two
objects with the same properties, they will be equal but never
identical. To fix the test, change the assertion as follows:

    $expectedBasicCustomer = new Basic(1, 'han', 'solo',
    'han@solo.com'); $this->assertEquals( $customer,
    $expectedBasicCustomer, 'Customer object is not as expected.' );

Let's now write the tests for the sale domain object at
tests/Domain/SaleTest.php. This class is very easy to test and allows us
to use some new assertions, as follows:

    <?php namespace Bookstore\\Tests\\Domain\\Customer; use
    Bookstore\\Domain\\Sale; use PHPUnit\_Framework\_TestCase; class
    SaleTest extends PHPUnit\_Framework\_TestCase { public function
    testNewSaleHasNoBooks() { $sale = new Sale(); $this->assertEmpty(
    $sale->getBooks(), 'When new, sale should have no books.' ); }
    public function testAddNewBook() { $sale = new Sale();
    $sale->addBook(123); $this->assertCount( 1, $sale->getBooks(),
    'Number of books not valid.' ); $this->assertArrayHasKey( 123,
    $sale->getBooks(), 'Book id could not be found in array.' );
    $this->assertSame( $sale->getBooks()[123], 1, 'When not specified,
    amount of books is 1.' ); } }

We added two tests here: one makes sure that for a new sale instance,
the list of books associated with it is empty. For this, we used the
assertEmpty method, which takes an array as an argument and will assert
that it is empty. The second test is adding a book to the sale and then
making sure that the list of books has the correct content. For this, we
will use the assertCount method, which verifies that the array, that is,
the second argument, has as many elements as the first argument
provided. In this case, we expect that the list of books has only one
entry. The second assertion of this test is verifying that the array of
books contains a specific key, which is the ID of the book, with the
assertArrayHasKey method, in which the first argument is the key, and
the second one is the array. Finally, we will check with the already
known assertSame method that the amount of books inserted is 1.

Even though these two new assertion methods are useful sometimes, all
the three assertions of the last test can be replaced by just an
assertSame method, comparing the whole array of books with the expected
one, as follows:

    $this->assertSame( [123 => 1], $sale->getBooks(), 'Books array does
    not match.' );

The suite of tests for the sale domain object would not be enough if we
were not testing how the class behaves when adding multiple books. In
this case, using assertCount and assertArrayHasKey would make the test
unnecessarily long, so let's just compare the array with an expected one
via the following code:

    public function testAddMultipleBooks() { $sale = new Sale();
    $sale->addBook(123, 4); $sale->addBook(456, 2); $sale->addBook(456,
    8); $this->assertSame( [123 => 4, 456 => 10], $sale->getBooks(),
    'Books are not as expected.' ); }

Expecting exceptions
--------------------

Sometimes, a method is expected to throw an exception for certain
unexpected use cases. When this happens, you could try to capture this
exception inside the test or take advantage of another tool that PHPUnit
offers: expecting exceptions. To mark a test to expect a given
exception, just add the @expectedException annotation followed by the
exception's class full name. Optionally, you can use
@expectedExceptionMessage to assert the message of the exception. Let's
add the following tests to our CustomerFactoryTest class:

    /\*\* \* @expectedException \\InvalidArgumentException \*
    @expectedExceptionMessage Wrong type. \*/ public function
    testCreatingWrongTypeOfCustomer() { $customer =
    CustomerFactory::factory( 'deluxe', 1, 'han', 'solo', 'han@solo.com'
    ); }

In this test we will try to create a deluxe customer with our factory,
but as this type of customer does not exist, we will get an exception.
The type of the expected exception is InvalidArgumentException, and the
error message is "Wrong type". If you run the tests, you will see that
they pass.

If we defined an expected exception and the exception is never thrown,
the test will fail; expecting exceptions is just another type of
assertion. To see this happen, add the following to your test and run
it; you will get a failure, and PHPUnit will complain saying that it
expected the exception, but it was never thrown:

    /\*\* \* @expectedException \\InvalidArgumentException \*/ public
    function testCreatingCorrectCustomer() { $customer =
    CustomerFactory::factory( 'basic', 1, 'han', 'solo', 'han@solo.com'
    ); }

Data providers
--------------

If you think about the flow of a test, most of the time, we invoke a
method with an input and expect an output. In order to cover all the
edge cases, it is natural that we will repeat the same action with a set
of inputs and expected outputs. PHPUnit gives us the ability to do so,
thus removing a lot of duplicated code. This feature is called data
providing.

A data provider is a public method defined in the test class that
returns an array with a specific schema. Each entry of the array
represents a test in which the key is the name of the test—optionally,
you could use numeric keys—and the value is the parameter that the test
needs. A test will declare that it needs a data provider with the
@dataProvider annotation, and when executing tests, the data provider
injects the arguments that the test method needs. Let's consider an
example to make it easier. Write the following two methods in your
CustomerFactoryTest class:

    public function providerFactoryValidCustomerTypes() { return [
    'Basic customer, lowercase' => [ 'type' => 'basic', 'expectedType'
    => '\\Bookstore\\Domain\\Customer\\Basic' ], 'Basic customer,
    uppercase' => [ 'type' => 'BASIC', 'expectedType' =>
    '\\Bookstore\\Domain\\Customer\\Basic' ], 'Premium customer,
    lowercase' => [ 'type' => 'premium', 'expectedType' =>
    '\\Bookstore\\Domain\\Customer\\Premium' ], 'Premium customer,
    uppercase' => [ 'type' => 'PREMIUM', 'expectedType' =>
    '\\Bookstore\\Domain\\Customer\\Premium' ] ]; } /\*\* \*
    @dataProvider providerFactoryValidCustomerTypes \* @param string
    $type \* @param string $expectedType \*/ public function
    testFactoryValidCustomerTypes( string $type, string $expectedType )
    { $customer = CustomerFactory::factory( $type, 1, 'han', 'solo',
    'han@solo.com' ); $this->assertInstanceOf( $expectedType, $customer,
    'Factory created the wrong type of customer.' ); }

The test here is testFactoryValidCustomerTypes, which expects two
arguments: $type and $expectedType. The test uses them to create a
customer with the factory and verify the type of the result, which we
already did by hardcoding the types. The test also declares that it
needs the providerFactoryValidCustomerTypes data provider. This data
provider returns an array of four entries, which means that the test
will be executed four times with four different sets of arguments. The
name of each test is the key of each entry—for example, "Basic customer,
lowercase". This is very useful in case a test fails because it will be
displayed as part of the error messages. Each entry is a map with two
values, type and expectedType, which are the names of the arguments of
the test method. The values of these entries are the values that the
test method will get.

The bottom line is that the code we wrote would be the same as if we
wrote testFactoryValidCustomerTypes four times, hardcoding $type and
$expectedType each time. Imagine now that the test method contains tens
of lines of code or we want to repeat the same test with tens of
datasets; do you see how powerful it is?

Testing with doubles
====================

So far, we tested classes that are quite isolated; that is, they do not
have much interaction with other classes. Nevertheless, we have classes
that use several classes, such as controllers. What can we do with these
interactions? The idea of unit tests is to test a specific method and
not the whole code base, right?

PHPUnit allows you to mock these dependencies; that is, you can provide
fake objects that look similar to the dependencies that the tested class
needs, but they do not use code from those classes. The goal of this is
to provide a dummy instance that the class can use and invoke its
methods without the side effect of what these invocations might have.
Imagine as an example the case of the models: if the controller uses a
real model, then when invoking methods from it, the model would access
the database each time, making the tests quite unpredictable.

If we use a mock as the model instead, the controller can invoke its
methods as many times as needed without any side effect. Even better, we
can make assertions of the arguments that the mock received or force it
to return specific values. Let's take a look at how to use them.

Injecting models with DI
------------------------

The first thing we need to understand is that if we create objects using
new inside the controller, we will not be able to mock them. This means
that we need to inject all the dependencies—for example, using a
dependency injector. We will do this for all of the dependencies but
one: the models. In this section, we will test the borrow method of the
BookController class, so we will show the changes that this method
needs. Of course, if you want to test the rest of the code, you should
apply these same changes to the rest of the controllers.

The first thing to do is to add the BookModel instance to the dependency
injector in our index.php file. As this class also has a dependency,
PDO, use the same dependency injector to get an instance of it, as
follows:

    $di->set('BookModel', new BookModel($di->get('PDO')));

Now, in the borrow method of the BookController class, we will change
the new instantiation of the model to the following:

    public function borrow(int $bookId): string { $bookModel =
    $this->di->get('BookModel'); try { //...

Customizing TestCase
--------------------

When writing your unit test's suite, it is quite common to have a
customized TestCase class from which all tests extend. This class always
extends from PHPUnit\_Framework\_TestCase, so we still get all the
assertions and other methods. As all tests have to import this class,
let's change our autoloader so that it can recognize namespaces from the
tests directory. After this, run composer update, as follows:

    "autoload": { "psr-4": { "Bookstore\\\\Tests\\\\": "tests",
    "Bookstore\\\\": "src" } }

With this change, we will tell Composer that all the namespaces starting
with Bookstore\\Tests will be located under the tests directory, and the
rest will follow the previous rules.

Let's add now our customized TestCase class. The only helper method we
need right now is one to create mocks. It is not really necessary, but
it makes things cleaner. Add the following class in
tests/AbstractTestClase.php:

    <?php namespace Bookstore\\Tests; use PHPUnit\_Framework\_TestCase;
    use InvalidArgumentException; abstract class AbstractTestCase
    extends PHPUnit\_Framework\_TestCase { protected function
    mock(string $className) { if (strpos($className, '\\\\') !== 0) {
    $className = '\\\\' . $className; } if (!class\_exists($className))
    { $className = '\\Bookstore\\\\' . trim($className, '\\\\'); if
    (!class\_exists($className)) { throw new InvalidArgumentException(
    "Class $className not found." ); } } return
    $this->getMockBuilder($className) ->disableOriginalConstructor()
    ->getMock(); } }

This method takes the name of a class and tries to figure out whether
the class is part of the Bookstore namespace or not. This will be handy
when mocking objects of our own codebase as we will not have to write
Bookstore each time. After figuring out what the real full class name
is, it uses the mock builder from PHPUnit to create one and then returns
it.

More helpers! This time, they are for controllers. Every single
controller will always need the same dependencies: logger, database
connection, template engine, and configuration reader. Knowing this,
let's create a ControllerTestCase class from where all the tests
covering controllers will extend. This class will contain a setUp method
that creates all the common mocks and sets them in the dependency
injector. Add it as your tests/ControllerTestCase.php file, as follows:

    <?php namespace Bookstore\\Tests; use
    Bookstore\\Utils\\DependencyInjector; use Bookstore\\Core\\Config;
    use Monolog\\Logger; use Twig\_Environment; use PDO; abstract class
    ControllerTestCase extends AbstractTestCase { protected $di; public
    function setUp() { $this->di = new DependencyInjector();
    $this->di->set('PDO', $this->mock(PDO::class));
    $this->di->set('Utils\\Config', $this->mock(Config::class));
    $this->di->set( 'Twig\_Environment',
    $this->mock(Twig\_Environment::class) ); $this->di->set('Logger',
    $this->mock(Logger::class)); } }

Using mocks
-----------

Well, we've had enough of the helpers; let's start with the tests. The
difficult part here is how to play with mocks. When you create one, you
can add some expectations and return values. The methods are:

-  expects: This specifies the amount of times the mock's method is
   invoked. You can send $this->never(), $this->once(), or $this->any()
   as an argument to specify 0, 1, or any invocations.

-  method: This is used to specify the method we are talking about. The
   argument that it expects is just the name of the method.

-  with: This is a method used to set the expectations of the arguments
   that the mock will receive when it is invoked. For example, if the
   mocked method is expected to get basic as the first argument and 123
   as the second, the with method will be invoked as with("basic", 123).
   This method is optional, but if we set it, PHPUnit will throw an
   error in case the mocked method does not get the expected arguments,
   so it works as an assertion.

-  will: This is used to define what the mock will return. The two most
   common usages are $this->returnValue($value) or
   $this->throwException($exception). This method is also optional, and
   if not invoked, the mock will always return null.

Let's add the first test to see how it would work. Add the following
code to the tests/Controllers/BookControllerTest.php file:

    <?php namespace Bookstore\\Tests\\Controllers; use
    Bookstore\\Controllers\\BookController; use
    Bookstore\\Core\\Request; use
    Bookstore\\Exceptions\\NotFoundException; use
    Bookstore\\Models\\BookModel; use
    Bookstore\\Tests\\ControllerTestCase; use Twig\_Template; class
    BookControllerTest extends ControllerTestCase { private function
    getController( Request $request = null ): BookController { if
    ($request === null) { $request = $this->mock('Core\\Request'); }
    return new BookController($this->di, $request); } public function
    testBookNotFound() { $bookModel = $this->mock(BookModel::class);
    $bookModel ->expects($this->once()) ->method('get') ->with(123)
    ->will( $this->throwException( new NotFoundException() ) );
    $this->di->set('BookModel', $bookModel); $response = "Rendered
    template"; $template = $this->mock(Twig\_Template::class); $template
    ->expects($this->once()) ->method('render') ->with(['errorMessage'
    => 'Book not found.']) ->will($this->returnValue($response));
    $this->di->get('Twig\_Environment') ->expects($this->once())
    ->method('loadTemplate') ->with('error.twig')
    ->will($this->returnValue($template)); $result =
    $this->getController()->borrow(123); $this->assertSame( $result,
    $response, 'Response object is not the expected one.' ); } }

The first thing the test does is to create a mock of the BookModel
class. Then, it adds an expectation that goes like this: the get method
will be called once with one argument, 123, and it will throw
NotFoundException. This makes sense as the test tries to emulate a
scenario in which we cannot find the book in the database.

The second part of the test consists of adding the expectations of the
template engine. This is a bit more complex as there are two mocks
involved. The loadTemplate method of Twig\_Environment is expected to be
called once with the error.twig argument as the template name. This mock
should return Twig\_Template, which is another mock. The render method
of this second mock is expected to be called once with the correct error
message, returning the response, which is a hardcoded string. After all
the dependencies are defined, we just need to invoke the borrow method
of the controller and expect a response.

Remember that this test does not have only one assertion, but four: the
assertSame method and the three mock expectations. If any of them are
not accomplished, the test will fail, so we can say that this method is
quite robust.

With our first test, we verified that the scenario in which the book is
not found works. There are two more scenarios that fail as well: when
there are not enough copies of the book to borrow and when there is a
database error when trying to save the borrowed book. However, you can
see now that all of them share a piece of code that mocks the template.
Let's extract this code to a protected method that generates the mocks
when it is given the template name, the parameters are sent to the
template, and the expected response is received. Run the following:

    protected function mockTemplate( string $templateName, array
    $params, $response ) { $template =
    $this->mock(Twig\_Template::class); $template
    ->expects($this->once()) ->method('render') ->with($params)
    ->will($this->returnValue($response));
    $this->di->get('Twig\_Environment') ->expects($this->once())
    ->method('loadTemplate') ->with($templateName)
    ->will($this->returnValue($template)); } public function
    testNotEnoughCopies() { $bookModel = $this->mock(BookModel::class);
    $bookModel ->expects($this->once()) ->method('get') ->with(123)
    ->will($this->returnValue(new Book())); $bookModel
    ->expects($this->never()) ->method('borrow');
    $this->di->set('BookModel', $bookModel); $response = "Rendered
    template"; $this->mockTemplate( 'error.twig', ['errorMessage' =>
    'There are no copies left.'], $response ); $result =
    $this->getController()->borrow(123); $this->assertSame( $result,
    $response, 'Response object is not the expected one.' ); } public
    function testErrorSaving() { $controller = $this->getController();
    $controller->setCustomerId(9); $book = new Book(); $book->addCopy();
    $bookModel = $this->mock(BookModel::class); $bookModel
    ->expects($this->once()) ->method('get') ->with(123)
    ->will($this->returnValue($book)); $bookModel
    ->expects($this->once()) ->method('borrow') ->with(new Book(), 9)
    ->will($this->throwException(new DbException()));
    $this->di->set('BookModel', $bookModel); $response = "Rendered
    template"; $this->mockTemplate( 'error.twig', ['errorMessage' =>
    'Error borrowing book.'], $response ); $result =
    $controller->borrow(123); $this->assertSame( $result, $response,
    'Response object is not the expected one.' ); }

The only novelty here is when we expect that the borrow method is never
invoked. As we do not expect it to be invoked, there is no reason to use
the with nor will method. If the code actually invokes this method,
PHPUnit will mark the test as failed.

We already tested and found that all the scenarios that can fail have
failed. Let's add a test now where a user can successfully borrow a
book, which means that we will return valid books and customers from the
database, the save method will be invoked correctly, and the template
will get all the correct parameters. The test looks as follows:

    public function testBorrowingBook() { $controller =
    $this->getController(); $controller->setCustomerId(9); $book = new
    Book(); $book->addCopy(); $bookModel =
    $this->mock(BookModel::class); $bookModel ->expects($this->once())
    ->method('get') ->with(123) ->will($this->returnValue($book));
    $bookModel ->expects($this->once()) ->method('borrow') ->with(new
    Book(), 9); $bookModel ->expects($this->once())
    ->method('getByUser') ->with(9) ->will($this->returnValue(['book1',
    'book2'])); $this->di->set('BookModel', $bookModel); $response =
    "Rendered template"; $this->mockTemplate( 'books.twig', [ 'books' =>
    ['book1', 'book2'], 'currentPage' => 1, 'lastPage' => true ],
    $response ); $result = $controller->borrow(123); $this->assertSame(
    $result, $response, 'Response object is not the expected one.' ); }

So this is it. You have written one of the most complex tests you will
need to write during this book. What do you think of it? Well, as you do
not have much experience with tests, you might be quite satisfied with
the result, but let's try to analyze it a bit further.

Database testing
================

This will be the most controversial of the sections of this chapter by
far. When it comes to database testing, there are different schools of
thought. Should we use the database or not? Should we use our
development database or one in memory? It is quite out of the scope of
the book to explain how to mock the database or prepare a fresh one for
each test, but we will try to summarize some of the techniques here:

-  We will mock the database connection and write expectations to all
   the interactions between the model and the database. In our case,
   this would mean that we would inject a mock of the PDO object. As we
   will write the queries manually, chances are that we might introduce
   a wrong query. Mocking the connection would not help us detect this
   error. This solution would be good if we used ORM instead of writing
   the queries manually, but we will leave this topic out of the book.

-  For each test, we will create a brand new database in which we add
   the data we would like to have for the specific test. This approach
   might take a lot of time, but it assures you that you will be testing
   against a real database and that there is no unexpected data that
   might make our tests fail; that is, the tests are fully isolated. In
   most of the cases, this would be the preferable approach, even though
   it might not be the one that performs faster. To solve this
   inconvenience, we will create in-memory databases.

-  Tests run against an already existing database. Usually, at the
   beginning of the test we start a transaction that we roll back at the
   end of the test, leaving the database without any change. This
   approach emulates a real scenario, in which we can find all sorts of
   data and our code should always behave as expected. However, using a
   shared database always has some side effects; for example, if we want
   to introduce changes to the database schema, we will have to apply
   them to the database before running the tests, but the rest of the
   applications or developers that use the database are not yet ready
   for these changes.

In order to keep things small, we will try to implement a mixture of the
second and third options. We will use our existing database, but after
starting the transaction of each test, we will clean all the tables
involved with the test. This looks as though we need a ModelTestCase to
handle this. Add the following into tests/ModelTestCase.php:

    <?php namespace Bookstore\\Tests; use Bookstore\\Core\\Config; use
    PDO; abstract class ModelTestCase extends AbstractTestCase {
    protected $db; protected $tables = []; public function setUp() {
    $config = new Config(); $dbConfig = $config->get('db'); $this->db =
    new PDO( 'mysql:host=127.0.0.1;dbname=bookstore', $dbConfig['user'],
    $dbConfig['password'] ); $this->db->beginTransaction();
    $this->cleanAllTables(); } public function tearDown() {
    $this->db->rollBack(); } protected function cleanAllTables() {
    foreach ($this->tables as $table) { $this->db->exec("delete from
    $table"); } } }

The setUp method creates a database connection with the same credentials
found in the config/app.yml file. Then, we will start a transaction and
invoke the cleanAllTables method, which iterates the tables in the
$tables property and deletes all the content from them. The tearDown
method rolls back the transaction.

Note

Extending from ModelTestCase

If you write a test extending from this class that needs to implement
either the setUp or tearDown method, always remember to invoke the ones
from the parent.

Let's write tests for the borrow method of the BookModel class. This
method uses books and customers, so we would like to clean the tables
that contain them. Create the test class and save it in
tests/Models/BookModelTest.php:

    <?php namespace Bookstore\\Tests\\Models; use
    Bookstore\\Models\\BookModel; use Bookstore\\Tests\\ModelTestCase;
    class BookModelTest extends ModelTestCase { protected $tables = [
    'borrowed\_books', 'customer', 'book' ]; protected $model; public
    function setUp() { parent::setUp(); $this->model = new
    BookModel($this->db); } }

Note how we also overrode the setUp method, invoking the one in the
parent and creating the model instance that all tests will use, which is
safe to do as we will not keep any context on this object. Before adding
the tests though, let's add some more helpers to ModelTestCase: one to
create book objects given an array of parameters and two to save books
and customers in the database. Run the following code:

    protected function buildBook(array $properties): Book { $book = new
    Book(); $reflectionClass = new ReflectionClass(Book::class); foreach
    ($properties as $key => $value) { $property =
    $reflectionClass->getProperty($key); $property->setAccessible(true);
    $property->setValue($book, $value); } return $book; } protected
    function addBook(array $params) { $default = [ 'id' => null, 'isbn'
    => 'isbn', 'title' => 'title', 'author' => 'author', 'stock' => 1,
    'price' => 10.0, ]; $params = array\_merge($default, $params);
    $query = <<<SQL insert into book (id, isbn, title, author, stock,
    price) values(:id, :isbn, :title, :author, :stock, :price) SQL;
    $this->db->prepare($query)->execute($params); } protected function
    addCustomer(array $params) { $default = [ 'id' => null, 'firstname'
    => 'firstname', 'surname' => 'surname', 'email' => 'email', 'type'
    => 'basic' ]; $params = array\_merge($default, $params); $query =
    <<<SQL insert into customer (id, firstname, surname, email, type)
    values(:id, :firstname, :surname, :email, :type) SQL;
    $this->db->prepare($query)->execute($params); }

As you can note, we added default values for all the fields, so we are
not forced to define the whole book/customer each time we want to save
one. Instead, we just sent the relevant fields and merged them to the
default ones.

Also, note that the buildBook method used a new concept, reflection, to
access the private properties of an instance. This is way beyond the
scope of the book, but if you are interested, you can read more at
`*http://php.net/manual/en/book.reflection.php* <http://php.net/manual/en/book.reflection.php>`__.

We are now ready to start writing tests. With all these helpers, adding
tests will be very easy and clean. The borrow method has different use
cases: trying to borrow a book that is not in the database, trying to
use a customer not registered, and borrowing a book successfully. Let's
add them as follows:

    /\*\* \* @expectedException \\Bookstore\\Exceptions\\DbException \*/
    public function testBorrowBookNotFound() { $book =
    $this->buildBook(['id' => 123]); $this->model->borrow($book, 123); }
    /\*\* \* @expectedException \\Bookstore\\Exceptions\\DbException \*/
    public function testBorrowCustomerNotFound() { $book =
    $this->buildBook(['id' => 123]); $this->addBook(['id' => 123]);
    $this->model->borrow($book, 123); } public function testBorrow() {
    $book = $this->buildBook(['id' => 123, 'stock' => 12]);
    $this->addBook(['id' => 123, 'stock' => 12]);
    $this->addCustomer(['id' => 123]); $this->model->borrow($book, 123);
    }

Impressed? Compared to the controller tests, these tests are way
simpler, mainly because their code performs only one action, but also
thanks to all the methods added to ModelTestCase. Once you need to work
with other objects, such as sales, you can add addSale or buildSale to
this same class to make things cleaner.

Test-driven development
=======================

You might realize already that there is no unique way to do things when
talking about developing an application. It is out of the scope of this
book to show you all of them—and by the time you are done reading these
lines, more techniques will have been incorporated already—but there is
one approach that is very useful when it comes to writing good, testable
code: test-driven development (TDD).

This methodology consists of writing the unit tests before writing the
code itself. The idea, though, is not to write all the tests at once and
then write the class or method but rather to do it in a progressive way.
Let's consider an example to make it easier. Imagine that your Sale
class is yet to be implemented and the only thing we know is that we
have to be able to add books. Rename your src/Domain/Sale.php file to
src/Domain/Sale2.php or just delete it so that the application does not
know about it.

Note

Is all this verbosity necessary?

You will note in this example that we will perform an excessive amount
of steps to come up with a very simple piece of code. Indeed, they are
too many for this example, but there will be times when this amount is
just fine. Finding these moments comes with experience, so we recommend
you to practice first with simple examples. Eventually, it will come
naturally to you.

The mechanics of TDD consist of four steps, as follows:

1. Write a test for some functionality that is not yet implemented.

2. Run the unit tests, and they should fail. If they do not, either your
   test is wrong, or your code already implements this functionality.

3. Write the minimum amount of code to make the tests pass.

4. Run the unit tests again. This time, they should pass.

We do not have the sale domain object, so the first thing, as we should
start from small things and then move on to bigger things, is to assure
that we can instantiate the sale object. Write the following unit test
in tests/Domain/SaleTest.php as we will write all the existing tests,
but using TDD; you can remove the existing tests in this file.

    <?php namespace Bookstore\\Tests\\Domain; use
    Bookstore\\Domain\\Sale; use PHPUnit\_Framework\_TestCase; class
    SaleTest extends PHPUnit\_Framework\_TestCase { public function
    testCanCreate() { $sale = new Sale(); } }

Run the tests to make sure that they are failing. In order to run one
specific test, you can mention the file of the test when running
PHPUnit, as shown in the following script:

|image34|

Good, they are failing. That means that PHP cannot find the object to
instantiate it. Let's now write the minimum amount of code required to
make this test pass. In this case, creating the class would be enough,
and you can do this through the following lines of code:

    <?php namespace Bookstore\\Domain; class Sale { }

Now, run the tests to make sure that there are no errors.

|image35|

This is easy, right? So, what we need to do is repeat this process,
adding more functionality each time. Let's focus on the books that a
sale holds; when created, the book's list should be empty, as follows:

    public function testWhenCreatedBookListIsEmpty() { $sale = new
    Sale(); $this->assertEmpty($sale->getBooks()); }

Run the tests to make sure that they fail—they do. Now, write the
following method in the class:

    public function getBooks(): array { return []; }

Now, if you run... wait, what? We are forcing the getBooks method to
return an empty array always? This is not the implementation that we
need—nor the one we deserve—so why do we do it? The reason is the
wording of step 3: "Write the minimum amount of code to make the tests
pass.". Our test suite should be extensive enough to detect this kind of
problem, and this is our way to make sure it does. This time, we will
write bad code on purpose, but next time, we might introduce a bug
unintentionally, and our unit tests should be able to detect it as soon
as possible. Run the tests; they will pass.

Now, let's discuss the next functionality. When adding a book to the
list, we should see this book with amount 1. The test should be as
follows:

    public function testWhenAddingABookIGetOneBook() { $sale = new
    Sale(); $sale->addBook(123); $this->assertSame( $sale->getBooks(),
    [123 => 1] ); }

This test is very useful. Not only does it force us to implement the
addBook method, but also it helps us fix the getBooks method—as it is
hardcoded right now—to always return an empty array. As the getBooks
method now expects two different results, we cannot trick the tests any
more. The new code for the class should be as follows:

    class Sale { private $books = []; public function getBooks(): array
    { return $this->books; } public function addBook(int $bookId) {
    $this->books[123] = 1; } }

A new test we can write is the one that allows you to add more than one
book at a time, sending the amount as the second argument. The test
would look similar to the following:

    public function testSpecifyAmountBooks() { $sale = new Sale();
    $sale->addBook(123, 5); $this->assertSame( $sale->getBooks(), [123
    => 5] ); }

Now, the tests do not pass, so we need to fix them. Let's refactor
addBook so that it can accept a second argument as the amount :

    public function addBook(int $bookId, int $amount = 1) {
    $this->books[123] = $amount; }

The next functionality we would like to add is the same book invoking
the method several times, keeping track of the total amount of books
added. The test could be as follows:

    public function testAddMultipleTimesSameBook() { $sale = new Sale();
    $sale->addBook(123, 5); $sale->addBook(123); $sale->addBook(123, 5);
    $this->assertSame( $sale->getBooks(), [123 => 11] ); }

This test will fail as the current execution will not add all the
amounts but will instead keep the last one. Let's fix it by executing
the following code:

    public function addBook(int $bookId, int $amount = 1) { if
    (!isset($this->books[123])) { $this->books[123] = 0; }
    $this->books[123] += $amount; }

Well, we are almost there. There is one last test we should add, which
is the ability to add more than one different book. The test is as
follows:

    public function testAddDifferentBooks() { $sale = new Sale();
    $sale->addBook(123, 5); $sale->addBook(456, 2); $sale->addBook(789,
    5); $this->assertSame( $sale->getBooks(), [123 => 5, 456 => 2, 789
    => 5] ); }

This test fails due to the hardcoded book ID in our implementation. If
we did not do this, the test would have already passed. Let's fix it
then; run the following:

    public function addBook(int $bookId, int $amount = 1) { if
    (!isset($this->books[$bookId])) { $this->books[$bookId] = 0; }
    $this->books[$bookId] += $amount; }

We are done! Does it look familiar? It is the same code we wrote on our
first implementation except for the rest of the properties. You can now
replace the sale domain object with the previous one, so you have all
the functionalities needed.

Theory versus practice
----------------------

As mentioned before, this is a quite long and verbose process that very
few experienced developers follow from start to end but one that most of
them encourage people to follow. Why is this so? When you write all your
code first and leave the unit tests for the end, there are two problems:

-  Firstly, in too many cases developers are lazy enough to skip tests,
   telling themselves that the code already works, so there is no need
   to write the tests. You already know that one of the goals of tests
   is to make sure that future changes do not break the current
   features, so this is not a valid reason.

-  Secondly, the tests written after the code usually test the code
   rather than the functionality. Imagine that you have a method that
   was initially meant to perform an action. After writing the method,
   we will not perform the action perfectly due to a bug or bad design;
   instead, we will either do too much or leave some edge cases
   untreated. When we write the test after writing the code, we will
   test what we see in the method, not what the original functionality
   was!

If you instead force yourself to write the tests first and then the
code, you make sure that you always have tests and that they test what
the code is meant to do, leading to a code that performs as expected and
is fully covered. Also, by doing it in small intervals, you get quick
feedback and don't have to wait for hours to know whether all the tests
and code you wrote make sense at all. Even though this idea is quite
simple and makes a lot of sense, many novice developers find it hard to
implement.

Experienced developers have written code for several years, so they have
already internalized all of this. This is the reason why some of them
prefer to either write several tests before starting with the code or
the other way around, that is, writing code and then testing it as they
are more productive this way. However, if there is something that all of
them have in common it is that their applications will always be full of
tests.

Summary
=======

In this chapter, you learned the importance of testing your code using
unit tests. You now know how to configure PHPUnit on your application so
that you can not only run your tests but also get good feedback. You got
a good introduction on how to write unit tests properly, and now, it is
safer for you to introduce changes in your application.

In the next chapter, we will study some existing frameworks, which you
can use instead of writing your own every time you start an application.
In this way, not only will you save time and effort, but also other
developers will be able to join you and understand your code easily.

Chapter 8. Using Existing PHP Frameworks

In the same way that you wrote your framework with PHP, other people did
it too. It did not take long for people to realize that entire
frameworks were reusable too. Of course, one man's meat is another man's
poison, and as with many other examples in the IT world, loads of
frameworks started to appear. You will never hear about most of them,
but a handful of these frameworks got quite a lot of users.

As we write, there are four or five main frameworks that most PHP
developers know of: Symfony and Zend Framework were the main characters
of this last PHP generation, but Laravel is also there, providing a
lightweight and fast framework for those who need fewer features. Due to
the nature of this book, we will focus on the latest ones, Silex and
Laravel, as they are quick enough to learn in a chapter—or at least
their basics are.

In this chapter, you will learn about:

-  The importance of frameworks

-  Other features of frameworks

-  Working with Laravel

-  Writing applications with Silex

Reviewing frameworks
====================

In `*Chapter 6* <#Top_of_ch06_html>`__, *Adapting to MVC*, we barely
introduced the idea of frameworks using the MVC design pattern. In fact,
we did not explain what a framework is; we just developed a very simple
one. If you are looking for a definition, here it is: a framework is the
structure that you choose to build your program on. Let's discuss this
in more detail.

The purpose of frameworks
-------------------------

When you write an application, you need to add your models, views, and
controllers if you use the MVC design pattern, which we really encourage
you to do. These three elements, together with the JavaScript and CSS
files that complete your views, are the ones that differentiate your
application from others. There is no way you can skip on writing them.

On the other hand, there is a set of classes that, even though you need
them for the correct functioning of your application, they are common to
all other applications, or at least, they are very similar. Examples of
these classes are the ones we have in the src/Core directory, such as
the router, the configuration reader, and so on.

The purpose of frameworks is clear and necessary: they add some
structure to your application and connect the different elements of it.
In our example, it helped us route the HTTP requests to the correct
controller, connect to the database, and generate dynamic HTML as the
response. However, the idea that has to strive is the reusability of
frameworks. If you had to write the framework each time you start an
application, would that be okay?

So, in order for a framework to be useful, it must be easy to reuse in
different environments. This means that the framework has to be
downloaded from a source, and it has to be easy to install. Download and
install a dependency? It seems Composer is going to be useful again!
Even though this was quite different some years ago, nowadays, all the
main frameworks can be installed using Composer. We will show you how to
in a bit.

The main parts of a framework
-----------------------------

If we open source our framework so that other developers can make use of
it, we need to structure our code in a way that is intuitive. We need to
reduce the learning curve as much as we can; nobody wants to spend weeks
on learning how to work with a framework.

As MVC is the de facto web design pattern used in web applications, most
frameworks will separate the three layers, model, view, and controller,
in three different directories. Depending on the framework, they will be
under a src/ directory, even though it is quite common to find the views
outside of this directory, as we did with our own. Nevertheless, most
frameworks will give you enough flexibility to decide where to place
each of the layers.

The rest of the classes that complete the frameworks used to be all
grouped in a separate directory—for example, src/Core. It is important
to separate these elements from yours so that you do not mix the code
and modify a core class unintentionally, thus messing up the whole
framework. Even better, this last generation of PHP frameworks used to
incorporate the core components as independent modules, which will be
required via Composer. In doing so, the framework's composer.json file
will require all the different components, such as routers,
configuration, database connections, loggers, template engine, and so
on, and Composer will download them in the vendor/ directory, making
them available with the autogenerated autoloader.

Separating the different components in different codebases has many
benefits. First of all, it allows different teams of developers to work
in an isolated way with the different components. Maintaining them is
also easier as the code is separated enough not to affect each other.
Finally, it allows the end user to choose which components to get for
his application in an attempt to customize the framework, leaving out
those heavy components that are not used.

Either the framework is organized in independent modules or everything
is together; however, there are always the same common components, which
are:

-  The router: This is the class that, given an HTTP request, finds the
   correct controller, instantiates it, and executes it, returning the
   HTTP response.

-  The request: This contains a handful of methods that allows you to
   access parameters, cookies, headers, and so on. This is mostly used
   by the router and sent to the controller.

-  The configuration handler: This allows you to get the correct
   configuration file, read it, and use its contents to configure the
   rest of the components.

-  The template engine: This merges HTML with content from the
   controller in order to render the template with the response.

-  The logger: This adds entries to a log file with the errors or other
   messages that we consider important.

-  The dependency injector: This manages all the dependencies that your
   classes need. Maybe the framework does not have a dependency
   injector, but it has something similar—that is, a service
   locator—which tries to help you in a similar way.

-  The way you can write and run your unit tests: Most of the time, the
   frameworks include PHPUnit, but there are more options in the
   community.

Other features of frameworks
============================

Most frameworks have more than just the features that we described in
the previous section, even though these are enough to build simple
applications as you already did by yourself. Still, most web
applications have a lot more common features, so the frameworks tried to
implement generic solutions to each of them. Thanks to this, we do not
have to reinvent the wheel with features that virtually all medium and
big web applications need to implement. We will try to describe some of
the most useful ones so that you have a better idea when choosing a
framework.

Authentication and roles
------------------------

Most websites enforce users to authenticate in order to perform some
action. The reason for this is to let the system know whether the user
trying to perform certain action has the right to do so. Therefore,
managing users and their roles is something that you will probably end
up implementing in all your web applications. The problem comes when way
too many people try to attack your system in order to get the
information of other users or performing actions authenticated as
someone else, which is called impersonification. It is for this reason
that your authentication and authorization systems should be as secure
as possible—a task that is never easy.

Several frameworks include a pretty secure way of managing users,
permissions, and sessions. Most of the time, you can manage this through
a configuration file probably by pointing the credentials to a database
where the framework can add the user data, your customized roles, and
some other customizations. The downside is that each framework has its
own way of configuring it, so you will have to dig into the
documentation of the framework you are using at this time. Still, it
will save you more time than if you had to implement it by yourself.

ORM
---

Object-relational mapping (ORM) is a technique that converts data from a
database or any other data storage into objects. The main goal is to
separate the business logic as much as possible from the structure of
the database and to reduce the complexity of your code. When using ORM,
you will probably never write a query in MySQL; instead, you will use a
chain of methods. Behind the scenes, ORM will write the query with each
method invocation.

There are good and bad things when using ORM. On one hand, you do not
have to remember all the SQL syntax all the time and only the correct
methods to invoke, which can be easier if you work with an IDE that can
autocomplete methods. It is also good to abstract your code from the
type of storage system, because even though it is not very common, you
might want to change it later. If you use ORM, you probably have to
change only the type of connection, but if you were writing raw queries,
you would have a lot of work to do in order to migrate your code.

The arguable downside of using ORM could be that it may be quite
difficult to write complicated queries using method chains, and you will
end up writing them manually. You are also at the mercy of ORM in order
to speed up the performance of your queries, whereas when writing them
manually, it is you who can choose better what and how to use when
querying. Finally, something that OOP purists complain about quite a lot
is that using ORM fills your code with a large amount of dummy objects,
similar to the domain objects that you already know.

As you can see, using ORM is not always an easy decision, but just in
case you choose to use it, most of the big frameworks include one. Take
your time in deciding whether or not to use one in your applications; in
case you do, choose wisely which one. You might end up requiring an ORM
different from the one that the framework provides.

Cache
-----

The bookstore is a pretty good example that may help in describing the
cache feature. It has a database of books that is queried every time
that someone either lists all the books or asks for the details of a
specific one. Most of the time the information related to books will be
the same; the only change would be the stock of the books from time to
time. We could say that our system has way more reads than writes, where
reads means querying for data and writes means updating it. In this kind
of system, it seems like a waste of time and resources to access the
database each time, knowing that most of the time, we will get the same
results. This feeling increases if we do some expensive transformation
to the data that we retrieve.

A cache layer allows the application to store temporary data in a
storage system faster than our database, usually in memory rather than
disk. Even though cache systems are getting more complex, they usually
allow you to store data by key-value pairs, as in an array.

The idea is not to access the database for data that we know is the same
as the last time we accessed it in order to save time and resources.
Implementations can vary quite a lot, but the main flow is as follows:

1. You try to access a certain piece of data for the first time. We ask
   the cache whether a certain key is there, which it is not.

2. You query the database, getting back the result. After processing
   it—and maybe transforming it to your domain objects—you store the
   result in the cache. The key would be the same you used in step 1,
   and the value would be the object/array/JSON that you generated.

3. You try to access the same piece of data again. You ask the cache
   whether the key is there; here, it is, so you do not need to access
   the database at all.

It seems easy, right? The main problem with caches comes when we need to
invalidate a certain key. How and when should we do it? There are a
couple of approaches that are worth mentioning:

-  You will set an expiration time to the key-value pair in the cache.
   After this time passes, the cache will remove the key-value pair
   automatically, so you will have to query the database again. Even
   though this system might work for some applications, it does not for
   ours. If the stock changes to 0 before the cache expires, the user
   will see books that they cannot borrow or buy.

-  The data never expires, but each time we make a change in the
   database, we will identify which keys in the cache are affected by
   this change and then purge them. This is ideal since the data will be
   in the cache until it is no longer valid, whether this is 2 seconds
   or 3 weeks. The downside is that identifying these keys could be a
   hard task depending on your data structure. If you miss deleting some
   of them, you will have corrupted data in your cache, which is quite
   difficult to debug and detect.

You can see that cache is a double-edged sword, so we would recommend
you to only use it when necessary and not just because your framework
comes with it. As with ORM, if you are not convinced by the cache system
that your framework provides, using a different one should not be
difficult. In fact, your code should not be aware of which cache system
you are using except when creating the connection object.

Internationalization
--------------------

English is not the only language out there, and you would like to make
your website as accessible as possible. Depending on your target, it
would be a good idea to have your website translated to other languages
too, but how do you do this? We hope that by now you did not answer:
"Copy-pasting all the templates and translating them". This is way too
inefficient; when making a little change in a template, you need to
replicate the change everywhere.

There are tools that can be integrated with either controllers and/or
template engines in order to translate strings. You usually keep a file
for each language that you have, in which you will add all the strings
that need to be translated plus their translation. One of the most
common formats for this is PO files, in which you have a map of
key-value pairs with originally translated pairs. Later on, you will
invoke a translate method sending the original string, which will return
the translated string depending on the language you selected.

When writing templates, it might be tiring to invoke the translation
each time you want to display a string, but you will end up with only
one template, which is much easier to maintain than any other option.

Usually, internationalization is very much tied to the framework that
you use; however, if you have the opportunity to use the system of your
choice, pay special attention to its performance, the translation files
it uses, and how it manages strings with parameters—that is, how we can
ask the system to translate messages such as "Hello %s, who are you?" in
which "%s" needs to be injected each time.

Types of frameworks
===================

Now that you know quite a lot about what a framework can offer you, you
are in a position to decide what kind of framework you would like to
use. In order to make this decision, it might be useful to know what
kinds of frameworks are available. This categorization is nothing
official, just some guidelines that we offer you to make your choice
easier.

Complete and robust frameworks
------------------------------

This type of framework comes with the whole package. It contains all the
features that we discussed earlier, so it will allow you to develop very
complete applications. Usually, these frameworks allow you to create
applications very easily with just a few configuration files that define
things such as how to connect to a database, what kind of roles you
need, or whether you want to use a cache. Other than this, you will just
have to add your controllers, views, and models, which saves you a lot
of time.

The problem with these frameworks is the learning curve. Given all the
features they contain, you need to spend quite a lot of time on learning
how to use each one, which is usually not very pleasant. In fact, most
companies looking for web developers require that you have experience
with the framework they use; otherwise, it will be a bad investment for
them.

Another thing you should consider when choosing these frameworks is
whether they are structured in modules or come as a huge monolith. In
the first case, you will be able to choose which modules to use that add
a lot of flexibility. On the other hand, if you have to stick with all
of them, it might make your application slow even if you do not use all
of the features.

Lightweight and flexible frameworks
-----------------------------------

Even when working on a small application, you would like to use a
framework to save you a lot of time and pain, but you should avoid using
one of the larger frameworks as they will be too much to handle for what
you really need. In this case, you should choose a lightweight
framework, one that contains very few features, similar to what we
implemented in previous chapters.

The benefit of these frameworks is that even though you get the basic
features such as routing, you are completely free to implement the login
system, cache layer, or internationalization system that suits your
specific application better. In fact, you could build a more complete
framework using this one as the base and then adding all the complements
you need, making it completely customized.

As you can note, both types have their pros and cons. It will be up to
you to choose the correct one each time, depending on your needs, the
time that you can spend, and the experience that you have with each one.

An overview of famous frameworks
================================

You already have a good idea about what a framework can offer and what
types there are. Now, it is time to review some of the most important
ones out there so that you get an idea of where to start looking for
your next PHP web application. Note that with the release of PHP 7,
there will be quite a lot of new or improved PHP frameworks. Try to
always be in the loop!

Symfony 2
---------

Symfony has been one of the most favorite frameworks of developers
during the last 10 years. After reinventing itself for its version 2,
Symfony entered the generation of frameworks by modules. In fact, it is
quite common to find other projects using Symfony 2 components mixed up
with some other framework as you just need to add the name of the module
in your Composer file to use it.

You can start applications with Symfony 2 by just executing a command.
Symfony 2 creates all the directories, empty configuration files, and so
on ready for you. You can also add empty controllers from the command
line. They use Doctrine 2 as ORM, which is probably one of the most
reliable ORMs that PHP can offer nowadays. For the template engine, you
will find Twig, which is the same as what we used in our framework.

In general, this is a very attractive framework with a huge community
behind it giving support; plus, a lot of companies also use it. It is
always worth at least checking the list of modules in case you do not
want to use the whole framework but want to take advantage of some bits
of it.

Zend Framework 2
----------------

The second big PHP framework, at least since last year, is Zend
Framework 2. As with Symfony, it has been out there for quite a long
time too. Also, as with any other modern framework, it is built in an
OOP way, trying to implement all the good design patterns used for web
applications. It is composed of multiple components that you can reuse
in other projects, such as their well-known authentication system. It
lacks some elements, such as a template engine—usually they mix PHP and
HTML—and ORM, but you can easily integrate the ones that you prefer.

There is a lot of work going on in order to release Zend Framework 3,
which will come with support for PHP 7, performance improvements, and
some other new components. We recommend you to keep an eye on it; it
could be a good candidate.

Other frameworks
----------------

Even though Symfony and Zend Framework are the two big players, more and
more PHP frameworks have appeared in these last years, evolving quite
fast and bringing to the game more interesting features. Names such as
CodeIgniter, Yii, PHPCake, and others will start to sound familiar as
soon as you start browsing PHP projects. As some of them came into play
later than Symfony and Zend Framework, they implement some new features
that the others do not have, such as components related to JavaScript
and jQuery, integration with Selenium for UI testing, and others.

Even though it is always a good thing to have diversification simply
because you will probably get exactly what you need from one or the
other, be smart when choosing your framework. The community plays an
important role here because if you have any problem, it will help you to
fix it or you can just help evolve the framework with each new PHP
release.

The Laravel framework
=====================

Even though Symfony and Zend Framework have been the big players for
quite a long time, during this last couple of years, a third framework
came into play that has grown in popularity so much that nowadays it is
the favorite framework among developers. Simplicity, elegant code, and
high speed of development are the trump cards of this "framework for
artisans". In this section, you will have a glance at what Laravel can
do, taking the first steps to create a very simple application.

Installation
------------

Laravel comes with a set of command-line tools that will make your life
easier. Because of this, it is recommended to install it globally
instead of per project—that is, to have Laravel as another program in
your environment. You can still do this with Composer by running the
following command:

    $ composer global require "laravel/installer"

This command should download the Laravel installer to
~/.composer/vendor. In order to be able to use the executable from the
command line, you will need to run something similar to this:

    $ sudo ln -s ~/.composer/vendor/bin/laravel /usr/bin/laravel

Now, you are able to use the laravel command. To ensure that everything
went all right, just run the following:

    $ laravel –version

If everything went OK, this should output the version installed.

Project setup
-------------

Yes, we know. Every single tutorial starts by creating a blog. However,
we are building web applications, and this is the easiest approach we
can take that adds some value to you. Let's start then; execute the
following command wherever you want to add your application:

    $ laravel new php-blog

This command will output something similar to what Composer does, simply
because it fetches dependencies using Composer. After a few seconds, the
application will hopefully tell you that everything was installed
successfully and that you are ready to go.

Laravel created a new php-blog directory with quite a lot of content.
You should have something similar to the directory structure shown in
the following screenshot:

|image36|

Let's set up the database. The first thing you should do is update the
.env file with the correct database credentials. Update the DB\_DATABASE
values with your own; here's an example:

    DB\_HOST=localhost DB\_DATABASE=php\_blog DB\_USERNAME=root
    DB\_PASSWORD=

You will also need to create the php\_blog database. Do it with just one
command, as follows:

    $ mysql -u root -e "CREATE SCHEMA php\_blog"

With Laravel, you have a migrations system; that is, you keep all the
database schema changes under database/migrations so that anyone else
using your code can quickly set up their database. The first step is to
run the following command, which will create a migrations file for the
blogs table:

    $ php artisan make:migration create\_posts\_table --create=posts

Open the generated file, which should be something similar to
database/migrations/<date>\_create\_posts\_table.php. The up method
defines the table blogs with an autoincremental ID and timestamp field.
We would like to add a title, the content of the post, and the user ID
that created it. Replace the up method with the following:

    public function up() { Schema::create('posts', function (Blueprint
    $table) { $table->increments('id'); $table->timestamps();
    $table->string('title'); $table->text('content');
    $table->integer('user\_id')->unsigned(); $table->foreign('user\_id')
    ->references('id')->on('users'); }); }

Here, the title will be a string, whereas the content is a text. The
difference is in the length of these fields, string being a simple
VARCHAR and text a TEXT data type. For the user ID we defined INT
UNSIGNED, which references the id field of the users table. Laravel
already defined the users table when creating the project, so you do not
have to worry about it. If you are interested in how it looks, check the
database/migrations/2014\_10\_12\_000000\_create\_users\_table.php file.
You will note that a user is composed by an ID, a name, the unique
e-mail, and the password.

So far, we have just written the migration files. In order to apply
them, you need to run the following command:

    $ php artisan migrate

If everything went as expected, you should have a blogs table now
similar to the following:

|image37|

To finish with all the preparations, we need to create a model for our
blogs table. This model will extend from
Illuminate\\Database\\Eloquent\\Model, which is the ORM that Laravel
uses. To generate this model automatically, run the following command:

    $ php artisan make:model Post

The name of the model should be the same as that of the database table
but in singular. After running this command, you can find the empty
model in app/Post.php.

Adding the first endpoint
-------------------------

Let's add a quick endpoint just to understand how routes work and how to
link controllers with templates. In order to avoid database access,
let's build the add new post view, which will display a form that allows
the user to add a new post with a title and text. Let's start by adding
the route and controller. Open the app/Http/routes.php file and add the
following:

    Route::group(['middleware' => ['web']], function () {
    Route::get('/new', function () { return view('new'); }); });

These three very simple lines say that for the /new endpoint, we want to
reply with the new view. Later on, we will complicate things here in the
controller, but for now, let's focus on the views.

Laravel uses Blade as the template engine instead of Twig, but the way
they work is quite similar. They can also define layouts from where
other templates can extend. The place for your layouts is in
resources/views/layouts. Create an app.blade.php file with the following
content inside this directory, as follows:

    <!DOCTYPE html> <html lang="en"> <head> <title>PHP Blog</title>
    <link rel="stylesheet" href="{{ URL::asset('css/layout.css') }}"
    type="text/css"> @yield('css') </head> <body> <div class="navbar">
    <ul> <li><a href="/new">New article</a></li> <li><a
    href="/">Articles</a></li> </ul> </div> <div class="content">
    @yield('content') </div> </body> </html>

This is just a normal layout with a title, some CSS, and an ul list of
sections in the body, which will be used as the navigation bar. There
are two important elements to note here other than the HTML code that
should already sound familiar:

-  To define a block, Blade uses the @yield annotation followed by the
   name of the block. In our layout, we defined two blocks: css and
   content.

-  There is a feature that allows you to build URLs in templates. We
   want to include the CSS file in public/css/layout.css, so we will use
   URL::asset to build this URL. It is also helpful to include JS files.

As you saw, we included a layout.css file. CSS and JS files are stored
under the public directory. Create yours in public/css/layout.css with
the following code:

    .content { position: fixed; top: 50px; width: 100% } .navbar ul {
    position: fixed; top: 0; width: 100%; list-style-type: none; margin:
    0; padding: 0; overflow: hidden; background-color: #333; } .navbar
    li { float: left; border-right: 1px solid #bbb; } .navbar
    li:last-child { border-right: none; } .navbar li a { display: block;
    color: white; text-align: center; padding: 14px 16px;
    text-decoration: none; } .navbar li a:hover { background-color:
    #111; }

Now, we can focus on our view. Templates are stored in resources/views,
and, as with layouts, they need the .blade.php file extension. Create
your view in resources/views/new.blade.php with the following content:

    @extends('layouts.app') @section('css') <link rel="stylesheet"
    href="{{ URL::asset('css/new.css') }}" type="text/css"> @endsection
    @section('content') <h2>Add new post</h2> <form method="post"
    action="/new"> <div class="component"> <label
    for="title">Title</label> <input type="text" name="title"/> </div>
    <div class="component"> <label>Text</label> <textarea rows="20"
    name="content"></textarea> </div> <div class="component"> <button
    type="submit">Save</button> </div> </form> @endsection

The syntax is quite intuitive. This template extends from the layouts'
one and defines two sections or blocks: css and content. The CSS file
included follows the same format as the previous one. You can create it
in public/css/new.css with content similar to the following:

    label { display: block; } input { width: 80%; } button { font-size:
    30px; float: right; margin-right: 20%; } textarea { width: 80%; }
    .component { padding: 10px; }

The rest of the template just defines the POST form pointing to the same
URL with title and text fields. Everything is ready to test it in your
browser! Try accessing http://localhost:8080/new or the port number of
your choice. You should see something similar to the following
screenshot:

|image38|

Managing users
--------------

As explained before, user authentication and authorization is one of the
features that most frameworks contain. Laravel makes our lives very easy
by providing the user model and the registration and authentication
controllers. It is quite easy to make use of them: you just need to add
the routes pointing to the already existing controllers and add the
views. Let's begin.

There are five routes that you need to consider here. There are two that
belong to the registration step, one to get the form and another one for
the form to submit the information provided by the user. The other three
are related to the authentication part: one to get the form, one to post
the form, and one for the logout. All five of them are included in the
Auth\\AuthController class. Add to your routes.php file the following
routes:

    // Registration routes... Route::get('auth/register',
    'Auth\\AuthController@getRegister'); Route::post('auth/register',
    'Auth\\AuthController@postRegister'); // Authentication routes...
    Route::get('/login', 'Auth\\AuthController@getLogin');
    Route::post('login', 'Auth\\AuthController@postLogin');
    Route::get('logout', 'Auth\\AuthController@getLogout');

Note how we defined these routes. As opposed to the one that we created
previously, the second argument of these is a string with the
concatenation of the controller's class name and method. This is a
better way to create routes because it separates the logic to a
different class that can later be reused and/or unit tested.

If you are interested, you can browse the code for this controller. You
will find a complex design, where the functions the routes will invoke
are actually part of two traits that the AuthController class uses:
RegistersUsers and AuthenticatesUsers. Checking these methods will
enable you to understand what goes on behind the scenes.

Each get route expects a view to render. For the user's registration, we
need to create a template in resources/views/auth/register.blade.php,
and for the login view, we need a template in
resources/views/auth/login.blade.php. As soon as we send the correct
POST parameters to the correct URL, we can add any content that we think
necessary.

User registration
~~~~~~~~~~~~~~~~~

Let's start with the registration form; this form needs four POST
parameters: name, e-mail, password, and password confirmation, and as
the route says, we need to submit it to /auth/register. The template
could look similar to the following:

    @extends('layouts.app') @section('css') <link rel="stylesheet"
    href="{{ URL::asset('css/register.css') }}" type="text/css">
    @endsection @section('content') <h2>Account registration</h2> <form
    method="post" action="/auth/register"> {{ csrf\_field() }} <div
    class="component"> <label for="name">Name</label> <input type="text"
    name="name" value="{{ old('name') }}" /> </div> <div
    class="component"> <label>Email</label> <input type="email"
    name="email" value="{{ old('email') }}"/> </div> <div
    class="component"> <label>Password</label> <input type="password"
    name="password" /> </div> <div class="component"> <label>Password
    confirmation</label> <input type="password"
    name="password\_confirmation" /> </div> <div class="component">
    <button type="submit">Create</button> </div> </form> @endsection

This template is quite similar to the form for new posts: it extends the
layout, adds a CSS file, and populates the content section with a form.
The new addition here is the use of the old function that retrieves the
value submitted on the previous request in case that the form was not
valid and we showed it back to the user.

Before we try it, we need to add a register.css file with the styles for
this form. A simple one could be as follows:

    div.content { text-align: center; } label { display: block; } input
    { width: 250px; } button { font-size: 20px; } .component { padding:
    10px; }

Finally, we should edit the layout in order to add a link on the menu
pointing to the registration and login pages. This is as simple as
adding the following li elements at the end of the ul tag:

    <li class="right"><a href="/auth/register">Sign up</a></li> <li
    class="right"><a href="/login">Sign in</a></li>

Add also the style for the right class at the end of layout.css:

    div.alert { color: red; }

To make things even more useful, we could add the information for what
went wrong when submitting the form. Laravel flashes the errors into the
session, and they can be accessed via the errors template variable. As
this is common to all forms and not only to the registration one, we
could add it to the app.blade.php layout, as follows:

    <div class="content"> @if (count($errors) > 0) <div class="alert">
    <strong>Whoops! Something went wrong!</strong> @foreach
    ($errors->all() as $error) <p>{{ $error }}</p> @endforeach </div>
    @endif @yield('content')

In this piece of code, we will use Blade's @if conditional and @foreach
loop. The syntax is the same as PHP; the only difference is the @
prefix.

Now, we are ready to go. Launch your application and click on the
registration link on the right-hand side of the menu. Attempt to submit
the form, but leave some fields blank so that we can note how the errors
are displayed. The result should be something similar to this:

|image39|

One thing that we should customize is where the user will be redirected
once the registration is successful. In this case, we can redirect them
to the login page. In order to achieve this, you need to change the
value of the $redirectTo property of AuthController. So far, we only
have the new post page, but later, you could add any path that you want
via the following:

    protected $redirectPath= '/new;

User login
~~~~~~~~~~

The user's login has a few more changes other than the registration. We
not only need to add the login view, we should also modify the menu in
the layout in order to acknowledge the authenticated user, remove the
register link, and add a logout one. The template, as mentioned earlier,
has to be saved in resources/views/auth/login.blade.php. The form needs
an e-mail and password and optionally a checkbox for the *remember me*
functionality. An example could be the following:

    @extends('layouts.app') @section('css') <link rel="stylesheet"
    href="{{ URL::asset('css/register.css') }}" type="text/css">
    @endsection @section('content') <h2>Login</h2> <form method="POST"
    action="/login"> {!! csrf\_field() !!} <div class="component">
    <label>Email</label> <input type="email" name="email" value="{{
    old('email') }}"> </div> <div class="component">
    <label>Password</label> <input type="password" name="password">
    </div> <div class="component"> <input class="checkbox"
    type="checkbox" name="remember"> Remember Me </div> <div
    class="component"> <button type="submit">Login</button> </div>
    </form> @endsection

The layout has to be changed slightly. Where we displayed the links to
register and log in users, now we need to check whether there is a user
already authenticated; if so, we should rather show a logout link. You
can get the authenticated user through the Auth::user() method even from
the view. If the result is not empty, it means that the user was
authenticated successfully. Change the two links using the following
code:

    <ul> <li><a href="/new">New article</a></li> <li><a
    href="/">Articles</a></li> @if (Auth::user() !== null) <li
    class="right"> <a href="/logout">Logout</a> </li> @else <li
    class="right"> <a href="/auth/register">Sign up</a> </li> <li
    class="right"> <a href="/login">Sign in</a> </li> @endif </ul>

Protected routes
~~~~~~~~~~~~~~~~

This last part of the user management session is probably the most
important one. One of the main goals when authenticating users is to
authorize them to certain content—that is, to allow them to visit
certain pages that unauthenticated users cannot. In Laravel, you can
define which routes are protected in this way by just adding the auth
middleware. Update the new post route with the following code:

    Route::get('/new', ['middleware' => 'auth', function () { return
    view('new'); }]);

Everything is ready! Try to access the new post page after logging out;
you will be redirected automatically to the login page. Can you feel how
powerful a framework can be?

Setting up relationships in models
----------------------------------

As we mentioned before, Laravel comes with an ORM, Eloquent ORM, which
makes dealing with models a very easy task. In our simple database, we
defined one table for posts, and we already had another one for users.
Posts contain the ID of the user that owns it—that is, user\_id. It is
good practice to use the singular of the name of the table followed by
\_id so that Eloquent will know where to look. This was all we did
regarding the foreign key.

We should also mention this relationship on the model side. Depending on
the type of the relationship (one to one, one to many, or many to many),
the code will be slightly different. In our case, we have a one-to-many
relationship because one user can have many posts. To say so in Laravel,
we need to update both the Post and the User models. The User model
needs to specify that it has many posts, so you need to add a posts
method with the following content:

    public function posts() { return $this->hasMany('App\\Post'); }

This method says that the model for users has many posts. The other
change that needs to be made in Post is similar: we need to add a user
method that defines the relationship. The method should be similar to
this one:

    public function user() { return $this->belongsTo('App\\User'); }

It looks like very little, but this is the whole configuration that we
need. In the next section, you will see how easy it is to save and query
using these two models.

Creating complex controllers
----------------------------

Even though the title of this section mentions complex controllers, you
will note that we can create complete and powerful controllers with very
little code. Let's start by adding the code that will manage the
creation of posts. This controller needs to be linked to the following
route:

    Route::post('/new', 'Post\\PostController@createPost');

As you can imagine, now, we need to create the Post\\PostController
class with the createPost method in it. Controllers should be stored in
app/Http/Controllers, and if they can be organized in folders, it would
be even better. Save the following class in
app/Http/Controllers/Post/PostController.php:

    <?php namespace App\\Http\\Controllers\\Post; use
    App\\Http\\Controllers\\Controller; use Illuminate\\Http\\Request;
    use Illuminate\\Support\\Facades\\Auth; use
    Illuminate\\Support\\Facades\\Validator; use App\\Post; class
    PostController extends Controller { public function
    createPost(Request $request) { } }

So far, the only two things we can note from this class are:

-  Controllers extend from the App\\Http\\Controllers\\Controller class,
   which contains some general helpers for all the controllers.

-  Methods of controllers can get the Illuminate\\Http\\Request argument
   as the user's request. This object will contain elements such as the
   posted parameters, cookies, and so on. This is very similar to the
   one we created in our own application.

The first thing we need to do in this kind of controller is check
whether the parameters posted are correct. For this, we will use the
following code:

    public function createPost(Request $request) { $validator =
    Validator::make($request->all(), [ 'title' => 'required\|max:255',
    'content' => 'required\|min:20', ]); if ($validator->fails()) {
    return redirect()->back() ->withInput() ->withErrors($validator); }
    }

The first thing we did is create a validator. For this, we used the
Validator::make function and sent two arguments: the first one contains
all the parameters from the request, and the second one is an array with
the expected fields and their constraints. Note that we expect two
required fields: title and content. Here, the first one can be up to 255
characters long, and the second one needs to be at least 20 characters
long.

Once the validator object is created, we can check whether the data
posted by the user matches the requirements with the fails method. If it
returns true—that is, the validation fails—we will redirect the user
back to the previous page with redirect()->back(). To perform this
invocation, we will add two more method calls: withInput will send the
submitted values so that we can display them again, and withErrors will
send the errors the same way AuthController did.

At this point, it would be helpful to the user if we show the previously
submitted title and text in case the post is not valid. For this, use
the already known old method in the view:

    {{--...--}} <input type="text" name="title" value="{{ old('title')
    }}"/> </div> <div class="component"> <label>Text</label> <textarea
    rows="20" name="content"> {{ old('content') }} </textarea>
    {{--...--}}

At this point, we can already test how the controller behaves when the
post does not match the required validations. If you miss any of the
parameters or they do not have correct lengths, you will get an error
page similar to the following one:

|image40|

Let's now add the logic to save the post in case it is valid. If you
remember the interaction with the models from our previous application,
you will be gladly surprised at how easy it is to work with them here.
Take a look at the following:

    public function createPost(Request $request) { $validator =
    Validator::make($request->all(), [ 'title' => 'required\|max:255',
    'content' => 'required\|min:20', ]); if ($validator->fails()) {
    return redirect()->back() ->withInput() ->withErrors($validator); }
    $post = new Post(); $post->title = $request->title; $post->content =
    $request->content; Auth::user()->posts()->save($post); return
    redirect('/new'); }

The first thing we will do is create a post object setting the title and
content from the request values. Then, given the result of Auth::user(),
which gives us the instance of the currently authenticated user model,
we will save the post that we just created through posts()->save($post).
If we wanted to save the post without the information of the user, we
could use $post->save(). Really, that is all.

Let's quickly add another endpoint to retrieve the list of posts for a
given user so that we can take a look at how Eloquent ORM allows us to
fetch data easily. Add the following route:

    Route::get('/', ['middleware' => 'auth', function () { $posts =
    Auth::user() ->posts() ->orderBy('created\_at') ->get(); return
    view('posts', ['posts' => $posts]); }]);

The way we retrieve data is very similar to how we save it. We need the
instance of a model—in this case, the authenticated user—and we will add
a concatenation of method invocations that will internally generate the
query to execute. In this case, we will ask for the posts ordered by the
creation date. In order to send information to the view, we need to pass
a second argument, which will be an array of parameter names and values.

Add the following template as resources/views/posts.blade.php, which
will display the list of posts for the authenticated user as a table.
Note how we will use the $post object, which is an instance of the
model, in the following code:

    @extends('layouts.app') @section('css') <link rel="stylesheet"
    href="{{ URL::asset('css/posts.css') }}" type="text/css">
    @endsection @section('content') <h2>Your posts</h2> <table> @foreach
    ($posts as $post) <tr> <td>{{ $post->title }}</td> <td>{{
    $post->created\_at }}</td> <td>{{ str\_limit($post->content, 100)
    }}</td> </tr> @endforeach </table> @endsection

The lists of posts are finally displayed. The result should be something
similar to the following screenshot:

|image41|

Adding tests
------------

In a very short time, we created an application that allows you to
register, log in, and create and list posts from scratch. We will end
this section by talking about how to test your Laravel application with
PHPUnit.

It is extremely easy to write tests in Laravel as it has a very nice
integration with PHPUnit. There is already a phpunit.xml file, a
customized TestCase class, customized assertions, and plenty of helpers
in order to test with the database. It also allows you to test routes,
emulating the HTTP request instead of testing the controllers. We will
visit all these features while testing the creation of new posts.

First of all, we need to remove tests/ExampleTest.php because it tested
the home page, and as we modified it, it will fail. Do not worry; this
is an example test that helps developers to start testing, and making it
fail is not a problem at all.

Now, we need to create our new test. To do this, we can either add the
file manually or use the command line and run the following command:

    $ php artisan make:test NewPostTest

This command creates the tests/NewPostTest.php file, which extends from
TestCase. If you open it, you will note that there is already a dummy
test, which you can also remove. Either way, you can run PHPUnit to make
sure everything passes. You can do it in the same way we did previously,
as follows:

    $ ./vendor/bin/phpunit

The first test we can add is one where we try to add a new post but the
data passed by the POST parameters is not valid. In this case, we should
expect that the response contains errors and old data, so the user can
edit it instead of rewriting everything again. Add the following test to
the NewPostTest class:

    <?php class NewPostTest extends TestCase { public function
    testWrongParams() { $user = factory(App\\User::class)
    ->make(['email' => 'test@user.laravel']); $this->be($user);
    $this->call( 'POST', '/new', ['title' => 'the title', 'content' =>
    'ojhkjhg'] ); $this->assertSessionHasErrors('content');
    $this->assertHasOldInput(); } }

The first thing we can note in the test is the creation of a user
instance using a factory. You can pass an array with any parameter that
you want to set to the make invocation; otherwise, defaults will be
used. After we get the user instance, we will send it to the be method
to let Laravel know that we want that user to be the authorized one for
this test.

Once we set the grounds for the test, we will use the call helper that
will emulate a real HTTP request. To this method, we have to send the
HTTP method (in this case, POST), the route to request, and optionally
the parameters. Note that the call method returns the response object in
case you need it.

We will send a title and the content, but this second one is not long
enough, so we will expect some errors. Laravel comes with several
customized assertions, especially when testing these kinds of responses.
In this case, we could use two of them: assertSessionHasErrors, which
checks whether there are any flash errors in the session (in particular,
the ones for the content parameter), and assertHasOldInput, which checks
whether the response contains old data in order to show it back to the
user.

The second test that we would like to add is the case where the user
posts valid data so that we can save the post in the database. This test
is trickier as we need to interact with the database, which is usually a
not a very pleasant experience. However, Laravel gives us enough tools
to help us in this task. The first and most important is to let PHPUnit
know that we want to use database transactions for each test. Then, we
need to persist the authenticated user in the database as the post has a
foreign key pointing to it. Finally, we should assert that the post is
saved in the database correctly. Add the following code to the
NewPostTest class:

    use DatabaseTransactions; //... public function testNewPost() {
    $postParams = [ 'title' => 'the title', 'content' => 'In a place far
    far away.' ]; $user = factory(App\\User::class) ->make(['email' =>
    'test@user.laravel']); $user->save(); $this->be($user);
    $this->call('POST', '/new', $postParams);
    $this->assertRedirectedTo('http://localhost/new');
    $this->seeInDatabase('posts', $postParams); }

The DatabaseTransactions trait will make the test to start a transaction
at the beginning and then roll it back once the test is done, so we will
not leave the database with data from tests. Saving the authenticated
user in the database is also an easy task as the result of the factory
is an instance of the user's model, and we can just invoke the save
method on it.

The assertRedirectedTo assertion will make sure that the response
contains the valid headers that redirect the user to the specified URL.
More interestingly, seeInDatabase will verify that there is an entity in
the posts table, which is the first argument, with the data provided in
the array, which is the second argument.

There are quite a lot of assertions, but as you can note, they are
extremely useful, reducing what could be a long test to a very few
lines. We recommend you to visit the official documentation for the full
list.

The Silex microframework
========================

After a taste of what Laravel can offer you, you most likely do not want
to hear about minimalist microframeworks. Still, we think it is good to
know more than one framework. You can get to know different approaches,
be more versatile, and everyone will want you in their team.

We chose Silex because it is a microframework, which is very different
from Laravel, and also because it is part of the Symfony family. With
this introduction to Silex, you will learn how to use your second
framework, which is of a totally different type, and you will be one
step closer to knowing Symfony as well, which is one of the big players.

What is the benefit of microframeworks? Well, they provide the very
basics—that is, a router, a simple dependency injector, request helpers,
and so on, but this is the end of it. You have plenty of room to choose
and build what you really need, including external libraries or even
your own ones. This means that you can have a framework specially
customized for each different project. In fact, Silex provides a handful
of built-in service providers that you can integrate very easily, from
template engines to logging or security.

Installation
------------

There's no news here. Composer does everything for you, as it does with
Laravel. Execute the following command on your command line at the root
of your new project in order to include Silex in your composer.json
file:

    $ composer require silex/silex

You may require more dependencies, but let's add them when we need them.

Project setup
-------------

Silex's most important class is Silex\\Application. This class, which
extends from Pimple (a lightweight dependency injector), manages almost
anything. You can use it as an array as it implements the ArrayAccess
interface, or you could invoke its methods to add dependencies, register
services, and so on. The first thing to do is to instantiate it in your
public/index.php file, as follows:

    <?php use Silex\\Application; require\_once \_\_DIR\_\_ .
    '/../vendor/autoload.php'; $app = new Application();

Managing configuration
~~~~~~~~~~~~~~~~~~~~~~

One of the first things we like to do is load the configuration. We
could do something very simple, such as including a file with PHP or
JSON content, but let's make use of one of the service providers,
ConfigServiceProvider. Let's add it with Composer via the following
line:

    $ composer require igorw/config-service-provider

This service allows us to have multiple configuration files, one for
each environment we need. Imagining that we want to have two
environments, prod and dev, this means we need two files: one in
config/prod.json and one in config/dev.json. The config/dev.json file
would look similar to this:

    { "debug": true, "cache": false, "database": { "user": "dev",
    "password": "" } }

The config/prod.json file would look similar to this:

    { "debug": false, "cache": true, "database ": { "user": "root",
    "password": "fsd98na9nc" } }

In order to work in a development environment, you will need to set the
correct value to the environment variable by running the following
command:

    export APP\_ENV=dev

The APP\_ENV environment variable will be the one telling us which
environment we are in. Now, it is time to use this service provider. In
order to register it by reading from the configuration file of the
current environment, add the following lines to your index.php file:

    $env = getenv('APP\_ENV') ?: 'prod'; $app->register( new
    Igorw\\Silex\\ConfigServiceProvider( \_\_DIR\_\_ .
    "/../config/$env.json" ) );

The first thing we did here is to get the environment from the
environment variable. By default, we set it to prod. Then, we invoked
register from the $app object to add an instance of
ConfigServiceProvider by passing the correct configuration file path.
From now on, the $app "array" will contain three entries: debug, cache,
and db with the content of the configuration files. We will be able to
access them whenever we have access to $app, which will be mostly
everywhere.

Setting the template engine
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Another of the handy service providers is Twig. As you might remember,
Twig is the template engine that we used in our own framework, and it
is, in fact, from the same people that developed Symfony and Silex. You
also already know how to add the dependency with Composer; simply run
the following:

    $ composer require twig/twig

To register the service, we will need to add the following lines in our
public/index.php file:

    $app->register( new Silex\\Provider\\TwigServiceProvider(),
    ['twig.path' => \_\_DIR\_\_ . '/../views'] );

Also, create the views/ directory where we will later store our
templates. Now, you have the Twig\_Environment instance available by
just accessing $app['twig'].

Adding a logger
~~~~~~~~~~~~~~~

The last one of the service providers that we will register for now is
the logger. This time, the library to use is Monolog, and you can
include this via the following:

    $ composer require monolog/monolog

The quickest way to register a service is by just providing the path of
the log file, which can be done as follows:

    $app->register( new Silex\\Provider\\MonologServiceProvider(),
    ['monolog.logfile' => \_\_DIR\_\_ . '/../app.log'] );

If you would like to add more information to this service provider, such
as what level of logs you want to save, the name of the log, and so on,
you can add them to the array together with the log file. Take a look at
the documentation at
`*http://silex.sensiolabs.org/doc/providers/monolog.html* <http://silex.sensiolabs.org/doc/providers/monolog.html>`__
for the full list of parameters available.

As with the template engine, from now on, you can access the
Monolog\\Logger instance from the Application object by accessing
$app['monolog'].

Adding the first endpoint
-------------------------

It is time to see how the router works in Silex. We would like to add a
simple endpoint for the home page. As we already mentioned, the $app
instance can manage almost anything, including routes. Add the following
code at the end of the public/index.php file:

    $app->get('/', function(Application $app) { return
    $app['twig']->render('home.twig'); });

This is a similar way of adding routes to the one that Laravel follows.
We invoked the get method as it is a GET endpoint, and we passed the
route string and the Application instance. As we mentioned here, $app
also acts as a dependency injector—in fact, it extends from one:
Pimple—so you will notice the Application instance almost everywhere.
The result of the anonymous function will be the response that we will
send to the user—in this case, a rendered Twig template.

Right now, this will not do the trick. In order to let Silex know that
you are done setting up your application, you need to invoke the run
method at the very end of the public/index.php file. Remember that if
you need to add anything else to this file, it has to be before this
line:

    $app->run();

You have already worked with Twig, so we will not spend too much time on
this. The first thing to add is the views/home.twig template:

    {% extends "layout.twig" %} {% block content %} <h1>Hi visitor!</h1>
    {% endblock %}

Now, as you might have already guessed, we will add the
views/layout.twig template, as follows:

    <html> <head> <title>Silex Example</title> </head> <body> {% block
    content %} {% endblock %} </body> </html>

Try accessing the home page of your application; you should get the
following result:

|image42|

Accessing the database
----------------------

For this section, we will write an endpoint that will create recipes for
our cookbook. Run the following MySQL queries in order to set up the
cookbook database and create the empty recipes table:

    mysql> CREATE SCHEMA cookbook; Query OK, 1 row affected (0.00 sec)
    mysql> USE cookbook; Database changed mysql> CREATE TABLE recipes(
    -> id INT UNSIGNED NOT NULL AUTO\_INCREMENT PRIMARY KEY, -> name
    VARCHAR(255) NOT NULL, -> ingredients TEXT NOT NULL, -> instructions
    TEXT NOT NULL, -> time INT UNSIGNED NOT NULL); Query OK, 0 rows
    affected (0.01 sec)

Silex does not come with any ORM integration, so you will need to write
your SQL queries by hand. However, there is a Doctrine service provider
that gives you a simpler interface than the one PDO offers, so let's try
to integrate it. To install this, run the following command:

    $ composer require "doctrine/dbal:~2.2"

Now, we are ready to register the service provider. As with the rest of
services, add the following code to your public/index.php before the
route definitions:

    $app->register(new Silex\\Provider\\DoctrineServiceProvider(), [
    'dbs.options' => [ [ 'driver' => 'pdo\_mysql', 'host' =>
    '127.0.0.1', 'dbname' => 'cookbook', 'user' =>
    $app['database']['user'], 'password' => $app['database']['password']
    ] ] ]);

When registering, you need to provide the options for the database
connection. Some of them will be the same regardless of the environment,
such as the driver or even the host, but some will come from the
configuration file, such as $app['database']['user']. From now on, you
can access the database connection via $app['db'].

With the database set up, let's add the routes that will allow us to add
and fetch recipes. As with Laravel, you can specify either the anonymous
function, as we already did, or a controller and method to execute.
Replace the current route with the following three routes:

    $app->get( '/',
    'CookBook\\\\Controllers\\\\RecipesController::getAll' );
    $app->post( '/recipes',
    'CookBook\\\\Controllers\\\\RecipesController::create' ); $app->get(
    '/recipes',
    'CookBook\\\\Controllers\\\\RecipesController::getNewForm' );

As you can observe, there will be a new controller,
CookBook\\Controllers\\RecipesController, which will be placed in
src/Controllers/RecipesController.php. This means that you need to
change the autoloader in Composer. Edit your composer.json file with the
following:

    "autoload": { "psr-4": {"CookBook\\\\": "src/"} }

Now, let's add the controller class, as follows:

    <?php namespace CookBook\\Controllers; class Recipes { }

The first method we will add is the getNewForm method, which will just
render the add a new recipe page. The method looks similar to this:

    public function getNewForm(Application $app): string { return
    $app['twig']->render('new\_recipe.twig'); }

The method will just render new\_recipe.twig. An example of this
template could be as follows:

    {% extends "layout.twig" %} {% block content %} <h1>Add recipe</h1>
    <form method="post"> <div> <label for="name">Name</label> <input
    type="text" name="name" value="{{ name is defined ? name : "" }}" />
    </div> <div> <label for="ingredients">Ingredients</label> <textarea
    name="ingredients"> {{ ingredients is defined ? ingredients : "" }}
    </textarea> </div> <div> <label
    for="instructions">Instructions</label> <textarea
    name="instructions"> {{ instructions is defined ? instructions : ""
    }} </textarea> </div> <div> <label for="time">Time (minutes)</label>
    <input type="number" name="time" value="{{ time is defined ? time :
    "" }}" /> </div> <div> <button type="submit">Save</button> </div>
    </form> {% endblock %}

This template sends the name, ingredients, instructions, and the time
that it takes to prepare the dish. The endpoint that will get this form
needs to get the response object in order to extract this information.
In the same way that we could get the Application instance as an
argument, we can get the Request one too if we specify it in the method
definition. Accessing the POST parameters is as easy as invoking the get
method by sending the name of the parameter or calling
$request->request->all() to get all of them as an array. Add the
following method that checks whether all the data is valid and renders
the form again if it is not, sending the submitted data and errors:

    public function create(Application $app, Request $request): string {
    $params = $request->request->all(); $errors = []; if
    (empty($params['name'])) { $errors[] = 'Name cannot be empty.'; } if
    (empty($params['ingredients'])) { $errors[] = 'Ingredients cannot be
    empty.'; } if (empty($params['instructions'])) { $errors[] =
    'Instructions cannot be empty.'; } if ($params['time'] <= 0) {
    $errors[] = 'Time has to be a positive number.'; } if
    (!empty($errors)) { $params = array\_merge($params, ['errors' =>
    $errors]); return $app['twig']->render('new\_recipe.twig', $params);
    } }

The layout.twig template needs to be edited too in order to show the
errors returned. We can do this by executing the following:

    {# ... #} {% if errors is defined %} <p>Something went wrong!</p>
    <ul> {% for error in errors %} <li>{{ error }}</li> {% endfor %}
    </ul> {% endif %} {% block content %} {# ... #}

At this point, you can already try to access http://localhost/recipes,
fill the form leaving something empty, submitting, and getting the form
back with the errors. It should look something similar to this (with
some extra CSS styles):

|image43|

The continuation of the controller should allow us to store the correct
data as a new recipe in the database. To do so, it would be a good idea
to create a separate class, such as CookBook\\Models\\RecipeModel;
however, to speed things up, let's add the following few lines that
would go into the model to the controller. Remember that we have the
Doctrine service provider, so there is no need to use PDO directly:

    $sql = 'INSERT INTO recipes (name, ingredients, instructions, time)
    ' . 'VALUES(:name, :ingredients, :instructions, :time)'; $result =
    $app['db']->executeUpdate($sql, $params); if (!$result) { $params =
    array\_merge($params, ['errors' => $errors]); return
    $app['twig']->render('new\_recipe.twig', $params); } return
    $app['twig']->render('home.twig');

Doctrine also helps when fetching data. To see it working, check the
third and final method, in which we will fetch all the recipes in order
to show the user:

    public function getAll(Application $app): string { $recipes =
    $app['db']->fetchAll('SELECT \* FROM recipes'); return
    $app['twig']->render( 'home.twig', ['recipes' => $recipes] ); }

With only one line, we performed a query. It is not as clean as the
Eloquent ORM of Laravel, but at least it is much less verbose than using
raw PDO. Finally, you can update your home.twig template with the
following content in order to display the recipes that we just fetched
from the database:

    {% extends "layout.twig" %} {% block content %} <h1>Hi visitor!</h1>
    <p>Check our recipes!</p> <table> <th>Name</th> <th>Time</th>
    <th>Ingredients</th> <th>Instructions</th> {% for recipe in recipes
    %} <tr> <td>{{ recipe.name }}</td> <td>{{ recipe.time }}</td> <td>{{
    recipe.ingredients }}</td> <td>{{ recipe.instructions }}</td> </tr>
    {% endfor %} </table> {% endblock %}

Silex versus Laravel
====================

Even though we did some similar comparison before starting the chapter,
it is time to recapitulate what we said and compare it with what you
noted by yourself. Laravel belongs to the type of framework that allows
you to create great things with very little work. It contains all the
components that you, as a web developer, will ever need. There has to be
some good reason for how fast it became the most popular framework of
the year!

On the other hand, Silex is a microframework, which by itself does very
little. It is just the skeleton on which you can build the framework
that you exactly need. It already provides quite a lot of service
providers, and we did not discuss even half of them; we recommend you to
visit
`*http://silex.sensiolabs.org/doc/providers.html* <http://silex.sensiolabs.org/doc/providers.html>`__
for the full list. However, if you prefer, you can always add other
dependencies with Composer and use them. If, for some reason, you stop
liking the ORM or the template engine that you use, or it just happens
that a new and better one appears in the community, switching them
should be easy. On the other hand, when working with Laravel, you will
probably stick to what it comes with it.

There is always an occasion for each framework, and we would like to
encourage you to be open to all the possibilities that there are out
there, keep up to date, and explore new frameworks or technologies from
time to time.

Summary
=======

In this chapter, you learned how important it is to know some of the
most important frameworks. You also learned the basics of two famous
ones: Laravel and Silex. Now, you are ready to either use your framework
or to use these two for your next application. With this, you also have
the capacity to take any other similar framework and understand it
easily.

In the next chapter, we will study what REST APIs are and how to write
one with Laravel. This will expand your set of skills and give you more
flexibility for when you need to decide which approach to take when
designing and writing applications.

Chapter 9. Building REST APIs

Most non-developers probably think that creating applications means
building either software for your PC or Mac, games, or web pages,
because that is what they can see and use. But once you join the
developers' community, either by your own or professionally, you will
eventually realize how much work is done for applications and tools that
do not have a user interface.

Have you ever wondered how someone's website can access your Facebook
profile, and later on, post an automatic message on your wall? Or how
websites manage to send/receive information in order to update the
content of the page, without refreshing or submitting any form? All of
these features, and many more interesting ones, are possible thanks to
the integration of applications working "behind the scenes". Knowing how
to use them will open the doors for creating more interesting and useful
web applications.

In this chapter, you will learn the following:

-  Introduction to APIs and REST APIs, and their use

-  The foundation of REST APIs

-  Using third-party APIs

-  Tools for REST API developers

-  Designing and writing REST APIs with Laravel

-  Different ways of testing your REST APIs

Introducing APIs
================

API stands for Application Program Interface. Its goal is to provide an
interface so that other programs can send commands that will trigger
some process inside the application, possibly returning some output. The
concept might seem a bit abstract, but in fact, there are APIs virtually
in everything which is somehow related to computers. Let's see some real
life examples:

-  Operating systems or OS, like Windows or Linux, are the programs that
   allow you to use computers. When you use any application from your
   computer, it most probably needs to talk to the OS in one way or
   another, for example by requesting a certain file, sending some audio
   to the speakers, and so on. All these interactions between the
   application and the OS are possible thanks to the APIs that the OS
   provides. In this way, the application need not interact with the
   hardware straight away, which is a very tiring task.

-  To interact with the user, a mobile application provides a GUI. The
   interface captures all the events that the user triggers, like
   clicking or typing, in order to send them to the server. The GUI
   communicates with the server using an API in the same way the program
   communicates with the OS as explained earlier.

-  When you create a website that needs to display tweets from the
   user's Twitter account, you need to communicate with Twitter. They
   provide an API that can be accessed via HTTP. Once authenticated, by
   sending the correct HTTP requests, you can update and/or retrieve
   data from their application.

As you can see, there are different places where APIs are useful. In
general, when you have a system that should be accessed externally, you
need to provide potential users an API. When we say externally, we mean
from another application or library, but it can very well be inside the
same machine.

Introducing REST APIs
=====================

REST APIs are a specific type of APIs. They use HTTP as the protocol to
communicate with them, so you can imagine that they will be the most
used ones by web applications. In fact, they are not very different from
the websites that you've already built, since the client sends an HTTP
request, and the server replies with an HTTP response. The difference
here is that REST APIs make heavy use of HTTP status codes to understand
what the response is, and instead of returning HTML resources with CSS
and JS, the response uses JSON, XML, or any other document format with
just information, and not a graphic user interface.

Let's take an example. The Twitter API, once authenticated, allows
developers to get the tweets of a given user by sending an HTTP GET
request to https://api.twitter.com/1.1/statuses/user\_timeline.json. The
response to this request is an HTTP message with a JSON map of tweets as
the body and the status code 200. We've already mentioned status code in
`*Chapter 2* <#Top_of_ch02_html>`__, *Web Applications with PHP*, but we
will review them shortly.

The REST API also allows developers to post tweets on behalf of the
user. If you were already authenticated, as in the previous example, you
just need to send a POST request to
https://api.twitter.com/1.1/statuses/update.json with the appropriate
POST parameters in the body, like the text that you want to tweet. Even
though this request is not a GET, and thus, you are not requesting data
but rather sending it, the response of this request is quite important
too. The server will use the status codes of the response to let the
requester know if the tweet was posted successfully, or if they could
not understand the request, there was an internal server error, the
authentication was not valid, and so on. Each of these scenarios has a
different status code, which is the same across all applications. This
makes it very easy to communicate with different APIs, since you will
not need to learn a new list of status code each time. The server can
also add some extra information to the body in order to throw some light
on why the error happened, but that will depend on the application.

You can imagine that these REST APIs are provided to developers so they
can integrate them with their applications. They are not user-friendly,
but HTTP-friendly.

The foundations of REST APIs
============================

Even though REST APIs do not have an official standard, most developers
agree on the same foundation. It helps that HTTP, which is the protocol
that this technology uses to communicate, does have a standard. In this
section, we will try to describe how REST APIs should work.

HTTP request methods
--------------------

We've already introduced the idea of HTTP methods in `*Chapter
2* <#Top_of_ch02_html>`__, *Web Applications with PHP*. We explained
that an HTTP method is just the verb of the request, which defines what
kind of action it is trying to perform. We've already defined this
method when working with HTML forms: the form tag can get an optional
attribute, method, which will make the form submit with that specific
HTTP method.

You will not use forms when working with REST APIs, but you can still
specify the method of the request. In fact, two requests can go to the
same endpoint with the same parameters, headers, and so on, and yet have
completely different behaviors due to their methods, which makes them a
very important part of the request.

As we are giving so much importance to HTTP methods in order to identify
what a request is trying to do, it is natural that we will need a
handful of them. So far, we have introduced GET and POST, but there are
actually eight different methods: GET, POST, PUT, DELETE, OPTIONS, HEAD,
TRACE, and CONNECT. You will usually work with just four of them. Let's
look at them in detail.

GET
~~~

When a request uses the GET method, it means that it is requesting for
information about a given entity. The endpoint should contain
information of what that entity is, like the ID of a book. GET can also
be used to query for a list of objects, either all of them, filtered, or
paginated.

GET requests can add extra information to the request when needed. For
example, if we are try to retrieve all the books that contain the string
"rings", or if we want the page number 2 of the full list of books. As
you already know, this extra information is added to the query string as
GET parameters, which is a list of key-value pairs concatenated by an
ampersand (&). So, that means that the request for
http://bookstore.com/books?year=2001&page3 is probably used for getting
the second page of the list of books published during 2001.

REST APIs have extensive documentation on the available endpoints and
parameters, so it should be easy for you to learn to query properly.
Still, even though it will be documented, you should expect parameters
with intuitive names, like the ones in the example.

POST and PUT
~~~~~~~~~~~~

POST is the second type of HTTP method that you already know about. You
used it in forms with the intention of "posting" data, that is, trying
to update a resource on the server side. When you wanted to add or
update a new book, you sent a POST request with the data of the book as
the POST parameters.

POST parameters are sent in a format similar to the GET parameters, but
instead of being part of the query string, they are included as part of
the request's body. Forms in HTML are already doing that for you, but
when you need to talk to a REST API, you should know how to do this by
yourself. In the next section, we will show you how to perform POST
using tools other than forms. Also note that you can add any data to the
body of the request; it is quite common to send JSON in the body instead
of POST parameters.

The PUT method is quite similar to the POST method. This too tries to
add or update data on the server side, and for this purpose, it also
adds extra information on the body of the request. Why should we have
two different methods that do the same thing? There are actually two
main differences between these methods:

-  PUT requests either create a resource or update it, but the affected
   resource is the one defined by the endpoint and nothing else. That
   means that if we want to update a book, the endpoint should state
   that the resource is a book, and specify it, for example,
   http://bookstore.com/books/8734. On the other hand, if you do not
   identify the resource to be created or updated in the endpoint, or
   you affect other resources at the same time, you should use POST
   requests.

-  Idempotent is a complicated word for a simple concept. An idempotent
   HTTP method is one that can be called many times, and the result will
   always be the same. For example, if you are trying to update the
   title of a book to "Don Quixote", it does not matter how many times
   you call it, the result will always be the same: the resource will
   have the title "Don Quixote". On the other hand, non-idempotent
   methods might return different results when executing the same
   request. An example could be an endpoint that increases the stock of
   some book. Each time you call it, you will increase the stock more
   and more, and thus, the result is not the same. PUT requests are
   idempotent, whereas POST requests are not.

Even with this explanation in mind, misusing POST and PUT is quite a
common mistake among developers, especially when they lack enough
experience in developing REST APIs. Since forms in HTML only send data
with POST and not PUT, the first one is more popular. You might find
REST APIs where all the endpoints that update data are POST, even though
some of them should be PUT.

DELETE
~~~~~~

The DELETE HTTP method is quite self-explanatory. It is used when you
want to delete a resource on the server. As with PUT requests, DELETE
endpoints should identify the specific resource to be deleted. An
example would be when we want to remove one book from our database. We
could send a DELETE request to an endpoint similar to
http://bookstore.com/books/23942.

DELETE requests just delete resources, and they are already determined
by the URL. Still, if you need to send extra information to the server,
you could use the body of the request as you do with POST or PUT. In
fact, you can always send information within the body of the request,
including GET requests, but that does not mean it is a good practice to
do so.

Status codes in responses
-------------------------

If HTTP methods are very important for requests, status codes are almost
indispensable for responses. With just one number, the client will know
what happened with the request. This is especially useful when you know
that status codes are a standard, and they are extensively documented on
the Internet.

We've already described the most important ones in `*Chapter
2* <#Top_of_ch02_html>`__, *Web Applications with PHP*, but let's list
them again, adding a few more that are important for REST APIs. For the
full list of status codes, you can visit
`*https://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html* <https://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html>`__.

2xx – success
~~~~~~~~~~~~~

All the status codes that start with 2 are used for responses where the
request was processed successfully, regardless of whether it was a GET
or POST. Some of the most commonly used ones in this category are as
follows:

-  200 OK: It is the generic "everything was OK" response. If you were
   asking for a resource, you will get it in the body of the response,
   and if you were updating a resource, this will mean that the new data
   has been successfully saved.

-  201 created: It is the response used when resources are created
   successfully with POST or PUT.

-  202 accepted: This response means that the request has been accepted,
   but it has not been processed yet. This might be useful when the
   client needs a straightforward response for a very heavy operation:
   the server sends the accepted response, and then starts processing
   it.

3xx – redirection
~~~~~~~~~~~~~~~~~

Even though you might think there is only one type of redirection, there
are a few refinements:

-  301 moved permanently: This means that the resource has been moved to
   a different URL, so from then on, you should try to access it through
   the URL provided in the body of the response.

-  303 see other: This means that the request has been processed but, in
   order to see the response, you need to access the URL provided in the
   body of the response.

4xx – client error
~~~~~~~~~~~~~~~~~~

This category has status codes describing what went wrong due to the
client's request:

-  400 bad request: This is a generic response to a malformed request,
   that is, there is a syntax error in the endpoint, or some of the
   expected parameters were not provided.

-  401 unauthorized: This means the client has not been authenticated
   successfully yet, and the resource that it is trying to access needs
   this authentication.

-  403 forbidden: This error message means that even though the client
   has been authenticated, it does not have enough permissions to access
   that resource.

-  404 not found: The specific resource has not been found.

-  405 method not allowed: This means that the endpoint exists, but it
   does not accept the HTTP method used on the request, for example, we
   were trying to use PUT, but the endpoint only accepts POST requests.

5xx – server error
~~~~~~~~~~~~~~~~~~

There are up to 11 different errors on the server side, but we are only
interested in one: the 500 internal server error. You could use this
status code when something unexpected, like a database error, happens
while processing the request.

REST API security
-----------------

REST APIs are a powerful tool since they allow developers to retrieve
and/or update data from the server. But with great power comes great
responsibility, and when designing a REST API, you should think about
making your data as secure as possible. Imagine— anyone could post
tweets on your behalf with a simple HTTP request!

Similar to using web applications, there are two concepts here:
authentication and authorization. Authenticating someone is identifying
who he or she is, that is, linking his or her request to a user in the
database. On the other hand, authorizing someone is to allow that
specific user to perform certain actions. You could think of
authentication as the login of the user, and authorization as giving
permissions.

REST APIs need to manage these two concepts very carefully. Just because
a developer has been authenticated does not mean he can access all the
data on the server. Sometimes, users can access only their own data,
whereas sometimes you would like to implement a roles system where each
role has different access levels. It always depends on the type of
application you are building.

Although authorization happens on the server side, that is, it's the
server's database that will decide whether a given user can access a
certain resource or not, authentications have to be triggered by the
client. This means that the client has to know what authentication
system the REST API is using in order to proceed with the
authentication. Each REST API will implement its own authentication
system, but there are some well known implementations.

Basic access authentication
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Basic access authentication—BA for short—is, as its name suggests,
basic. The client adds the information about the user in the headers of
each request, that is, username and password. The problem is that this
information is only encoded using BASE64 but not encrypted, making it
extremely easy for an intruder to decode the header and obtain the
password in plain text. If you ever have to use it, since, to be honest,
it is a very easy way of implementing some sort of authentication, we
would recommend you to use it with HTTPS.

In order to use this method, you need to concatenate the username and
password like username:password, encode the resultant string using
Base64, and add the authorization header as:

    Authorization: Basic <encoded-string>

OAuth 2.0
~~~~~~~~~

If basic authentication was very simple, and insecure, OAuth 2.0 is the
most secure system that REST APIs use in order to authenticate, and so
was the previous OAuth 1.0. There are actually different versions of
this standard, but all of them work on the same foundation:

1. There are no usernames and passwords. Instead, the provider of the
   REST API assigns a pair of credentials—a token and the secret—to the
   developer.

2. In order to authenticate, the developer needs to send a POST request
   to the "token" endpoint, which is different in each REST API but has
   the same concept. This request has to include the encoded developer
   credentials.

3. The server replies to the previous request with a session token. This
   (and not the credentials mentioned in the first step) is to be
   included in each request that you make to the REST API. The session
   token expires for security reasons, so you will have to repeat the
   second step again when that happens.

Even though this standard is kind of recent (2012 onwards), several big
companies like Google or Facebook have already implemented it for their
REST APIs. It might look a bit overcomplicated, but you will soon get to
use it, and even implement it.

Using third-party APIs
======================

That was enough theory about REST APIs; it is time to dive into a real
world example. In this section, we will write a small PHP application
that interacts with Twitter's REST API; that includes requesting
developer credentials, authenticating, and sending requests. The goal is
to give you your first experience in working with REST APIs, and showing
you that it is easier than you could expect. It will also help you to
understand better how they work, so it will be easier to build your own
later.

Getting the application's credentials
-------------------------------------

REST APIs usually have the concept of application. An application is
like an account on their development site that identifies who uses the
API. The credentials that you will use to access the API will be linked
to this application, which means that you can have multiple applications
linked to the same account.

Assuming that you have a Twitter account, go to
`*https://apps.twitter.com* <https://apps.twitter.com>`__ in order to
create a new application. Click on the Create New App button in order to
access the form for application details. The fields are very
self-explanatory—just a name for the application, the description, and
the website URL. The callback URL is not necessary here, since that will
be used only for applications that require access to someone else's
account. Agree with the terms and conditions in order to proceed.

Once you have been redirected to your application's page, you will see
all sort of information that you can edit. Since this is just an
example, let's go straight to what matters: the credentials. Click on
the Keys and Access Tokens tab to see the values of Consumer key (API
key) and Consumer Secret (API secret). There is nothing else that we
need from here. You can save them on your filesystem, as
~/.twitter\_php7.json, for example:

    { "key": "iTh4Mzl0EAPn9HAm98hEhAmVEXS", "secret":
    "PfoWM9yq4Bh6rGbzzJhr893j4r4sMIAeVRaPMYbkDer5N6F" }

Tip

Securing your credentials

Securing your REST API credentials should be taken seriously. In fact,
you should take care of all kinds of credentials, like the database
ones. But the difference is that you will usually host your database in
your server, which makes things slightly more difficult to whoever wants
to attack. On the other hand, the third-party REST API is not part of
your system, and someone with your credentials can use your account
freely on your behalf.

Never include your credentials in your code base, especially if you have
your code in GitHub or some other repository. One solution would be to
have a file in your server, outside your code, with the credentials; if
that file is encrypted, that is even better. And try to refresh your
credentials regularly, which you can probably do on the provider's
website.

Setting up the application
--------------------------

Our application will be extremely simple. It will consist of one class
that will allow us to fetch tweets. This will be managed by our app.php
script.

As we have to make HTTP requests, we can either write our own functions
that use cURL (a set of PHP native functions), or make use of the famous
PHP library, Guzzle. This library can be found in Packagist, so we will
use Composer to include it:

    $ composer require guzzlehttp/guzzle

We will have a Twitter class, which will get the credentials from the
constructor, and one public method: fetchTwits. For now, just create the
skeleton so that we can work with it; we will implement such methods in
later sections. Add the following code to src/Twitter.php:

    <?php namespace TwitterApp; class Twitter { private $key; private
    $secret; public function \_\_construct(String $key, String $secret)
    { $this->key = $key; $this->secret = $secret; } public function
    fetchTwits(string name, int $count): array { return []; } }

Since we set the namespace TwitterApp, we need to update our
composer.json file with the following addition. Remember to run composer
update to update the autoloader.

    "autoload": { "psr-4": {"TwitterApp\\\\": "src"} }

Finally, we will create a basic app.php file, which includes the
Composer autoloader, reads the credentials file, and creates a Twitter
instance:

    <?php use TwitterApp\\Twitter; require \_\_DIR\_\_ .
    '/vendor/autoload.php'; $path = $\_SERVER['HOME'] .
    '/.twitter\_php7.json'; $jsonCredentials =
    file\_get\_contents($path); $credentials =
    json\_decode($jsonCredentials, true); $twitter = new
    Twitter($credentials['key'], $credentials['secret']);

Requesting an access token
--------------------------

In a real world application, you would probably want to separate the
code related to authentication from the one that deals with operations
like fetching or posting data. To keep things simple here, we will let
the Twitter class know how to authenticate by itself.

Let's start by adding a $client property to the class which will contain
an instance of Guzzle's Client class. This instance will contain the
base URI of the Twitter API, which we can have as the constant
TWITTER\_API\_BASE\_URI. Instantiate this property in the constructor so
that the rest of the methods can make use of it. You can also add an
$accessToken property which will contain the access token returned by
the Twitter API when authenticating. All these changes are highlighted
here:

    <?php namespace TwitterApp; use Exception; use GuzzleHttp\\Client;
    class Twitter { const TWITTER\_API\_BASE\_URI =
    'https://api.twitter.com'; private $key; private $secret; private
    $accessToken; private $client; public function \_\_construct(String
    $key, String $secret) { $this->key = $key; $this->secret = $secret;
    $this->client = new Client( ['base\_uri' =>
    self::TWITTER\_API\_BASE\_URI] ); } //... }

The next step would be to write a method that, given the key and secret
are provided, requests an access token to the provider. More
specifically:

-  Concatenate the key and the secret with a :. Encode the result using
   Base64.

-  Send a POST request to /oauth2/token with the encoded credentials as
   the Authorization header. Also include a Content-Type header and a
   body (check the code for more information).

We now invoke the post method of Guzzle's client instance sending two
arguments: the endpoint string (/oauth2/token) and an array with
options. These options include the headers and the body of the request,
as you will see shortly. The response of this invocation is an object
that identifies the HTTP response. You can extract the content (body) of
the response with getBody. Twitter's API response is a JSON with some
arguments. The one that you care about the most is the access\_token,
the token that you will need to include in each subsequent request to
the API. Extract it and save it. The full method looks as follows:

    private function requestAccessToken() { $encodedString =
    base64\_encode( $this->key . ':' . $this->secret ); $headers = [
    'Authorization' => 'Basic ' . $encodedString, 'Content-Type' =>
    'application/x-www-form-urlencoded;charset=UTF-8' ]; $options = [
    'headers' => $headers, 'body' => 'grant\_type=client\_credentials'
    ]; $response = $this->client->post(self:: OAUTH\_ENDPOINT,
    $options); $body = json\_decode($response->getBody(), true);
    $this->accessToken = $body['access\_token']; }

You can already try this code by adding these two lines at the end of
the constructor:

    $this->requestAccessToken(); var\_dump($this->accessToken);

Run the application in order to see the access token given by the
provider using the following command. Remember to remove the preceding
two lines in order to proceed with the section.

    $ php app.php

Keep in mind that, even though having a key and secret and getting an
access token is the same across all OAuth authentications, the specific
way of encoding, the endpoint used, and the response received from the
provider are exclusive from Twitter's API. It could be that several
others are exactly the same, but always check the documentation for each
one.

Fetching tweets
---------------

We finally arrive to the section where we actually make use of the API.
We will implement the fetchTwits method in order to get a list of the
last *N* number of tweets for a given user. In order to perform
requests, we need to add the Authorization header to each one, this time
with the access token. Since we want to make this class as reusable as
possible, let's extract this to a private method:

    private function getAccessTokenHeaders(): array { if
    (empty($this->accessToken)) { $this->requestAccessToken(); } return
    ['Authorization' => 'Bearer ' . $this->accessToken]; }

As you can see, the preceding method also allows us to fetch the access
token from the provider. This is useful, since if we make more than one
request, we will just request the access token once, and we have one
unique place to do so. Add now the following method implementation:

    const GET\_TWITS = '/1.1/statuses/user\_timeline.json'; //... public
    function fetchTwits(string $name, int $count): array { $options = [
    'headers' => $this->getAccessTokenHeaders(), 'query' => [ 'count' =>
    $count, 'screen\_name' => $name ] ]; $response =
    $this->client->get(self::GET\_TWITS, $options); $responseTwits =
    json\_decode($response->getBody(), true); $twits = []; foreach
    ($responseTwits as $twit) { $twits[] = [ 'created\_at' =>
    $twit['created\_at'], 'text' => $twit['text'], 'user' =>
    $twit['user']['name'] ]; } return $twits; }

The first part of the preceding method builds the options array with the
access token headers and the query string arguments—in this case, with
the number of tweets to retrieve and the user. We perform the GET
request and decode the JSON response into an array. This array contains
a lot of information that we might not need, so we iterate it in order
to extract those fields that we really want—in this example, the date,
the text, and the user.

In order to test the application, just invoke the fetchTwits method at
the end of the app.php file, specifying the Twitter ID of one of the
people you are following, or yourself.

    $twits = $twitter->fetchTwits('neiltyson', 10); var\_dump($twits);

You should get a response similar to ours, shown in the following
screenshot:

|image44|

One thing to keep in mind is that access tokens expire after some time,
returning an HTTP response with a 4xx status code (usually, 401
unauthorized). Guzzle throws an exception when the status code is either
4xx or 5xx, so it is easy manage these scenarios. You could add this
code when performing the GET request:

    try { $response = $this->client->get(self::GET\_TWITS, $options); }
    catch (ClientException $e) { if ($e->getCode() == 401) {
    $this->requestAccessToken(); $response =
    $this->client->get(self::GET\_TWITS, $options); } else { throw $e; }
    }

The toolkit of the REST API developer
=====================================

While you are developing your own REST API, or writing an integration
for a third-party one, you might want to test it before you start
writing your code. There are a handful of tools that will help you with
this task, whether you want to use your browser, or you are a fan of the
command line.

Testing APIs with browsers
--------------------------

There are actually several add-ons that allow you to perform HTTP
requests from browsers, depending on which one you use. Some famous
names are *Advanced Rest Client* for Chrome and *RESTClient* for
Firefox. At the end of the day, all those clients allow you to perform
the same HTTP requests, where you can specify the URL, the method, the
headers, the body, and so on. These clients will also show you all the
details you can imagine from the response, including the status code,
the time spent, and the body. The following screenshot displays an
example of a request using Chrome's *Advanced Rest Client*:

|image45|

If you want to test GET requests with your own API, and all that you
need is the URL, that is, you do not need to send any headers, you can
just use your browser as if you were trying to access any other website.
If you do so, and if you are working with JSON responses, you can
install another add-on to your browser that will help you in viewing
your JSON in a more "beautiful" way. Look for *JSONView* on any browser
for a really handy one.

Testing APIs using the command line
-----------------------------------

Some people feel more comfortable using the command line; so luckily,
for them there are tools that allow them to perform any HTTP request
from their consoles. We will give a brief introduction to one of the
most famous ones: cURL. This tool has quite a lot of features, but we
will focus only on the ones that you will be using more often: the HTTP
method, post parameters, and headers:

-  -X <method>: This specifies the HTTP method to use

-  --data: This adds the parameters specified, which can be added as
   key-value pairs, JSON, plain text, and so on

-  --header: This adds a header to the request

The following is an example of the way to send a POST request with cURL:

    curl -X POST --data "text=This is sparta!" \\ > --header
    "Authorization: Bearer 8s8d7bf8asdbf8sbdf8bsa" \\ >
    https://api.twitter.com/1.1/statuses/update.json
    {"errors":[{"code":89,"message":"Invalid or expired token."}]}

If you are using a Unix system, you will probably be able to format the
resulting JSON by appending \| python -m json.tool so that it gets
easier to read:

    $ curl -X POST --data "text=This is sparta!" \\ > --header
    "Authorization: Bearer 8s8d7bf8asdbf8sbdf8bsa" \\ >
    https://api.twitter.com/1.1/statuses/update.json \\ > \| python -m
    json.tool { "errors": [ { "code": 89, "message": "Invalid or expired
    token." } ] }

cURL is quite a powerful tool that lets you do quite a few tricks. If
you are interested, go ahead and check the documentation or some
tutorial on how to use all its features.

Best practices with REST APIs
=============================

We've already gone through some of the best practices when writing REST
APIs, like using HTTP methods properly, or choosing the correct status
code for your responses. We also described two of the most used
authentication systems. But there is still a lot to learn about creating
proper REST APIs. Remember that they are meant to be used by developers
like yourself, so they will always be grateful if you do things
properly, and make their lives easier. Ready?

Consistency in your endpoints
-----------------------------

When deciding how to name your endpoints, try keeping them consistent.
Even though you are free to choose, there is a set of spoken rules that
will make your endpoints more intuitive and easy to understand. Let's
list some of them:

-  For starters, an endpoint should point to a specific resource (for
   example, books or tweets), and you should make that clear in your
   endpoint. If you have an endpoint that returns the list of all books,
   do not name it /library, as it is not obvious what it will be
   returning. Instead, name it /books or /books/all.

-  The name of the resource can be either plural or singular, but make
   it consistent. If sometimes you use /books and sometimes /user, it
   might be confusing, and people will probably make mistakes. We
   personally prefer to use the plural form, but that is totally up to
   you.

-  When you want to retrieve a specific resource, do it by specifying
   the ID whenever possible. IDs must be unique in your system, and any
   other parameter might point to two different entities. Specify the ID
   next to the name of the resource, such as /books/249234-234-23-42.

-  If you can understand what an endpoint does by just the HTTP method,
   there is no need to add this information as part of the endpoint. For
   example, if you want to get a book, or you want to delete it,
   /books/249234-234-23-42 along with the HTTP methods GET and DELETE
   are more than enough. If it is not obvious, state it as a verb at the
   end of the endpoint, like /employee/9218379182/promote.

Document as much as you can
---------------------------

The title says everything. You are probably not going to be the one
using the REST API, others will. Obviously, even if you design a very
intuitive set of endpoints, developers will still need to know the whole
set of available endpoints, what each of them does, what optional
parameters are available, and so on.

Write as much documentation as possible, and keep it up to date. Take a
look at other documented APIs to gather ideas on how to display the
information. There are plenty of templates and tools that will help you
deliver a well-presented documentation, but you are the one that has to
be consistent and methodical. Developers have a special hate towards
documenting anything, but we also like to find clear and beautifully
presented documentation when we need to use someone else's APIs.

Filters and pagination
----------------------

One of the common usages of an API is to list resources and filter them
by some criteria. We already saw an example when we were building our
own bookstore; we wanted to get the list of books that contained a
certain string in their titles or authors.

Some developers try to have beautiful endpoints, which a priori is a
good thing to do. Imagine that you want to filter just by title, you
might end up having an endpoint like /books/title/<string>. We add also
the ability to filter by author, and we now get two more endpoints:
/books/title/<string>/author/<string> and /books/author/<string>. Now
let's add the description too—do you see where we are going?

Even though some developers do not like to use query strings as
arguments, there is nothing wrong with it. In fact, if you use them
properly, you will end up with cleaner endpoints. You want to get books?
Fine, just use /books, and add whichever filter you need using the query
string.

Pagination occurs when you have way too many resources of the same type
to retrieve all at once. You should think of pagination as another
optional filter to be specified as a GET parameter. You should have
pages with a default size, let's say 10 books, but it is a good idea to
give the developers the ability to define their own size. In this case,
developers can specify the length and the number of pages to retrieve.

API versioning
--------------

Your API is a reflection of what your application can do. Chances are
that your code will evolve, improving the already existing features or
adding new ones. Your API should be updated too, exposing those new
features, updating existing endpoints, or even removing some of them.

Imagine now that someone else is using your REST API, and their whole
website relies on it. If you change your existing endpoints, their
website will stop working! They will not be happy at all, and will try
to find someone else that can do what you were doing. Not a good
scenario, but then, how do you improve your API?

The solution is to use versioning. When you release a new version of the
API, do not nuke down the existing one; you should give some time to the
users to upgrade their integrations. And how can two different versions
of the API coexist? You already saw one of the options—the one that we
recommend you: by specifying the version of the API to use as part of
the endpoint. Do you remember the endpoint of the Twitter API
/1.1/statuses/user\_timeline.json? The 1.1 refers to the version that we
want to use.

Using HTTP cache
----------------

If the main feature of REST APIs is that they make heavy use of HTTP,
why not take advantage of HTTP cache? Well, there are actual reasons for
not using it, but most of them are due to a lack of knowledge about
using it properly. It is out of the scope of this book to explain every
single detail of its implementation, but let's try to give a short
introduction to the topic. Plenty of resources on the Internet can help
you to understand the parts that you are more interested in.

HTTP responses can be divided as public and private. Public responses
are shared between all users of the API, whereas the private ones are
meant to be unique for each user. You can specify which type of response
is yours using the Cache-Control header, allowing the response to be
cached if the method of the request was a GET. This header can also
expose the expiration of the cache, that is, you can specify the
duration for which your response will remain the same, and thus, can be
cached.

Other systems rely on generating a hash of the representation of a
resource, and add it as the ETag (Entity tag) header in order to know if
the resource has changed or not. In a similar way, you can set the
Last-Modified header to let the client know when was the last time that
the given resource changed. The idea behind those systems is to identify
when the client already contains valid data. If so, the provider does
not process the request, but returns an empty response with the status
code 304 (not modified) instead. When the client gets that response, it
uses its cached content.

Creating a REST API with Laravel
================================

In this section, we will build a REST API with Laravel from scratch.
This REST API will allow you to manage different clients at your
bookstore, not only via the browser, but via the UI as well. You will be
able to perform pretty much the same actions as before, that is, listing
books, buying them, borrowing for free, and so on.

Once the REST API is done, you should remove all the business logic from
the bookstore that you built during the previous chapters. The reason is
that you should have one unique place where you can actually manipulate
your databases and the REST API, and the rest of the applications, like
the web one, should able to communicate with the REST API for managing
data. In doing so, you will be able to create other applications for
different platforms, like mobile apps, that will use the REST API too,
and both the website and the mobile app will always be synchronized,
since they will be using the same sources.

As with our previous Laravel example, in order to create a new project,
you just need to run the following command:

    $ laravel new bookstore\_api

Setting OAuth2 authentication
-----------------------------

The first thing that we are going to implement is the authentication
layer. We will use OAuth2 in order to make our application more secure
than basic authentication. Laravel does not provide support for OAuth2
out of the box, but there is a service provider which does that for us.

Installing OAuth2Server
~~~~~~~~~~~~~~~~~~~~~~~

To install OAuth2, add it as a dependency to your project using
Composer:

    $ composer require "lucadegasperi/oauth2-server-laravel:5.1.\*"

This service provider needs quite a few changes. We will go through them
without going into too much detail on how things work exactly. If you
are more interested in the topic, or if you want to create your own
service providers for Laravel, we recommend you to go though the
extensive official documentation.

To start with, we need to add the new OAuth2Server service provider to
the array of providers in the config/app.php file. Add the following
lines at the end of the providers array:

    /\* \* OAuth2 Server Service Providers... \*/
    LucaDegasperi\\OAuth2Server\\Storage\\FluentStorageServiceProvider::class,
    LucaDegasperi\\OAuth2Server\\OAuth2ServerServiceProvider::class,

In the same way, you need to add a new alias to the aliases array in the
same file:

    'Authorizer' =>
    LucaDegasperi\\OAuth2Server\\Facades\\Authorizer::class,

Let's move to the app/Http/Kernel.php file, where we need to make some
changes too. Add the following entry to the $middleware array property
of the Kernel class:

    \\LucaDegasperi\\OAuth2Server\\Middleware\\OAuthExceptionHandlerMiddleware::class,

Add the following key-value pairs to the $routeMiddleware array property
of the same class:

    'oauth' =>
    \\LucaDegasperi\\OAuth2Server\\Middleware\\OAuthMiddleware::class,
    'oauth-user' =>
    \\LucaDegasperi\\OAuth2Server\\Middleware\\OAuthUserOwnerMiddleware::class,
    'oauth-client' =>
    \\LucaDegasperi\\OAuth2Server\\Middleware\\OAuthClientOwnerMiddleware::class,
    'check-authorization-params' =>
    \\LucaDegasperi\\OAuth2Server\\Middleware\\CheckAuthCodeRequestMiddleware::class,
    'csrf' => \\App\\Http\\Middleware\\VerifyCsrfToken::class,

We added a CSRF token verifier to the $routeMiddleware, so we need to
remove the one already defined in $middlewareGroups, since they are
incompatible. Use the following line to do so:

    \\App\\Http\\Middleware\\VerifyCsrfToken::class,

Setting up the database
~~~~~~~~~~~~~~~~~~~~~~~

Let's set up the database now. In this section, we will assume that you
already have the bookstore database in your environment. If you do not
have it, go back to `*Chapter 5* <#Top_of_ch05_html>`__, *Using
Databases*, to create it in order to proceed with this setup.

The first thing to do is to update the database credentials in the .env
file. They should look something similar to the following lines, but
with your username and password:

    DB\_HOST=localhost DB\_DATABASE=bookstore DB\_USERNAME=root
    DB\_PASSWORD=

In order to prepare the configuration and database migration files from
the OAuth2Server service provider, we need to publish it. In Laravel,
you do it by executing the following command:

    $ php artisan vendor:publish

Now the database/migrations directory contains all the necessary
migration files that will create the necessary tables related to OAuth2
in our database. To execute them, we run the following command:

    $ php artisan migrate

We need to add at least one client to the oauth\_clients table, which is
the table that stores the key and secrets for all clients that want to
connect to our REST API. This new client will be the one that you will
use during the development process in order to test what you have done.
We can set a random ID—the key—and the secret as follows:

    mysql> INSERT INTO oauth\_clients(id, secret, name) ->
    VALUES('iTh4Mzl0EAPn90sK4EhAmVEXS', ->
    'PfoWM9yq4Bh6rGbzzJhr8oDDsNZwGlsMIAeVRaPM', -> 'Toni'); Query OK, 1
    row affected, 1 warning (0.00 sec)

Enabling client-credentials authentication
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Since we published the plugins in vendor in the previous step, now we
have the configuration files for the OAuth2Server. This plugin allows us
different authentication systems (all of them with OAuth2), depending on
our necessities. The one that we are interested in for our project is
the client\_credentials type. To let Laravel know, add the following
lines at the end of the array in the config/oauth2.php file:

    'grant\_types' => [ 'client\_credentials' => [ 'class' =>
    '\\League\\OAuth2\\Server\\Grant\\ClientCredentialsGrant',
    'access\_token\_ttl' => 3600 ] ]

These preceding lines grant access to the client\_credentials type,
which are managed by the ClientCredentialsGrant class. The
access\_token\_ttl value refers to the time period of the access token,
that is, for how long someone can use it. In this case, it is set to 1
hour, that is, 3,600 seconds.

Finally, we need to enable a route so we can post our credentials in
exchange for an access token. Add the following route to the routes file
in app/Http/routes.php:

    Route::post('oauth/access\_token', function() { return
    Response::json(Authorizer::issueAccessToken()); });

Requesting an access token
~~~~~~~~~~~~~~~~~~~~~~~~~~

It is time to test what we have done so far. To do so, we need to send a
POST request to the /oauth/access\_token endpoint that we enabled just
now. This request needs the following POST parameters:

-  client\_id with the key from the database

-  client\_secret with the secret from the database

-  grant\_type to specify the type of authentication that we are trying
   to perform, in this case client\_credentials

The request issued using the *Advanced REST Client* add-on from Chrome
looks as follows:

|image46|

The response that you should get should have the same format as this
one:

    { "access\_token": "MPCovQda354d10zzUXpZVOFzqe491E7ZHQAhSAax"
    "token\_type": "Bearer" "expires\_in": 3600 }

Note that this is a different way of requesting for an access token than
what the Twitter API does, but the idea is still the same: given a key
and a secret, the provider gives us an access token that will allow us
to use the API for some time.

Preparing the database
----------------------

Even though we've already done the same in the previous chapter, you
might think: "Why do we start by preparing the database?". We could
argue that you first need to know the kind of endpoints you want to
expose in your REST API, and only then you can start thinking about what
your database should look like. But you could also think that, since we
are working with an API, each endpoint should manage one resource, so
first you need to define the resources you are dealing with. This *code
first versus database/model first* is an ongoing war on the Internet.
But whichever way you think is better, the fact is that we already know
what the users will need to do with our REST API, since we already built
the UI previously; so it does not really matter.

We need to create four tables: books, sales, sales\_books, and
borrowed\_books. Remember that Laravel already provides a users table,
which we can use as our customers. Run the following four commands to
create the migrations files:

    $ php artisan make:migration create\_books\_table --create=books $
    php artisan make:migration create\_sales\_table --create=sales $ php
    artisan make:migration create\_borrowed\_books\_table \\
    --create=borrowed\_books $ php artisan make:migration
    create\_sales\_books\_table \\ --create=sales\_books

Now we have to go file by file to define what each table should look
like. We will try to replicate the data structure from `*Chapter
5* <#Top_of_ch05_html>`__, *Using Databases*, as much as possible.
Remember that the migration files can be found inside the
database/migrations directory. The first file that we can edit is the
create\_books\_table.php. Replace the existing empty up method by the
following one:

    public function up() { Schema::create('books', function (Blueprint
    $table) { $table->increments('id');
    $table->string('isbn')->unique(); $table->string('title');
    $table->string('author'); $table->smallInteger('stock')->unsigned();
    $table->float('price')->unsigned(); }); }

The next one in the list is create\_sales\_table.php. Remember that this
one has a foreign key pointing to the users table. You can use
references(field)->on(tablename) to define this constraint.

    public function up() { Schema::create('sales', function (Blueprint
    $table) { $table->increments('id');
    $table->string('user\_id')->references('id')->on('users');
    $table->timestamps(); }); }

The create\_sales\_books\_table.php file contains two foreign keys: one
pointing to the ID of the sale, and one to the ID of the book. Replace
the existing up method by the following one:

    public function up() { Schema::create('sales\_books', function
    (Blueprint $table) { $table->increments('id');
    $table->integer('sale\_id')->references('id')->on('sales');
    $table->integer('book\_id')->references('id')->on('books');
    $table->smallInteger('amount')->unsigned(); }); }

Finally, edit the create\_borrowed\_books\_table.php file, which has the
book\_id foreign key and the start and end timestamps:

    public function up() { Schema::create('borrowed\_books', function
    (Blueprint $table) { $table->increments('id');
    $table->integer('book\_id')->references('id')->on('books');
    $table->string('user\_id')->references('id')->on('users');
    $table->timestamp('start'); $table->timestamp('end'); }); }

The migration files are ready so we just need to migrate them in order
to create the database tables. Run the following command:

    $ php artisan migrate

Also, add some books to the database manually so that you can test
later. For example:

    mysql> INSERT INTO books (isbn,title,author,stock,price) VALUES ->
    ("9780882339726","1984","George Orwell",12,7.50), ->
    ("9789724621081","1Q84","Haruki Murakami",9,9.75), ->
    ("9780736692427","Animal Farm","George Orwell",8,3.50), ->
    ("9780307350169","Dracula","Bram Stoker",30,10.15), ->
    ("9780753179246","19 minutes","Jodi Picoult",0,10); Query OK, 5 rows
    affected (0.01 sec) Records: 5 Duplicates: 0 Warnings: 0

Setting up the models
---------------------

The next thing to do on the list is to add the relationships that our
data has, that is, to translate the foreign keys from the database to
the models. First of all, we need to create those models, and for that
we just run the following commands:

    $ php artisan make:model Book $ php artisan make:model Sale $ php
    artisan make:model BorrowedBook $ php artisan make:model SalesBook

Now we have to go model by model, and add the one to one and one to many
relationships as we did in the previous chapter. For BookModel, we will
only specify that the model does not have timestamps, since they come by
default. To do so, add the following highlighted line to your
app/Book.php file:

    <?php namespace App; use Illuminate\\Database\\Eloquent\\Model;
    class Book extends Model { public $timestamps = false; }

For the BorrowedBook model, we need to specify that it has one book, and
it belongs to a user. We also need to specify the fields we will fill
once we need to create the object—in this case, book\_id and start. Add
the following two methods in app/BorrowedBook.php:

    <?php namespace App; use Illuminate\\Database\\Eloquent\\Model;
    class BorrowedBook extends Model { protected $fillable =
    ['user\_id', 'book\_id', 'start']; public $timestamps = false;
    public function user() { return $this->belongsTo('App\\User'); }
    public function book() { return $this->hasOne('App\\Book'); } }

Sales can have many "sale books" (we know it might sound a little
awkward), and they also belong to just one user. Add the following to
your app/Sale.php:

    <?php namespace App; use Illuminate\\Database\\Eloquent\\Model;
    class Sale extends Model { protected $fillable = ['user\_id'];
    public function books() { return $this->hasMany('App\\SalesBook'); }
    public function user() { return $this->belongsTo('App\\User'); } }

Like borrowed books, sale books can have one book and belong to one sale
instead of to one user. The following lines should be added to
app/SalesBook.php:

    <?php namespace App; use Illuminate\\Database\\Eloquent\\Model;
    class SaleBook extends Model { public $timestamps = false; protected
    $fillable = ['book\_id', 'sale\_id', 'amount']; public function
    sale() { return $this->belongsTo('App\\Sale'); } public function
    books() { return $this->hasOne('App\\Book'); } }

Finally, the last model that we need to update is the User model. We
need to add the opposite relationship to the belongs we used earlier in
Sale and BorrowedBook. Add these two functions, and leave the rest of
the class intact:

    <?php namespace App; use Illuminate\\Foundation\\Auth\\User as
    Authenticatable; class User extends Authenticatable { //... public
    function sales() { return $this->hasMany('App\\Sale'); } public
    function borrowedBooks() { return
    $this->hasMany('App\\BorrowedBook'); } }

Designing endpoints
-------------------

In this section, we need to come up with the list of endpoints that we
want to expose to the REST API clients. Keep in mind the "rules"
explained in the *Best practices with REST APIs* section. In short, keep
the following rules in mind:

-  One endpoint interacts with one resource

-  A possible schema could be <API version>/<resource name>/<optional
   id>/<optional action>

-  Use GET parameters for filtering and pagination

So what will the user need to do? We already have a good idea about
that, since we created the UI. A brief summary would be as follows:

-  List all the available books with some filtering (by title and
   author), and paginated when necessary. Also retrieve the information
   on a specific book, given the ID.

-  Allow the user to borrow a specific book if available. In the same
   way, the user should be able to return books, and list the history of
   borrowed books too (filtered by date and paginated).

-  Allow the user to buy a list of books. This could be improved, but
   for now let's force the user to buy books with just one request,
   including the full list of books in the body. Also, list the sales of
   the user following the same rules as that with borrowed books.

We will start straightaway with our list of endpoints, specifying the
path, the HTTP method, and the optional parameters. It will also give
you an idea on how to document your REST APIs.

-  GET /books

   -  title: Optional and filters by title

   -  author: Optional and filters by author

   -  page: Optional, default is 1, and specifies the page to return

   -  page-size: Optional, default is 50, and specifies the page size to
      return

-  GET /books/<book id>

-  POST /borrowed-books

   -  book-id: Mandatory and specifies the ID of the book to borrow

-  GET /borrowed-books

   -  from: Optional and returns borrowed books from the specified date

   -  page: Optional, default is 1, and specifies the page to return

   -  page-size: Optional, default is 50, and specifies the number of
      borrowed books per page

-  PUT /borrowed-books/<borrowed book id>/return

-  POST /sales

   -  books: Mandatory and it is an array listing the book IDs to buy
      and their amounts, that is, *{"book-id-1": amount, "book-id-2":
      amount, ...}*

-  GET /sales

   -  from: Optional and returns borrowed books from the specified date

   -  page: Optional, default is 1, and specifies the page to return

   -  page-size: Optional, default is 50, and specifies the number of
      sales per page

-  GET /sales/<sales id>

We use POST requests when creating sales and borrowed books, since we do
not know the ID of the resource that we want to create a priori, and
posting the same request will create multiple resources. On the other
hand, when returning a book, we do know the ID of the borrowed book, and
sending the same request multiple times will leave the database in the
same state. Let's translate these endpoints to routes in
app/Http/routes.php:

    /\* \* Books endpoints. \*/ Route::get('books', ['middleware' =>
    'oauth', 'uses' => 'BookController@getAll']);
    Route::get('books/{id}', ['middleware' => 'oauth', 'uses' =>
    'BookController@get']); /\* \* Borrowed books endpoints. \*/
    Route::post('borrowed-books', ['middleware' => 'oauth', 'uses' =>
    'BorrowedBookController@borrow']); Route::get('borrowed-books',
    ['middleware' => 'oauth', 'uses' => 'BorrowedBookController@get']);
    Route::put('borrowed-books/{id}/return', ['middleware' => 'oauth',
    'uses' => 'BorrowedBookController@returnBook']); /\* \* Sales
    endpoints. \*/ Route::post('sales', ['middleware' => 'oauth', 'uses'
    => 'SalesController@buy]); Route::get('sales', ['middleware' =>
    'oauth', 'uses' => 'SalesController@getAll']);
    Route::get('sales/{id}', ['middleware' => 'oauth', 'uses' =>
    'SalesController@get']);

In the preceding code, note how we added the middleware oauth to all the
endpoints. This will require the user to provide a valid access token in
order to access them.

Adding the controllers
----------------------

From the previous section, you can imagine that we need to create three
controllers: BookController, BorrowedBookController, and
SalesController. Let's start with the easiest one: returning the
information of a book given the ID. Create the file
app/Http/Controllers/BookController.php, and add the following code:

    <?php namespace App\\Http\\Controllers; use App\\Book; use
    Illuminate\\Http\\JsonResponse; use Illuminate\\Http\\Response;
    class BookController extends Controller { public function get(string
    $id): JsonResponse { $book = Book::find($id); if (empty($book)) {
    return new JsonResponse ( null, JsonResponse::HTTP\_NOT\_FOUND ); }
    return response()->json(['book' => $book]); } }

Even though this preceding example is quite easy, it contains most of
what we will need for the rest of the endpoints. We try to fetch a book
given the ID from the URL, and when not found, we reply with a 404 (not
found) empty response—the constant Response::HTTP\_NOT\_FOUND is 404. In
case we have the book, we return it as JSON with response->json(). Note
how we add the seemingly unnecessary key book; it is true that we do not
return anything else and, since we ask for the book, the user will know
what we are talking about, but as it does not really hurt, it is good to
be as explicit as possible.

Let's test it! You already know how to get an access token—check the
*Requesting an access token* section. So get one, and try to access the
following URLs:

-  http://localhost/books/0?access\_token=12345

-  http://localhost/books/1?access\_token=12345

Assuming that 12345 is your access token, that you have a book in the
database with ID 1, and you do not have a book with ID 0, the first URL
should return a 404 response, and the second one, a response something
similar to the following:

    { "book": { "id": 1 "isbn": "9780882339726" "title": "1984"
    "author": "George Orwell" "stock": 12 "price": 7.5 } }

Let's now add the method to get all the books with filters and
pagination. It looks quite verbose, but the logic that we use is quite
simple:

    public function getAll(Request $request): JsonResponse { $title =
    $request->get('title', ''); $author = $request->get('author', '');
    $page = $request->get('page', 1); $pageSize =
    $request->get('page-size', 50); $books = Book::where('title',
    'like', "%$title%") ->where('author', 'like', "%$author%")
    ->take($pageSize) ->skip(($page - 1) \* $pageSize) ->get(); return
    response()->json(['books' => $books]); }

We get all the parameters that can come from the request, and set the
default values of each one in case the user does not include them (since
they are optional). Then, we use the Eloquent ORM to filter by title and
author using where(), and limiting the results with take()->skip(). We
return the JSON in the same way we did with the previous method. In this
one though, we do not need any extra check; if the query does not return
any book, it is not really a problem.

You can now play with your REST API, sending different requests with
different filters. The following are some examples:

-  http://localhost/books?access\_token=12345

-  http://localhost/books?access\_token=12345&title=19&page-size=1

-  http://localhost/books?access\_token=12345&page=2

The next controller in the list is BorrowedBookController. We need to
add three methods: borrow, get, and returnBook. As you already know how
to work with requests, responses, status codes, and the Eloquent ORM, we
will write the entire class straightaway:

    <?php namespace App\\Http\\Controllers; use App\\Book; use
    App\\BorrowedBook; use Illuminate\\Http\\JsonResponse; use
    Illuminate\\Http\\Request; use
    LucaDegasperi\\OAuth2Server\\Facades\\Authorizer; class
    BorrowedBookController extends Controller { public function get():
    JsonResponse { $borrowedBooks = BorrowedBook::where( 'user\_id',
    '=', Authorizer::getResourceOwnerId() )->get(); return
    response()->json( ['borrowed-books' => $borrowedBooks] ); } public
    function borrow(Request $request): JsonResponse { $id =
    $request->get('book-id'); if (empty($id)) { return new JsonResponse(
    ['error' => 'Expecting book-id parameter.'],
    JsonResponse::HTTP\_BAD\_REQUEST ); } $book = Book::find($id); if
    (empty($book)) { return new JsonResponse( ['error' => 'Book not
    found.'], JsonResponse::HTTP\_BAD\_REQUEST ); } else if
    ($book->stock < 1) { return new JsonResponse( ['error' => 'Not
    enough stock.'], JsonResponse::HTTP\_BAD\_REQUEST ); }
    $book->stock--; $book->save(); $borrowedBook = BorrowedBook::create(
    [ 'book\_id' => $book->id, 'start' => date('Y-m-d H:i:s'),
    'user\_id' => Authorizer::getResourceOwnerId() ] ); return
    response()->json(['borrowed-book' => $borrowedBook]); } public
    function returnBook(string $id): JsonResponse { $borrowedBook =
    BorrowedBook::find($id); if (empty($borrowedBook)) { return new
    JsonResponse( ['error' => 'Borrowed book not found.'],
    JsonResponse::HTTP\_BAD\_REQUEST ); } $book =
    Book::find($borrowedBook->book\_id); $book->stock++; $book->save();
    $borrowedBook->end = date('Y-m-d H:m:s'); $borrowedBook->save();
    return response()->json(['borrowed-book' => $borrowedBook]); } }

The only thing to note in the preceding code is how we also update the
stock of the book by increasing or decreasing the stock, and invoke the
save method to save the changes in the database. We also return the
borrowed book object as the response when borrowing a book so that the
user can know the borrowed book ID, and use it when querying or
returning the book.

You can test how this set of endpoints works with the following use
cases:

-  Borrow a book. Check that you get a valid response.

-  Get the list of borrowed books. The one that you just created should
   be there with a valid starting date and an empty end date.

-  Get the information of the book you borrowed. The stock should be one
   less.

-  Return the book. Fetch the list of borrowed books to check the end
   date and the returned book to check the stock.

Of course, you can always try to trick the API and ask for books without
stock, non-existing borrowed books, and the like. All these edge cases
should respond with the correct status codes and error messages.

We finish this section, and the REST API, by creating the
SalesController. This controller is the one that contains more logic,
since creating a sale implies adding entries to the sales books table,
prior to checking for enough stock for each one. Add the following code
to app/Html/SalesController.php:

    <?php namespace App\\Http\\Controllers; use App\\Book; use
    App\\Sale; use App\\SalesBook; use Illuminate\\Http\\JsonResponse;
    use Illuminate\\Http\\Request; use
    LucaDegasperi\\OAuth2Server\\Facades\\Authorizer; class
    SalesController extends Controller { public function get(string
    $id): JsonResponse { $sale = Sale::find($id); if (empty($sale)) {
    return new JsonResponse( null, JsonResponse::HTTP\_NOT\_FOUND ); }
    $sale->books = $sale->books()->getResults(); return
    response()->json(['sale' => $sale]); } public function buy(Request
    $request): JsonResponse { $books =
    json\_decode($request->get('books'), true); if (empty($books) \|\|
    !is\_array($books)) { return new JsonResponse( ['error' => 'Books
    array is malformed.'], JsonResponse::HTTP\_BAD\_REQUEST ); }
    $saleBooks = []; $bookObjects = []; foreach ($books as $bookId =>
    $amount) { $book = Book::find($bookId); if (empty($book) \|\|
    $book->stock < $amount) { return new JsonResponse( ['error' => "Book
    $bookId not valid."], JsonResponse::HTTP\_BAD\_REQUEST ); }
    $bookObjects[] = $book; $saleBooks[] = [ 'book\_id' => $bookId,
    'amount' => $amount ]; } $sale = Sale::create( ['user\_id' =>
    Authorizer::getResourceOwnerId()] ); foreach ($bookObjects as $key
    => $book) { $book->stock -= $saleBooks[$key]['amount'];
    $saleBooks[$key]['sale\_id'] = $sale->id;
    SalesBook::create($saleBooks[$key]); } $sale->books =
    $sale->books()->getResults(); return response()->json(['sale' =>
    $sale]); } public function getAll(Request $request): JsonResponse {
    $page = $request->get('page', 1); $pageSize =
    $request->get('page-size', 50); $sales = Sale::where( 'user\_id',
    '=', Authorizer::getResourceOwnerId() ) ->take($pageSize)
    ->skip(($page - 1) \* $pageSize) ->get(); foreach ($sales as $sale)
    { $sale->books = $sale->books()->getResults(); } return
    response()->json(['sales' => $sales]); } }

In the preceding code, note how we first check the availability of all
the books before creating the sales entry. This way, we make sure that
we do not leave any unfinished sale in the database when returning an
error to the user. You could change this, and use transactions instead,
and if a book is not valid, just roll back the transaction.

In order to test this, we can follow similar steps as we did with
borrowed books. Just remember that the books parameter, when posting a
sale, is a JSON map; for example, {"1": 2, "4": 1} means that I am
trying to buy two books with ID 1 and one book with ID 4.

Testing your REST APIs
======================

You have already been testing your REST API after finishing each
controller by making some request and expecting a response. As you might
imagine, this can be handy sometimes, but it is for sure not the way to
go. Testing should be automatic, and should cover as much as possible.
We will have to think of a solution similar to unit testing.

In `*Chapter 10* <#Top_of_ch10_html>`__, *Behavioral Testing*, you will
learn more methodologies and tools for testing an application end to
end, and that will include REST APIs. However, due to the simplicity of
our REST API, we can add some pretty good tests with what Laravel
provides us as well. Actually, the idea is very similar to the tests
that we wrote in `*Chapter 8* <#Top_of_ch08_html>`__, *Using Existing
PHP Frameworks*, where we made a request to some endpoint, and expected
a response. The only difference will be in the kind of assertions that
we use (which can check if a JSON response is OK), and the way we
perform requests.

Let's add some tests to the set of endpoints related to books. We need
some books in the database in order to query them, so we will have to
populate the database before each test, that is, use the setUp method.
Remember that in order to leave the database clean of test data, we need
to use the trait DatabaseTransactions. Add the following code to
tests/BooksTest.php:

    <?php use Illuminate\\Foundation\\Testing\\DatabaseTransactions; use
    App\\Book; class BooksTest extends TestCase { use
    DatabaseTransactions; private $books = []; public function setUp() {
    parent::setUp(); $this->addBooks(); } private function addBooks() {
    $this->books[0] = Book::create( [ 'isbn' => '293842983648273',
    'title' => 'Iliad', 'author' => 'Homer', 'stock' => 12, 'price' =>
    7.40 ] ); $this->books[0]->save(); $this->books[0] =
    $this->books[0]->fresh(); $this->books[1] = Book::create( [ 'isbn'
    => '9879287342342', 'title' => 'Odyssey', 'author' => 'Homer',
    'stock' => 8, 'price' => 10.60 ] ); $this->books[1]->save();
    $this->books[1] = $this->books[1]->fresh(); $this->books[2] =
    Book::create( [ 'isbn' => '312312314235324', 'title' => 'The
    Illuminati', 'author' => 'Larry Burkett', 'stock' => 22, 'price' =>
    5.10 ] ); $this->books[2]->save(); $this->books[2] =
    $this->books[2]->fresh(); } }

As you can see in the preceding code, we add three books to the
database, and to the class property $books too. We will need them when
we want to assert that a response is valid. Also note the use of the
fresh method; this method synchronizes the model that we have with the
content in the database. We need to do this in order to get the ID
inserted in the database, since we do not know it a priori.

There is another thing we need to do before we run each test:
authenticating our client. We will need to make a POST request to the
access token generation endpoint sending valid credentials, and storing
the access token that we receive so that it can be used in the remaining
requests. You are free to choose how to provide the credentials, since
there are different ways to do it. In our case, we just provide the
credentials of a client test that we know exists in the database, but
you might prefer to insert that client into the database each time.
Update the test with the following code:

    <?php use Illuminate\\Foundation\\Testing\\DatabaseTransactions; use
    App\\Book; class BooksTest extends TestCase { use
    DatabaseTransactions; private $books = []; private $accessToken;
    public function setUp() { parent::setUp(); $this->addBooks();
    $this->authenticate(); } //... private function authenticate() {
    $this->post( 'oauth/access\_token', [ 'client\_id' =>
    'iTh4Mzl0EAPn90sK4EhAmVEXS', 'client\_secret' =>
    'PfoWM9yq4Bh6rhr8oDDsNZM', 'grant\_type' => 'client\_credentials' ]
    ); $response = json\_decode( $this->response->getContent(), true );
    $this->accessToken = $response['access\_token']; } }

In the preceding code, we use the post method in order to send a POST
request. This method accepts a string with the endpoint, and an array
with the parameters to be included. After making a request, Laravel
saves the response object into the $response property. We can
JSON-decode it, and extract the access token that we need.

It is time to add some tests. Let's start with an easy one: requesting a
book given an ID. The ID is used to make the GET requests with the ID of
the book (do not forget the access token), and check if the response
matches the expected one. Remember that we have the $books array
already, so it will be pretty easy to perform these checks.

We will be using two assertions: seeJson, which compares the received
JSON response with the one that we provide, and assertResponseOk, which
you already know from previous tests—it just checks that the response
has a 200 status code. Add this test to the class:

    public function testGetBook() { $expectedResponse = [ 'book' =>
    json\_decode($this->books[1], true) ]; $url = 'books/' .
    $this->books[1]->id . '?' . $this->getCredentials();
    $this->get($url) ->seeJson($expectedResponse) ->assertResponseOk();
    } private function getCredentials(): string { return
    'grant\_access=client\_credentials&access\_token=' .
    $this->accessToken; }

We use the get method instead of post, since this is a GET request. Also
note that we use the getCredentials helper, since we will have to use it
in each test. To see another example, let's add a test that checks the
response when requesting the books that contain the given title:

    public function testGetBooksByTitle() { $expectedResponse = [
    'books' => [ json\_decode($this->books[0], true),
    json\_decode($this->books[2], true) ] ]; $url = 'books/?title=Il&' .
    $this->getCredentials(); $this->get($url)
    ->seeJson($expectedResponse) ->assertResponseOk(); }

The preceding test is pretty much the same as the previous one, isn't
it? The only changes are the endpoint and the expected response. Well,
the remaining tests will all follow the same pattern, since so far, we
can only fetch books and filter them.

To see something different, let's check how to test an endpoint that
creates resources. There are different options, one of them being to
first make the request, and then going to the database to check that the
resource has been created. Another option, the one that we prefer, is to
first send the request that creates the resource, and then, with the
information in the response, send a request to fetch the newly created
resource. This is preferable, since we are testing only the REST API,
and we do not need to know the specific schema that the database is
using. Also, if the REST API changes its database, the tests will keep
passing—and they should—since we test through the interface only.

One good example could be borrowing a book. The test should first send a
POST in order to borrow the book, specifying the book ID, then extract
the borrowed book ID from the response, and finally send a GET request
asking for that borrowed book. To save time, you can add the following
test to the already existing tests/BooksTest.php:

    public function testBorrowBook() { $params = ['book-id' =>
    $this->books[1]->id]; $params = array\_merge($params,
    $this->postCredentials()); $this->post('borrowed-books', $params)
    ->seeJsonContains(['book\_id' => $this->books[1]->id])
    ->assertResponseOk(); $response =
    json\_decode($this->response->getContent(), true); $url =
    'borrowed-books' . '?' . $this->getCredentials(); $this->get($url)
    ->seeJsonContains(['id' => $response['borrowed-book']['id']])
    ->assertResponseOk(); } private function postCredentials(): array {
    return [ 'grant\_access' => 'client\_credentials', 'access\_token'
    => $this->accessToken ]; }

Summary
=======

In this chapter, you learned the importance of REST APIs in the web
world. Now you are able not only to use them, but also write your own
REST APIs, which has turned you into a more resourceful developer. You
can also integrate your applications with third-party APIs to give more
features to your users, and for making your websites more interesting
and useful.

In the next and last chapter, we will end this book discovering a type
of testing other than unit testing: behavioral testing, which improves
the quality and reliability of your web applications.

Chapter 10. Behavioral Testing

In `*Chapter 7* <#Top_of_ch07_html>`__, *Testing Web Applications*, you
learned how to write unit tests in order to test small pieces of code in
an isolated way. Even though this is a must, it is not enough alone to
make sure your application works as it should. The scope of your test
could be so small that even though the algorithm that you test makes
sense, it would not be what the business asked you to create.

Acceptance tests were born in order to add this level of security to the
business side, complementing the already existing unit tests. In the
same way, BDD originated from TDD in order to write code based on these
acceptance tests in an attempt to involve business and managers in the
development process. As PHP is one of the favorite languages of web
developers, it is just natural to find powerful tools to implement BDD
in your projects. You will be positively surprised by what you can do
with Behat and Mink, the two most popular BDD frameworks at the moment.

In this chapter, you will learn about:

-  Acceptance tests and BDD

-  Writing features with Gherkin

-  Implementing and running tests with Behat

-  Writing tests against browsers with Mink

Behavior-driven development
===========================

We already exposed in `*Chapter 7* <#Top_of_ch07_html>`__, *Testing Web
Applications*, the different tools we can use in order to make our
applications bug-free, such as automated tests. We described what unit
tests are and how they can help us achieve our goals, but this is far
from enough. In this section, we will describe the process of creating a
real-world application, how unit tests are not enough, and what other
techniques we can include in this life cycle in order to succeed in our
task—in this case, behavioral tests.

Introducing continuous integration
----------------------------------

There is a huge difference between developing a small web application by
yourself and being part of a big team of developers, managers, marketing
people, and so on, that works around the same big web application.
Working on an application used by thousands or millions of users has a
clear risk: if you mess it up, there will be a huge number of unhappy
affected users, which may translate into sales going down, partnerships
terminated, and so on.

From this scenario, you can imagine that people would be scared when
they have to change anything in production. Before doing so, they will
make sure that everything works perfectly fine. For this reason, there
is always a heavy process around all the changes affecting a web
application in production, including loads of tests of all kinds.

Some think that by reducing the number of times they deploy to
production, they can reduce the risk of failure, which ends up with them
having releases every several months with an uncountable number of
changes.

Now, imagine releasing the result of two or three months of code changes
at once and something mysteriously fails in production: do you know
where to even start looking for the cause of the problem? What if your
team is good enough to make perfect releases, but the end result is not
what the market needs? You might end up wasting months of work!

Even though there are different approaches and not all companies use
them, let's try to describe one of the most famous ones from the last
few years: continuous integration (CI). The idea is to integrate small
pieces of work often rather than big ones every once in a while. Of
course, releasing is still a constraint in your system, which means that
it takes a lot of time and resources. CI tries to automatize this
process as much as possible, reducing the amount of time and resources
that you need to invest. There are huge benefits with this approach,
which are as follows:

-  Releases do not take forever to be done, and there isn't an entire
   team focused on releasing as this is done automatically.

-  You can release changes one by one as they come. If something fails,
   you know exactly what the change was and where to start looking for
   the error. You can even revert the changes easily if you need to.

-  As you release so often, you can get quick feedback from everyone.
   You will be able to change your plans in time if you need to instead
   of waiting for months to get any feedback and wasting all the effort
   you put on this release.

The idea seems perfect, but how do we implement it? First, let's focus
on the manual part of the process: developing the features using a
version control system (VCS). The following diagram shows a very common
approach:

|image47|

As we already mentioned, a VCS allows developers to work on the same
codebase, tracking all the changes that everyone makes and helping on
the resolution of conflicts. A VCS usually allows you to have different
branches; that is, you can diverge from the main line of development and
continue to do work without messing with it. The previous graph shows
you how to use branches to write new features and can be explained as
follows:

-  A: A team needs to start working on feature A. They create a new
   branch from the master, in which they will add all the changes for
   this feature.

-  B: A different team also needs to start working on a feature. They
   create a new branch from master, same as before. At this point, they
   are not aware of what the first team is doing as they do it on their
   own branch.

-  C: The second team finishes their job. No one else changed master, so
   they can merge their changes straight away. At this point, the CI
   process will start the release process.

-  D: The first team finishes the feature. In order to merge it to
   master, they need to first rebase their branch with the new changes
   of master and solve any conflicts that might take place. The older
   the branch is the more chances of getting conflicts you will have, so
   you can imagine that smaller and faster features are preferred.

Now, let's take a look at how the automated side of the process looks.
The following graph shows you all the steps from the merging into master
to production deployment:

|image48|

Until you merge your code into master, you are in the development
environment. The CI tool will listen to all the changes on the master
branch of your project, and for each of them, it will trigger a job.
This job will take care of building the project if necessary and then
run all the tests. If there is any error or test failure, it will let
everyone now, and the team that triggered this job should take care of
fixing it. The master branch is considered unstable at this point.

If all tests pass, the CI tool will deploy your code into staging.
Staging is an environment that emulates production as much as possible;
that is, it has the same server configuration, database structure, and
so on. Once the application is here, you can run all the tests that you
need until you are confident to continue the deployment to production.
As you make small changes, you do not need to manually test absolutely
everything. Instead, you can test your changes and the main use cases of
your application.

Unit tests versus acceptance tests
----------------------------------

We said that the goal of CI is to have a process as automatized as
possible. However, we still need to manually test the application in
staging, right? Acceptance tests to the rescue!

Writing unit tests is nice and a must, but they test only small pieces
of code in an isolated way. Even if your entire unit tests suite passes,
you cannot be sure that your application works at all as you might not
integrate all the parts properly because you are missing functionalities
or the functionalities that you built were not what the business needed.
Acceptance tests test the entire flow of a specific use case.

If your application is a website, acceptance tests will probably launch
a browser and emulate user actions, such as clicking and typing, in
order to assert that the page returns what is expected. Yes, from a few
lines of code, you can execute all the tests that were previously manual
in an automated way.

Now, imagine that you wrote acceptance tests for all the features of
your application. Once the code is in staging, the CI tool can
automatically run all of these tests and make sure that the new code
does not break any existing functionality. You can even run them using
as many different browsers as you need to make sure that your
application works fine in all of them. If a test fails, the CI tool will
notify the team responsible, and they will have to fix it. If all the
tests pass, the CI tool can automatically deploy your code into
production.

Why do we need to write unit tests then, if acceptance tests test what
the business really cares about? There are several reasons to keep both
acceptance and unit tests; in fact, you should have way more unit tests
than acceptance tests.

-  Unit tests check small pieces of code, which make them
   orders-of-magnitude faster than acceptance tests, which test the
   whole flow against a browser. That means that you can run all your
   unit tests in a few seconds or minutes, but it will take much longer
   to run all your acceptance tests.

-  Writing acceptance tests that cover absolutely all the possible
   combinations of use cases is virtually impossible. Writing unit tests
   that cover a high percentage of use cases for a given method or piece
   of code is rather easy. You should have loads of unit tests testing
   as many edge cases as possible but only some acceptance tests testing
   the main use cases.

When should you run each type of test then? As unit tests are faster,
they should be executed during the first stages of deployment. Only once
we know that they all have passed do we want to spend time deploying to
staging and running acceptance tests.

TDD versus BDD
--------------

In `*Chapter 7* <#Top_of_ch07_html>`__, *Testing Web Applications*, you
learned that TDD or test-driven development is the practice of writing
first the unit tests and then the code in an attempt to write testable
and cleaner code and to make sure that your test suite is always up to
date. With the appearance of acceptance tests, TDD evolved to BDD or
behavior-driven development.

BDD is quite similar to TDD, in that you should write the tests first
and then the code that makes these tests pass. The only difference is
that with BDD, we write tests that specify the desired behavior of the
code, which can be translated to acceptance tests. Even though it will
always depend on the situation, you should write acceptance tests that
test a very specific part of the application rather than long use cases
that contain several steps. With BDD, as with TDD, you want to get quick
feedback, and if you write a broad test, you will have to write a lot of
code in order to make it pass, which is not the goal that BDD wants to
achieve.

Business writing tests
----------------------

The whole point of acceptance tests and BDD is to make sure that your
application works as expected, not only your code. Acceptance tests,
then, should not be written by developers but by the business itself. Of
course, you cannot expect that managers and executives will learn how to
code in order to create acceptance tests, but there is a bunch of tools
that allow you to translate plain English instructions or behavioral
specifications into acceptance tests' code. Of course, these
instructions have to follow some patterns. Behavioral specifications
have the following parts:

-  A title, which describes briefly, but in a very clear way, what use
   case the behavioral specification covers.

-  A narrative, which specifies who performs the test, what the business
   value is, and what the expected outcome is. Usually the format of the
   narrative is the following:

    In order to <business value> As a <stakeholder> I want to <expected
    outcome>

-  A set of scenarios, which is a description and a set of steps of each
   specific use case that we want to cover. Each scenario has a
   description and a list of instructions in the Given-When-Then format;
   we will discuss more on this in the next section. A common patterns
   is:

    Scenario: <short description> Given <set up scenario> When <steps to
    take> Then <expected outcome>

In the next two sections, we will discover two tools in PHP that you can
use in order to understand behavioral scenarios and run them as
acceptance tests.

BDD with Behat
==============

The first of the tools we will introduce is Behat. Behat is a PHP
framework that can transform behavioral scenarios into acceptance tests
and then run them, providing feedback similar to PHPUnit. The idea is to
match each of the steps in English with the scenarios in a PHP function
that performs some action or asserts some results.

In this section, we will try to add some acceptance tests to our
application. The application will be a simple database migration script
that will allow us to keep track of the changes that we will add to our
schema. The idea is that each time that you want to change your
database, you will write the changes on a migration file and then
execute the script. The application will check what was the last
migration executed and will perform new ones. We will first write the
acceptance tests and then introduce the code progressively as BDD
suggests.

In order to install Behat on your development environment, you can use
Composer. The command is as follows:

    $ composer require behat/behat

Behat actually does not come with any set of assertion functions, so you
will have to either implement your own by writing conditionals and
throwing exceptions or you could integrate any library that provides
them. Developers usually choose PHPUnit for this as they are already
used to its assertions. Add it, then, to your project via the following:

    $ composer require phpunit/phpunit

As with PHPUnit, Behat needs to know where your test suite is located.
You can either have a configuration file stating this and other
configuration options, which is similar to the phpunit.xml configuration
file for PHPUnit, or you could follow the conventions that Behat sets
and skip the configuration step. If you choose the second option, you
can let Behat create the folder structure and PHP test class for you
with the following command:

    $ ./vendor/bin/behat --init

After running this command, you should have a
features/bootstrap/FeatureContext.php file, which is where you need to
add the steps of the PHP functions' matching scenarios. More on this
shortly, but first, let's find out how to write behavioral
specifications so that Behat can understand them.

Introducing the Gherkin language
--------------------------------

Gherkin is the language, or rather the format, that behavioral
specifications have to follow. Using Gherkin naming, each behavioral
specification is a feature. Each feature is added to the features
directory and should have the .feature extension. Feature files should
start with the Feature keyword followed by the title and the narrative
in the same format that we already mentioned before—that is, the *In
order to–As a–I need to* structure. In fact, Gherkin will only print
these lines, but keeping it consistent will help your developers and
business know what they are trying to achieve.

Our application will have two features: one for the setup of our
database to allow the migrations tool to work, and the other one for the
correct behavior when adding migrations to the database. Add the
following content to the features/setup.feature file:

    Feature: Setup In order to run database migrations As a developer I
    need to be able to create the empty schema and migrations table.

Then, add the following feature definition to the
features/migrations.feature file:

    Feature: Migrations In order to add changes to my database schema As
    a developer I need to be able to run the migrations script

Defining scenarios
------------------

The title and narrative of features does not really do anything more
than give information to the person who runs the tests. The real work is
done in scenarios, which are specific use cases with a set of steps to
take and some assertions. You can add as many scenarios as you need to
each feature file as long as they represent different use cases of the
same feature. For example, for setup.feature, we can add a couple of
scenarios: one where it is the first time that the user runs the script,
so the application will have to set up the database, and one where the
user already executed the script previously, so the application does not
need to go through the setup process.

As Behat needs to be able to translate the scenarios written in plain
English to PHP functions, you will have to follow some conventions. In
fact, you will see that they are very similar to the ones that we
already mentioned in the behavioral specifications section.

Writing Given-When-Then test cases
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A scenario must start with the Scenario keyword followed by a short
description of what use case the scenario covers. Then, you need to add
the list of steps and assertions. Gherkin allows you to use four
keywords for this: Given, When, Then, and And. In fact, they all have
the same meaning when it comes to code, but they add a lot of semantic
value to your scenarios. Let's consider an example; add the following
scenario at the end of your setup.feature file:

    Scenario: Schema does not exist and I do not have migrations Given I
    do not have the "bdd\_db\_test" schema And I do not have migration
    files When I run the migrations script Then I should have an empty
    migrations table And I should get: """ Latest version applied is 0.
    """

This scenario tests what happens when we do not have any schema
information and run the migrations script. First, it describes the state
of the scenario: *Given I do not have the bdd\_db\_test schema And I do
not have migration files*. These two lines will be translated to one
method each, which will remove the schema and all migration files. Then,
the scenario describes what the user will do: *When I run the migrations
script*. Finally, we set the expectations for this scenario: *Then I
should have an empty migrations table And I should get Latest version
applied is 0.*.

In general, the same step will always start by the same keyword—that is,
*I run the migrations script* will always be preceded by When. The And
keyword is a special one as it matches all the three keywords; its only
purpose is to have steps as English-friendly as possible; although if
you prefer, you could write *Given I do not have migration files*.

Another thing to note in this example is the use of arguments as part of
the step. The line *And I should get* is followed by a string enclosed
by """. The PHP function will get this string as an argument, so you can
have one unique step definition—that is, the function—for a wide variety
of situations just using different strings.

Reusing parts of scenarios
~~~~~~~~~~~~~~~~~~~~~~~~~~

It is quite common that for a given feature, you always start from the
same scenario. For example, setup.feature has a scenario in which we can
run the migrations for the first time without any migration file, but we
will also add another scenario in which we want to run the migrations
script for the first time with some migration files to make sure that it
will apply all of them. Both scenarios have in common one thing: they do
not have the database set up.

Gherkin allows you to define some steps that will be applied to all the
scenarios of the feature. You can use the Background keyword and a list
of steps, usually Given. Add these two lines between the feature
narrative and scenario definition:

    Background: Given I do not have the "bdd\_db\_test" schema

Now, you can remove the first step from the existing scenario as
Background will take care of it.

Writing step definitions
------------------------

So far, we have written features using the Gherkin language, but we
still have not considered how any of the steps in each scenario is
translated to actual code. The easiest way to note this is by asking
Behat to run the acceptance tests; as the steps are not defined
anywhere, Behat will print out all the functions that you need to add to
your FeatureContext class. To run the tests, just execute the following
command:

    $ ./vendor/bin/behat

The following screenshot shows the output that you should get if you
have no step definitions:

|image49|

As you can note, Behat complained about some missing steps and then
printed in yellow the methods that you could use in order to implement
them. Copy and paste them into your autogenerated
features/bootstrap/FeatureContext.php file. The following FeatureContext
class has already implemented all of them:

    <?php use Behat\\Behat\\Context\\Context; use
    Behat\\Behat\\Context\\SnippetAcceptingContext; use
    Behat\\Gherkin\\Node\\PyStringNode; require\_once \_\_DIR\_\_ .
    '/../../vendor/phpunit/phpunit/src/Framework/Assert/Functions.php';
    class FeatureContext implements Context, SnippetAcceptingContext {
    private $db; private $config; private $output; public function
    \_\_construct() { $configFileContent = file\_get\_contents(
    \_\_DIR\_\_ . '/../../config/app.json' ); $this->config =
    json\_decode($configFileContent, true); } private function getDb():
    PDO { if ($this->db === null) { $this->db = new PDO(
    "mysql:host={$this->config['host']}; " . "dbname=bdd\_db\_test",
    $this->config['user'], $this->config['password'] ); } return
    $this->db; } /\*\* \* @Given I do not have the "bdd\_db\_test"
    schema \*/ public function iDoNotHaveTheSchema() {
    $this->executeQuery('DROP SCHEMA IF EXISTS bdd\_db\_test'); } /\*\*
    \* @Given I do not have migration files \*/ public function
    iDoNotHaveMigrationFiles() { exec('rm db/migrations/\*.sql >
    /dev/null 2>&1'); } /\*\* \* @When I run the migrations script \*/
    public function iRunTheMigrationsScript() { exec('php migrate.php',
    $this->output); } /\*\* \* @Then I should have an empty migrations
    table \*/ public function iShouldHaveAnEmptyMigrationsTable() {
    $migrations = $this->getDb() ->query('SELECT \* FROM migrations')
    ->fetch(); assertEmpty($migrations); } private function
    executeQuery(string $query) { $removeSchemaCommand = sprintf( 'mysql
    -u %s %s -h %s -e "%s"', $this->config['user'],
    empty($this->config['password']) ? '' :
    "-p{$this->config['password']}", $this->config['host'], $query );
    exec($removeSchemaCommand); } }

As you can note, we read the configuration from the config/app.json
file. This is the same configuration file that the application will use,
and it contains the database's credentials. We also instantiated a PDO
object to access the database so that we could add or remove tables or
take a look at what the script did.

Step definitions are a set of methods with a comment on each of them.
This comment is an annotation as it starts with @ and is basically a
regular expression matching the plain English step defined in the
feature. Each of them has its implementation: either removing a database
or migration files, executing the migrations script, or checking what
the migrations table contains.

The parameterization of steps
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In the previous FeatureContext class, we intentionally missed the
iShouldGet method. As you might recall, this step has a string argument
identified by a string enclosed between """. The implementation for this
method looks as follows:

    /\*\* \* @Then I should get: \*/ public function
    iShouldGet(PyStringNode $string) { assertEquals(implode("\\n",
    $this->output), $string); }

Note how the regular expression does not contain the string. This
happens when using long strings with """. Also, the argument is an
instance of PyStringNode, which is a bit more complex than a normal
string. However, fear not; when you compare it with a string, PHP will
look for the \_\_toString method, which just prints the content of the
string.

Running feature tests
---------------------

In the previous sections, we wrote acceptance tests using Behat, but we
have not written a single line of code yet. Before running them, though,
add the config/app.json configuration file with the credentials of your
database user so that the FeatureContext constructor can find it, as
follows:

    { "host": "127.0.0.1", "schema": "bdd\_db\_test", "user": "root",
    "password": "" }

Now, let's run the acceptance tests, expecting them to fail; otherwise,
our tests will not be valid at all. The output should be something
similar to this:

|image50|

As expected, the Then steps failed. Let's implement the minimum code
necessary in order to make the tests pass. For starters, add the
autoloader into your composer.json file and run composer update:

    "autoload": { "psr-4": { "Migrations\\\\": "src/" } }

We would like to implement a Schema class that contains the helpers
necessary to set up a database, run migrations, and so on. Right now,
the feature is only concerned about the setup of the database—that is,
creating the database, adding the empty migrations table to keep track
of all the migrations added, and the ability to get the latest migration
registered as successful. Add the following code as src/Schema.php:

    <?php namespace Migrations; use Exception; use PDO; class Schema {
    const SETUP\_FILE = \_\_DIR\_\_ . '/../db/setup.sql'; const
    MIGRATIONS\_DIR = \_\_DIR\_\_ . '/../db/migrations/'; private
    $config; private $connection; public function \_\_construct(array
    $config) { $this->config = $config; } private function
    getConnection(): PDO { if ($this->connection === null) {
    $this->connection = new PDO( "mysql:host={$this->config['host']};" .
    "dbname={$this->config['schema']}", $this->config['user'],
    $this->config['password'] ); } return $this->connection; } }

Even though the focus of this chapter is to write acceptance tests,
let's go through the different implemented methods:

-  The constructor and getConnection just read the configuration file in
   config/app.json and instantiated the PDO object.

-  The createSchema executed CREATE SCHEMA IF NOT EXISTS, so if the
   schema already exists, it will do nothing. We executed the command
   with exec instead of PDO as PDO always needs to use an existing
   database.

-  The getLatestMigration will first check whether the migrations table
   exists; if not, we will create it using setup.sql and then fetch the
   last successful migration.

We also need to add the migrations/setup.sql file with the query to
create the migrations table, as follows:

    CREATE TABLE IF NOT EXISTS migrations( version INT UNSIGNED NOT
    NULL, \`time\` TIMESTAMP NOT NULL DEFAULT CURRENT\_TIMESTAMP, status
    ENUM('success', 'error'), PRIMARY KEY (version, status) );

Finally, we need to add the migrate.php file, which is the one that the
user will execute. This file will get the configuration, instantiate the
Schema class, set up the database, and retrieve the last migration
applied. Run the following code:

    <?php require\_once \_\_DIR\_\_ . '/vendor/autoload.php';
    $configFileContent = file\_get\_contents(\_\_DIR\_\_ .
    '/config/app.json'); $config = json\_decode($configFileContent,
    true); $schema = new Migrations\\Schema($config);
    $schema->createSchema(); $version = $schema->getLatestMigration();
    echo "Latest version applied is $version.\\n";

You are now good to run the tests again. This time, the output should be
similar to this screenshot, where all the steps are in green:

|image51|

Now that our acceptance test is passing, we need to add the rest of the
tests. To make things quicker, we will add all the scenarios, and then
we will implement the necessary code to make them pass, but it would be
better if you add one scenario at a time. The second scenario of
setup.feature could look as follows (remember that the feature contains
a Background section, in which we clean the database):

    Scenario: Schema does not exists and I have migrations Given I have
    migration file 1: """ CREATE TABLE test1(id INT); """ And I have
    migration file 2: """ CREATE TABLE test2(id INT); """ When I run the
    migrations script Then I should only have the following tables: \|
    migrations \| \| test1 \| \| test2 \| And I should have the
    following migrations: \| 1 \| success \| \| 2 \| success \| And I
    should get: """ Latest version applied is 0. Applied migration 1
    successfully. Applied migration 2 successfully. """

This scenario is important as it used parameters inside the step
definitions. For example, the *I have migration file* step is presented
twice, each time with a different migration file number. The
implementation of this step is as follows:

    /\*\* \* @Given I have migration file :version: \*/ public function
    iHaveMigrationFile( string $version, PyStringNode $file ) {
    $filePath = \_\_DIR\_\_ . "/../../db/migrations/$version.sql";
    file\_put\_contents($filePath, $file->getRaw()); }

The annotation of this method, which is a regular expression, used
:version as a wildcard. Any step that starts with *Given I have
migration file* followed by something else will match this step
definition, and the "something else" bit will be received as the
$version argument as a string.

Here, we introduced yet another type of argument: tables. The *Then I
should only have the following tables* step defined a table of two rows
of one column each, and the *Then I should have the following
migrations* bit sent a table of two rows of two columns each. The
implementation for the new steps is as follows:

    /\*\* \* @Then I should only have the following tables: \*/ public
    function iShouldOnlyHaveTheFollowingTables(TableNode $tables) {
    $tablesInDb = $this->getDb() ->query('SHOW TABLES')
    ->fetchAll(PDO::FETCH\_NUM); assertEquals($tablesInDb,
    array\_values($tables->getRows())); } /\*\* \* @Then I should have
    the following migrations: \*/ public function
    iShouldHaveTheFollowingMigrations( TableNode $migrations ) { $query
    = 'SELECT version, status FROM migrations'; $migrationsInDb =
    $this->getDb() ->query($query) ->fetchAll(PDO::FETCH\_NUM);
    assertEquals($migrations->getRows(), $migrationsInDb); }

The tables are received as TableNode arguments. This class contains a
getRows method that returns an array with the rows defined in the
feature file.

The other feature that we would like to add is
features/migrations.feature. This feature will assume that the user
already has the database set up, so we will add a Background section
with this step. We will add one scenario in which the migration file
numbers are not consecutive, in which case the application should stop
at the last consecutive migration file. The other scenario will make
sure that when there is an error, the application does not continue the
migration process. The feature should look similar to the following:

    Feature: Migrations In order to add changes to my database schema As
    a developer I need to be able to run the migrations script
    Background: Given I have the bdd\_db\_test Scenario: Migrations are
    not consecutive Given I have migration 3 And I have migration file
    4: """ CREATE TABLE test4(id INT); """ And I have migration file 6:
    """ CREATE TABLE test6(id INT); """ When I run the migrations script
    Then I should only have the following tables: \| migrations \| \|
    test4 \| And I should have the following migrations: \| 3 \| success
    \| \| 4 \| success \| And I should get: """ Latest version applied
    is 3. Applied migration 4 successfully. """ Scenario: A migration
    throws an error Given I have migration file 1: """ CREATE TABLE
    test1(id INT); """ And I have migration file 2: """ CREATE TABLE
    test1(id INT); """ And I have migration file 3: """ CREATE TABLE
    test3(id INT); """ When I run the migrations script Then I should
    only have the following tables: \| migrations \| \| test1 \| And I
    should have the following migrations: \| 1 \| success \| \| 2 \|
    error \| And I should get: """ Latest version applied is 0. Applied
    migration 1 successfully. Error applying migration 2: Table 'test1'
    already exists. """

There aren't any new Gherkin features. The two new step implementations
look as follows:

    /\*\* \* @Given I have the bdd\_db\_test \*/ public function
    iHaveTheBddDbTest() { $this->executeQuery('CREATE SCHEMA
    bdd\_db\_test'); } /\*\* \* @Given I have migration :version \*/
    public function iHaveMigration(string $version) {
    $this->getDb()->exec( file\_get\_contents(\_\_DIR\_\_ .
    '/../../db/setup.sql') ); $query = <<<SQL INSERT INTO migrations
    (version, status) VALUES(:version, 'success') SQL; $this->getDb()
    ->prepare($query) ->execute(['version' => $version]); }

Now, it is time to add the needed implementation to make the tests pass.
There are only two changes needed. The first one is an
applyMigrationsFrom method in the Schema class that, given a version
number, will try to apply the migration file for this number. If the
migration is successful, it will add a row in the migrations table, with
the new version added successfully. If the migration failed, we would
add the record in the migrations table as a failure and then throw an
exception so that the script is aware of it. Finally, if the migration
file does not exist, the returning value will be false. Add this code to
the Schema class:

    public function applyMigrationsFrom(int $version): bool { $filePath
    = self::MIGRATIONS\_DIR . "$version.sql"; if
    (!file\_exists($filePath)) { return false; } $connection =
    $this->getConnection(); if
    ($connection->exec(file\_get\_contents($filePath)) === false) {
    $error = $connection->errorInfo()[2];
    $this->registerMigration($version, 'error'); throw new
    Exception($error); } $this->registerMigration($version, 'success');
    return true; } private function registerMigration(int $version,
    string $status) { $query = <<<SQL INSERT INTO migrations (version,
    status) VALUES(:version, :status) SQL; $params = ['version' =>
    $version, 'status' => $status];
    $this->getConnection()->prepare($query)->execute($params); }

The other bit missing is in the migrate.php script. We need to call the
newly created applyMigrationsFrom method with consecutive versions
starting from the latest one, until we get either a false value or an
exception. We also want to print out information about what is going on
so that the user is aware of what migrations were added. Add the
following code at the end of the migrate.php script:

    do { $version++; try { $result =
    $schema->applyMigrationsFrom($version); if ($result) { echo "Applied
    migration $version successfully.\\n"; } } catch (Exception $e) {
    $error = $e->getMessage(); echo "Error applying migration $version:
    $error.\\n"; exit(1); } } while ($result);

Now, run the tests and voilà! They all pass. You now have a library that
manages database migrations, and you are 100% sure that it works thanks
to your acceptance tests.

Testing with a browser using Mink
=================================

So far, we have been able to write acceptance tests for a script, but
most of you are reading this book in order to write nice and shiny web
applications. How can you take advantage of acceptance tests then? It is
time to introduce the second PHP tool of this chapter: Mink.

Mink is actually an extension of Behat, which adds implementations of
several steps related to web browser testing. For example, if you add
Mink to your application, you will be able to add scenarios where Mink
will launch a browser and click or type as requested, saving you a lot
of time and effort in manual testing. However, first, let's take a look
at how Mink can achieve this.

Types of web drivers
--------------------

Mink makes use of web drivers—that is, libraries that have an API that
allows you to interact with a browser. You can send commands, such as
*go to this page*, *click on this link*, *fill this input field with
this text*, and so on, and the web driver will translate this into the
correct instruction for your browser. There are several web drivers,
each of them implemented following a different approach. It is for this
reason that depending on the web driver, you will have some features or
others.

Web drivers can be divided into two groups depending on how they work:

-  Headless browsers: These drivers do not really launch a browser; they
   only try to emulate one. They actually request for the web page and
   render the HTML and JavaScript code, so they are aware of how the
   page looks, but they do not display it. They have a huge benefit:
   they are easy to install and manage, and as they do not have to build
   the graphical representation, they are extremely fast. The
   disadvantage is that they have severe restrictions in terms of CSS
   and some JavaScript functionalities, especially AJAX.

-  Web drivers that launch real browsers like a user would do: These web
   drivers can do almost anything and are way more powerful than
   headless browsers. The problem is that they can be a bit tricky to
   install and are very, very slow—as slow as a real user trying to go
   through the scenarios.

So, which one should you choose? As always, it will depend on what your
application is. If you have an application that does not make heavy use
of CSS and JavaScript and it is not critical for your business, you
could use headless browsers. Instead, if the application is the
cornerstone of your business and you need to be absolutely certain that
all the UI features work as expected, you might want to go for web
drivers that launch browsers.

Installing Mink with Goutte
---------------------------

In this chapter, we will use Goutte, a headless web driver written by
the same guys that worked on Symfony, to add some acceptance tests to
the repositories page of GitHub. The required components of your project
will be Behat, Mink, and the Goutte driver. Add them with Composer via
the following commands:

    $ composer require behat/behat $ composer require
    behat/mink-extension $ composer require behat/mink-goutte-driver

Now, execute the following line to ask Behat to create the basic
directory structure:

    $ ./vendor/bin/behat –init

The only change we will add to the FeatureContext class is where it
extends from. This time, we will use MinkContext in order to get all the
step definitions related to web testing. The FeatureContext class should
look similar to this:

    <?php use Behat\\MinkExtension\\Context\\MinkContext; require
    \_\_DIR\_\_ . '/../../vendor/autoload.php'; class FeatureContext
    extends MinkContext { }

Mink also needs some configuration in order to let Behat know which web
driver we want to use or what the base URL for our tests is. Add the
following information to behat.yml:

    default: extensions: Behat\\MinkExtension: base\_url:
    "https://github.com" sessions: default\_session: goutte: ~

With this configuration, we let Behat know that we are using the Mink
extension, that Mink will use Goutte in all the sessions (you could
actually define different sessions with different web drivers if
necessary), and that the base URL for these tests is the GitHub one.
Behat is already instructed to look for the behat.yml file in the same
directory that we executed it in, so there is nothing else that we need
to do.

Interaction with the browser
----------------------------

Now, let's look at the magic. If you know the steps to use, writing
acceptance tests with Mink will be like a game. First, add the following
feature in feature/search.feature:

    Feature: Search In order to find repositories As a website user I
    need to be able to search repositories by name Background: Given I
    am on "/picahielos" And I follow "Repositories" Scenario: Searching
    existing repository When I fill in "zap" for "q" And I press
    "Search" Then I should see "picahielos/zap" Scenario: Searching
    non-existing repository When I fill in "yolo" for "q" And I press
    "Search" Then I should not see "picahielos/yolo"

The first thing to note is that we have a Background section. This
section assumes that the user visited the
`*https://github.com/picahielos* <https://github.com/picahielos>`__ page
and clicked on the Repositories link. Using *I follow* with some string
is the equivalent of trying to find a link with this string and clicking
on it.

The first scenario used the *When I fill <field> with <value>* step,
which basically tries to find the input field on the page (you can
either specify the ID or name), and types the value for you. In this
case, the q field was the search bar, and we typed zap. Then, similar to
when clicking on the links, the *I press <button>* line will try to find
the button by name, ID, or value, and will click on it. Finally, *Then I
should see* followed by a string will assert that the given string could
be found on the page. In short, the test launched a browser, going to
the specified URL, clicking on the Repositories link, searching for the
zap repository, and asserting that it could find it. In a similar way,
the second scenario tried to find a repository that does not exist.

If you run the tests, they should pass, but you will not see any
browser. Remember that Goutte is a headless browser web driver. However,
check how fast these tests are executed; in my laptop, it took less than
3 seconds! Can you imagine anyone performing these two tests manually in
less than this time?

One last thing: having a cheat sheet of predefined Mink steps is one of
the handiest things to have near your desk; you can find one at
`*http://blog.lepine.pro/images/2012-03-behat-cheat-sheet-en.pdf* <http://blog.lepine.pro/images/2012-03-behat-cheat-sheet-en.pdf>`__.
As you can see, we did not write a single line of code, and we still
have two tests making sure that the website works as expected. Also, if
you need to add a fancier step, do not worry; you can still implement
your step definitions as we did in Behat previously while taking
advantage of the web driver's interface that Mink provides. We recommend
you to go through the official documentation in order to take a look at
the complete list of things that you can do with Mink.

Summary
=======

In this concluding chapter, you learned how important it is to
coordinate the business with the application. For this, you saw what BDD
is and how to implement it with your PHP web applications using Behat
and Mink. This also gives you the ability to test the UI with web
drivers, which you could not do it with unit tests and PHPUnit. Now, you
can make sure that not only is your application bug-free and secure, but
also that it does what the business needs it to do.

Congratulations on reaching the end of the book! You started as an
inexperienced developer, but now you are able to write simple and
complex websites and REST APIs with PHP and have an extensive knowledge
of good test practices. You have even worked with a couple of famous PHP
frameworks, so you are ready to either start a new project with them or
join a team that uses one of them.

Now, you might be wondering: what do I do next? You already know the
theory—well, some of it—so we would recommend that you practice a lot.
There are several ways you can do this: by creating your own
application, joining a team working on open source projects, or working
for a company. Try to keep up to date with new releases of the language
or the tools and frameworks, discover a new framework from time to time,
and never stop reading. Expanding your set of skills is always a great
idea!

If you run out of ideas on what to read next, here are some hints. We
did not go through the frontend part too much, so you might be
interested in reading about CSS and specially JavaScript. JavaScript has
become the main character in these last few years, so do not miss it
out. If you are rather interested in the backend side and how to manage
applications properly, try discovering new technologies, such as
continuous integration tools similar to Jenkins. Finally, if you prefer
to focus on the theory and "science" side, you can read about how to
write quality code with *Code Complete*, *Steve McConnell*, or how to
make good use of design patterns with *Design Patterns: Elements of
Reusable Object-Oriented Software*, *Erich Gamma, John Vlissides, Ralph
Johnson, and Richard Helm*, a gang of four.

Always enjoy and have fun when developing. Always!

Index

A

-  abstract classes

   -  about / `Abstract classes <#Top_of_ch04s06_html>`__

-  acceptance tests

   -  about / `Types of tests <#Top_of_ch07_html>`__

   -  versus unit tests / `Unit tests versus acceptance
      tests <#Top_of_ch10_html>`__

-  aliases

   -  URL / `Managing dependencies <#Top_of_ch06s02_html>`__

-  anonymous functions

   -  about / `Anonymous functions <#Top_of_ch04s11_html>`__

-  Apache

   -  reference / `The PHP built-in server <#Top_of_ch02s03_html>`__

-  API

   -  about / `Introducing APIs <#Top_of_ch09_html>`__

-  APIs

   -  testing, with browsers / `Testing APIs with
      browsers <#Top_of_ch09s05_html>`__

   -  testing, with command line / `Testing APIs using the command
      line <#Top_of_ch09s05_html>`__

-  arguments by value

   -  versus arguments by reference / `*Function
      arguments* <#Top_of_ch03s08_html>`__

-  arithmetic operators

   -  about / `Arithmetic operators <#Top_of_ch03s03_html>`__

-  array functions

   -  about / `Other array functions <#Top_of_ch03s05_html>`__

-  arrays

   -  about / `*Arrays* <#Top_of_ch03s05_html>`__

   -  initializing / `Initializing arrays <#Top_of_ch03s05_html>`__

   -  populating / `Populating arrays <#Top_of_ch03s05_html>`__

   -  accessing / `Accessing arrays <#Top_of_ch03s05_html>`__

   -  isset function / `The empty and isset
      functions <#Top_of_ch03s05_html>`__

   -  empty function / `The empty and isset
      functions <#Top_of_ch03s05_html>`__

   -  elements, searching in / `Searching for elements in an
      array <#Top_of_ch03s05_html>`__

   -  ordering / `Ordering arrays <#Top_of_ch03s05_html>`__

-  assertions

   -  about / `*Assertions* <#Top_of_ch07s03_html>`__

   -  reference / `*Assertions* <#Top_of_ch07s03_html>`__

-  assignment operators

   -  about / `Assignment operators <#Top_of_ch03s03_html>`__

-  authentication

   -  about / `REST API security <#Top_of_ch09s03_html>`__

-  authorization

   -  about / `REST API security <#Top_of_ch09s03_html>`__

-  autoloader

   -  about / `Autoloading classes <#Top_of_ch04s05_html>`__

-  autoloading

   -  about / `Autoloading classes <#Top_of_ch04s05_html>`__

B

-  BDD

   -  versus TDD / `TDD versus BDD <#Top_of_ch10_html>`__

-  BDD, with Behat

   -  about / `BDD with Behat <#Top_of_ch10s02_html>`__

-  Behat

   -  about / `BDD with Behat <#Top_of_ch10s02_html>`__

-  behavior-driven development

   -  about / `Behavior-driven development <#Top_of_ch10_html>`__

-  behavioral specifications

   -  about / `Business writing tests <#Top_of_ch10_html>`__

-  best practices, REST APIs

   -  about / `Best practices with REST APIs <#Top_of_ch09s06_html>`__

   -  consistency, in endpoints / `Consistency in your
      endpoints <#Top_of_ch09s06_html>`__

   -  documenting / `Document as much as you
      can <#Top_of_ch09s06_html>`__

   -  filters / `Filters and pagination <#Top_of_ch09s06_html>`__

   -  pagination / `Filters and pagination <#Top_of_ch09s06_html>`__

   -  API versioning / `*API versioning* <#Top_of_ch09s06_html>`__

   -  HTTP cache, using / `*Using HTTP cache* <#Top_of_ch09s06_html>`__

-  browsers

   -  APIs, testing with / `Testing APIs with
      browsers <#Top_of_ch09s05_html>`__

-  business writing tests

   -  about / `Business writing tests <#Top_of_ch10_html>`__

C

-  cache layer

   -  about / `*Cache* <#Top_of_ch08s02_html>`__

-  callable

   -  about / `Anonymous functions <#Top_of_ch04s11_html>`__

-  casting

   -  about / `Getting information from the
      user <#Top_of_ch03s06_html>`__

   -  versus type juggling / `Getting information from the
      user <#Top_of_ch03s06_html>`__

-  C for controller

   -  defining / `C for controller <#Top_of_ch06s06_html>`__

   -  error controller / `The error controller <#Top_of_ch06s06_html>`__

   -  login controller / `The login controller <#Top_of_ch06s06_html>`__

   -  book controller / `The book controller <#Top_of_ch06s06_html>`__

   -  books, borrowing / `*Borrowing books* <#Top_of_ch06s06_html>`__

   -  sales controller / `The sales controller <#Top_of_ch06s06_html>`__

-  class

   -  about / `Classes and objects <#Top_of_ch04_html>`__

-  class constructors

   -  about / `Class constructors <#Top_of_ch04_html>`__

-  classes

   -  conventions / `Properties and methods
      visibility <#Top_of_ch04s02_html>`__

   -  autoloading / `Autoloading classes <#Top_of_ch04s05_html>`__

-  class methods

   -  about / `Class methods <#Top_of_ch04_html>`__

-  class properties

   -  about / `Class properties <#Top_of_ch04_html>`__

-  code coverage

   -  about / `Unit tests and code coverage <#Top_of_ch07_html>`__

-  command line

   -  APIs, testing with / `Testing APIs using the command
      line <#Top_of_ch09s05_html>`__

-  comparison operators

   -  about / `Comparison operators <#Top_of_ch03s03_html>`__

-  components, frameworks

   -  router / `The main parts of a framework <#Top_of_ch08_html>`__

   -  request / `The main parts of a framework <#Top_of_ch08_html>`__

   -  configuration handler / `The main parts of a
      framework <#Top_of_ch08_html>`__

   -  template engine / `The main parts of a
      framework <#Top_of_ch08_html>`__

   -  logger / `The main parts of a framework <#Top_of_ch08_html>`__

   -  dependency injector / `The main parts of a
      framework <#Top_of_ch08_html>`__

-  Composer

   -  reference / `Installing Composer <#Top_of_ch01s03_html>`__

   -  using / `Using Composer <#Top_of_ch06s02_html>`__

   -  dependencies, managing / `*Managing
      dependencies* <#Top_of_ch06s02_html>`__

   -  autoloader, with PSR-4 / `*Autoloader with
      PSR-4* <#Top_of_ch06s02_html>`__

   -  metadata, adding / `*Adding metadata* <#Top_of_ch06s02_html>`__

   -  index.php file / `The index.php file <#Top_of_ch06s02_html>`__

-  conditionals

   -  about / `Control structures <#Top_of_ch03s07_html>`__,
      `Conditionals <#Top_of_ch03s07_html>`__

-  constraints

   -  about / `Keys and constraints <#Top_of_ch05s03_html>`__

-  continuous integration (CI)

   -  about / `Introducing continuous integration <#Top_of_ch10_html>`__

-  controllers

   -  about / `The MVC pattern <#Top_of_ch06_html>`__

-  control structures

   -  about / `Control structures <#Top_of_ch03s07_html>`__

   -  conditionals / `*Conditionals* <#Top_of_ch03s07_html>`__

   -  switch…case / `*Switch…case* <#Top_of_ch03s07_html>`__

   -  loops / `*Loops* <#Top_of_ch03s07_html>`__

-  cookies

   -  data, persisting with / `Persisting data with
      cookies <#Top_of_ch03s06_html>`__

-  CSS

   -  about / `HTML, CSS, and JavaScript <#Top_of_ch02s02_html>`__

-  cURL

   -  about / `Setting up the application <#Top_of_ch09s04_html>`__

D

-  data

   -  persisting, with cookies / `Persisting data with
      cookies <#Top_of_ch03s06_html>`__

   -  inserting / `Inserting data <#Top_of_ch05s04_html>`__

   -  querying / `Querying data <#Top_of_ch05s05_html>`__

   -  updating / `Updating and deleting data <#Top_of_ch05s09_html>`__,
      `Updating data <#Top_of_ch05s09_html>`__

   -  deleting / `Deleting data <#Top_of_ch05s09_html>`__

-  databases

   -  versus files / `*Writing files* <#Top_of_ch03s09_html>`__

   -  about / `Introducing databases <#Top_of_ch05_html>`__

   -  MySQL / `*MySQL* <#Top_of_ch05_html>`__

-  databases, data types

   -  about / `Database data types <#Top_of_ch05s02_html>`__

   -  numeric data types / `*Numeric data
      types* <#Top_of_ch05s02_html>`__

   -  string data types / `*String data types* <#Top_of_ch05s02_html>`__

   -  list of values / `*List of values* <#Top_of_ch05s02_html>`__

   -  date and time data types / `*Date and time data
      types* <#Top_of_ch05s02_html>`__

-  database testing

   -  about / `Database testing <#Top_of_ch07s05_html>`__

-  data providers

   -  about / `Data providers <#Top_of_ch07s03_html>`__

-  data providing

   -  about / `Data providers <#Top_of_ch07s03_html>`__

-  Data Source Name (DSN) / `Connecting to the
   database <#Top_of_ch05s06_html>`__

-  data types

   -  about / `Data types <#Top_of_ch03s02_html>`__

   -  Booleans / `*Data types* <#Top_of_ch03s02_html>`__

   -  integers / `*Data types* <#Top_of_ch03s02_html>`__

   -  floats / `Data types <#Top_of_ch03s02_html>`__

   -  strings / `Data types <#Top_of_ch03s02_html>`__

   -  reference / `Database data types <#Top_of_ch05s02_html>`__

-  date and time data types

   -  about / `Date and time data types <#Top_of_ch05s02_html>`__

   -  reference link / `Date and time data
      types <#Top_of_ch05s02_html>`__

-  decrementing operators

   -  about / `Incrementing and decrementing
      operators <#Top_of_ch03s03_html>`__

-  DELETE method / `*DELETE* <#Top_of_ch09s03_html>`__

-  dependency injection

   -  defining / `Dependency injection <#Top_of_ch06s07_html>`__

   -  about / `Dependency injection <#Top_of_ch06s07_html>`__

   -  need for / `Why is dependency injection
      necessary? <#Top_of_ch06s07_html>`__

-  dependency injector

   -  implementing / `Implementing our own dependency
      injector <#Top_of_ch06s07_html>`__

-  design patterns

   -  about / `Design patterns <#Top_of_ch04s10_html>`__

   -  factory / `*Factory* <#Top_of_ch04s10_html>`__

   -  singleton / `*Singleton* <#Top_of_ch04s10_html>`__

-  DesignPatternsPHP

   -  reference / `Design patterns <#Top_of_ch04s10_html>`__

-  DI

   -  models, injecting with / `*Injecting models with
      DI* <#Top_of_ch07s04_html>`__

-  doubles

   -  testing with / `Testing with doubles <#Top_of_ch07s04_html>`__

-  do…while loop / `*Do…while* <#Top_of_ch03s07_html>`__

E

-  elements

   -  searching, in array / `Searching for elements in an
      array <#Top_of_ch03s05_html>`__

-  Eloquent JavaScript

   -  reference / `HTML, CSS, and JavaScript <#Top_of_ch02s02_html>`__

-  empty function

   -  about / `The empty and isset functions <#Top_of_ch03s05_html>`__

-  encapsulation

   -  about / `*Encapsulation* <#Top_of_ch04s02_html>`__

-  environment

   -  setting up, with Vagrant / `Setting up the environment with
      Vagrant <#Top_of_ch01_html>`__

-  environment setup, on OS X

   -  about / `Setting up the environment on OS
      X <#Top_of_ch01s02_html>`__

   -  PHP, installing / `*Installing PHP* <#Top_of_ch01s02_html>`__

   -  MySQL, installing / `*Installing MySQL* <#Top_of_ch01s02_html>`__

   -  Nginx, installing / `*Installing Nginx* <#Top_of_ch01s02_html>`__

   -  Composer, installing / `*Installing
      Composer* <#Top_of_ch01s02_html>`__

-  environment setup, on Ubuntu

   -  about / `Setting up the environment on
      Ubuntu <#Top_of_ch01s04_html>`__

   -  PHP, installing / `*Installing PHP* <#Top_of_ch01s04_html>`__

   -  MySQL, installing / `*Installing MySQL* <#Top_of_ch01s04_html>`__

   -  Nginx, installing / `*Installing Nginx* <#Top_of_ch01s04_html>`__

-  environment setup, on Windows

   -  about / `Setting up the environment on
      Windows <#Top_of_ch01s03_html>`__

   -  PHP, installing / `*Installing PHP* <#Top_of_ch01s03_html>`__

   -  MySQL, installing / `*Installing MySQL* <#Top_of_ch01s03_html>`__

   -  Nginx, installing / `*Installing Nginx* <#Top_of_ch01s03_html>`__

   -  Composer, installing / `*Installing
      Composer* <#Top_of_ch01s03_html>`__

-  escape characters

   -  about / `Working with strings <#Top_of_ch03s04_html>`__

-  exception handling

   -  trycatch block / `The try…catch block <#Top_of_ch04s09_html>`__

   -  finally block / `The finally block <#Top_of_ch04s09_html>`__

-  exceptions

   -  handling / `Handling exceptions <#Top_of_ch04s09_html>`__

   -  catching / `Catching different types of
      exceptions <#Top_of_ch04s09_html>`__

-  exit condition / `*For* <#Top_of_ch03s07_html>`__

-  expecting exceptions

   -  about / `Expecting exceptions <#Top_of_ch07s03_html>`__

-  expression

   -  about / `*Operators* <#Top_of_ch03s03_html>`__

F

-  factory design pattern

   -  about / `*Factory* <#Top_of_ch04s10_html>`__

-  feature

   -  about / `Introducing the Gherkin
      language <#Top_of_ch10s02_html>`__

-  features, frameworks

   -  about / `Other features of frameworks <#Top_of_ch08s02_html>`__

   -  authentication / `Authentication and
      roles <#Top_of_ch08s02_html>`__

   -  roles / `Authentication and roles <#Top_of_ch08s02_html>`__

   -  Object-relational mapping (ORM) / `*ORM* <#Top_of_ch08s02_html>`__

   -  cache / `*Cache* <#Top_of_ch08s02_html>`__

   -  internationalization /
      `*Internationalization* <#Top_of_ch08s02_html>`__

-  feature tests

   -  running / `Running feature tests <#Top_of_ch10s02_html>`__

-  fetch mode

   -  advantages / `The book model <#Top_of_ch06s04_html>`__

   -  disadvantages / `*The book model* <#Top_of_ch06s04_html>`__

-  fields

   -  about / `Schemas and tables <#Top_of_ch05s02_html>`__

-  fields, table

   -  NOT NULL / `Managing tables <#Top_of_ch05s02_html>`__

   -  UNSIGNED / `Managing tables <#Top_of_ch05s02_html>`__

   -  DEFAULT <value> / `*Managing tables* <#Top_of_ch05s02_html>`__

-  files

   -  reading / `Reading files <#Top_of_ch03s09_html>`__

   -  writing / `Writing files <#Top_of_ch03s09_html>`__

   -  versus databases / `*Writing files* <#Top_of_ch03s09_html>`__

-  filesystem

   -  about / `The filesystem <#Top_of_ch03s09_html>`__

-  filesystem functions

   -  about / `Other filesystem functions <#Top_of_ch03s09_html>`__

-  finally block

   -  about / `The finally block <#Top_of_ch04s09_html>`__

-  foreach loop / `*Foreach* <#Top_of_ch03s07_html>`__

-  foreign key behaviors / `*Foreign key
   behaviors* <#Top_of_ch05s09_html>`__

-  foreign keys

   -  about / `Foreign keys <#Top_of_ch05s03_html>`__

-  for loop / `*For* <#Top_of_ch03s07_html>`__

-  foundations, REST APIs

   -  HTTP request methods / `*HTTP request
      methods* <#Top_of_ch09s03_html>`__

   -  status codes, in responses / `*Status codes in
      responses* <#Top_of_ch09s03_html>`__

   -  REST API security / `*REST API security* <#Top_of_ch09s03_html>`__

-  framework, types

   -  about / `Types of frameworks <#Top_of_ch08s03_html>`__

   -  complete / `Complete and robust
      frameworks <#Top_of_ch08s03_html>`__

   -  robust / `Complete and robust frameworks <#Top_of_ch08s03_html>`__

   -  lightweight / `Lightweight and flexible
      frameworks <#Top_of_ch08s03_html>`__

   -  flexible / `Lightweight and flexible
      frameworks <#Top_of_ch08s03_html>`__

-  frameworks

   -  reviewing / `Reviewing frameworks <#Top_of_ch08_html>`__

   -  purpose / `The purpose of frameworks <#Top_of_ch08_html>`__

   -  parts / `The main parts of a framework <#Top_of_ch08_html>`__

   -  components / `The main parts of a framework <#Top_of_ch08_html>`__

   -  features / `Other features of frameworks <#Top_of_ch08s02_html>`__

   -  overview / `An overview of famous
      frameworks <#Top_of_ch08s04_html>`__

   -  Symfony 2 / `*Symfony 2* <#Top_of_ch08s04_html>`__

   -  Zend Framework 2 / `*Zend Framework 2* <#Top_of_ch08s04_html>`__

-  function arguments / `*Function arguments* <#Top_of_ch03s08_html>`__

-  functions

   -  about / `*Functions* <#Top_of_ch03s08_html>`__

   -  declaring / `Function declaration <#Top_of_ch03s08_html>`__

-  functions, arrays

   -  reference / `Ordering arrays <#Top_of_ch03s05_html>`__, `Other
      array functions <#Top_of_ch03s05_html>`__

-  functions, date and time data types

   -  DAY() / `Date and time data types <#Top_of_ch05s02_html>`__

   -  MONTH() / `Date and time data types <#Top_of_ch05s02_html>`__

   -  YEAR() / `Date and time data types <#Top_of_ch05s02_html>`__

   -  HOUR() / `Date and time data types <#Top_of_ch05s02_html>`__

   -  MINUTE() / `Date and time data types <#Top_of_ch05s02_html>`__

   -  SECOND() / `Date and time data types <#Top_of_ch05s02_html>`__

   -  CURRENT\_DATE() / `Date and time data
      types <#Top_of_ch05s02_html>`__

   -  CURRENT\_TIME() / `Date and time data
      types <#Top_of_ch05s02_html>`__

   -  NOW() / `Date and time data types <#Top_of_ch05s02_html>`__

   -  DATE\_FORMAT() / `Date and time data
      types <#Top_of_ch05s02_html>`__

   -  DATE\_ADD() / `Date and time data types <#Top_of_ch05s02_html>`__

-  functions, PDO

   -  beginTransaction / `Working with
      transactions <#Top_of_ch05s10_html>`__

   -  commit / `Working with transactions <#Top_of_ch05s10_html>`__

   -  rollBack / `Working with transactions <#Top_of_ch05s10_html>`__

-  functions, PHP files

   -  include / `*PHP files* <#Top_of_ch03_html>`__

   -  require / `*PHP files* <#Top_of_ch03_html>`__

   -  include\_once / `*PHP files* <#Top_of_ch03_html>`__

   -  require\_once / `*PHP files* <#Top_of_ch03_html>`__

-  functions, strings

   -  reference / `Working with strings <#Top_of_ch03s04_html>`__

   -  strlen / `Working with strings <#Top_of_ch03s04_html>`__

   -  trim / `Working with strings <#Top_of_ch03s04_html>`__

   -  strtolower / `Working with strings <#Top_of_ch03s04_html>`__

   -  strtoupper / `Working with strings <#Top_of_ch03s04_html>`__

   -  str\_replace / `Working with strings <#Top_of_ch03s04_html>`__

   -  substr / `Working with strings <#Top_of_ch03s04_html>`__

   -  strpos / `Working with strings <#Top_of_ch03s04_html>`__

G

-  GET method / `*GET* <#Top_of_ch09s03_html>`__

-  getter

   -  about / `*Encapsulation* <#Top_of_ch04s02_html>`__

-  Gherkin

   -  about / `Introducing the Gherkin
      language <#Top_of_ch10s02_html>`__

-  Given-When-Then test cases

   -  writing / `Writing Given-When-Then test
      cases <#Top_of_ch10s02_html>`__

-  Goutte

   -  Mink, installing with / `Installing Mink with
      Goutte <#Top_of_ch10s03_html>`__

-  Graphical User Interface (GUI)

   -  about / `*MySQL* <#Top_of_ch05_html>`__

-  Guzzle

   -  about / `Setting up the application <#Top_of_ch09s04_html>`__

H

-  HTML

   -  about / `HTML, CSS, and JavaScript <#Top_of_ch02s02_html>`__

-  HTML forms

   -  about / `HTML forms <#Top_of_ch03s06_html>`__

-  HTTP

   -  about / `The HTTP protocol <#Top_of_ch02_html>`__

-  HTTP message, parts

   -  about / `Parts of the message <#Top_of_ch02_html>`__

   -  URI / `*URL* <#Top_of_ch02_html>`__

   -  HTTP method / `The HTTP method <#Top_of_ch02_html>`__

   -  body / `*Body* <#Top_of_ch02_html>`__

   -  headers / `*Headers* <#Top_of_ch02_html>`__

   -  status code / `The status code <#Top_of_ch02_html>`__

-  HTTP method

   -  about / `The HTTP method <#Top_of_ch02_html>`__

   -  GET / `The HTTP method <#Top_of_ch02_html>`__

   -  POST / `The HTTP method <#Top_of_ch02_html>`__

   -  PUT / `The HTTP method <#Top_of_ch02_html>`__

   -  DELETE / `The HTTP method <#Top_of_ch02_html>`__

   -  OPTION / `The HTTP method <#Top_of_ch02_html>`__

-  HTTP protocol

   -  about / `The HTTP protocol <#Top_of_ch02_html>`__

   -  interchange of messages, example / `*A simple
      example* <#Top_of_ch02_html>`__

   -  complex example / `A more complex example <#Top_of_ch02_html>`__

-  HTTP request methods

   -  about / `HTTP request methods <#Top_of_ch09s03_html>`__

   -  GET / `*GET* <#Top_of_ch09s03_html>`__

   -  POST / `POST and PUT <#Top_of_ch09s03_html>`__

   -  PUT / `POST and PUT <#Top_of_ch09s03_html>`__

   -  DELETE / `*DELETE* <#Top_of_ch09s03_html>`__

I

-  500 internal server error / `*5xx – server
   error* <#Top_of_ch09s03_html>`__

-  Illuminate\\Database\\Eloquent\\Model / `*Project
   setup* <#Top_of_ch08s05_html>`__

-  impersonification

   -  about / `Authentication and roles <#Top_of_ch08s02_html>`__

-  incrementing operators

   -  about / `Incrementing and decrementing
      operators <#Top_of_ch03s03_html>`__

-  indexes

   -  about / `*Indexes* <#Top_of_ch05s03_html>`__

-  infinite loops / `*While* <#Top_of_ch03s07_html>`__

-  information hiding

   -  about / `*Encapsulation* <#Top_of_ch04s02_html>`__

-  inheritance

   -  about / `Inheritance <#Top_of_ch04s06_html>`__, `Introducing
      inheritance <#Top_of_ch04s06_html>`__

   -  methods, overriding / `*Overriding
      methods* <#Top_of_ch04s06_html>`__

   -  abstract classes / `*Abstract classes* <#Top_of_ch04s06_html>`__

-  installing

   -  Vagrant / `Installing Vagrant <#Top_of_ch01_html>`__

   -  Mink, with Goutte / `Installing Mink with
      Goutte <#Top_of_ch10s03_html>`__

-  integration tests

   -  about / `Types of tests <#Top_of_ch07_html>`__

-  interface

   -  about / `*Interfaces* <#Top_of_ch04s07_html>`__

-  internationalization

   -  about / `*Internationalization* <#Top_of_ch08s02_html>`__

-  isset function

   -  about / `The empty and isset functions <#Top_of_ch03s05_html>`__

J

-  JavaScript

   -  about / `HTML, CSS, and JavaScript <#Top_of_ch02s02_html>`__

-  join queries

   -  about / `Joining tables <#Top_of_ch05s07_html>`__

K

-  keys

   -  about / `Keys and constraints <#Top_of_ch05s03_html>`__

   -  primary keys / `*Primary keys* <#Top_of_ch05s03_html>`__

   -  foreign keys / `*Foreign keys* <#Top_of_ch05s03_html>`__

   -  unique keys / `*Unique keys* <#Top_of_ch05s03_html>`__

L

-  lambda functions

   -  about / `Anonymous functions <#Top_of_ch04s11_html>`__

-  Laravel

   -  versus Silex / `Silex versus Laravel <#Top_of_ch08s07_html>`__

-  Laravel framework

   -  about / `The Laravel framework <#Top_of_ch08s05_html>`__

   -  installation / `*Installation* <#Top_of_ch08s05_html>`__

   -  project setup / `*Project setup* <#Top_of_ch08s05_html>`__

   -  first endpoint, adding / `Adding the first
      endpoint <#Top_of_ch08s05_html>`__

   -  users, managing / `*Managing users* <#Top_of_ch08s05_html>`__

   -  relationships, setting up in models / `*Setting up relationships
      in models* <#Top_of_ch08s05_html>`__

   -  complex controllers, creating / `*Creating complex
      controllers* <#Top_of_ch08s05_html>`__

   -  tests, adding / `*Adding tests* <#Top_of_ch08s05_html>`__

-  layout

   -  about / `Layouts and blocks <#Top_of_ch06s05_html>`__

-  lazy load

   -  about / `The sales model <#Top_of_ch06s04_html>`__

-  left joins

   -  about / `Joining tables <#Top_of_ch05s07_html>`__

-  list of values

   -  about / `List of values <#Top_of_ch05s02_html>`__

-  lists

   -  about / `*Arrays* <#Top_of_ch03s05_html>`__

-  logical operators

   -  about / `Logical operators <#Top_of_ch03s03_html>`__

-  loops

   -  about / `Control structures <#Top_of_ch03s07_html>`__,
      `Loops <#Top_of_ch03s07_html>`__

   -  while loop / `*While* <#Top_of_ch03s07_html>`__

   -  do…while loop / `*Do…while* <#Top_of_ch03s07_html>`__

   -  for loop / `*For* <#Top_of_ch03s07_html>`__

   -  foreach / `*Foreach* <#Top_of_ch03s07_html>`__

M

-  magic methods

   -  about / `Magic methods <#Top_of_ch04_html>`__

   -  \_\_toString / `Magic methods <#Top_of_ch04_html>`__

   -  \_\_call / `Magic methods <#Top_of_ch04_html>`__

   -  \_\_get / `Magic methods <#Top_of_ch04_html>`__

-  maps

   -  about / `*Arrays* <#Top_of_ch03s05_html>`__

-  methods

   -  overriding / `Overriding methods <#Top_of_ch04s06_html>`__

-  methods visibility

   -  about / `Properties and methods
      visibility <#Top_of_ch04s02_html>`__

-  M for model

   -  defining / `M for model <#Top_of_ch06s04_html>`__

   -  customer model / `The customer model <#Top_of_ch06s04_html>`__

   -  book model / `The book model <#Top_of_ch06s04_html>`__

   -  sales model / `The sales model <#Top_of_ch06s04_html>`__

-  Mink

   -  used, for testing with browser / `Testing with a browser using
      Mink <#Top_of_ch10s03_html>`__

   -  installing, with Goutte / `Installing Mink with
      Goutte <#Top_of_ch10s03_html>`__

   -  browser interaction / `Interaction with the
      browser <#Top_of_ch10s03_html>`__

-  mocks

   -  using / `Using mocks <#Top_of_ch07s04_html>`__

-  models

   -  about / `The MVC pattern <#Top_of_ch06_html>`__

   -  injecting, with DI / `Injecting models with
      DI <#Top_of_ch07s04_html>`__

-  Monolog

   -  about / `Adding a logger <#Top_of_ch08s06_html>`__

   -  reference / `Adding a logger <#Top_of_ch08s06_html>`__

-  MVC pattern

   -  defining / `The MVC pattern <#Top_of_ch06_html>`__

-  MySQL

   -  about / `*MySQL* <#Top_of_ch05_html>`__

-  MySQL engines

   -  reference / `Managing tables <#Top_of_ch05s02_html>`__

-  MySQL server installer

   -  reference / `Installing MySQL <#Top_of_ch01s02_html>`__,
      `Installing MySQL <#Top_of_ch01s03_html>`__

-  MySQL Workbench

   -  reference / `Installing MySQL <#Top_of_ch01s02_html>`__

N

-  namespaces

   -  about / `*Namespaces* <#Top_of_ch04s04_html>`__

-  Nginx

   -  reference / `The PHP built-in server <#Top_of_ch02s03_html>`__

-  numeric data types

   -  about / `Numeric data types <#Top_of_ch05s02_html>`__

O

-  OAuth2 authentication

   -  database, setting up / `Setting up the
      database <#Top_of_ch09s07_html>`__

   -  client-credentials authentication, enabling / `*Enabling
      client-credentials authentication* <#Top_of_ch09s07_html>`__

   -  access token, requesting / `*Requesting an access
      token* <#Top_of_ch09s07_html>`__

-  OAuth 2.0

   -  about / `OAuth 2.0 <#Top_of_ch09s03_html>`__

-  OAuth2Server

   -  installing / `Installing OAuth2Server <#Top_of_ch09s07_html>`__

-  Object-relational mapping (ORM)

   -  about / `*ORM* <#Top_of_ch08s02_html>`__

-  objects

   -  about / `Classes and objects <#Top_of_ch04_html>`__

-  operator precedence

   -  about / `Operator precedence <#Top_of_ch03s03_html>`__

-  operators

   -  about / `*Operators* <#Top_of_ch03s03_html>`__

   -  arithmetic operators / `*Arithmetic
      operators* <#Top_of_ch03s03_html>`__

   -  assignment operators / `*Assignment
      operators* <#Top_of_ch03s03_html>`__

   -  comparison operators / `*Comparison
      operators* <#Top_of_ch03s03_html>`__

   -  logical operators / `*Logical operators* <#Top_of_ch03s03_html>`__

   -  decrementing operators / `Incrementing and decrementing
      operators <#Top_of_ch03s03_html>`__

   -  incrementing operators / `Incrementing and decrementing
      operators <#Top_of_ch03s03_html>`__

-  optional arguments / `*Function arguments* <#Top_of_ch03s08_html>`__

-  overindexing

   -  about / `*Indexes* <#Top_of_ch05s03_html>`__

-  overloaded functions / `*Function
   declaration* <#Top_of_ch03s08_html>`__

P

-  Packagist

   -  about / `Adding metadata <#Top_of_ch06s02_html>`__, `Setting up
      the application <#Top_of_ch09s04_html>`__

   -  references / `Adding metadata <#Top_of_ch06s02_html>`__

-  PDO

   -  using / `Using PDO <#Top_of_ch05s06_html>`__

   -  connecting, to database / `Connecting to the
      database <#Top_of_ch05s06_html>`__

   -  queries, performing / `*Performing
      queries* <#Top_of_ch05s06_html>`__

   -  prepared statements / `*Prepared
      statements* <#Top_of_ch05s06_html>`__

-  PHP

   -  reference / `Autoloader with PSR-4 <#Top_of_ch06s02_html>`__

-  PHP, and HTML

   -  mixing / `*Conditionals* <#Top_of_ch03s07_html>`__

-  PHP, in web applications

   -  about / `PHP in web applications <#Top_of_ch03s06_html>`__

   -  information, obtaining from user / `*Getting information from the
      user* <#Top_of_ch03s06_html>`__

   -  HTML forms / `*HTML forms* <#Top_of_ch03s06_html>`__

   -  data, persisting with cookies / `*Persisting data with
      cookies* <#Top_of_ch03s06_html>`__

-  PHP built-in server

   -  about / `The PHP built-in server <#Top_of_ch02s03_html>`__

-  PHP files

   -  about / `PHP files <#Top_of_ch03_html>`__

   -  functions / `*PHP files* <#Top_of_ch03_html>`__

-  PHP functions, filesystem

   -  file\_exists / `Other filesystem
      functions <#Top_of_ch03s09_html>`__

   -  is\_writable / `Other filesystem
      functions <#Top_of_ch03s09_html>`__

   -  reference / `Other filesystem functions <#Top_of_ch03s09_html>`__

-  PHP installer

   -  reference / `Installing PHP <#Top_of_ch01s03_html>`__

-  PHPUnit

   -  about / `Integrating PHPUnit <#Top_of_ch07s02_html>`__

   -  integrating / `Integrating PHPUnit <#Top_of_ch07s02_html>`__

-  phpunit.xml file

   -  about / `The phpunit.xml file <#Top_of_ch07s02_html>`__

-  Pimple

   -  about / `Project setup <#Top_of_ch08s06_html>`__

-  polymorphism

   -  about / `*Polymorphism* <#Top_of_ch04s07_html>`__

-  POST method / `*POST and PUT* <#Top_of_ch09s03_html>`__

-  prepared statements / `*Prepared
   statements* <#Top_of_ch05s06_html>`__

-  primary keys

   -  about / `Primary keys <#Top_of_ch05s03_html>`__

-  production web servers

   -  about / `The PHP built-in server <#Top_of_ch02s03_html>`__

-  project setup, Silex microframework

   -  about / `Project setup <#Top_of_ch08s06_html>`__

   -  configuration, managing / `*Managing
      configuration* <#Top_of_ch08s06_html>`__

   -  template engine, setting / `Setting the template
      engine <#Top_of_ch08s06_html>`__

   -  logger, adding / `*Adding a logger* <#Top_of_ch08s06_html>`__

-  properties visibility

   -  about / `Properties and methods
      visibility <#Top_of_ch04s02_html>`__

-  PUT method / `*POST and PUT* <#Top_of_ch09s03_html>`__

Q

-  queries

   -  grouping / `Grouping queries <#Top_of_ch05s08_html>`__

R

-  receiver

   -  about / `A simple example <#Top_of_ch02_html>`__

-  reflection

   -  about / `Database testing <#Top_of_ch07s05_html>`__

   -  reference / `Database testing <#Top_of_ch07s05_html>`__

-  requests

   -  working with / `Working with requests <#Top_of_ch06s03_html>`__

   -  request object / `The request object <#Top_of_ch06s03_html>`__

   -  parameters, filtering from / `Filtering parameters from
      requests <#Top_of_ch06s03_html>`__

   -  routes, mapping to controllers / `*Mapping routes to
      controllers* <#Top_of_ch06s03_html>`__

   -  router / `The router <#Top_of_ch06s03_html>`__

-  REST API, creating with Laravel

   -  about / `Creating a REST API with
      Laravel <#Top_of_ch09s07_html>`__

   -  OAuth2 authentication, setting / `*Setting OAuth2
      authentication* <#Top_of_ch09s07_html>`__

   -  database, preparing / `Preparing the
      database <#Top_of_ch09s07_html>`__

   -  models, setting up / `Setting up the
      models <#Top_of_ch09s07_html>`__

   -  endpoints, designing / `*Designing
      endpoints* <#Top_of_ch09s07_html>`__

   -  controllers, adding / `Adding the
      controllers <#Top_of_ch09s07_html>`__

-  REST API developer

   -  toolkit / `The toolkit of the REST API
      developer <#Top_of_ch09s05_html>`__

-  REST APIs

   -  about / `Introducing REST APIs <#Top_of_ch09s02_html>`__

   -  foundations / `The foundations of REST
      APIs <#Top_of_ch09s03_html>`__

   -  best practices / `Best practices with REST
      APIs <#Top_of_ch09s06_html>`__

   -  testing / `Testing your REST APIs <#Top_of_ch09s08_html>`__

-  REST API security

   -  about / `REST API security <#Top_of_ch09s03_html>`__

   -  basic access authentication / `*Basic access
      authentication* <#Top_of_ch09s03_html>`__

   -  OAuth 2.0 / `*OAuth 2.0* <#Top_of_ch09s03_html>`__

-  return statement / `The return statement <#Top_of_ch03s08_html>`__

-  return type / `Type hinting and return
   types <#Top_of_ch03s08_html>`__

-  RFC2068 standard

   -  reference / `The HTTP protocol <#Top_of_ch02_html>`__

-  router

   -  about / `The router <#Top_of_ch06s03_html>`__

   -  URLs matching, with regular expressions / `*URLs matching with
      regular expressions* <#Top_of_ch06s03_html>`__

   -  arguments, extracting of URL / `Extracting the arguments of the
      URL <#Top_of_ch06s03_html>`__

   -  controller, executing / `Executing the
      controller <#Top_of_ch06s03_html>`__

S

-  scenarios

   -  defining / `Defining scenarios <#Top_of_ch10s02_html>`__

   -  Given-When-Then test cases, writing / `*Writing Given-When-Then
      test cases* <#Top_of_ch10s02_html>`__

   -  parts, reusing of / `Reusing parts of
      scenarios <#Top_of_ch10s02_html>`__

-  schemas

   -  about / `Schemas and tables <#Top_of_ch05s02_html>`__,
      `Understanding schemas <#Top_of_ch05s02_html>`__

-  sender

   -  about / `A simple example <#Top_of_ch02_html>`__

-  setter

   -  about / `*Encapsulation* <#Top_of_ch04s02_html>`__

-  Silex

   -  versus Laravel / `Silex versus Laravel <#Top_of_ch08s07_html>`__

   -  reference / `Silex versus Laravel <#Top_of_ch08s07_html>`__

-  Silex microframework

   -  about / `The Silex microframework <#Top_of_ch08s06_html>`__

   -  installation / `*Installation* <#Top_of_ch08s06_html>`__

   -  project setup / `*Project setup* <#Top_of_ch08s06_html>`__

   -  first endpoint, adding / `Adding the first
      endpoint <#Top_of_ch08s06_html>`__

   -  database, accessing / `Accessing the
      database <#Top_of_ch08s06_html>`__

-  singleton design pattern

   -  about / `*Singleton* <#Top_of_ch04s10_html>`__

-  spl\_autoload\_register function

   -  using / `Using the spl\_autoload\_register
      function <#Top_of_ch04s05_html>`__

-  standards, PHP

   -  PSR-0 / `Autoloader with PSR-4 <#Top_of_ch06s02_html>`__

   -  PSR-4 / `Autoloader with PSR-4 <#Top_of_ch06s02_html>`__

-  static methods

   -  about / `Static properties and methods <#Top_of_ch04s03_html>`__

-  static properties

   -  about / `Static properties and methods <#Top_of_ch04s03_html>`__

-  status codes

   -  200 / `The status code <#Top_of_ch02_html>`__

   -  401 / `The status code <#Top_of_ch02_html>`__

   -  404 / `The status code <#Top_of_ch02_html>`__

   -  500 / `The status code <#Top_of_ch02_html>`__

   -  reference / `Status codes in responses <#Top_of_ch09s03_html>`__

-  status codes, in responses

   -  about / `Status codes in responses <#Top_of_ch09s03_html>`__

   -  2xx - success / `*2xx – success* <#Top_of_ch09s03_html>`__

   -  3xx - redirection / `*3xx – redirection* <#Top_of_ch09s03_html>`__

   -  4xx - client error / `*4xx – client
      error* <#Top_of_ch09s03_html>`__

   -  5xx - server error / `*5xx – server
      error* <#Top_of_ch09s03_html>`__

-  step definitions

   -  writing / `Writing step definitions <#Top_of_ch10s02_html>`__

-  steps

   -  parameterization / `The parameterization of
      steps <#Top_of_ch10s02_html>`__

-  string data types

   -  about / `String data types <#Top_of_ch05s02_html>`__

-  strings

   -  working with / `Working with strings <#Top_of_ch03s04_html>`__

-  superglobals

   -  about / `Other superglobals <#Top_of_ch03s06_html>`__

   -  reference / `Other superglobals <#Top_of_ch03s06_html>`__

-  switch…case

   -  about / `Switch…case <#Top_of_ch03s07_html>`__

-  Symfony

   -  about / `Installing Mink with Goutte <#Top_of_ch10s03_html>`__

-  Symfony 2

   -  about / `Symfony 2 <#Top_of_ch08s04_html>`__

T

-  tables

   -  about / `Schemas and tables <#Top_of_ch05s02_html>`__

   -  managing / `Managing tables <#Top_of_ch05s02_html>`__

   -  joining / `Joining tables <#Top_of_ch05s07_html>`__

-  TDD

   -  versus BDD / `TDD versus BDD <#Top_of_ch10_html>`__

-  test-driven development (TDD)

   -  about / `Test-driven development <#Top_of_ch07s06_html>`__

   -  theory, versus practice / `*Theory versus
      practice* <#Top_of_ch07s06_html>`__

-  TestCase

   -  customizing / `Customizing TestCase <#Top_of_ch07s04_html>`__

-  tests

   -  need for / `The necessity for tests <#Top_of_ch07_html>`__

   -  types / `Types of tests <#Top_of_ch07_html>`__

   -  unit tests / `Types of tests <#Top_of_ch07_html>`__

   -  integration tests / `*Types of tests* <#Top_of_ch07_html>`__

   -  acceptance tests / `*Types of tests* <#Top_of_ch07_html>`__

   -  about / `Your first test <#Top_of_ch07s02_html>`__

   -  running / `Running tests <#Top_of_ch07s02_html>`__

-  tests, features

   -  automatic / `Types of tests <#Top_of_ch07_html>`__

   -  extensive / `Types of tests <#Top_of_ch07_html>`__

   -  immediate / `Types of tests <#Top_of_ch07_html>`__

   -  open / `Types of tests <#Top_of_ch07_html>`__

   -  useful / `Types of tests <#Top_of_ch07_html>`__

-  third-party APIs

   -  using / `Using third-party APIs <#Top_of_ch09s04_html>`__

   -  application's credentials, obtaining / `*Getting the application's
      credentials* <#Top_of_ch09s04_html>`__

   -  application, setting up / `Setting up the
      application <#Top_of_ch09s04_html>`__

   -  access token, requesting / `*Requesting an access
      token* <#Top_of_ch09s04_html>`__

   -  tweets, fetching / `*Fetching tweets* <#Top_of_ch09s04_html>`__

-  timestamps / `Persisting data with cookies <#Top_of_ch03s06_html>`__

-  tools installation, with Composer

   -  reference / `Integrating PHPUnit <#Top_of_ch07s02_html>`__

-  traits

   -  about / `*Traits* <#Top_of_ch04s08_html>`__

-  transactions

   -  working with / `Working with
      transactions <#Top_of_ch05s10_html>`__

-  trycatch block

   -  about / `The try…catch block <#Top_of_ch04s09_html>`__

-  Twig

   -  about / `Introduction to Twig <#Top_of_ch06s05_html>`__

-  Twitter

   -  reference / `Getting the application's
      credentials <#Top_of_ch09s04_html>`__

-  type hinting / `Type hinting and return
   types <#Top_of_ch03s08_html>`__

-  type juggling / `*Data types* <#Top_of_ch03s02_html>`__

   -  versus casting / `Getting information from the
      user <#Top_of_ch03s06_html>`__

U

-  unique keys

   -  about / `Unique keys <#Top_of_ch05s03_html>`__

-  unit tests

   -  about / `Types of tests <#Top_of_ch07_html>`__, `Unit tests and
      code coverage <#Top_of_ch07_html>`__

   -  writing / `Writing unit tests <#Top_of_ch07s03_html>`__

   -  start / `The start and end of a test <#Top_of_ch07s03_html>`__

   -  end / `The start and end of a test <#Top_of_ch07s03_html>`__

   -  versus acceptance tests / `Unit tests versus acceptance
      tests <#Top_of_ch10_html>`__

-  user management, Laravel framework

   -  about / `Managing users <#Top_of_ch08s05_html>`__

   -  user registration / `*User registration* <#Top_of_ch08s05_html>`__

   -  user login / `*User login* <#Top_of_ch08s05_html>`__

   -  protected routes / `*Protected routes* <#Top_of_ch08s05_html>`__

V

-  Vagrant

   -  environment, setting up with / `Setting up the environment with
      Vagrant <#Top_of_ch01_html>`__

   -  about / `Introducing Vagrant <#Top_of_ch01_html>`__

   -  installing / `Installing Vagrant <#Top_of_ch01_html>`__

   -  download page link / `*Installing Vagrant* <#Top_of_ch01_html>`__

   -  using / `Using Vagrant <#Top_of_ch01_html>`__

-  variable expanding

   -  about / `Working with strings <#Top_of_ch03s04_html>`__

-  variables

   -  about / `*Variables* <#Top_of_ch03s02_html>`__

-  variable scope / `Function declaration <#Top_of_ch03s08_html>`__

-  version control systems (VCS)

   -  about / `Introducing continuous integration <#Top_of_ch10_html>`__

-  V for view

   -  defining / `*V for view* <#Top_of_ch06s05_html>`__

   -  Twig, defining / `Introduction to Twig <#Top_of_ch06s05_html>`__

   -  book view / `The book view <#Top_of_ch06s05_html>`__

   -  layouts / `Layouts and blocks <#Top_of_ch06s05_html>`__

   -  blocks / `Layouts and blocks <#Top_of_ch06s05_html>`__

   -  paginated book list / `*Paginated book
      list* <#Top_of_ch06s05_html>`__

   -  sales view / `The sales view <#Top_of_ch06s05_html>`__

   -  error template / `The error template <#Top_of_ch06s05_html>`__

   -  login template / `The login template <#Top_of_ch06s05_html>`__

-  views

   -  about / `The MVC pattern <#Top_of_ch06_html>`__

-  visibility

   -  about / `Properties and methods
      visibility <#Top_of_ch04s02_html>`__

   -  private / `Properties and methods
      visibility <#Top_of_ch04s02_html>`__

   -  protected / `Properties and methods
      visibility <#Top_of_ch04s02_html>`__

   -  public / `Properties and methods
      visibility <#Top_of_ch04s02_html>`__

   -  working / `Properties and methods
      visibility <#Top_of_ch04s02_html>`__

W

-  web applications

   -  about / `Web applications <#Top_of_ch02s02_html>`__

-  web drivers

   -  types / `Types of web drivers <#Top_of_ch10s03_html>`__

-  web forms

   -  submitting / `A more complex example <#Top_of_ch02_html>`__

-  web page

   -  about / `Web applications <#Top_of_ch02s02_html>`__

-  web servers

   -  about / `Web servers <#Top_of_ch02s03_html>`__

   -  working / `How they work <#Top_of_ch02s03_html>`__

-  website

   -  about / `Web applications <#Top_of_ch02s02_html>`__

-  while loop / `*While* <#Top_of_ch03s07_html>`__

X

-  2xx - success status codes

   -  200 OK / `2xx – success <#Top_of_ch09s03_html>`__

   -  201 created / `*2xx – success* <#Top_of_ch09s03_html>`__

   -  202 accepted / `*2xx – success* <#Top_of_ch09s03_html>`__

-  3xx - redirection status codes

   -  301 moved permanently / `*3xx –
      redirection* <#Top_of_ch09s03_html>`__

   -  303 see other / `3xx – redirection <#Top_of_ch09s03_html>`__

-  4xx - client error status codes

   -  400 bad request / `4xx – client error <#Top_of_ch09s03_html>`__

   -  401 unauthorized / `*4xx – client error* <#Top_of_ch09s03_html>`__

   -  403 forbidden / `4xx – client error <#Top_of_ch09s03_html>`__

   -  404 not found / `4xx – client error <#Top_of_ch09s03_html>`__

   -  405 method not allowed / `*4xx – client
      error* <#Top_of_ch09s03_html>`__

-  5xx - server error / `*5xx – server error* <#Top_of_ch09s03_html>`__

Z

-  Zend Framework 2

   -  about / `Zend Framework 2 <#Top_of_ch08s04_html>`__

-  ZIP file, Nginx

   -  reference / `Installing Nginx <#Top_of_ch01s03_html>`__

