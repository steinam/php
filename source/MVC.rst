.. include:: images.txt

.. toctree::
   :maxdepth: 2
   :caption: Inhaltsverzeichnis
 

Chapter 6. Adapting to MVC
==========================

Web applications are more complex than what we have built so far. The more functionality you add, the more difficult the code is to maintain and understand. It is for this reason that structuring your code in an organized way is crucial. You could design your own structure, but as with OOP, there already exist some design patterns that try to solve this problem.

MVC (model-view-controller) has been the favorite pattern for web developers. It helps us separate the different parts of a web application, leaving the code easy to understand even for beginners. We will try to refactor our bookstore example to use the MVC pattern, and you will realize how quickly you can add new functionality after that.

In this chapter, you will learn the following:

-  Using Composer to manage dependencies

-  Designing a router for your application

-  Organizing your code into models, views, and controllers

-  Twig as the template engine

-  Dependency injection

The MVC pattern
---------------

So far, each time we have had to add a feature, we added a new PHP file with a mixture of PHP and HTML for that specific page. For chunks of code with a single purpose, and which we have to reuse, we created functions and added them to the functions file. Even for very small web applications like ours, the code starts becoming very confusing, and the ability to reuse code is not as helpful as it could be. Now imagine an application with a large number of features: that would be pretty much chaos itself.

The problems do not stop here. In our code, we have mixed HTML and PHP code in a single file. That will give us a lot of trouble when trying to change the design of the web application, or even if we want to perform a very small change across all pages, such as changing the menu or footer of the page. The more complex the application, the more problems we will encounter.

MVC came up as a pattern to help us divide the different parts of the application. These parts are known as models, views, and controllers. Models manage the data and/or the business logic, views contain the templates for our responses (for example, HTML pages), and controllers orchestrate requests, deciding what data to use and how to render the appropriate template. We will go through them in later sections of this chapter.

Using Composer
--------------

Even though this is not a necessary component when implementing the MVC pattern, Composer has been an indispensable tool for any PHP web application over the last few years. The main goal of this tool is to help you manage the dependencies of your application, that is, the third-party libraries (of code) that we need to use in our application. We can achieve that by just creating a configuration file that lists them, and by running a command in your command line.

You need to install Composer on your development machine (see `*Chapter 1* <#Top_of_ch01_html>`__, *Setting Up the Environment*). Make sure that you have it by executing the following command:

::

    $ composer –version

This should return the version of your Composer installation. If it does not, return to the installation section to fix the problem.

Managing dependencies
~~~~~~~~~~~~~~~~~~~~~

As we stated earlier, the main goal of Composer is to manage dependencies. For example, we've already implemented our configuration reader, the Config class, but if we knew of someone that implemented a better version of it, we could just use theirs instead of reinventing the wheel; just make sure that they allow you to do so!

.. admonition:: Open source

    Open source refers to the code that developers write and share with the community in order to be used by others without restrictions. There are actually different types of licenses, and some give you more flexibility than others, but the basic idea is that we can reuse the libraries that other developers have written in our applications. That helps the community to grow in knowledge, as we can learn what others have done, improve it, and share it afterwards.


We've already implemented a decent configuration reader, but there are other elements of our application that need to be done. Let's take advantage of Composer to reuse someone else's libraries. There are a couple of ways of adding a dependency to our project: executing a command in our command line, or editing the configuration file manually. As we still do not have Composer's configuration file, let's use the first option. Execute the following command in the root directory of your application:

.. code-block:: php

    $ composer require monolog/monolog

This command will show the following result:

.. code-block:: bash

    Using version ^1.17 for monolog/monolog 
    ./composer.json has been created 
    Loading composer repositories with package information
    Updating dependencies (including require-dev) - 
    Installing psr/log (1.0.0) 
    Downloading: 100% - Installing monolog/monolog (1.17.2)
    Downloading: 100% ... Writing lock file Generating autoload files

With this command, we asked Composer to add the library monolog/monolog as a dependency of our application. Having executed that, we can now see some changes in our directory:

-  We have a new file named composer.json. This is the configuration
   file where we can add our dependencies.

-  We have a new file named composer.lock. This is a file that Composer
   uses in order to track the dependencies that have already been
   installed and their versions.

-  We have a new directory named vendor. This directory contains the
   code of the dependencies that Composer downloaded.

The output of the command also shows us some extra information. In this case, it says that it downloaded two libraries or packages, even though we asked for only one. The reason is that the package that we needed also contained other dependencies that were resolved by Composer. Also note the version that Composer downloaded; as we did not specify any version, Composer took the most recent one available, but you can always try to write the specific version that you need.

We will need another library, in this case twig/twig. Let's add it to our dependencies list with the following command:

    $ composer require twig/twig

This command will show the following result:

.. code-block:: bash

    Using version ^1.23 for twig/twig ./composer.json has been updated
    Loading composer repositories with package information Updating
    dependencies (including require-dev) - 
    Installing twig/twig (v1.23.1) 
    Downloading: 100% 
    Writing lock file 
    Generating autoload files

If we check the composer.json file, we will see the following content:

.. code-block:: json

    { "require": { "monolog/monolog": "^1.17", "twig/twig": "^1.23" } }

The file is just a JSON map that contains the configuration of our application; in this case, the list of the two dependencies that we installed. As you can see, the dependencies' name follows a pattern: two words separated by a slash. The first of the words refers to the vendor that developed the library. The second of them is the name of the library itself. The dependency has a version, which could be the exact version number—as in this case—or it could contain wildcard characters or tag names. You can read more about this at
`*https://getcomposer.org/doc/articles/aliases.md* <https://getcomposer.org/doc/articles/aliases.md>`__.

Finally, if you would like to add another dependency, or edit the composer.json file in any other way, you should run composer update in your command line, or wherever the composer.json file is, in order to update the dependencies.

Autoloader with PSR-4
~~~~~~~~~~~~~~~~~~~~~

In the previous chapters, we also added an autoloader to our application. As we are now using someone else's code, we need to know how to load their classes too. Soon, developers realized that this scenario without a standard would be virtually impossible to manage, and they came out with some standards that most developers follow. You can find a lot of information on this topic at
`*http://www.php-fig.org* <http://www.php-fig.org>`__.

Nowadays, PHP has two main standards for autoloading: PSR-0 and PSR-4. They are very similar, but we will be implementing the latter, as it is the most recent standard published. This standard basically follows what we've already introduced when talking about namespaces: the namespace of a class must be the same as the directory where it is, and the name of the class should be the name of the file, followed by the extension .php. For example, the file in src/Domain/Book.php contains the class Book inside the namespace Bookstore\Domain.

Applications using Composer should follow one of those standards, and they should note in their respective composer.json file which one they are using. This means that Composer knows how to autoload its own application files, so we will not need to take care of it when we download external libraries. To specify that, we edit our composer.json file, and add the following content:

.. code-block:: json

    { "require": { "monolog/monolog": "^1.17", "twig/twig": "^1.23" },
    "autoload": { "psr-4": { "Bookstore\\\\": "src" } } }

The preceding code means that we will use PSR-4 in our application, and that all the namespaces that start with Bookstore should be found inside the src/ directory. This is exactly what our autoloader was doing already, but reduced to a couple of lines in a configuration file. We can safely remove our autoloader and any reference to it now.

Composer generates some mappings that help to speed up the loading of classes. In order to update those maps with the new information added to the configuration file, we need to run the composer update command that we ran earlier. This time, the output will tell us that there is no package to update, but the autoload files will be generated again:

.. code-block:: bash

    $ composer update 
    Loading composer repositories with package information 
    Updating dependencies (including require-dev) 
    Nothing to install or update 
    Writing lock file 
    Generating autoload files


Adding metadata
~~~~~~~~~~~~~~~

In order to know where to find the libraries that you define as dependencies, Composer keeps a repository of packages and versions, known as Packagist. This repository keeps a lot of useful information for developers, such as all the versions available for a given package, the authors, some description of what the package does (or a website pointing to that information), and the dependencies that this package will download. You can also browse the packages, searching by name or categories.

But how does Packagist know about this? It is all thanks to the composer.json file itself. In there, you can define all the metadata of your application in a format that Composer understands. Let's see an example. Add the following content to your composer.json file:

.. code-block:: json

    { "name": "picahielos/bookstore", "description": "Manages an online
    bookstore.", "minimum-stability": "stable", "license": "Apache-2.0",
    "type": "project", "authors": [ { "name": "Antonio Lopez", "email":
    "antonio.lopez.zapata@gmail.com" } ], 
    // ... }

The configuration file now contains the name of the package following the Composer convention: vendor name, slash, and the package name—in this case, picahielos/bookstore. We also add a description, license, authors, and other metadata. If you have your code in a pubic repository such as GitHub, adding this composer.json file will allow you to go to Packagist and insert the URL of your repository. Packagist will add your code as a new package, extracting the info from your composer.json file. It will show the available versions based on your tags or branches. In order to learn more about it, we encourage you to visit the official documentation at `*https://getcomposer.org/doc/04-schema.md* <https://getcomposer.org/doc/04-schema.md>`__.

The index.php file
~~~~~~~~~~~~~~~~~~


In MVC applications, we usually have one file that gets all the requests, and routes them to the specific controller depending on the URL. This logic can generally be found in the index.php file in our root directory. We already have one, but as we are adapting our features to the MVC pattern, we will not need the current index.php anymore. Hence, you can safely replace it with the following:


.. code-block:: php

    <?php require_once __DIR__ . '/vendor/autoload.php';

The only thing that this file will do now is include the file that handles all the autoloading from the Composer code. Later, we will initialize everything here, such as database connections, configuration readers, and so on, but right now, let's leave it empty.

Working with requests
---------------------

As you might recall from previous chapters, the main purpose of a web application is to process HTTP requests coming from the client and return a response. If that is the main goal of your application, managing requests and responses should be an important part of your code.

PHP is a language that can be used for scripts, but its main usage is in web applications. Due to this, the language comes ready with a lot of helpers for managing requests and responses. Still, the native way is not ideal, and as good OOP developers, we should come up with a set of classes that help with that. The main elements for this small project—still inside your application—are the request and the router. Let's start!

The request object
~~~~~~~~~~~~~~~~~~

As we start our mini framework, we need to change our directory structure a bit. We will create the src/Core directory for all the classes related to the framework. As the configuration reader from the previous chapters is also part of the framework (rather than functionality for the user), we should move the Config.php file to this directory too.

The first thing to consider is what a request looks like. If you remember `*Chapter 2* <#Top_of_ch02_html>`__, *Web Applications with PHP*, a request is basically a message that goes to a URL, and has a method—GET or POST for now. The URL is at the same time composed of two parts: the domain of the web application, that is, the name of your server, and the path of the request inside the server. For example, if you try to access http://bookstore.com/my-books, the first part, http://bookstore.com, would be the domain and /my-books would be the path. In fact, http would not be part of the domain, but we do not need that level of granularity for our application. You can get this information from the global array $\_SERVER that PHP populates for each request.

Our Request class should have a property for each of those three elements, followed by a set of getters and some other helpers that will be useful for the user. Also, we should initialize all the properties from $\_SERVER in the constructor. Let's see what it would look like:

.. code-block:: php

    <?php 
        namespace Bookstore\Core; 
        class Request { 
            const GET = 'GET';
            const POST = 'POST'; 
            
            private $domain; 
            private $path; 
            private $method; 
            
            public function __construct() { 
            
                $this->domain = $_SERVER['HTTP\_HOST']; 
                $this->path = $\_SERVER['REQUEST\_URI'];
                $this->method = $_SERVER['REQUEST\_METHOD']; 
            } 
            
            public function getUrl(): string { 
                return $this->domain . $this->path; 
            } 
            
            public function getDomain(): string { 
                return $this->domain; 
            } 
            
            public function getPath(): string { 
                return $this->path; 
            } 
            
            public function getMethod(): string { 
                return $this->method; 
            } 
            
            public function isPost(): bool { 
                return $this->method === self::POST; 
            } 
            
            public function isGet(): bool { 
                return $this->method === self::GET; 
            } 
        }


We can see in the preceding code that other than the getters for each property, we added the methods getUrl, isPost, and isGet. The user could find the same information using the already existing getters, but as they will be needed a lot, it is always good to make it easier for the user. Also note that the properties are coming from the values of the $_SERVER array: HTTP_HOST, REQUEST_URI, and REQUEST_METHOD.

Filtering parameters from requests
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Another important part of a request is the information that comes from the user, that is, the GET and POST parameters, and the cookies. As with the $_SERVER global array, this information comes from $_POST, $_GET, and $_COOKIE, but it is always good to avoid using them directly, without filtering, as the user could send malicious code.

We will now implement a class that will represent a map—key-value pairs—that can be filtered. We will call it FilteredMap, and will include it in our namespace, Bookstore\Core. We will use it to contain the parameters GET and POST and the cookies as two new properties in our Request class. The map will contain only one property, the array of data, and will have some methods to fetch information from it. To construct the object, we need to send the array of data as an argument to the constructor:

.. code-block:: php

    <?php namespace Bookstore\\Core; 
    class FilteredMap { 
        private $map;
        
        public function __construct(array $baseMap) { 
            $this->map = $baseMap; 
        } 
        
        public function has(string $name): bool { 
            return isset($this->map[$name]); 
        } 
        
        public function get(string $name) {
            return $this->map[$name] ?? null; 
        } 
    }

This class does not do much so far. We could have the same functionality with a normal array. The utility of this class comes when we add filters while fetching data. We will implement three filters, but you can add as many as you need:

.. code-block:: php

    public function getInt(string $name) { 
        return (int)
        $this->get($name); 
    } 
    
    public function getNumber(string $name) {
        return (float) $this->get($name); 
    } 
    
    public function getString(string $name, bool $filter = true) { 
        $value = (string) $this->get($name);
        return $filter ? addslashes($value) : $value; 
    }

These three methods in the preceding code allow the user to get parameters of a specific type. Let's say that the developer needs to get the ID of the book from the request. The best option is to use the getInt method to make sure that the returned value is a valid integer, and not some malicious code that can mess up our database. Also note the function getString, where we use the addSlashed method. This method adds slashes to some of the suspicious characters, such as slashes or quotes, trying to prevent malicious code with it.

Now we are ready to get the GET and POST parameters as well as the cookies from our Request class using our FilteredMap. The new code would look like the following:

.. code-block:: php

    <?php 
        namespace Bookstore\Core; 
        class Request { 
        // ... 
        private $params; 
        private $cookies; 
        
        public function __construct() {
            $this->domain = $_SERVER['HTTP_HOST']; 
            $this->path = explode('?', $_SERVER['REQUEST\_URI'])[0]; $this->method = $_SERVER['REQUEST_METHOD']; 
            $this->params = new FilteredMap( array_merge($_POST, $_GET) ); $this->cookies = new FilteredMap($_COOKIE); 
        } 
        
        // ... 
        
        public function getParams(): FilteredMap { 
            return $this->params; 
        } 
        
        public function getCookies(): FilteredMap { 
            return $this->cookies; 
        } 
    }

With this new addition, a developer could get the POST parameter price
with the following line of code:

    $price = $request->getParams()->getNumber('price');

This is way safer than the usual call to the global array:

    $price = $\_POST['price'];

Mapping routes to controllers
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you can recall from any URL that you use daily, you will probably not see any PHP file as part of the path, like we have with http://localhost:8000/init.php. Websites try to format their URLs to make them easier to remember instead of depending on the file that should handle that request. Also, as we've already mentioned, all our requests go through the same file, index.php, regardless of their path. Because of this, we need to keep a map of the URL paths, and who should handle them.

Sometimes, we have URLs that contain parameters as part of their path, which is different from when they contain the GET or POST parameters. For example, to get the page that shows a specific book, we might include the ID of the book as part of the URL, such as /book/12 or /book/3. The ID will change for each different book, but the same controller should handle all of these requests. To achieve this, we say that the URL contains an argument, and we could represent it by /book/:id, where id is the argument that identifies the ID of the book. Optionally, we could specify the kind of value this argument can take, for example, number, string, and so on.

Controllers, the ones in charge of processing requests, are defined by a method's class. This method takes as arguments all the arguments that the URL's path defines, such as the ID of the book. We group controllers by their functionality, that is, a BookController class will contain the methods related to requests about books.

Having defined all the elements of a route—a URL-controller relationship—we are ready to create our routes.json file, a configuration file that will keep this map. Each entry of this file should contain a route, the key being the URL, and the value, a map of information about the controller. Let's see an example:

.. code-block:: json

    { "books/:page": { "controller": "Book", "method": "getAllWithPage",
    "params": { "page": "number" } } }

The route in the preceding example refers to all the URLs that follow the pattern /books/:page, with page being any number. Thus, this route will match URLs such as /books/23 or /books/2, but it should not match /books/one or /books. The controller that will handle this request should be the getAllWithPage method from BookController; we will append Controller to all the class names. Given the parameters that we defined, the definition of the method should be something like the following:

.. code-block:: php

    public function getAllWithPage(int $page): string { //... }

There is one last thing we should consider when defining a route. For some endpoints, we should enforce the user to be authenticated, such as when the user is trying to access their own sales. We could define this rule in several ways, but we chose to do it as part of the route, adding the entry "login": true as part of the controller's information. With that in mind, let's add the rest of the routes that define all the views that we expect to have:

.. code-block:: php

    { //... "books": { "controller": "Book", "method": "getAll" },
    "book/:id": { "controller": "Book", "method": "get", "params": {
    "id": "number" } }, "books/search": { "controller": "Book",
    "method": "search" }, "login": { "controller": "Customer", "method":
    "login" }, "sales": { "controller": "Sales", "method": "getByUser" ,
    "login": true }, "sales/:id": { "controller": "Sales", "method":
    "get", "login": true, "params": { "id": "number" } }, "my-books": {
    "controller": "Book", "method": "getByUser", "login": true } }

These routes define all the pages we need; we can get all the books in a paginated way or specific books by their ID, we can search books, list the sales of the user, show a specific sale by its ID, and list all the books that a certain user has borrowed. However, we are still lacking some of the endpoints that our application should be able to handle. For all those actions that are trying to modify data rather than requesting it, that is, borrowing a book or buying it, we need to add endpoints too. Add the following to your routes.json file:

.. code-block:: json

    { // ... "book/:id/buy": { "controller": "Sales", "method": "add",
    "login": true "params": { "id": "number" } }, "book/:id/borrow": {
    "controller": "Book", "method": "borrow", "login": true "params": {
    "id": "number" } }, "book/:id/return": { "controller": "Book",
    "method": "returnBook", "login": true "params": { "id": "number" } }
    }

The router
~~~~~~~~~~

The router will be by far the most complicated piece of code in our application. The main goal is to receive a Request object, decide which controller should handle it, invoke it with the necessary parameters, and return the response from that controller. The main goal of this section is to understand the importance of the router rather than its detailed implementation, but we will try to describe each of its parts. Copy the following content as your src/Core/Router.php file:

.. code-block:: php

    <?php 
        namespace Bookstore\Core; 
        use Bookstore\Controllers\ErrorController; 
        use Bookstore\Controllers\CustomerController; 
        
        class Router { 
            private $routeMap; 
            private static $regexPatters = [ 'number' => '\d+', 'string' => '\w' ]; 
    
            public function __construct() { 
                $json = file_get_contents( __DIR__ . '/../../config/routes.json' );
                $this->routeMap = json_decode($json, true); 
            } 
            
            public function route(Request $request): string { 
                $path = $request->getPath();
                foreach ($this->routeMap as $route => $info) { 
                    $regexRoute = $this->getRegexRoute($route, $info); 
                    if(preg_match("@^/$regexRoute$@", $path)) { 
                        return $this->executeController( $route, $path, $info, $request ); 
                    } 
                }
                $errorController = new ErrorController($request); 
                return $errorController->notFound(); 
            } 
        }


The constructor of this class reads from the routes.json file, and stores the content as an array. Its main method, route, takes a Request object and returns a string, which is what we will send as output to the client. This method iterates all the routes from the array, trying to match each with the path of the given request. Once it finds one, it tries to execute the controller related to that route. If none of the routes are a good match to the request, the router will execute the notFound method of the ErrorController, which will then return an error page.


URLs matching with regular expressions
++++++++++++++++++++++++++++++++++++++

While matching a URL with the route, we need to take care of the arguments for dynamic URLs, as they do not let us perform a simple string comparison. PHP—and other languages—has a very strong tool for performing string comparisons with dynamic content: regular expressions. Being an expert in regular expressions takes time, and it is outside the scope of this book, but we will give you a brief introduction to them.

A regular expression is a string that contains some wildcard characters that will match the dynamic content. Some of the most important ones are as follows:

-  ^: This is used to specify that the matching part should be the start
   of the whole string

-  $: This is used to specify that the matching part should be the end
   of the whole string

-  \\d: This is used to match a digit

-  \\w: This is used to match a word

-  +: This is used for following a character or expression, to let that
   character or expression to appear at least once or many times

-  \*: This is used for following a character or expression, to let that
   character or expression to appear zero or many times

-  .: This is used to match any single character

Let's see some examples:

-  The pattern .\* will match anything, even an empty string

-  The pattern .+ will match anything that contains at least one
   character

-  The pattern ^\\d+$ will match any number that has at least one digit

In PHP, we have different functions to work with regular expressions. The easiest of them, and the one that we will use, is pregmatch. This function takes a pattern as its first argument (delimited by two characters, usually @ or /), the string that we are trying to match as the second argument, and optionally, an array where PHP stores the occurrences found. The function returns a Boolean value, being true if there was a match, false otherwise. We use it as follows in our Route class:

.. code-block:: php

    preg_match("@^/$regexRoute$@", $path)

The $path variable contains the path of the request, for example, /books/2. We match using a pattern that is delimited by @, has the ^ and $ wildcards to force the pattern to match the whole string, and contains the concatenation of / and the variable $regexRoute. The content of this variable is given by the following method; add this as well to your Router class:

.. code-block:: php

    private function getRegexRoute( string $route, array $info ): string { 
        if (isset($info['params'])) { 
            foreach ($info['params'] as $name => $type) { 
                $route = str_replace( ':' . $name, self::$regexPatters[$type], $route ); 
            } 
        } 
        return $route; 
    }

The preceding method iterates the parameters list coming from the information of the route. For each parameter, the function replaces the name of the parameter inside the route by the wildcard character corresponding to the type of parameter—check the static array, $regexPatterns. To illustrate the usage of this function, let's see some examples:

-  The route /books will be returned without a change, as it does not
   contain any argument

-  The route books/:id/borrow will be changed to books/\\d+/borrow, as
   the URL argument, id, is a number


Extracting the arguments of the URL
+++++++++++++++++++++++++++++++++++

In order to execute the controller, we need three pieces of data: the name of the class to instantiate, the name of the method to execute, and the arguments that the method needs to receive. We already have the first two as part of the route $info array, so let's focus our efforts on finding the third one. Add the following method to the Router class:

.. code-block:: php

    private function extractParams( string $route, string $path ): array { 
        $params = []; 
        $pathParts = explode('/', $path); 
        $routeParts = explode('/', $route); 
        foreach ($routeParts as $key => $routePart) {
            if (strpos($routePart, ':') === 0) { 
                $name = substr($routePart, 1);
                $params[$name] = $pathParts[$key+1]; 
            } 
        } 
        return $params; 
    }

This last method expects that both the path of the request and the URL of the route follow the same pattern. With the explode method, we get two arrays that should match each of their entries. We iterate them, and for each entry in the route array that looks like a parameter, we fetch its value in the URL. For example, if we had the route /books/:id/borrow and the path /books/12/borrow, the result of this method would be the array *['id' => 12]*.

Executing the controller
++++++++++++++++++++++++

We end this section by implementing the method that executes the controller in charge of a given route. We already have the name of the class, the method, and the arguments that the method needs, so we could make use of the call_user_func_array native function that, given an object, a method name, and the arguments for the method, invokes the method of the object passing the arguments. We have to make use of it as the number of arguments is not fixed, and we cannot perform a normal invocation.

But we are still missing a behavior introduced when creating our routes.json file. There are some routes that force the user to be logged in, which, in our case, means that the user has a cookie with the user ID. Given a route that enforces authorization, we will check whether our request contains the cookie, in which case we will set it to the controller class through setCustomerId. If the user does not have a cookie, instead of executing the controller for the current route, we will execute the showLogin method of the CustomerController class, which will render the template for the login form. Let's see how everything would look on adding the last method of our Router class:

.. code-block:: php

    private function executeController( string $route, string $path,
    array $info, Request $request ): string { 
        $controllerName = '\Bookstore\Controllers\\' . $info['controller'] . 'Controller';
        $controller = new $controllerName($request); 
        if(isset($info['login']) && $info['login']) { 
            if($request->getCookies()->has('user')) { 
                $customerId = $request->getCookies()->get('user');
                $controller->setCustomerId($customerId); 
            } 
            else { 
                $errorController = new CustomerController($request); 
                return $errorController->login();
            } 
        } 
        $params = $this->extractParams($route, $path); 
        return call_user_func_array( [$controller, $info['method']], $params );
    }

We have already warned you about the lack of security in our application, as this is just a project with didactic purposes. So, avoid copying the authorization system implemented here.

M for model
-----------

Imagine for a moment that our bookstore website is quite successful, so we think of building a mobile app to increase our market. Of course, we would want to use the same database that we use for our website, as we need to sync the books that people borrow or buy from both apps. We do not want to be in a position where two people buy the same last copy of a book!

Not only the database, but the queries used to get books, update them, and so on, have to be the same too, otherwise we would end up with unexpected behavior. Of course, one apparently easy option would be to replicate the queries in both codebases, but that has a huge maintainability problem. What if we change one single field of our database? We need to apply the same change to at least two different codebases. That does not seem to be useful at all.

Business logic plays an important role here too. Think of it as decisions you need to take that affect your business. In our case, that a premium customer is able to borrow 10 books and a normal one only 3, is business logic. This logic should be put in a common place too, because, if we want to change it, we will have the same problems as with our database queries.

We hope that by now we've convinced you that data and business logic should be separated from the rest of the code in order to make it reusable. Do not worry if it is hard for you to define what should go as part of the model or as part of the controller; a lot of people struggle with this distinction. As our application is very simple, and it does not have a lot of business logic, we will just focus on adding all the code related to MySQL queries.

As you can imagine, for an application integrated with MySQL, or any other database system, the database connection is an important element of a model. We chose to use PDO in order to interact with MySQL, and as you might remember, instantiating that class was a bit of a pain. Let's create a singleton class that returns an instance of PDO to make things easier. Add this code to src/Core/Db.php:

.. code-block:: php

    <?php 
        namespace Bookstore\Core; 
        use PDO; 
        
        class Db { 
            private static $instance; 
            private static function connect(): PDO { 
                $dbConfig = Config::getInstance()->get('db'); 
                return new PDO( 'mysql:host=127.0.0.1;dbname=bookstore', $dbConfig['user'], $dbConfig['password'] ); 
            } 
            
            public static function getInstance(){ 
                if(self::$instance == null) { 
                    self::$instance = self::connect(); 
                }
                return self::$instance; 
            } 
        }

This class, defined in the preceding code snippet, just implements the singleton pattern and wraps the creation of a PDO instance. From now on, in order to get a database connection, we just need to write Db::getInstance().

Although it might not be true for all models, in our application, they will always have to access the database. We could create an abstract class where all models extend. This class could contain a $db protected property that will be set on the constructor. With this, we avoid duplicating the same constructor and property definition across all our models. Copy the following class into src/Models/AbstractModel.php:

.. code-block:: php

    <?php 
        namespace Bookstore\Models; 
        use PDO; 
        abstract class AbstractModel { 
            private $db; public function __construct(PDO $db) { 
                $this->db = $db; 
            } 
        }

Finally, to finish the setup of the models, we could create a new exception (as we did with the NotFoundException class) that represents an error from the database. It will not contain any code, but we will be able to differentiate where an exception is coming from. We will save it in src/Exceptions/DbException.php:

.. code-block:: php

    <?php 
        namespace Bookstore\Exceptions; 
        use Exception; 
        class DbException extends Exception { }

Now that we've set the ground, we can start writing our models. It is up to you to organize your models, but it is a good idea to mimic the domain objects structure. In this case, we would have three models: CustomerModel, BookModel, and SalesModel. In the following sections, we will explain the contents of each of them.


The customer model
~~~~~~~~~~~~~~~~~~

Let's start with the easiest one. As our application is still very primitive, we will not allow the creation of new costumers, and work with the ones we inserted manually into the database instead. That means that the only thing we need to do with customers is to query them. Let's create a CustomerModel class in src/Models/CustomerModel.php with the following content:

.. code-block:: php

    <?php 
        namespace Bookstore\Models; 
        use Bookstore\Domain\Customer;
        use Bookstore\Domain\Customer\CustomerFactory; 
        use Bookstore\Exceptions\NotFoundException; 
        
        class CustomerModel extends AbstractModel { 
            public function get(int $userId): Customer {
                $query = 'SELECT * FROM customer WHERE customer_id = :user'; $sth = $this->db->prepare($query); 
                $sth->execute(['user' => $userId]);
                $row = $sth->fetch(); 
                if (empty($row)) { 
                    throw new NotFoundException(); 
                } 
                return CustomerFactory::factory( $row['type'], $row['id'], $row['firstname'], $row['surname'], $row['email'] ); 
            } 
            
            public function getByEmail(string $email): Customer { 
                $query = 'SELECT * FROM customer WHERE email = :user';
                $sth = $this->db->prepare($query); 
                $sth->execute(['user' => $email]); 
                $row = $sth->fetch(); 
                if (empty($row)) { 
                    throw new NotFoundException(); 
                } 
                return CustomerFactory::factory($row['type'], $row['id'], $row['firstname'], $row['surname'], $row['email'] ); 
            } 
        }


The CustomerModel class, which extends from the AbstractModel class, contains two methods; both of them return a Customer instance, one of them when providing the ID of the customer, and the other one when providing the e-mail. As we already have the database connection as the $db property, we just need to prepare the statement with the given query, execute the statement with the arguments, and fetch the result. As we expect to get a customer, if the user provided an ID or an e-mail that does not belong to any customer, we will need to throw an exception—in this case, a NotFoundException is just fine. If we find a customer, we use our factory to create the object and return it.

The book model
~~~~~~~~~~~~~~

Our BookModel class gives us a bit more of work. Customers had a factory, but it is not worth having one for books. What we use for creating them from MySQL rows is not the constructor, but a fetch mode that PDO has, and that allows us to map a row into an object. To do so, we need to adapt the Book domain object a bit:

-  The names of the properties have to be the same as the names of the
   fields in the database

-  There is no need for a constructor or setters, unless we need them
   for other purposes

-  To go with encapsulation, properties should be private, so we will
   need getters for all of them

The new Book class should look like the following:

.. code-block:: php

    <?php 
        namespace Bookstore\Domain; 
        
        class Book { 
            private $id; 
            private $isbn; 
            private $title; 
            private $author; 
            private $stock; 
            private $price; 
            
            public function getId(): int { 
                return $this->id; } 
            
            public function getIsbn(): string { 
                return $this->isbn; 
            } 
            
            public function getTitle(): string { 
                return $this->title; 
            } 
            
            public function getAuthor(): string { 
                return $this->author; 
            } 
            
            public function getStock(): int { 
                return $this->stock; 
            } 
            
            public function getCopy(): bool { 
                if ($this->stock < 1) { 
                    return false; 
                } 
                else {
                    $this->stock--; 
                    return true; 
                } 
            } 
            
            public function addCopy() {
                $this->stock++; 
            } 
            
            public function getPrice(): float { 
                return $this->price; 
            } 
        }

We retained the getCopy and addCopy methods even though they are not getters, as we will need them later. Now, when fetching a group of rows from MySQL with the fetchAll method, we can send two parameters: the constant PDO::FETCH_CLASS that tells PDO to map rows to a class, and the name of the class that we want to map to. Let's create the BookModel class with a simple get method that fetches a book from the database with a given ID. This method will return either a Book object or throw an exception in case the ID does not exist. Save it as src/Models/BookModel.php:

.. code-block:: php

    <?php 
        namespace Bookstore\Models; 
        use Bookstore\Domain\Book; 
        use Bookstore\Exceptions\DbException; 
        use Bookstore\Exceptions\NotFoundException; 
        use PDO; 
        
        class BookModel extends AbstractModel { 
            const CLASSNAME = '\Bookstore\Domain\Book'; 
            public function get(int $bookId): Book { 
                $query = 'SELECT * FROM book WHERE id = :id'; 
                $sth = $this->db->prepare($query); 
                $sth->execute(['id' => $bookId]); 
                $books = $sth->fetchAll( PDO::FETCH\_CLASS, self::CLASSNAME ); if (empty($books)) { 
                    throw new NotFoundException(); 
                } 
                return $books[0];
            } 
        }

There are advantages and disadvantages of using this fetch mode. On one hand, we avoid a lot of dull code when creating objects from rows. Usually, we either just send all the elements of the row array to the constructor of the class, or use setters for all its properties. If we add more fields to the MySQL table, we just need to add the properties to our domain class, instead of changing everywhere where we were instantiating the objects. On the other hand, you are forced to use the same names for the fields in both the table's as well as the class' properties, which means high coupling (always a bad idea). This also causes some conflicts when following conventions, because in MySQL, it is common to use book_id, but in PHP, the property is $bookId.

Now that we know how this fetch mode works, let's add three other methods that fetch data from MySQL. Add the following code to your model:

.. code-block:: php

    public function getAll(int $page, int $pageLength): array { 
        $start = $pageLength * ($page - 1); 
        $query = 'SELECT * FROM book LIMIT :page, :length'; 
        $sth = $this->db->prepare($query);
        $sth->bindParam('page', $start, PDO::PARAM_INT);
        $sth->bindParam('length', $pageLength, PDO::PARAM_INT);
        $sth->execute(); 
        return $sth->fetchAll(PDO::FETCH_CLASS, self::CLASSNAME); 
    } 
    
    public function getByUser(int $userId): array {
        $query = <<<SQL SELECT b.* FROM borrowed_books bb LEFT JOIN book b
            ON bb.book_id = b.id WHERE bb.customer_id = :id SQL; 
        $sth = $this->db->prepare($query); $sth->execute(['id' => $userId]); 
        return $sth->fetchAll(PDO::FETCH_CLASS, self::CLASSNAME); 
    } 
    
    public function search(string $title, string $author): array { 
        $query = <<<SQL SELECT * FROM book WHERE title LIKE :title AND author LIKE :author SQL; 
        $sth = $this->db->prepare($query);
        $sth->bindValue('title', "%$title%"); 
        $sth->bindValue('author', "%$author%"); 
        $sth->execute(); 
        return $sth->fetchAll(PDO::FETCH_CLASS, self::CLASSNAME); 
    }

The methods added are as follows:

-  getAll returns an array of all the books for a given page. Remember
   that LIMIT allows you to return a specific number of rows with an
   offset, which can work as a paginator.

-  getByUser returns all the books that a given customer has borrowed—we
   will need to use a join query for this. Note that we return b.\*,
   that is, only the fields of the book table, skipping the rest of the
   fields.

-  Finally, there is a method to search by either title or author, or
   both. We can do that using the operator LIKE and enclosing the
   patterns with %. If we do not specify one of the parameters, we will
   try to match the field with %%, which matches everything.

So far, we have been adding methods to fetch data. Let's add methods that will allow us to modify the data in our database. For the book model, we will need to be able to borrow books and return them. Here is the code for those two actions:

.. code-block:: php

    public function borrow(Book $book, int $userId) { 
        $query = <<<SQL INSERT INTO borrowed_books (book_id, customer\_id, start) VALUES(:book, :user, NOW()) SQL; 
        $sth = $this->db->prepare($query);
        $sth->bindValue('book', $book->getId()); 
        $sth->bindValue('user', $userId); 
        if (!$sth->execute()) { 
            throw new DbException($sth->errorInfo()[2]); 
        } 
        $this->updateBookStock($book);
    } 
    
    public function returnBook(Book $book, int $userId) { 
        $query = <<<SQL UPDATE borrowed_books SET end = NOW() WHERE book_id = :book AND customer_id = :user AND end IS NULL SQL; 
        $sth = $this->db->prepare($query); 
        $sth->bindValue('book', $book->getId());
        $sth->bindValue('user', $userId); 
        if (!$sth->execute()) { 
            throw newDbException($sth->errorInfo()[2]); 
        } 
        
        $this->updateBookStock($book);
    } 
    
    private function updateBookStock(Book $book) { 
        $query = 'UPDATE book SET stock = :stock WHERE id = :id'; 
        $sth = $this->db->prepare($query); 
        $sth->bindValue('id', $book->getId());
        $sth->bindValue('stock', $book->getStock()); 
        if (!$sth->execute()) {
            throw new DbException($sth->errorInfo()[2]); 
        } 
    }

When borrowing a book, you are adding a row to the borrower_books  table. When returning books, you do not want to remove that row, but rather to set the end date in order to keep a history of the books that a user has been borrowing. Both methods need to change the stock of the borrowed book: when borrowing it, reducing the stock by one, and when
returning it, increasing the stock. That is why, in the last code snippet, we created a private method to update the stock of a given book, which will be used from both the borrow and returnBook methods.

The sales model
~~~~~~~~~~~~~~~

Now we need to add the last model to our application: the SalesModel. Using the same fetch mode that we used with books, we need to adapt the domain class as well. We need to think a bit more in this case, as we will be doing more than just fetching. Our application has to be able to create new sales on demand, containing the ID of the customer and the books. We can already add books with the current implementation, but we need to add a setter for the customer ID. The ID of the sale will be given by the autoincrement ID in MySQL, so there is no need to add a setter for it. The final implementation would look as follows:

.. code-block:: php

    <?php 
        namespace Bookstore\Domain; 
        
        class Sale { 
            private $id; 
            private $customer_id; 
            private $books; 
            private $date; 
            
            public function setCustomerId(int $customerId) { 
                $this->customer_id = $customerId;
            } 
            
            public function getId(): int { 
                return $this->id; 
            } 
            
            public function getCustomerId(): int { 
                return $this->customer_id; 
            } 
            
            public function getBooks(): array { 
                return $this->books; 
            } 
            
            public function getDate(): string { 
                return $this->date; 
            } 
            
            public function addBook(int $bookId, int $amount = 1) { 
                if(!isset($this->books[$bookId])) { 
                    $this->books[$bookId] = 0; 
                }
                $this->books[$bookId] += $amount; 
            } 
            
            public function setBooks(array $books) { 
            
                $this->books = $books; 
            }
        }

The SalesModel will be the most difficult one to write. The problem with this model is that it includes manipulating different tables: sale and sale_book. For example, when getting the information of a sale, we need to get the information from the sale table, and then the information of all the books in the sale\_book table. You could argue about whether to have one unique method that fetches all the necessary information related to a sale, or to have two different methods, one to fetch the sale and the other to fetch the books, and let the controller to decide which one to use.

This actually starts a very interesting discussion. On one hand, we want to make things easier for the controller—having one unique method to fetch the entire Sale object. This makes sense as the controller does not need to know about the internal implementation of the Sale object, which lowers coupling. On the other hand, forcing the model to always fetch the whole object, even if we only need the information in the sale table, is a bad idea. Imagine if the sale contains a lot of books; fetching them from MySQL will decrease performance unnecessarily.

You should think how your controllers need to manage sales. If you will always need the entire object, you can have one method without being concerned about performance. If you only need to fetch the entire object sometimes, maybe you could add both methods. For our application, we will have one method to rule them all, since that is what we will always need.

.. admonition:: Lazy loading

    As with any other design challenge, other developers have already given a lot of thought to this problem. They came up with a design pattern named lazy load. This pattern basically lets the controller think that there is only one method to fetch the whole domain object, but we will actually be fetching only what we need from database.

The model fetches the most used information for the object and leaves the rest of the properties that need extra database queries empty. Once the controller uses a getter of a property that is empty, the model automatically fetches that data from the database. We get the best of both worlds: there is simplicity for the controller, but we do not spend more time than necessary querying unused data.

Add the following as your src/Models/SaleModel.php file:

.. code-block:: php

    <?php 
        namespace Bookstore\Models; 
        use Bookstore\Domain\Sale; 
        use Bookstore\Exceptions\DbException; 
        use PDO; 
        
        class SaleModel extends AbstractModel { 
            const CLASSNAME = '\Bookstore\Domain\Sale';
    
            public function getByUser(int $userId): array { 
                $query = 'SELECT * FROM sale WHERE s.customer_id = :user'; 
                $sth = $this->db->prepare($query); 
                $sth->execute(['user' => $userId]);
                return $sth->fetchAll(PDO::FETCH_CLASS, self::CLASSNAME); 
            } 
            
            public function get(int $saleId): Sale { 
                $query = 'SELECT * FROM sale WHERE id = :id'; 
                $sth = $this->db->prepare($query);
                $sth->execute(['id' => $saleId]); 
                $sales = $sth->fetchAll(PDO::FETCH_CLASS, self::CLASSNAME); 
                if (empty($sales)) { 
                    throw new NotFoundException('Sale not found.'); 
                }
                $sale = array_pop($sales); 
                $query = <<<SQL SELECT b.id, b.title, b.author, b.price, sb.amount as stock, b.isbn FROM sale s LEFT JOIN sale_book sb ON s.id = sb.sale_id LEFT JOIN book b ON sb.book_id
                = b.id WHERE s.id = :id SQL; 
                $sth = $this->db->prepare($query);
                $sth->execute(['id' => $saleId]); 
                $books = $sth->fetchAll(PDO::FETCH_CLASS, BookModel::CLASSNAME ); 
                $sale->setBooks($books);
                return $sale; 
            } 
        }

Another tricky method in this model is the one that takes care of creating a sale in the database. This method has to create a sale in the sale table, and then add all the books for that sale to the sale_book table. What would happen if we have a problem when adding one of the books? We would leave a corrupted sale in the database. To avoid that, we need to use transactions, starting with one at the beginning of the model's or the controller's method, and either rolling back in case of error, or committing it at the end of the method.

In the same method, we also need to take care of the ID of the sale. We do not set the ID of the sale when creating the sale object, because we rely on the autoincremental field in the database. But when inserting the books into sale_book, we do need the ID of the sale. For that, we need to request the PDO for the last inserted ID with the lastInsertId method. Let's add then the create method into your SaleModel:

.. code-block:: php

    public function create(Sale $sale) { 
        $this->db->beginTransaction();
        $query = <<<SQL INSERT INTO sale(customer\_id, date) VALUES(:id, NOW()) SQL; $sth = $this->db->prepare($query); 
        if (!$sth->execute(['id' => $sale->getCustomerId()])) { 
            $this->db->rollBack(); 
            throw new DbException($sth->errorInfo()[2]);
        } 
        $saleId = $this->db->lastInsertId(); 
        $query = <<<SQL INSERT INTO sale_book(sale_id, book_id, amount) VALUES(:sale, :book, :amount) SQL; 
        $sth = $this->db->prepare($query); 
        $sth->bindValue('sale', $saleId); 
        foreach ($sale->getBooks() as $bookId => $amount) {
            $sth->bindValue('book', $bookId); 
            $sth->bindValue('amount',  $amount); 
            if (!$sth->execute()) { 
                $this->db->rollBack(); 
                throw new DbException($sth->errorInfo()[2]); 
            } 
        } 
        $this->db->commit(); 
    }

One last thing to note from this method is that we prepare a statement, bind a value to it (the sale ID), and then bind and execute the same statement as many times as the books in the array. Once you have a statement, you can bind the values as many times as you want. Also, you can execute the same statement as many times as you want, and the values stay the same.

V for view
----------

The view is the layer that takes care of the… view. In this layer, you find all the templates that render the HTML that the user gets. Although the separation between views and the rest of the application is easy to see, that does not make views an easy part. In fact, you will have to learn a new technology in order to write views properly. Let's get into the details.

Introduction to Twig
~~~~~~~~~~~~~~~~~~~~

In our first attempt at writing views, we mixed up PHP and HTML code. We already know that the logic should not be mixed in the same place as HTML, but that is not the end of the story. When rendering HTML, we need some logic there too. For example, if we want to print a list of books, we need to repeat a certain block of HTML for each book. And since a priori we do not know the number of books to print, the best option would be a foreach loop.

One option that a lot of people take is minimizing the amount of logic that you can include in a view. You could set some rules, such as *we should only include conditionals and loops*, which is a reasonable amount of logic needed to render basic views. The problem is that there is not a way of enforcing this kind of rule, and other developers can easily start adding heavy logic in there. While some people are OK with that, assuming that no one will do it, others prefer to implement more restrictive systems. That was the beginning of template engines.

You could think of a template engine as another language that you need to learn. Why would you do that? Because this new "language" is more limited than PHP. These languages usually allow you to perform conditionals and simple loops, and that is it. The developer is not able to add PHP to that file, since the template engine will not treat it as PHP code. Instead, it will just print the code to the output—the response' body—as if it was plain text. Also, as it is specially oriented to write templates, the syntax is usually easier to read when mixed with HTML. Almost everything is an advantage.

The inconvenience of using a template engine is that it takes some time to translate the new language to PHP, and then to HTML. This can be quite time consuming, so it is very important that you choose a good template engine. Most of them also allow you to cache templates, improving the performance. Our choice is a quite light and widely used one: Twig. As we've already added the dependency in our Composer file, we can use it straight away.

Setting up Twig is quite easy. On the PHP side, you just need to specify the location of the templates. A common convention is to use the views directory for that. Create the directory, and add the following two lines into your index.php:

.. code-block:: php

    $loader = new Twig_Loader_Filesystem(__DIR__ . '/views');
    $twig = new Twig_Environment($loader);


The book view
~~~~~~~~~~~~~

In these sections, as we work with templates, it would be nice to see the result of your work. We have not yet implemented any controllers, so we will force our index.php to render a specific template, regardless of the request. We can start rendering the view of a single book. For that, let's add the following code at the end of your index.php, after creating your twig object:

.. code-block:: php

    $bookModel = new BookModel(Db::getInstance()); 
    $book = $bookModel->get(1); 
    $params = ['book' => $book]; 
    echo $twig->loadTemplate('book.twig')->render($params);

In the preceding code, we request the book with ID 1 to the BookModel, get the book object, and create an array where the book key has the value of the book object. After that, we tell Twig to load the template book.twig and to render it by sending the array. This takes the template and injects the $book object, so that you are able to use it inside the template.

Let's now create our first template. Write the following code into view/book.twig. By convention, all Twig templates should have the .twig extension:

.. code-block:: html

    <h2>{{ book.title }}</h2> 
    <h3>{{ book.author }}</h3> <hr> 
    <p>
        <strong>ISBN</strong> {{ book.isbn }} 
    </p> 
    <p>
        <strong>Stock</strong> {{ book.stock }} 
    </p> 
    <p>
        <strong>Price</strong> {{ book.price\|number\_format(2) }} € 
    </p>
    <hr> 
    <h3>Actions</h3> 
    <form method="post" action="/book/{{ book.id}}/borrow"> 
        <input type="submit" value="Borrow"> 
    </form> 
    
    <form method="post" action="/book/{{ book.id }}/buy"> 
        <input type="submit" value="Buy"> 
    </form>

Since this is your first Twig template, let's go step by step. You can see that most of the content is HTML: some headers, a couple of paragraphs, and two forms with two buttons. You can recognize the Twig part, since it is enclosed by {{ }}. In Twig, everything that is between those curly brackets will be printed out. The first one that we find contains book.title. Do you remember that we injected the book object when rendering the template? We can access it here, just not with the usual PHP syntax. To access an object's property, use . instead of ->. So, this book.title will return the value of the title property of the book object, and the {{ }} will make Twig print it out. The same applies to the rest of the template.

There is one that does a bit more than just access an object's property. The book.price\|number\_format(2) gets the price of the book and sends it as an argument (using the pipe symbol) to the function number_format, which has already got 2 as another argument. This bit of code basically formats the price to two digital figures. In Twig, you also have some functions, but they are mostly reduced to formatting the output, which is an acceptable amount of logic.

Are you convinced now about how clean it is to use a template engine for your views? You can try it in your browser: accessing any path, your web server should execute the index.php file, forcing the template book.twig to be rendered.

Layouts and blocks
~~~~~~~~~~~~~~~~~~

When you design your web application, usually you would want to share a common layout across most of your views. In our case, we want to always have a menu at the top of the view that allows us to go to the different sections of the website, or even to search books from wherever the user is. As with models, we want to avoid code duplication, since if we were to copy and paste the layout everywhere, updating it would be a nightmare. Instead, Twig comes with the ability to define layouts.

A layout in Twig is just another template file. Its content is just the common HTML code that we want to display across all views (in our case, the menu and search bar), and contains some tagged gaps (blocks in Twig's world), where you will be able to inject the specific HTML of each view. You can define one of those blocks with the tag {% block %}. Let's see what our views/layout.twig file would look like:

.. code-block:: html

    <html> 
        <head> 
            <title>{% block title %}{% endblock %}</title> 
        </head>
        <body> 
            <div style="border: solid 1px"> 
                <a href="/books">Books</a> 
                <a href="/sales">My Sales</a> 
                <a href="/my-books">My Books</a> 
                <hr>
                <form action="/books/search" method="get"> 
                    <label>Title</label> 
                    <input type="text" name="title"> 
                    <label>Author</label> 
                    <input type="text" name="author"> 
                    <input type="submit" value="Search">
                </form> 
            </div> 
            {% block content %}
            {% endblock %} 
        </body> 
    </html>

As you can see in the preceding code, blocks have a name so that templates using the layout can refer to them. In our layout, we defined two blocks: one for the title of the view and the other for the content itself. When a template uses the layout, we just need to write the HTML code for each of the blocks defined in the layout, and Twig will do the rest. Also, to let Twig know that our template wants to use the layout, we use the tag {% extends %} with the layout filename. Let's update views/book.twig to use our new layout:

.. code-block:: html

    {% extends 'layout.twig' %} 
    {% block title %} 
        {{ book.title }} 
    {%endblock %} 
    {% block content %} 
        <h2>{{ book.title }}</h2> //...
        </form> 
    {% endblock %}

At the top of the file, we add the layout that we need to use. Then, we open a block tag with the reference name, and we write inside it the HTML that we want to use. You can use anything valid inside a block, either Twig code or plain HTML. In our template, we used the title of the book as the title block, which refers to the title of the view, and we put all the previous HTML inside the content block. Note that everything in the file is inside a block now. Try it in your browser now to see the changes.


Paginated book list
~~~~~~~~~~~~~~~~~~~

Let's add another view, this time for a paginated list of books. In order to see the result of your work, update the content of index.php, replacing the code of the previous section with the following:

.. code-block:: php

    $bookModel = new BookModel(Db::getInstance()); 
    $books = $bookModel->getAll(1, 3); 
    $params = ['books' => $books, 'currentPage' => 2]; 
    echo $twig->loadTemplate('books.twig')->render($params);

In the preceding snippet, we force the application to render the books.twig template, sending an array of books from page number 1, and showing 3 books per page. This array, though, might not always return 3 books, maybe because there are only 2 books in the database. We should then use a loop to iterate the list instead of assuming the size of the array. In Twig, you can emulate a foreach loop using {% for <element> in <array> %} in order to iterate an array. Let's use it for your views/books.twig:

.. code-block:: html

    {% extends 'layout.twig' %} 
    {% block title %} 
        Books 
    {% endblock %}
    {% block content %} 
        <table> 
            <thead> 
                <th>Title</th> 
                <th>Author</th>
                <th></th> 
            </thead> 
            {% for book in books %} 
                <tr> 
                    <td>{{ book.title}}</td> 
                    <td>{{ book.author }}</td> 
                    <td><a href="/book/{{ book.id}}">View</a></td> 
                </tr> 
            {% endfor %} 
        </table> 
    {% endblock %}

We can also use conditionals in a Twig template, which work the same as the conditionals in PHP. The syntax is {% if <boolean expression> %}. Let's use it to decide if we should show the previous and/or following links on our page. Add the following code at the end of the content block:

.. code-block:: html

    {% if currentPage != 1 %} 
        <a href="/books/{{ currentPage - 1}}">Previous</a> 
    {% endif %} 
    {% if not lastPage %} 
        <a href="/books/{{ currentPage + 1 }}">Next</a> 
    {% endif %}

The last thing to note from this template is that we are not restricted to using only variables when printing out content with {{ }}. We can add any valid Twig expression that returns a value, as we did with {{ currentPage + 1 }}.

The sales view
~~~~~~~~~~~~~~

We have already shown you everything that you will need for using templates, and now we just have to finish adding all of them. The next one in the list is the template that shows the list of sales for a given user. Update your index.php file with the following hack:

.. code-block:: php

    $saleModel = new SaleModel(Db::getInstance()); 
    $sales = $saleModel->getByUser(1); 
    $params = ['sales' => $sales]; 
    echo $twig->loadTemplate('sales.twig')->render($params);


The template for this view will be very similar to the one listing the books: a table populated with the content of an array. The following is the content of views/sales.twig:

.. code-block:: html

    {% extends 'layout.twig' %} 
    {% block title %} My sales 
    {% endblock%} 
    {% block content %} 
        <table> 
            <thead> 
                <th>Id</th> 
                <th>Date</th>
            </thead> 
            {% for sale in sales %} 
                <tr> 
                    <td>{{ sale.id}}</td> 
                    <td>{{sale.date }}</td> 
                    <td><a href="/sales/{{ sale.id }}">View</a></td>
                </tr> 
            {% endfor %} 
        </table> 
    {% endblock %}

The other view related to sales is where we want to display all the content of a specific one. This sale, again, will be similar to the books list, as we will be listing the books related to that sale. The hack to force the rendering of this template is as follows:

.. code-block:: php

    $saleModel = new SaleModel(Db::getInstance()); 
    $sale = $saleModel->get(1); 
    $params = ['sale' => $sale]; 
    echo $twig->loadTemplate('sale.twig')->render($params);

And the Twig template should be placed in views/sale.twig:

.. code-block:: html

    {% extends 'layout.twig' %} 
    {% block title %} 
        Sale {{ sale.id }} 
    {%endblock %} 
    {% block content %} 
        <table> 
            <thead> 
                <th>Title</th>
                <th>Author</th> 
                <th>Amount</th> 
                <th>Price</th> 
                <th></th> 
            </thead> 
            {%for book in sale.books %} 
                <tr> 
                    <td>{{ book.title }}</td> 
                    <td>{{book.author }}</td> 
                    <td>{{ book.stock }}</td> 
                    <td>{{ (book.price * book.stock) |number_format(2) }} €</td> 
                    <td><a href="/book/{{ book.id }}">View</a></td> 
                </tr> 
            {% endfor %} 
        </table> 
    {% endblock %}


The error template
~~~~~~~~~~~~~~~~~~

We should add a very simple template that will be shown to the user when there is an error in our application, rather than showing a PHP error message. This template will just expect the errorMessage variable, and it could look like the following. Save it as views/error.twig:

.. code-block:: html

    {% extends 'layout.twig' %} 
    {% block title %} 
        Error 
    {% endblock %}
    {% block content %} 
        <h2>Error: {{ errorMessage }}</h2> 
        {% endblock%}

Note that even the error page extends from the layout, as we want the user to be able to do something else when this happens.


The login template
~~~~~~~~~~~~~~~~~~

Our last template will be the one that allows the user to log in. This template is a bit different from the others, as it will be used in two different scenarios. In the first one, the user accesses the login view for the first time, so we need to show the form. In the second one, the user has already tried to log in, and there was an error when doing so, that is, the e-mail address was not found. In this case, we will add an extra variable to the template, errorMessage, and we will add a conditional to show its contents only when this variable is defined. You can use the operator is defined to check that. Add the following template as views/login.twig:

.. code-block:: html

    {% extends 'layout.twig' %} 
    {% block title %} Login {% endblock %}
    {% block content %} 
        {% if errorMessage is defined %} 
            <strong>{{errorMessage }}</strong> 
        {% endif %} 
        <form action="/login" method="post"> 
            <label>Email</label> 
            <input type="text" name="email">
            <input type="submit"> 
        </form> 
    {% endblock %}



C for controller
----------------

It is finally time for the director of the orchestra. Controllers represent the layer in our application that, given a request, talks to the models and builds the views. They act like the manager of a team: they decide what resources to use depending on the situation.

As we stated when explaining models, it is sometimes difficult to decide if some piece of logic should go into the controller or the model. At the end of the day, MVC is a pattern, like a recipe that guides you, rather than an exact algorithm that you need to follow step by step. There will be scenarios where the answer is not straightforward, so it will be up to you; in these cases, just try to be consistent. The following are some common scenarios that might be difficult to localize:

-  The request points to a path that we do not support. This scenario is
   already covered in our application, and it is the router that should
   take care of it, not the controller.

-  The request tries to access an element that does not exist, for
   example, a book ID that is not in the database. In this case, the
   controller should ask the model if the book exists, and depending on
   the response, render a template with the book's contents, or another
   with a "Not found" message.

-  The user tries to perform an action, such as buying a book, but the
   parameters coming from the request are not valid. This is a tricky
   one. One option is to get all the parameters from the request without
   checking them, sending them straight to the model, and leaving the
   task of sanitizing the information to the model. Another option is
   that the controller checks that the parameters provided make sense,
   and then gives them to the model. There are other solutions, like
   building a class that checks if the parameters are valid, which can
   be reused in different controllers. In this case, it will depend on
   the amount of parameters and logic involved in the sanitization. For
   requests receiving a lot of data, the third option looks like the
   best of them, as we will be able to reuse the code in different
   endpoints, and we are not writing controllers that are too long. But
   in requests where the user sends one or two parameters, sanitizing
   them in the controller might be good enough.

Now that we've set the ground, let's prepare our application to use controllers. The first thing to do is to update our index.php, which has been forcing the application to always render the same template. Instead, we should be giving this task to the router, which will return the response as a string that we can just print with echo. Update your index.php file with the following content:

    <?php use Bookstore\\Core\\Router; use Bookstore\\Core\\Request;
    require\_once \_\_DIR\_\_ . '/vendor/autoload.php'; $router = new
    Router(); $response = $router->route(new Request()); echo $response;

As you might remember, the router instantiates a controller class, sending the request object to the constructor. But controllers have other dependencies as well, such as the template engine, the database connection, or the configuration reader. Even though this is not the best solution (you will improve it once we cover dependency injection in the next section), we could create an AbstractController that would be the parent of all controllers, and will set those dependencies. Copy the following as src/Controllers/AbstractController.php:

.. code-block:: php

    // it will print the result
    <?php 
        namespace Bookstore\Controllers; 
        use Bookstore\Core\Config;
        use Bookstore\Core\Db; 
        use Bookstore\Core\Request; 
        use Monolog\Logger; 
        use Twig_Environment; 
        use Twig_Loader_Filesystem; 
        use Monolog\Handler\StreamHandler;
    
        abstract class AbstractController { 
            protected $request; 
            protected $db; 
            protected $config; 
            protected $view; 
            protected $log; 
            
            public function __construct(Request $request) { 
                $this->request = $request; 
                $this->db = Db::getInstance(); 
                $this->config = Config::getInstance(); 
                $loader = new Twig_Loader_Filesystem(__DIR__ . '/../../views' ); 
                $this->view = new Twig_Environment($loader); 
                $this->log = new Logger('bookstore');
                $logFile = $this->config->get('log'); 
                $this->log->pushHandler( new StreamHandler($logFile, Logger::DEBUG) ); 
            } 
            
            public function setCustomerId(int $customerId) { 
                $this->customerId = $customerId; 
            }
        }

When instantiating a controller, we will set some properties that will be useful when handling requests. We already know how to instantiate the database connection, the configuration reader, and the template engine. The fourth property, $log, will allow the developer to write logs to a given file when necessary. We will use the Monolog library for that, but there are many other options. Notice that in order to instantiate the logger, we get the value of log from the configuration, which should be the path to the log file. The convention is to use the /var/log/ directory, so create the /var/log/bookstore.log file, and add "log": "/var/log/bookstore.log" to your configuration file.

Another thing that is useful to some controllers - but not all of them - is the information about the user performing the action. As this is only going to be available for certain routes, we should not set it when constructing the controller. Instead, we have a setter for the router to set the customer ID when available; in fact, the router does that already.

Finally, a handy helper method that we could use is one that renders a given template with parameters, as all the controllers will end up rendering one template or the other. Let's add the following protected method to the AbstractController class:

.. code-block:: php

    protected function render(string $template, array $params): string {
        return $this->view->loadTemplate($template)->render($params); 
    }

The error controller
~~~~~~~~~~~~~~~~~~~~

Let's start by creating the easiest of the controllers: the ErrorController. This controller does not do much; it just renders the error.twig template sending the "Page not found!" message. As you might remember, the router uses this controller when it cannot match the request to any of the other defined routes. Save the following class in src/Controllers/ErrorController.php:

.. code-block:: php

    <?php 
        namespace Bookstore\Controllers; 
        class ErrorController extends AbstractController { 
            public function notFound(): string {
                $properties = ['errorMessage' => 'Page not found!']; 
                return $this->render('error.twig', $properties); 
            } 
        }


The login controller
~~~~~~~~~~~~~~~~~~~~

The second controller that we have to add is the one that manages the login of the customers. If we think about the flow when a user wants to authenticate, we have the following scenarios:

-  The user wants to get the login form in order to submit the necessary
   information and log in.

-  The user tries to submit the form, but we could not get the e-mail
   address. We should render the form again, letting them know about the
   problem.

-  The user submits the form with an e-mail, but it is not a valid one.
   In this case, we should show the login form again with an error
   message explaining the situation.

-  The user submits a valid e-mail, we set the cookie, and we show the
   list of books so the user can start searching. This is absolutely
   arbitrary; you could choose to send them to their borrowed books
   page, their sales, and so on. The important thing here is to notice
   that we will be redirecting the request to another controller.

There are up to four possible paths. We will use the request object to decide which of them to use in each case, returning the corresponding response. Let's create, then, the CustomerController class in src/Controllers/CustomerController.php with the login method, as follows:

.. code-block:: php

    <?php 
        namespace Bookstore\Controllers; 
        use Bookstore\Exceptions\NotFoundException; 
        use Bookstore\Models\CustomerModel; 
        
        class CustomerController extends AbstractController { 
            public function login(string $email): string {
                if (!$this->request->isPost()) { 
                    return $this->render('login.twig', []); 
                } 
                $params = $this->request->getParams(); 
                if(!$params->has('email')) { 
                    $params = ['errorMessage' => 'No info provided.']; 
                    return $this->render('login.twig', $params); 
                } 
                $email = $params->getString('email'); 
                $customerModel = new CustomerModel($this->db); 
                try { 
                    $customer = $customerModel->getByEmail($email); 
                } 
                
                catch (NotFoundException $e) {
                    $this->log->warn('Customer email not found: ' . $email); 
                    $params = ['errorMessage' => 'Email not found.']; 
                    return $this->render('login.twig', $params); 
                } 
                setcookie('user', $customer->getId()); 
                $newController = new BookController($this->request); 
                return $newController->getAll(); 
            } 
        }

As you can see, there are four different returns for the four different cases. The controller itself does not do anything, but orchestrates the rest of the components, and makes decisions. First, we check if the request is a POST, and if it is not, we will assume that the user wants to get the form. If it is, we will check for the e-mail in the parameters, returning an error if the e-mail is not there. If it is, we will try to find the customer with that e-mail, using our model. If we get an exception saying that there is no such customer, we will render the form with a "Not found" error message. If the login is successful, we will set the cookie with the ID of the customer, and will execute the getAll method of BookController (still to be written), returning the list of books.

At this point, you should be able to test the login feature of your application end to end with the browser. Try to access http://localhost:8000/login to see the form, adding random e-mails to get the error message, and adding a valid e-mail (check your customer table in MySQL) to log in successfully. After this, you should see the cookie with the customer ID.

The book controller
~~~~~~~~~~~~~~~~~~~

The BookController class will be the largest of our controllers, as most of the application relies on it. Let's start by adding the easiest methods, the ones that just retrieve information from the database. Save this as src/Controllers/BookController.php:

.. code-block:: php

    <?php 
        namespace Bookstore\Controllers; 
        use Bookstore\Models\BookModel; 
        
        class BookController extends AbstractController { 
            const PAGE_LENGTH = 10; 
            
            public function getAllWithPage($page): string { 
                $page = (int)$page; 
                $bookModel = new BookModel($this->db); 
                $books = $bookModel->getAll($page, self::PAGE_LENGTH); 
                $properties = [ 'books' => $books, 'currentPage' => $page, 'lastPage' => count($books) <        
                    self::PAGE_LENGTH ]; 
                return $this->render('books.twig', $properties); 
            } 
            
            public function getAll(): string { 
                return $this->getAllWithPage(1); 
            } 
            
            public function get(int $bookId): string { 
                $bookModel = new BookModel($this->db); 
                try { 
                    $book = $bookModel->get($bookId); 
                } 
                
                catch (\Exception $e) {
                    $this->log->error( 'Error getting book: ' . $e->getMessage() );
                    $properties = ['errorMessage' => 'Book not found!']; 
                    return $this->render('error.twig', $properties); 
                } 
                
                $properties = ['book' =  $book]; 
                return $this->render('book.twig', $properties); 
            } 
            
            public function getByUser(): string { 
                $bookModel = new BookModel($this->db); 
                $books = $bookModel->getByUser($this->customerId); 
                $properties = [ 'books' => $books, 'currentPage' => 1, 'lastPage' => true ]; 
                return $this->render('books.twig', $properties); 
            } 
        }

There's nothing too special in this preceding code so far. The getAllWithPage and getAll methods do the same thing, one with the page number given by the user as a URL argument, and the other setting the page number as 1—the default case. They ask the model for the list of books to be displayed and passed to the view. The information of the current page—and whether or not we are on the last page—is also sent to the template in order to add the "previous" and "next" page links.

The get method will get the ID of the book that the customer is interested in. It will try to fetch it using the model. If the model throws an exception, we will render the error template with a "Book not found" message. Instead, if the book ID is valid, we will render the book template as expected.

The getByUser method will return all the books that the authenticated customer has borrowed. We will make use of the customerId property that we set from the router. There is no sanity check here, since we are not trying to get a specific book, but rather a list, which could be empty if the user has not borrowed any books yet—but that is not an issue.

Another getter controller is the one that searches for a book by its title and/or author. This method will be triggered when the user submits the form in the layout template. The form sends both the title and the author fields, so the controller will ask for both. The model is ready to use the arguments that are empty, so we will not perform any extra checking here. Add the method to the BookController class:

.. code-block:: php

    public function search(): string { 
        $title = $this->request->getParams()->getString('title'); 
        $author = $this->request->getParams()->getString('author'); 
        $bookModel = new BookModel($this->db); 
        $books = $bookModel->search($title, $author);
        $properties = [ 'books' => $books, 'currentPage' => 1, 'lastPage' => true ]; 
        return $this->render('books.twig', $properties); 
    }

Your application cannot perform any actions, but at least you can finally browse the list of books, and click on any of them to view the details. We are finally getting something here!

Borrowing books
~~~~~~~~~~~~~~~

Borrowing and returning books are probably the actions that involve the most logic, together with buying a book, which will be covered by a different controller. This is a good place to start logging the user's actions, since it will be useful later for debugging purposes. Let's see the code first, and then discuss it briefly. Add the following two methods to your BookController class:

.. code-block:: php

    public function borrow(int $bookId): string { 
        $bookModel = new BookModel($this->db); 
        
        try { 
            $book = $bookModel->get($bookId); 
        }
    
        catch (NotFoundException $e) { 
            $this->log->warn('Book not found: ' . $bookId); 
            $params = ['errorMessage' => 'Book not found.']; 
            return $this->render('error.twig', $params); 
        } 
        if (!$book->getCopy()) {
            $params = [ 'errorMessage' => 'There are no copies left.' ]; 
            return $this->render('error.twig', $params); 
        } 
        try {
            $bookModel->borrow($book, $this->customerId); 
        } 
        catch (DbException $e) { 
            $this->log->error( 'Error borrowing book: ' . $e->getMessage() ); 
            $params = ['errorMessage' => 'Error borrowing book.']; 
            return $this->render('error.twig', $params); 
        } 
        
        return $this->getByUser(); 
    }
    
    public function returnBook(int $bookId): string { 
        $bookModel = new BookModel($this->db); 
        
        try { 
            $book = $bookModel->get($bookId); 
        }

        catch (NotFoundException $e) { 
            $this->log->warn('Book not found: ' .$bookId); $params = ['errorMessage' => 'Book not found.']; 
            return $this->render('error.twig', $params); 
        } 
        
        $book->addCopy(); 
        try { 
            $bookModel->returnBook($book, $this->customerId); 
        } 
        
        catch (DbException $e) { 
            $this->log->error( 'Error returning book: ' . $e->getMessage() ); 
            $params = ['errorMessage' => 'Error returning book.']; 
            return $this->render('error.twig', $params); 
        } 
        return $this->getByUser(); 
    }

As we mentioned earlier, one of the new things here is that we are logging user actions, like when trying to borrow or return a book that is not valid. Monolog allows you to write logs with different priority levels: error, warning, and notices. You can invoke methods such as error, warn, or notice to refer to each of them. We use warnings when something unexpected, yet not critical, happens, for example, trying to borrow a book that is not there. Errors are used when there is an unknown problem from which we cannot recover, like an error from the database.

The modus operandi of these two methods is as follows: we get the book object from the 3database with the given book ID. As usual, if there is no such book, we return an error page. Once we have the book domain object, we make use of the helpers addCopy and getCopy in order to update the stock of the book, and send it to the model, together with the customer ID, to store the information in the database. There is also a sanity check when borrowing a book, just in case there are no more books available. In both cases, we return the list of books that the user has borrowed as the response of the controller.

The sales controller
~~~~~~~~~~~~~~~~~~~~

We arrive at the last of our controllers: the SalesController. With a different model, it will end up doing pretty much the same as the methods related to borrowed books. But we need to create the sale domain object in the controller instead of getting it from the model. Let's add the following code, which contains a method for buying a book, add, and two getters: one that gets all the sales of a given user and one that gets the info of a specific sale, that is, getByUser and get respectively. Following the convention, the file will be src/Controllers/SalesController.php:

.. code-block:: php

    <?php 
    namespace Bookstore\Controllers; 
    use Bookstore\Domain\Sale;
    use Bookstore\Models\SaleModel; 
    
    class SalesController extends AbstractController { 
        public function add($id): string { 
            $bookId = (int)$id; 
            $salesModel = new SaleModel($this->db); 
            $sale = new Sale(); 
            $sale->setCustomerId($this->customerId);
            $sale->addBook($bookId); 
            
            try { 
                $salesModel->create($sale); 
            } 
            
            catch (\Exception $e) { 
                $properties = [ 'errorMessage' => 'Error buying the book.' ]; 
                $this->log->error( 'Error buying book: ' . $e->getMessage() ); 
                return $this->render('error.twig', $properties);
            } 
            
            return $this->getByUser(); 
        } 
        
        public function getByUser(): string {
            $salesModel = new SaleModel($this->db); 
            $sales = $salesModel->getByUser($this->customerId); 
            $properties = ['sales' => $sales]; 
            return $this->render('sales.twig', $properties); 
        } 
        
        public function get($saleId): string { 
            $salesModel = new SaleModel($this->db); 
            $sale = $salesModel->get($saleId); 
            $properties = ['sale' => $sale]; 
            return $this->render('sale.twig', $properties);
        } 
    }


Dependency injection
--------------------

At the end of the chapter, we will cover one of the most interesting and controversial of the topics that come with, not only the MVC pattern, but OOP in general: dependency injection. We will show you why it is so important, and how to implement a solution that suits our specific application, even though there are quite a few different implementations that can cover different necessities.


Why is dependency injection necessary?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

We still need to cover the way to unit test your code, hence you have not experienced it by yourself yet. But one of the signs of a potential source of problems is when you use the new statement in your code to create an instance of a class that does not belong to your code base—also known as a dependency. Using new to create a domain object like Book or Sale is fine. Using it to instantiate models is also acceptable. But manually instantiating, which something else, such as the template engine, the database connection, or the logger, is something that you should avoid. There are different reasons that support this idea:

-  If you want to use a controller from two different places, and each
   of these places needs a different database connection or log file,
   instantiating those dependencies inside the controller will not allow
   us to do that. The same controller will always use the same
   dependency.

-  Instantiating the dependencies inside the controller means that the
   controller is fully aware of the concrete implementation of each of
   its dependencies, that is, the controller knows that we are using PDO
   with the MySQL driver and the location of the credentials for the
   connection. This means a high level of coupling in your
   application—so, bad news.

-  Replacing one dependency with another that implements the same
   interface is not easy if you are instantiating the dependency
   explicitly everywhere, as you will have to search all these places,
   and change the instantiation manually.

For all these reasons, and more, it is always good to provide the dependencies that a class such as a controller needs instead of letting it create its own. This is something that everybody agrees with. The problem comes when implementing a solution. There are different options:

-  We have a constructor that expects (through arguments) all the
   dependencies that the controller, or any other class, needs. The
   constructor will assign each of the arguments to the properties of
   the class.

-  We have an empty constructor, and instead, we add as many setter
   methods as the dependencies of the class.

-  A hybrid of both, where we set the main dependencies through a
   constructor, and set the rest of the dependencies via setters.

-  Sending an object that contains all the dependencies as a unique
   argument for the constructor, and the controller gets the
   dependencies that it needs from that container.

Each solution has its pros and cons. If we have a class with a lot of dependencies, injecting all of them via the constructor would make it counterintuitive, so it would be better if we inject them using setters, even though a class with a lot of dependencies looks like bad design. If we have just one or two dependencies, using the constructor could be acceptable, and we will write less code. For classes with several dependencies, but not all of them mandatory, using the hybrid version could be a good solution. The fourth option makes it easier when injecting the dependencies as we do not need to know what each object expects. The problem is that each class should know how to fetch its dependency, that is, the dependency name, which is not ideal.


Implementing our own dependency injector
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Open source solutions for dependency injectors are already available, but we think that it would be a good experience to implement a simple one by yourself. The idea of our dependency injector is a class that contains instances of the dependencies that your code needs. This class, which is basically a map of dependency names to dependency instances, will have two methods: a getter and a setter of dependencies. We do not want to use a static property for the dependencies array, as one of the goals is to be able to have more than one dependency injector with a different set of dependencies. Add the following class to src/Utils/DependencyInjector.php:

.. code-block:: php

    <?php namespace Bookstore\Utils; 
    use Bookstore\Exceptions\NotFoundException; 
    
    class DependencyInjector {
        private $dependencies = []; 
        
        public function set(string $name, $object) { 
            $this->dependencies[$name] = $object; 
        } 
        
        public function get(string $name) { 
            if (isset($this->dependencies[$name])) { 
                return $this->dependencies[$name]; 
            } 
            
            throw new NotFoundException( $name . ' dependency not found.' ); 
        } 
    }

Having a dependency injector means that we will always use the same instance of a given class every time we ask for it, instead of creating one each time. That means that singleton implementations are not needed anymore; in fact, as mentioned in `*Chapter 4* <#Top_of_ch04_html>`__, *Creating Clean Code with OOP*, it is preferable to avoid them. Let's get rid of them, then. One of the places where we were using it was in our configuration reader. Replace the existing code with the following in the src/Core/Config.php file:

.. code-block:: php

    <?php 
        namespace Bookstore\Core; 
        use Bookstore\Exceptions\NotFoundException; 
        
        class Config { 
        
            private $data; 
            
            public function __construct() { 
                $json = file_get_contents( __DIR__ . '/../../config/app.json' );
                $this->data = json_decode($json, true); 
            } 
            
            public function get($key) { 
                if (!isset($this->data[$key])) { 
                    throw new NotFoundException("Key $key not in config."); 
                } 
                return $this->data[$key]; 
            } 
        }

The other place where we were making use of the singleton pattern was in the DB class. In fact, the purpose of the class was only to have a singleton for our database connection, but if we are not making use of it, we can remove the entire class. So, delete your src/Core/DB.php file.

Now we need to define all these dependencies and add them to our dependency injector. The index.php file is a good place to have the dependency injector before we route the request. Add the following code just before instantiating the Router class:

.. code-block:: php

    $config = new Config(); 
    $dbConfig = $config->get('db'); 
    $db = new PDO( 'mysql:host=127.0.0.1;dbname=bookstore', $dbConfig['user'], $dbConfig['password'] ); 
    $loader = new Twig_Loader_Filesystem(__DIR\_\_ . '/../../views'); 
    $view = new Twig_Environment($loader); 
    $log = new Logger('bookstore'); 
    $logFile = $config->get('log'); 
    $log->pushHandler(new StreamHandler($logFile, Logger::DEBUG)); 
    $di = new DependencyInjector(); 
    $di->set('PDO', $db); 
    $di->set('Utils\\Config', $config);
    $di->set('Twig\_Environment', $view); $di->set('Logger', $log);
    $router = new Router($di); 
    //...

There are a few changes that we need to make now. The most important of them refers to the AbstractController, the class that will make heavy use of the dependency injector. Add a property named $di to that class, and replace the constructor with the following:

.. code-block:: php

    public function __construct( DependencyInjector $di, Request $request ) { 
        $this->request = $request; 
        $this->di = $di; 
        $this->db = $di->get('PDO'); 
        $this->log = $di->get('Logger'); 
        $this->view = $di->get('Twig_Environment'); 
        $this->config = $di->get('Utils\Config'); 
        $this->customerId = $_COOKIE['id']; }

The other changes refer to the Router class, as we are sending it now as part of the constructor, and we need to inject it to the controllers that we create. Add a $di property to that class as well, and change the constructor to the following one:

.. code-block:: php

    public function __construct(DependencyInjector $di) { 
        $this->di = $di; 
        $json = file_get_contents(__DIR__ . '/../../config/routes.json'); 
        $this->routeMap = json_decode($json, true); }

Also change the content of the executeController and route methods:

.. code-block:: php

    public function route(Request $request): string { 
        $path = $request->getPath(); 
        foreach ($this->routeMap as $route => $info) {
            $regexRoute = $this->getRegexRoute($route, $info); 
            if (preg\_match("@^/$regexRoute$@", $path)) { 
                return $this->executeController( $route, $path, $info, $request ); 
            } 
        }
        $errorController = new ErrorController( $this->di, $request );
        return $errorController->notFound(); 
    } 
    
    private function executeController( string $route, string $path, array $info, Request $request ): string {
        $controllerName = '\Bookstore\Controllers\\' . $info['controller'] . 'Controller';
        $controller = new $controllerName($this->di, $request); 
        if(isset($info['login']) && $info['login']) { 
            if($request->getCookies()->has('user')) { 
                $customerId = $request->getCookies()->get('user');
                $controller->setCustomerId($customerId); 
            } 
            else { 
                $errorController = new CustomerController( $this->di, $request ); 
                return $errorController->login(); 
            } 
        } 
        $params = $this->extractParams($route, $path); 
        return call_user_func_array([$controller, $info['method']], $params ); 
    }

There is one last place that you need to change. The login method of CustomerController was instantiating a controller too, so we need to inject the dependency injector there as well:

.. code-block:: php

    $newController = new BookController($this->di, $this->request);

Summary
-------

In this chapter, you learned what MVC is, and how to write an application that follows that pattern. You also know how to use a router to route requests to controllers, Twig to write templates, and Composer to manage your dependencies and autoloader. You were introduced to dependency injection, and you even built your own implementation, even though it is a very controversial topic with many different points of view.

In the next chapter, we will go through one of the most important parts needed when writing good code and good applications: unit testing your code to get quick feedback from it.
