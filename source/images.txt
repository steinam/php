
.. |image0| image:: media/image1.jpeg
   :width: 8.50000in
   :height: 11.00000in
.. |image1| image:: media/image2.jpeg
   :width: 1.54167in
   :height: 0.45833in
.. |image2| image:: media/image3.jpeg
   :width: 5.93056in
   :height: 3.29167in
.. |image3| image:: media/image4.jpeg
   :width: 5.93056in
   :height: 3.18056in
.. |image4| image:: media/image5.jpeg
   :width: 5.93056in
   :height: 4.69444in
.. |image5| image:: media/image6.jpeg
   :width: 5.93056in
   :height: 4.73611in
.. |image6| image:: media/image7.jpeg
   :width: 2.95833in
   :height: 1.55556in
.. |image7| image:: media/image8.jpeg
   :width: 5.93056in
   :height: 4.29167in
.. |image8| image:: media/image9.jpeg
   :width: 5.93056in
   :height: 5.33333in
.. |image9| image:: media/image10.jpeg
   :width: 4.76389in
   :height: 3.55556in
.. |image10| image:: media/image11.jpeg
   :width: 4.75000in
   :height: 3.55556in
.. |image11| image:: media/image12.jpeg
   :width: 4.75000in
   :height: 3.54167in
.. |image12| image:: media/image13.jpeg
   :width: 5.93056in
   :height: 3.72222in
.. |A simple example| image:: media/image14.jpeg
   :width: 2.95833in
   :height: 2.81944in
.. |A more complex example| image:: media/image15.jpeg
   :width: 2.95833in
   :height: 2.81944in
.. |How they work| image:: media/image16.jpeg
   :width: 2.95833in
   :height: 4.06944in
.. |image16| image:: media/image17.jpeg
   :width: 2.55556in
   :height: 1.13889in
.. |image17| image:: media/image18.jpeg
   :width: 2.72222in
   :height: 1.34722in
.. |image18| image:: media/image19.jpeg
   :width: 2.54167in
   :height: 2.15278in
.. |image19| image:: media/image20.jpeg
   :width: 1.33333in
   :height: 2.12500in
.. |image20| image:: media/image21.jpeg
   :width: 2.59722in
   :height: 2.11111in
.. |image21| image:: media/image22.jpeg
   :width: 3.72222in
   :height: 2.11111in
.. |image22| image:: media/image23.jpeg
   :width: 5.93056in
   :height: 1.22222in
.. |image23| image:: media/image24.jpeg
   :width: 3.55556in
   :height: 1.38889in
.. |image24| image:: media/image25.jpeg
   :width: 3.55556in
   :height: 1.38889in
.. |image25| image:: media/image26.jpeg
   :width: 2.95833in
   :height: 0.95833in
.. |image26| image:: media/image27.jpeg
   :width: 4.73611in
   :height: 2.05556in
.. |image27| image:: media/image28.jpeg
   :width: 3.55556in
   :height: 3.43056in
.. |image28| image:: media/image29.jpeg
   :width: 3.55556in
   :height: 4.00000in
.. |image29| image:: media/image30.jpeg
   :width: 1.79167in
   :height: 1.30556in
.. |image30| image:: media/image31.jpeg
   :width: 4.15278in
   :height: 1.27778in
.. |image31| image:: media/image32.jpeg
   :width: 4.15278in
   :height: 2.30556in
.. |image32| image:: media/image33.jpeg
   :width: 4.15278in
   :height: 2.30556in
.. |image33| image:: media/image34.jpeg
   :width: 4.15278in
   :height: 2.30556in
.. |image34| image:: media/image35.jpeg
   :width: 4.73611in
   :height: 2.48611in
.. |image35| image:: media/image36.jpeg
   :width: 4.73611in
   :height: 1.47222in
.. |image36| image:: media/image37.jpeg
   :width: 3.69444in
   :height: 7.41667in
.. |image37| image:: media/image38.jpeg
   :width: 4.73611in
   :height: 1.68056in
.. |image38| image:: media/image39.jpeg
   :width: 5.11111in
   :height: 4.01389in
.. |image39| image:: media/image40.jpeg
   :width: 5.93056in
   :height: 4.65278in
.. |image40| image:: media/image41.jpeg
   :width: 5.93056in
   :height: 4.29167in
.. |image41| image:: media/image42.jpeg
   :width: 5.93056in
   :height: 2.19444in
.. |image42| image:: media/image43.jpeg
   :width: 4.15278in
   :height: 2.72222in
.. |image43| image:: media/image44.jpeg
   :width: 5.93056in
   :height: 7.20833in
.. |image44| image:: media/image45.jpeg
   :width: 5.93056in
   :height: 4.15278in
.. |image45| image:: media/image46.jpeg
   :width: 5.93056in
   :height: 5.29167in
.. |image46| image:: media/image47.jpeg
   :width: 4.73611in
   :height: 3.41667in
.. |image47| image:: media/image48.jpeg
   :width: 4.73611in
   :height: 1.69444in
.. |image48| image:: media/image49.jpeg
   :width: 4.73611in
   :height: 1.73611in
.. |image49| image:: media/image50.jpeg
   :width: 4.29167in
   :height: 4.48611in
.. |image50| image:: media/image51.jpeg
   :width: 5.56944in
   :height: 2.86111in
.. |image51| image:: media/image52.jpeg
   :width: 4.91667in
   :height: 2.36111in
