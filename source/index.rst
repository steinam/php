.. Programmierung mit PHP 7 documentation master file, created by
   sphinx-quickstart on Thu Mar 29 20:27:53 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Programmierung mit PHP 7
========================

.. toctree::
   :maxdepth: 2
   :caption: Inhaltsverzeichnis
   :numbered:
   :titlesonly:
 
   chap_1_2.rst
   chap_3_php_basics.rst
   chap_4_php_web.rst
   chap_5_control_structures.rst
   chap_6_functions.rst
   chap_7_files.rst
   chap_8_OOP.rst
   Design_Pattern.rst
   MVC.rst
   Testing.rst
   Rest_APIs.rst
   Behavioral_Testing.rst

.. PHP_Frameworks.rst
.. Database.rst  
.. book_rest.rst

.. php7.rst



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

