.. include:: images.txt

Chapter 3. Understanding PHP Basics
===================================


Learning a new language is not easy. You need to understand not only the syntax of the language, but also its grammatical rules, that is, when and why to use each element of the language. Luckily for you, some languages come from the same root. For example, Spanish and French are Romance languages, as they both evolved from spoken Latin; that means that these two languages share a lot of rules, and if you already know French, learning Spanish becomes much easier.

Programming languages are quite the same. If you already know another programming language, it will be very easy for you to go through this chapter. If this is your first time though, you will need to understand all those grammatical rules from scratch, and so, it might take some more time. But fear not! We are here to help you in this endeavor.

In this chapter, you will learn about the following:

-  PHP files

-  Variables, strings, arrays, and operators in PHP

-  PHP in web applications

-  Control structures in PHP

-  Functions in PHP

-  The PHP filesystem

PHP files
---------

From now on, we will work on your index.php file, so you can just start the web server, and go to http://localhost:8080 to see the results. You might have already noticed that in order to write PHP code, you have to start the file with <?php. There are other options, and you can also finish the file with ?>, but none of them are needed. What is important to know is that you can mix PHP code with other content, like HTML, CSS, or JavaScript, in your PHP file as soon as you enclose the PHP bits with the <?php ?> tags.

    <?php echo 'hello world'; ?> bye world

If you check the result of the preceding code snippet in your browser, you will see that it prints both messages, hello world and bye world. The reason why this happens is simple: you already know that the PHP code there prints the hello world message. What happens next is that anything outside the PHP tags will be interpreted as is. If there is an HTML code for instance, it would not be printed as is, but will be interpreted by the browser.

You will learn in `*Chapter 6* <#Top_of_ch06_html>`__, *Adapting to MVC*, why it is usually a bad idea to mix PHP and HTML. For now, assuming that it is bad, let's try to avoid it. For that, you can include one file from another PHP file using any one of these four functions:

-  include: This will try to find and include the specified file each
   time it is invoked. If the file is not found, PHP will throw a
   warning, but will continue with the execution.

-  require: This will do the same as include, but PHP will throw an
   error instead of a warning if the file is not found.

-  include_once: This function will do what include does, but it will
   include the file only the first time that it is invoked. Subsequent
   calls will be ignored.

-  require_once: This works the same as require, but it will include
   the file only the first time that it is invoked. Subsequent calls
   will be ignored.

Each function has its own usage, so it is not right to say that one is better than the other. Just think carefully what your scenario is, and then decide. For example, let's try to include our index.html file from our index.php file such that we do not mix PHP with HTML, but have the best of both worlds:

.. code-block:: php

    <?php echo 'hello world'; 
    require 'index.html';

We chose require as we know the file is there—and if it is not, we are not interested in continuing the execution. Moreover, as it is some HTML code, we might want to include it multiple times, so we did not choose the require_once option. You can try to require a file that does not exist, and see what the browser says.

PHP does not consider empty lines; you can add as many as you want to make your code easier to read, and it will not have any repercussion on your application. Another element that helps in writing understandable code, and which is ignored by PHP, is comments. Let's see both in action:

.. code-block:: php

    <?php 
    /* 
       This is the first file loaded by the web server. 
       It prints some messages and html from other files. 
    */ 

    // let's print a message from php echo 'hello world'; 
    // and then include the rest of html 
    
    require 'index.html';

The code does the same job as the previous one, but now everyone will easily understand what we are trying to do. We can see two types of comments: single-line comments and multiple-line comments. The first type consists of a single line starting with //, and the second type encloses multiple lines within /\* and \*/. We start each commented line with an asterisk, but that is completely optional.


chap Variables
--------------

Variables keep a value for future reference. This value can change if we want it to; that is why they are called variables. Let's take a look at them in an example. Save this code in your index.php file:

    <?php $a = 1; $b = 2; $c = $a + $b; echo $c; // 3

In this preceding piece of code, we have three variables: $a has value 1, $b has 2, and $c contains the sum of $a and $b, hence, $c equals 3. Your browser should print the value of the variable $c, which is 3.

Assigning a value to a variable means to give it a value, and it is done with the equals sign as shown in the previous example. If you did not assign a value to a variable, we will get a notice from PHP when it checks its contents. A notice is just a message telling us that something is not exactly right, but it is a minor problem and you can continue with the execution. The value of an unassigned variable will be null, that is, nothing.

PHP variables start with the $ sign followed by the variable name. A valid variable name starts with a letter or an underscore followed by any combination of letters, numbers, and/or underscores. It is case sensitive. Let's see some examples:


.. code-block:: php

    <?php 
    $_some_value = 'abc'; 
    // valid $1number = 12.3; // not valid! 
    $some$signs% = '&^%'; // not valid! 
    $go_2_home = "ok"; //valid 
    $go_2_Home = 'no'; // this is a different variable
    $isThisCamelCase = true; // camel case

Remember that everything after // is a comment, and is thus ignored by PHP.

In this piece of code, we can see that variable names like $_some_value and $go_2_home are valid. $1number and $some$signs% are not valid as they start with a number, or they contain invalid symbols. As names are case sensitive, $go_2_home and $go_2_Home are two different variables. Finally, we show the CamelCase convention, which is the preferred option among most developers.

Data types
----------

We can assign more than just numbers to variables. PHP has eight primitive types, but for now, we will focus on its four scalar types:

-  Booleans: These take just true or false values

-  Integers: These are numeric values without a decimal point, for
   example, 2 or 5

-  Floating point numbers or floats: These are numbers with a decimal
   point, for example, 2.3

-  Strings: These are concatenations of characters which are surrounded
   by either single or double quotes, like 'this' or "that"

Even though PHP defines these types, it allows the user to assign different types of data to the same variable. Check the following code to see how it works:

.. code-block:: php

    <?php 
    $number = 123; 
    var_dump($number); 
    $number = 'abc';
    var_dump($number);

If you check the result on your browser, you will see the following:

::

    int(123) string(3) "abc"

The code first assigns the value 123 to the variable $number. As 123 is an integer, the type of the variable will be integer int. That is what we see when printing the content of the variable with var\_dump. After that, we assign another value to the same variable, this time a string. When printing the new content, we see that the type of the variable changed from integer to string, yet PHP did not complain at any time. This is called type juggling.

Let's check another piece of code:

.. code-block:: php

    <?php 
    $a = "1"; 
    $b = 2; 
    var_dump($a + $b); // 3 
    var_dump($a . $b); // 12

You already know that the + operator returns the sum of two numeric values. You will see later that the . operator concatenates two strings. Thus, the preceding code assigns a string and an integer to two variables, and then tries to add and concatenate them.

When trying to add them, PHP knows that it needs two numeric values, and so it tries to adapt the string to an integer. In this case, it is easy as the string represents a valid number. That is the reason why we see the first result as an integer 3 (*1 + 2*).

In the last line, we are performing a string concatenation. We have an integer in $b, so PHP will first try to convert it to a string—which is "2"—and then concatenate it with the other string, "1". The result is the string "12".

.. admonition:: Type juggling

    PHP tries to convert the data type of a variable only when there is a context where the type of variable needed is different. But PHP does not change the value and type of the variable itself. Instead, it will take the value and try to transform it, leaving the variable intact.


Operators
---------

Using variables is nice, but if we cannot make them interact with each other, there is nothing much we can do. Operators are elements that take some expressions—operands—and perform actions on them to get a result. The most common examples of operators are arithmetic operators, which you already saw previously.

An expression is almost anything that has a value. Variables, numbers, or text are examples of expressions, but you will see that they can get way more complicated. Operators expect expressions of a specific type, for example, arithmetic operators expect either integers or floats. But as you already know, PHP takes care of transforming the types of the expressions given whenever possible.

Let's take a look at the most important groups of operators.

Arithmetic operators
--------------------

Arithmetic operators are very intuitive, as you already know. Addition, subtraction, multiplication, and division (+, -, \*, and /) do as their names say. Modulus (%) gives the remainder of the division of two operands. Exponentiation (\*\*) raises the first operand to the power of the second. Finally, negation (-) negates the operand. This last one is the only arithmetic operator that takes just one operand.

Let's see some examples:

.. code-block:: php

    <?php 
    $a = 10; 
    $b = 3; 
    var_dump($a + $b);  // 13 
    var_dump($a - $b);  // 7 
    var_dump($a \* $b); // 30 
    var_dump($a / $b);  // 3.333333...
    var_dump($a % $b);  // 1 
    var_dump($a ** $b); // 1000
    var_dump(-$a);      // -10

As you can see, they are quite easy to understand!

Assignment operators
~~~~~~~~~~~~~~~~~~~~

You already know this one too, as we have been using it in our examples. The assignment operator assigns the result of an expression to a variable. Now you know that an expression can be as simple as a number, or, for example, the result of a series of arithmetic operations. The following example assigns the result of an expression to a variable:

.. code-block:: php

    <?php 
    $a = 3 + 4 + 5 - 2; 
    var_dump($a);   // 10

There are a series of assignment operators that work as shortcuts. You can build them combining an arithmetic operator and the assignment operator. Let's see some examples:

.. code-block:: php

    <?php
    $a = 13; 
    $a += 14;     // same as $a = $a + 14; 
    var_dump($a); 
    $a -= 2;      // same as $a = $a - 2; 
    var_dump($a); 
    $a *= 4;      // same as $a = $a * 4; 
    var_dump($a);

Comparison operators
~~~~~~~~~~~~~~~~~~~~

Comparison operators are one of the most used groups of operators. They take two operands and compare them, returning the result of thecomparison usually as a Boolean, that is, true or false.

There are four comparisons that are very intuitive: < (less than), <= (less or equal to), > (greater than), and >= (greater than or equal to). There is also the special operator <=> (spaceship) that compares both the operands and returns an integer instead of a Boolean. When comparing *a* with *b*, the result will be less than 0 if *a* is less than *b*, 0 if *a* equals *b*, and greater than 0 if *a* is greater than *b*. Let's see some examples:

.. code-block:: php

    <?php 
    var_dump(2 < 3); // true 
    var_dump(3 < 3); // false
    var_dump(3 <= 3); // true 
    var_dump(4 <= 3); // false 
    var_dump(2 > 3); // false 
    var_dump(3 >= 3); // true 
    var_dump(3 > 3); // false
    var_dump(1 <=> 2); // int less than 0 
    var_dump(1 <=> 1); // 0
    var_dump(3 <=> 2); // int greater than 0

There are comparison operators to evaluate if two expressions are equal or not, but you need to be careful with type juggling. The == (equals) operator evaluates two expressions *after* type juggling, that is, it will try to transform both expressions to the same type, and then compare them. Instead, the === (identical) operator evaluates two expressions *without* type juggling, so even if they look the same, if they are not of the same type, the comparison will return false. The same applies to != or <> (not equal to) and !== (not identical):

.. code-block:: php

    <?php 
    $a = 3; 
    $b = '3'; $c = 5; 
    var\_dump($a == $b); // true
    var\_dump($a === $b); // false 
    var\_dump($a != $b); // false
    var\_dump($a !== $b); // true 
    var\_dump($a == $c); // false
    var\_dump($a <> $c); // true

You can see that when asking if a string and an integer that represent the same number are equal, it replies affirmatively; PHP first transforms both to the same type. On the other hand, when asked if they are identical, it replies they are not as they are of different types.

Logical operators
~~~~~~~~~~~~~~~~~

Logical operators apply a logic operation—also known as a binary operation—to its operands, returning a Boolean response. The most used ones are ! (not), && (and), and \|\| (or). && will return true only if both operands evaluate to true. \|\| will return true if any or both of the operands are true. ! will return the negated value of the operand, that is, true if the operand is false or false if the operand is true. Let's see some examples:

.. code-block:: php


    <?php 
    var_dump(true && true); // true 
    var_dump(true && false); // false 
    var_dump(true || false); // true 
    var_dump(false || false); // false 
    var_dump(!false); // true

Incrementing and decrementing operators
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Incrementing/decrementing operators are also shortcuts like += or -=, and they only work on variables. There are four of them, and they needspecial attention. We've already seen the first two:

-  ++: This operator on the left of the variable will increase the
   variable by 1, and then return the result. On the right, it will
   return the content of the variable, and after that increase it by 1.

-  --: This operator works the same as ++ but decreases the value by 1
   instead of increasing by 1.

Let's see an example:

.. code-block:: php


    <?php 
    $a = 3; 
    $b = $a++; // $b is 3, $a is 4 
    var_dump($a, $b); 
    $b = ++$a; // $a and $b are 5 
    var_dump($a, $b);

In the preceding code, on the first assignment to $b, we use $a++. The operator on the right will return first the value of $a, which is 3, assign it to $b, and only then increase $a by 1. In the second assignment, the operator on the left first increases $a by 1, changes the value of $a to 5, and then assigns that value to $b.

Operator precedence
~~~~~~~~~~~~~~~~~~~

You can add multiple operators to an expression to make it as long as it needs to be, but you need to be careful as some operators have higher precedence than others, and thus, the order of execution might not be the one you expect. The following table shows the order of precedence of the operators that we've studied until now:

::

    +---------------------------------+-------------------------+
    | Operator                        | Type                    |
    +---------------------------------+-------------------------+
    | \*\*                            | Arithmetic              |
    +---------------------------------+-------------------------+
    | ++, --                          | Increasing/decreasing   |
    +---------------------------------+-------------------------+
    | !                               | Logical                 |
    +---------------------------------+-------------------------+
    | \*, /, %                        | Arithmetic              |
    +---------------------------------+-------------------------+
    | +, -                            | Arithmetic              |
    +---------------------------------+-------------------------+
    | <, <=, >, >=                    | Comparison              |
    +---------------------------------+-------------------------+
    | ==, !=, ===, !==                | Comparison              |
    +---------------------------------+-------------------------+
    | &&                              | Logical                 |
    +---------------------------------+-------------------------+
    | ||                              | Logical                 |
    +---------------------------------+-------------------------+
    | =, +=, -=, *=, /=, %=, **=      | Assignment              |
    +---------------------------------+-------------------------+

The preceding table shows us that the expression *3+2*3* will first evaluate the product *2\*3* and then the sum, so the result is 9 rather than 15. If you want to perform operations in a specific order, different from the natural order of precedence, you can force it by enclosing the operation within parentheses. Hence, *(3+2)\*3* will first perform the sum and then the product, giving the result 15 this time.

Let's see some examples to clarify this quite tricky subject:

.. code-block:: php

    <?php 
    $a = 1; 
    $b = 3; 
    $c = true; 
    $d = false; 
    $e = $a + $b > 5 || $c; // true 
    var_dump($e); 
    $f = $e == true && !$d; // true
    var_dump($f); $g = ($a + $b) * 2 + 3 * 4; // 20 
    var_dump($g);

This preceding example could be endless, and still not be able to cover all the scenarios you can imagine, so let's keep it simple. In the first highlighted line, we have a combination of arithmetic, comparison, and logical operators, plus the assignment operator. As there are no parentheses, the order is the one detailed in the previous table. The operator with the highest preference is the sum, so we perform it first: $a + $b equals 4. The next one is the comparison operator, so *4 > 5*, which is false. Finally, the logical operator, false || $c ($c is true) results in true.

The second example might need a bit more explanation. The first operator we see in the table is the negation, so we resolve it. !$d is !false, so it is true. The expression is now, $e == true && true. First we need to solve the comparison $e == true. Knowing that $e is true, the comparison results in true. The final operation then is the logical end, and it results in true.

Try to work out the last example by yourself to get some practice. Do not be afraid if you think we are not covering operators enough. During the next few sections, we will see a lot of examples.


Working with strings
----------------------------

Working with strings in real life is really easy. Actions like *Check if this string contains this* or *Tell me how many times this character appears* are very easy to perform. But when programming, strings are concatenations of characters that you cannot see at once when searching for something. Instead, you have to look one by one and keep track of what the content is. In this scenario, those really easy actions are not that easy any more.

Luckily for you, PHP brings a whole set of predefined functions that help you in interacting with strings. You can find the entire list of functions at
`*http://php.net/manual/en/ref.strings.php* <http://php.net/manual/en/ref.strings.php>`__, but we will only cover the ones that are used the most. Let's look at some examples:

.. code-block:: php


    <?php 
    $text = ' How can a clam cram in a clean cream can? '; 
    echo strlen($text);              // 45 
    $text = trim($text); echo $text; // How can a clam cram in a clean cream can? 
    echo strtoupper($text);          // HOW CAN A CLAM CRAM IN A CLEAN CREAM CAN? 
    echo strtolower($text);          // how can a clam cram in a clean cream can? 
    $text = str_replace('can', 'could', $text); 
    echo $text;                      // How could a clam cram in a clean cream could?
    echo substr($text, 2, 6);        // w coul 
    var_dump(strpos($text, 'can'));  // false 
    var_dump(strpos($text, 'could'));// 4

In the preceding long piece of code, we are playing with a string with different functions:

-  strlen: This function returns the number of characters that the
   string contains.

-  trim: This function returns the string, removing all the blank spaces
   to the left and to the right.

-  strtoupper and strtolower: These functions return the string with all
   the characters in upper or lower case respectively.

-  str_replace: This function replaces all occurrences of a given
   string by the replacement string.

-  substr: This function extracts the string contained between the
   positions specified by parameters, with the first character being at
   position 0.

-  strpos: This function shows the position of the first occurrence of
   the given string. It returns false if the string cannot be found.

Additionally, there is an operator for strings (.) which concatenates two strings (or two variables transformed to a string when possible). Using it is really simple: in the following example, the last statement will concatenate all the strings and variables forming the sentence, *I am Hiro Nakamura!*.

.. code-block:: php

    <?php 
     $firstname = 'Hiro'; 
     $surname = 'Nakamura'; 
     echo 'I am ' . $firstname . ' ' . $surname . '!';

Another thing to note about strings is the way they are represented. So far, we have been enclosing the strings within single quotes, but you can also enclose them within double quotes. The difference is that within single quotes, a string is exactly as it is represented, but within double quotes, some rules are applied before showing the final result. There are two elements that double quotes treat differently than single quotes: escape characters and variable expansions.

-  Escape characters: These are special characters than cannot be
   represented easily. Examples of escape characters are new lines or
   tabs. To represent them, we use escape sequences, which are the
   concatenation of a backslash (\\) followed by some other character.
   For example, \\n represents a new line, and \\t represents a
   tabulation.

-  Variable expanding: This allows you to include variable references
   inside the string, and PHP replaces them by their current value. You
   have to include the $ sign too.

Have a look at the following example:

.. code-block:: php

    <?php 
    $firstname = 'Hiro'; 
    $surname = 'Nakamura'; 
    echo "My name is $firstname $surname.\n I am a master of time and space.
    \"Yatta!\"";

The preceding piece of code will print the following in the browser:

    My name is Hiro Nakamura. I am a master of time and space. "Yatta!"

Here, \n inserted a new line. \" added the double quotes (you need to escape them too, as PHP would understand that you want to end your string), and the variables $firstname and $surname were replaced by their values.



chap4_Arrays
-------------

If you have some experience with other programming languages or data structures in general, you might be aware of two data structures that are very common and useful: lists and maps. A list is an ordered set of elements, whereas a map is a set of elements identified by keys. Let's see an example:


.. code-block:: php

    List: ["Harry", "Ron", "Hermione"] 
    Map: { "name": "James Potter", "status": "dead" }

The first element is a list of names that contains three values: Harry, Ron, and Hermione. The second one is a map, and it defines two values: James Potter and dead. Each of these two values is identified with a key: name and status respectively.

In PHP, we do not have lists and maps; we have arrays. An array is a data structure that implements both, a list and a map.

Initializing arrays
~~~~~~~~~~~~~~~~~~~

You have different options for initializing an array. You can initialize an empty array, or you can initialize an array with data. There are different ways of writing the same data with arrays too. Let's see some examples:

.. code-block:: php

    <?php 
    $empty1 = []; 
    $empty2 = array(); 
    $names1 = ['Harry', 'Ron', 'Hermione']; 
    $names2 = array('Harry', 'Ron', 'Hermione'); 
    $status1 = [ 'name' => 'James Potter', 'status' => 'dead' ]; 
    $status2 = array('name' => 'James Potter', 'status' => 'dead' );

In the preceding example, we define the list and map from the previous section. $names1 and $names2 are exactly the same array, just using a different notation. The same happens with $status1 and $status2. Finally, $empty1 and $empty2 are two ways of creating an empty array.

Later you will see that lists are handled like maps. Internally, the array $names1 is a map, and its keys are ordered numbers. In this case,another initialization for $names1 that leads to the same array could be as follows:


.. code-block:: php

    $names1 = [ 0 => 'Harry', 1 => 'Ron', 2 => 'Hermione' ];

Keys of an array can be any alphanumeric value, like strings or numbers. Values of an array can be anything: strings, numbers, Booleans, other arrays, and so on. You could have something like the following:

.. code-block:: php

    <?php 
    $books = [ '1984' => [ 'author' => 'George Orwell', 'finished'
    => true, 'rate' => 9.5 ], 'Romeo and Juliet' => [ 'author' =>
    'William Shakespeare', 'finished' => false ] ];

This array is a list that contains two arrays—maps. Each map contains different values like strings, doubles, and Booleans.

Populating arrays
~~~~~~~~~~~~~~~~~

Arrays are not immutable, that is, they can change after being initialized. You can change the content of an array either by treating it as a map or as a list. Treating it as a map means that you specify the key that you want to override, whereas treating it as a list means appending another element to the end of the array:

.. code-block:: php

    <?php 
    $names = ['Harry', 'Ron', 'Hermione']; 
    $status = [ 'name' => 'James Potter', 'status' => 'dead' ]; 
    $names[] = 'Neville';
    $status['age'] = 32; 
    print_r($names, $status);

In the preceding example, the first highlighted line appends the name Neville to the list of names, hence the list will look like *['Harry', 'Ron', 'Hermione', 'Neville']*. The second change actually adds a new key-value to the array. You can check the result from your browser by using the function print_r. It does something similar to var_dump, just without the type and size of each value.

.. admonition:: print\_r and var\_dump in a browser

    When printing the content of an array, it is useful to see one key-value per line, but if you check your browser, you will see that it displays the whole array in one line. That happens because what the browser tries to display is HTML, and it ignores new lines or whitespaces. To check the content of the array as PHP wants you to see it, check the source code of the page—you will see the option by right-clicking on the page.

If you need to remove an element from the array, instead of adding or updating one, you can use the unset function:

.. code-block:: php

    <?php 
    $status = [ 'name' => 'James Potter', 'status' => 'dead' ];
    unset($status['status']); print_r ($status);

The new $status array contains the key name only.

Accessing arrays
~~~~~~~~~~~~~~~~

Accessing an array is as easy as specifying the key as when you were updating it. For that, you need to understand how lists work. You already know that lists are treated internally as a map with numeric keys in order. The first key is always 0; so, an array with *n* elements will have keys from 0 to *n-1*.

You can add any key to a given array, even if it previously consisted of numeric entries. The problem arises when adding numeric keys, and later, you try to append an element to the array. What do you think will happen?

.. code-block:: php

    <?php 
    $names = ['Harry', 'Ron', 'Hermione']; 
    $names['badguy'] = 'Voldemort'; 
    $names[8] = 'Snape'; 
    $names[] = 'McGonagall';
    print_r($names);

The result of that last piece of code is as follows:

::

    Array ( [0] => Harry [1] => Ron [2] => Hermione [badguy] =>
    Voldemort [8] => Snape [9] => McGonagall )

When trying to append a value, PHP inserts it after the last numeric key, in this case 8.

You might've already figured it out by yourself, but you can always print any part of the array by specifying its key:

.. code-block:: php

    <?php 
    $names = ['Harry', 'Ron', 'Hermione']; 
    print_r($names[1]); //prints 'Ron'

Finally, trying to access a key that does not exist in an array will return you a null and throw a notice, as PHP identifies that you are doing something wrong in your code.

.. code-block:: php

    <?php 
    $names = ['Harry', 'Ron', 'Hermione']; 
    var_dump($names[4]); // null and a PHP notice

The empty and isset functions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

There are two useful functions for enquiring about the content of an array. If you want to know if an array contains any element at all, you can ask if it is empty with the empty function. That function actually works with strings too, an empty string being a string with no characters (' '). The isset function takes an array position, and returns true or false depending on whether that position exists or not:

.. code-block:: php

    <?php 
    $string = ''; 
    $array = []; 
    $names = ['Harry', 'Ron', 'Hermione']; 
    var_dump(empty($string));   // true
    var_dump(empty($array));    // true 
    var_dump(empty($names));    // false
    var_dump(isset($names[2])); // true 
    var_dump(isset($names[3])); // false

In the preceding example, we can see that an array with no elements or a string with no characters will return true when asked if it is empty, and false otherwise. When we use isset($names[2]) to check if the position 2 of the array exists, we get true, as there is a value for that key: Hermione. Finally, isset($names[3]) evaluates to false as the key 3 does not exist in that array.

Searching for elements in an array
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Probably, one of the most used functions with arrays is in_array. This function takes two values, the value that you want to search for and the array. The function returns true if the value is in the array and false otherwise. This is very useful, because a lot of times what you want to know from a list or a map is if it contains an element, rather than knowing that it does or its location.

Even more useful sometimes is array_search. This function works in the same way except that instead of returning a Boolean, it returns the key where the value is found, or false otherwise. Let's see both functions:

.. code-block:: php

    <?php 
    $names = ['Harry', 'Ron', 'Hermione']; 
    $containsHermione = in_array('Hermione', $names); 
    var_dump($containsHermione);      // true
    $containsSnape = in_array('Snape', $names);
    var_dump($containsSnape);         // false 
    $wheresRon = array_search('Ron', $names); 
    var_dump($wheresRon);             // 1
    $wheresVoldemort = array_search('Voldemort', $names);
    var_dump($wheresVoldemort);       // false

Ordering arrays
~~~~~~~~~~~~~~~

An array can be sorted in different ways, so there are a lot of chances that the order that you need is different from the current one. By default, the array is sorted by the order in which the elements were added to it, but you can sort an array by its key or by its value, both ascending and descending. Furthermore, when sorting an array by its values, you can choose to preserve their keys or to generate new ones as a list.

There is a complete list of these functions on the official documentation website at
`*http://php.net/manual/en/array.sorting.php* <http://php.net/manual/en/array.sorting.php>`__, but here we will display the most important ones:

::

    +----------+------------+-----------------------------+-----------------+
    | Name     | Sorts by   | Maintains key association   | Order of sort   |
    +----------+------------+-----------------------------+-----------------+
    | sort     | Value      | No                          | Low to high     |
    +----------+------------+-----------------------------+-----------------+
    | rsort    | Value      | No                          | High to low     |
    +----------+------------+-----------------------------+-----------------+
    | asort    | Value      | Yes                         | Low to high     |
    +----------+------------+-----------------------------+-----------------+
    | arsort   | Value      | Yes                         | High to low     |
    +----------+------------+-----------------------------+-----------------+
    | ksort    | Key        | Yes                         | Low to high     |
    +----------+------------+-----------------------------+-----------------+
    | krsort   | Key        | Yes                         | High to low     |
    +----------+------------+-----------------------------+-----------------+


These functions always take one argument, the array, and they do not return anything. Instead, they directly sort the array we pass to them. Let's see some of them:

.. code-block:: php

    <?php 
    $properties = [ 'firstname' => 'Tom', 'surname' => 'Riddle', 'house' => 'Slytherin' ]; 
    $properties1 = $properties2 = $properties3 = $properties; 
    sort($properties1); 
    var_dump($properties1);
    asort($properties3); var_dump($properties3); 
    ksort($properties2);
    var_dump($properties2);

Okay, there is a lot going on in the last example. First of all, we initialize an array with some key values and assign it to $properties. Then we create three variables that are copies of the original array—the syntax should be intuitive. Why do we do that? Because if we sort the original array, we will not have the original content any more. This is not what we want in this specific example, as we want to see how the different sort functions affect the same array. Finally, we perform three different sorts, and print each of the results. The browser should show you something like the following:

.. code-block:: php

    array(3) { [0]=> string(6) "Riddle" [1]=> string(9) "Slytherin"
    [2]=> string(3) "Tom" } array(3) { ["surname"]=> string(6) "Riddle"
    ["house"]=> string(9) "Slytherin" ["firstname"]=> string(3) "Tom" }
    array(3) { ["firstname"]=> string(3) "Tom" ["house"]=> string(9)
    "Slytherin" ["surname"]=> string(6) "Riddle" }

The first function, sort, orders the values alphabetically. Also, if you check the keys, now they are numeric as in a list, instead of the original keys. Instead, asort orders the values in the same way, but keeps the association of key-values. Finally, ksort orders the elements by their keys, alphabetically.

.. admonition:: How to remember so many function names

    PHP has a lot of function helpers that will save you from writing customized functions by yourself, for example, it provides you with up to 13 different sorting functions. And you can always rely on the official documentation. But, of course, you would like to write code without going back and forth from the docs. So, here are some tips to remember what each sorting function does:

    -  An a in the name means associative, and thus, will preserve the key-value association.

    -  An r in the name means reverse, so the order will be from high to low.

    -  A k means key, so the sorting will be based on the keys instead of the values.

Other array functions
~~~~~~~~~~~~~~~~~~~~~

There are around 80 different functions related to arrays. As you can imagine, you will never even hear about some of them, as they have very specific purposes. The complete list can be found at `*http://php.net/manual/en/book.array.php* <http://php.net/manual/en/book.array.php>`__.

We can get a list of the keys of the array with array\_keys, and a list of its values with array_values:

.. code-block:: php

    <?php 
    $properties = [ 'firstname' => 'Tom', 'surname' => 'Riddle', 'house' => 'Slytherin' ]; 
    $keys = array_keys($properties);
    var_dump($keys); 
    $values = array_values($properties);
    var_dump($values);

We can get the number of elements in an array with the count function:

.. code-block:: php

    <?php 
    $names = ['Harry', 'Ron', 'Hermione']; 
    $size = count($names);
    var_dump($size); // 3

And we can merge two or more arrays into one with array_merge:

.. code-block:: php

    <?php 
    $good = ['Harry', 'Ron', 'Hermione']; 
    $bad = ['Dudley', 'Vernon', 'Petunia']; 
    $all = array_merge($good, $bad);
    var_dump($all);

The last example will print the following array:

.. code-block:: php

    array(6) { [0]=> string(5) "Harry" [1]=> string(3) "Ron" [2]=>
    string(8) "Hermione" [3]=> string(6) "Dudley" [4]=> string(6)
    "Vernon" [5]=> string(7) "Petunia" }

As you can see, the keys of the second array are now different, as originally, both the arrays had the same numeric keys, and an array cannot have two values for the same key.


.. TODO::

    Hier noch HAshes einbauen